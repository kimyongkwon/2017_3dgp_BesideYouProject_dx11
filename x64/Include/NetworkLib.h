#pragma once
#pragma comment(lib,"ws2_32")
#include <Ws2tcpip.h>
#include<WinSock2.h>
#include<stdio.h>
#include<Windows.h>
#include"json.h"
#include<iostream>
#include<array>
#include<string>
#include<unordered_map>
#include<queue>

using namespace std;

#define MWM_SOCK (WM_APP + 1)
#include"Type.h"
#include"Singleton.h"


#include"Stream.h"
#include"PacketHeader.h"
#include"Packet.h"
#include"PacketAnalyzer.h"

#include"PacketFactory.h"
#include"AfterTreatment.h"
#include"Session.h"
#include"CNetwork.h"
#include"ClientSession.h"
//#include"ClientNetwork.h"
