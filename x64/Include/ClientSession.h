#pragma once
#include"NetworkLib.h"
#include"Session.h"

class Packet;
class Session;
class Stream;
//enum {
//	READ_BUF,
//	WRITE_BUF,
//	MAX_BUF,
//	SOCKET_BUF_SIZE = 10240
//};
class IoData {
private:
	size_t _totalBytes;
	size_t _currentBytes;
	std::array<char, SOCKET_BUF_SIZE> _buf;

public:
	IoData() : _currentBytes(0), _totalBytes(0) {
		_buf.fill(NULL);
	}
	void erase();
	~IoData();
	int32_t setupTotalBytes();
	bool needMoreIO(size_t tsize);
	bool NonRecving();
	char * data();
	size_t leakdatasize();
	size_t currentByte();
	size_t totalBytes();
	bool setData(Stream & stream);
};
// Client Session
// WSAAsyncsSelete 는 fd_write 의 경우 초기 연결시 발생, Send함수의 실패시 다시 실행된다.
// fd_read의 경우 recv의 버퍼에 데이터가 있을시 호출된다.


class ClientSession : public Session
{
private:
	void initalize();
	bool cheakErroRecv(int retval);
	bool cheakErrSend(int retval);
	
	int recv();
	int send();
public:
	void Err_Quit(WCHAR * msg);
	ClientSession() : Session() 
	{ }
	virtual ~ClientSession() {}
	std::array<IoData, MAX_BUF> _ioData;
	// 반환값(bool) true : recv 필요, false : recv다받음.
	bool isRecving(size_t transferSize);
	void onSend();
	void sendPacket(Packet * packet);
	Packet * onRecv();
	void recvStandBy();
};