#pragma once
#include"NetworkLib.h"
class Session {
private:
	SOCKET _clientSocket;
	SOCKADDR_IN _clientaddr;
public:
	Session();
	virtual void init(char * IP, DWORD port);
	virtual ~Session();
	SOCKADDR* clientaddr();
	size_t sizeaddr();
	SOCKET & Socket();
	str_t ServerAddress();
};