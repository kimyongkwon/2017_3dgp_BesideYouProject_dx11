#pragma once
#include"stdafx.h"
#include"PacketHeader.h"
#include"Stream.h"


class Packet {
public:
	virtual PacketType type() = 0;
	virtual void encode(Stream &stream) { stream << (INT64)this->type(); }
	virtual void decode(Stream &stream) { };
};


class PK_C_NTY_LATENCYCHECK : public Packet
{
public:
	PacketType type() { return PE_C_NTY_LATENCYCHECK; }
	UINT64 STime;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << STime;
	}

	void decode(Stream &stream) {
		stream >> &STime;
	}

};

class PK_C_REQ_ATT : public Packet
{
public:
	PacketType type() { return PE_C_REQ_ATT; }
	INT64 uid;
	std::vector<FLOAT> pos;
	std::vector<FLOAT> vpos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << pos;
		stream << vpos;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &pos;
		stream >> &vpos;
	}

};

class PK_C_REQ_EXITROOM : public Packet
{
public:
	PacketType type() { return PE_C_REQ_EXITROOM; }
	INT64 uid;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
	}

	void decode(Stream &stream) {
		stream >> &uid;
	}

};

class PK_C_REQ_FIRSTSIGNAL : public Packet
{
public:
	PacketType type() { return PE_C_REQ_FIRSTSIGNAL; }
	INT64 uid;
	INT64 roomNumber;
	UINT64 ClientTime;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << roomNumber;
		stream << ClientTime;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &roomNumber;
		stream >> &ClientTime;
	}

};

class PK_C_REQ_GAMEOUT : public Packet
{
public:
	PacketType type() { return PE_C_REQ_GAMEOUT; }
	INT64 uid;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
	}

	void decode(Stream &stream) {
		stream >> &uid;
	}

};

class PK_C_REQ_GAMEREADYOFF : public Packet
{
public:
	PacketType type() { return PE_C_REQ_GAMEREADYOFF; }
	INT64 uid;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
	}

	void decode(Stream &stream) {
		stream >> &uid;
	}

};

class PK_C_REQ_GAMEREADYON : public Packet
{
public:
	PacketType type() { return PE_C_REQ_GAMEREADYON; }
	INT64 uid;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
	}

	void decode(Stream &stream) {
		stream >> &uid;
	}

};

class PK_C_REQ_GENEOFF : public Packet
{
public:
	PacketType type() { return PE_C_REQ_GENEOFF; }
	INT64 uid;
	std::vector<FLOAT> pos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << pos;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &pos;
	}

};

class PK_C_REQ_GENEON : public Packet
{
public:
	PacketType type() { return PE_C_REQ_GENEON; }
	INT64 uid;
	std::vector<FLOAT> pos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << pos;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &pos;
	}

};

class PK_C_REQ_JOIN : public Packet
{
public:
	PacketType type() { return PE_C_REQ_JOIN; }
	wstr_t id;
	wstr_t pw;
	wstr_t name;
	wstr_t email;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << id;
		stream << pw;
		stream << name;
		stream << email;
	}

	void decode(Stream &stream) {
		stream >> &id;
		stream >> &pw;
		stream >> &name;
		stream >> &email;
	}

};

class PK_C_REQ_LOGIN : public Packet
{
public:
	PacketType type() { return PE_C_REQ_LOGIN; }
	wstr_t id;
	wstr_t pw;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << id;
		stream << pw;
	}

	void decode(Stream &stream) {
		stream >> &id;
		stream >> &pw;
	}

};

class PK_C_REQ_OUTSTAGE : public Packet
{
public:
	PacketType type() { return PE_C_REQ_OUTSTAGE; }
	INT64 uid;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
	}

	void decode(Stream &stream) {
		stream >> &uid;
	}

};

class PK_C_REQ_OWNLOOK : public Packet
{
public:
	PacketType type() { return PE_C_REQ_OWNLOOK; }
	INT64 uid;
	std::vector<FLOAT> rotatep;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << rotatep;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &rotatep;
	}

};

class PK_C_REQ_OWNMOVE : public Packet
{
public:
	PacketType type() { return PE_C_REQ_OWNMOVE; }
	INT64 uid;
	INT32 roomnumber;
	float degree;
	std::vector<float> pos;
	UINT64 time;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << roomnumber;
		stream << degree;
		stream << pos;
		stream << time;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &roomnumber;
		stream >> &degree;
		stream >> &pos;
		stream >> &time;
	}

};

class PK_C_REQ_SEARCHROOM : public Packet
{
public:
	PacketType type() { return PE_C_REQ_SEARCHROOM; }
	INT64 uid;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
	}

	void decode(Stream &stream) {
		stream >> &uid;
	}

};

class PK_C_REQ_SELECTPART : public Packet
{
public:
	PacketType type() { return PE_C_REQ_SELECTPART; }
	INT64 uid;
	BYTE  role;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << role;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &role;
	}

};

class PK_C_REQ_SITDOWN : public Packet
{
public:
	PacketType type() { return PE_C_REQ_SITDOWN; }
	INT64 uid;
	std::vector<FLOAT> pos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << pos;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &pos;
	}

};

class PK_C_REQ_STANDUP : public Packet
{
public:
	PacketType type() { return PE_C_REQ_STANDUP; }
	INT64 uid;
	std::vector<FLOAT> pos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << pos;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &pos;
	}

};

class PK_C_REQ_TRAPON : public Packet
{
public:
	PacketType type() { return PE_C_REQ_TRAPON; }
	INT64 uid;
	std::vector<FLOAT> pos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << pos;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &pos;
	}

};

class PK_C_REQ_WALKOFF : public Packet
{
public:
	PacketType type() { return PE_C_REQ_WALKOFF; }
	INT64 uid;
	std::vector<FLOAT> pos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << pos;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &pos;
	}

};

class PK_C_REQ_WALKON : public Packet
{
public:
	PacketType type() { return PE_C_REQ_WALKON; }
	INT64 uid;
	std::vector<FLOAT> pos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << pos;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &pos;
	}

};

class PK_S_ANS_ADDUSERVIEW : public Packet
{
public:
	PacketType type() { return PE_S_ANS_ADDUSERVIEW; }
	BYTE ucount;
	std::vector<INT64> uid;
	std::vector<FLOAT> pos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << ucount;
		stream << uid;
		stream << pos;
	}

	void decode(Stream &stream) {
		stream >> &ucount;
		stream >> &uid;
		stream >> &pos;
	}

};

class PK_S_ANS_ALIVEUSERSTATEUPDATE : public Packet
{
public:
	PacketType type() { return PE_S_ANS_ALIVEUSERSTATEUPDATE; }
	INT64 uid;
	BYTE state;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << state;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &state;
	}

};

class PK_S_ANS_DEFEATSTATE : public Packet
{
public:
	PacketType type() { return PE_S_ANS_DEFEATSTATE; }
	INT64 uid;
	BYTE role;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << role;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &role;
	}

};

class PK_S_ANS_ENTERROOMFAIL : public Packet
{
public:
	PacketType type() { return PE_S_ANS_ENTERROOMFAIL; }
};

class PK_S_ANS_ENTERROOMSUCC : public Packet
{
public:
	PacketType type() { return PE_S_ANS_ENTERROOMSUCC; }
	INT64 roomNumber;
	std::vector<INT64> uid;
	std::vector<wstr_t> id;
	std::vector<BYTE> role;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << roomNumber;
		stream << uid;
		stream << id;
		stream << role;
	}

	void decode(Stream &stream) {
		stream >> &roomNumber;
		stream >> &uid;
		stream >> &id;
		stream >> &role;
	}

};

class PK_S_ANS_GENONSUCC : public Packet
{
public:
	PacketType type() { return PE_S_ANS_GENONSUCC; }
	BYTE genstate;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << genstate;
	}

	void decode(Stream &stream) {
		stream >> &genstate;
	}

};

class PK_S_ANS_GENSTATEUPDATE : public Packet
{
public:
	PacketType type() { return PE_S_ANS_GENSTATEUPDATE; }
	BYTE genstate;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << genstate;
	}

	void decode(Stream &stream) {
		stream >> &genstate;
	}

};

class PK_S_ANS_HITJUDGE : public Packet
{
public:
	PacketType type() { return PE_S_ANS_HITJUDGE; }
	INT64 uid;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
	}

	void decode(Stream &stream) {
		stream >> &uid;
	}

};

class PK_S_ANS_JOINFAIL : public Packet
{
public:
	PacketType type() { return PE_S_ANS_JOINFAIL; }
	BYTE errcode;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << errcode;
	}

	void decode(Stream &stream) {
		stream >> &errcode;
	}

};

class PK_S_ANS_JOINSUCC : public Packet
{
public:
	PacketType type() { return PE_S_ANS_JOINSUCC; }
};

class PK_S_ANS_LOGINFAIL : public Packet
{
public:
	PacketType type() { return PE_S_ANS_LOGINFAIL; }
	BYTE errcode;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << errcode;
	}

	void decode(Stream &stream) {
		stream >> &errcode;
	}

};

class PK_S_ANS_LOGINSUCC : public Packet
{
public:
	PacketType type() { return PE_S_ANS_LOGINSUCC; }
	INT64 uid;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
	}

	void decode(Stream &stream) {
		stream >> &uid;
	}

};

class PK_S_ANS_OUTROOMSUCC : public Packet
{
public:
	PacketType type() { return PE_S_ANS_OUTROOMSUCC; }
	INT64 uid;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
	}

	void decode(Stream &stream) {
		stream >> &uid;
	}

};

class PK_S_ANS_SELECTPARTSUCC : public Packet
{
public:
	PacketType type() { return PE_S_ANS_SELECTPARTSUCC; }
	BYTE role;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << role;
	}

	void decode(Stream &stream) {
		stream >> &role;
	}

};

class PK_S_ANS_SUBUSERVIEW : public Packet
{
public:
	PacketType type() { return PE_S_ANS_SUBUSERVIEW; }
	std::vector<INT64> uid;
	std::vector<FLOAT> pos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << pos;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &pos;
	}

};

class PK_S_ANS_TRAPBUILDFAIL : public Packet
{
public:
	PacketType type() { return PE_S_ANS_TRAPBUILDFAIL; }
	INT64 uid;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
	}

	void decode(Stream &stream) {
		stream >> &uid;
	}

};

class PK_S_ANS_TRAPBUILDSUCC : public Packet
{
public:
	PacketType type() { return PE_S_ANS_TRAPBUILDSUCC; }
	INT64 uid;
	std::vector<FLOAT> trappos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << trappos;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &trappos;
	}

};

class PK_S_ANS_TRAPEXECUTE : public Packet
{
public:
	PacketType type() { return PE_S_ANS_TRAPEXECUTE; }
	INT64 uid;
	BYTE hp;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << hp;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &hp;
	}

};

class PK_S_ANS_USERMOVE : public Packet
{
public:
	PacketType type() { return PE_S_ANS_USERMOVE; }
	INT64 uid;
	std::vector<FLOAT> pos;
	std::vector<FLOAT> vpos;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << pos;
		stream << vpos;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &pos;
		stream >> &vpos;
	}

};

class PK_S_ANS_WINSTATE : public Packet
{
public:
	PacketType type() { return PE_S_ANS_WINSTATE; }
	INT64 uid;
	BYTE role;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << role;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &role;
	}

};

class PK_S_BRD_GAMESTARTSUCC : public Packet
{
public:
	PacketType type() { return PE_S_BRD_GAMESTARTSUCC; }
	std::string IP;
	INT32 PORT;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << IP;
		stream << PORT;
	}

	void decode(Stream &stream) {
		stream >> &IP;
		stream >> &PORT;
	}

};

class PK_S_BRD_OUTGAMESTATE : public Packet
{
public:
	PacketType type() { return PE_S_BRD_OUTGAMESTATE; }
	INT64 uid;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
	}

	void decode(Stream &stream) {
		stream >> &uid;
	}

};

class PK_S_BRD_ROOMSTATE : public Packet
{
public:
	PacketType type() { return PE_S_BRD_ROOMSTATE; }
	INT64 uid;
	BYTE update;
	wstr_t id;
	INT64 roomNumber;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << update;
		stream << id;
		stream << roomNumber;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &update;
		stream >> &id;
		stream >> &roomNumber;
	}

};

class PK_S_NTF_OVERLAPCRUSH : public Packet
{
public:
	PacketType type() { return PE_S_NTF_OVERLAPCRUSH; }
	wstr_t id;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << id;
	}

	void decode(Stream &stream) {
		stream >> &id;
	}

};

class PK_S_NTF_ROOMSEARCHNOTROLE : public Packet
{
public:
	PacketType type() { return PE_S_NTF_ROOMSEARCHNOTROLE; }
};

class PK_S_NTY_CYCLICLATENCY : public Packet
{
public:
	PacketType type() { return PE_S_NTY_CYCLICLATENCY; }
	INT64 uid;
	UINT64 STime;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << STime;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &STime;
	}

};

class PK_S_NTY_FIRSTUSERDATA : public Packet
{
public:
	PacketType type() { return PE_S_NTY_FIRSTUSERDATA; }
	UINT64 STime;
	std::vector<FLOAT> px;
	std::vector<FLOAT> py;
	std::vector<FLOAT> pz;
	std::vector<FLOAT> degree;
	std::vector<INT32> role;
	std::vector<INT32> hp;
	std::vector<INT64> uid;
	std::vector<wstr_t> id;
	std::vector<INT32> statusbar;
	INT32 trapcount;
	std::vector<INT32>generatorNumber_;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << STime;
		stream << px;
		stream << py;
		stream << pz;
		stream << degree;
		stream << role;
		stream << hp;
		stream << uid;
		stream << id;
		stream << statusbar;
		stream << trapcount;
		stream << generatorNumber_;
	}

	void decode(Stream &stream) {
		stream >> &STime;
		stream >> &px;
		stream >> &py;
		stream >> &pz;
		stream >> &degree;
		stream >> &role;
		stream >> &hp;
		stream >> &uid;
		stream >> &id;
		stream >> &statusbar;
		stream >> &trapcount;
		stream >> &generatorNumber_;
	}

};

class PK_S_REQ_LATENCYCHECK : public Packet
{
public:
	PacketType type() { return PE_S_REQ_LATENCYCHECK; }
	INT64 uid;
	UINT64 STime;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << STime;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &STime;
	}

};

class PK_T_NTF_STARTUSERDATA : public Packet
{
public:
	PacketType type() { return PE_T_NTF_STARTUSERDATA; }
	INT64 roomNumber;
	std::vector<INT64> uid;
	std::vector<wstr_t> id;
	std::vector<BYTE> role;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << roomNumber;
		stream << uid;
		stream << id;
		stream << role;
	}

	void decode(Stream &stream) {
		stream >> &roomNumber;
		stream >> &uid;
		stream >> &id;
		stream >> &role;
	}

};

class PK_T_NTY_TERMINAL : public Packet
{
public:
	PacketType type() { return PE_T_NTY_TERMINAL; }
};


// 새로 생긴 패킷 클래스 들
class PK_C_REQ_MOVESTART : public Packet
{
public:
	INT64 RoomNumber;
	INT64 uid;
	std::vector<FLOAT> ViewDirection;
	std::vector<FLOAT> MoveDirection;
	std::vector<FLOAT> MoveStartVecPos;
	UINT64 MoveStartPosTime;
	std::vector<FLOAT> NextFrameVecPos;
	UINT64 NextFramePosTime;
	PacketType type() { return PE_C_REQ_MOVESTART; }
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << RoomNumber;
		stream << uid;
		stream << ViewDirection;
		stream << MoveDirection;
		stream << MoveStartVecPos;
		stream << MoveStartPosTime;
		stream << NextFrameVecPos;
		stream << NextFramePosTime;
	}
	void decode(Stream &stream) {
		stream >> &RoomNumber;
		stream >> &uid;
		stream >> &ViewDirection;
		stream >> &MoveDirection;
		stream >> &MoveStartVecPos;
		stream >> &MoveStartPosTime;
		stream >> &NextFrameVecPos;
		stream >> &NextFramePosTime;
	}
};
class PK_C_REQ_MOVESTOP : public Packet
{
public:
	INT64 RoomNumber;
	INT64 uid;
	std::vector<FLOAT> StopVecPos;
	std::vector<FLOAT> StopDirection;
	UINT64 StopKeyTime;
	PacketType type() { return PE_C_REQ_MOVESTOP; }
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << RoomNumber;
		stream << uid;
		stream << StopVecPos;
		stream << StopDirection;
		stream << StopKeyTime;

	}
	void decode(Stream &stream) {
		stream >> &RoomNumber;
		stream >> &uid;
		stream >> &StopVecPos;
		stream >> &StopDirection;
		stream >> &StopKeyTime;
	}
};
class PK_C_REQ_MOVEUPDATE : public Packet
{
public:
	INT64 RoomNumber;
	INT64 uid;
	std::vector<FLOAT> ViewDirection;
	std::vector<FLOAT> MoveDirection;
	std::vector<FLOAT> MoveStartVecPos;
	UINT64 MoveStartPosTime;
	std::vector<FLOAT> NextFrameVecPos;
	UINT64 NextFramePosTime;
	PacketType type() { return PE_C_REQ_MOVEUPDATE; }
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << RoomNumber;
		stream << uid;
		stream << ViewDirection;
		stream << MoveDirection;
		stream << MoveStartVecPos;
		stream << MoveStartPosTime;
		stream << NextFrameVecPos;
		stream << NextFramePosTime;
	}
	void decode(Stream &stream) {
		stream >> &RoomNumber;
		stream >> &uid;
		stream >> &ViewDirection;
		stream >> &MoveDirection;
		stream >> &MoveStartVecPos;
		stream >> &MoveStartPosTime;
		stream >> &NextFrameVecPos;
		stream >> &NextFramePosTime;
	}
};
class PK_S_NTF_MOVESTART : public Packet
{
public:
	INT64 uid;
	std::vector<FLOAT> ViewDirection;
	std::vector<FLOAT> MoveDirection;
	std::vector<FLOAT> MoveStartVecPos;
	UINT64 MoveStartPosTime;
	std::vector<FLOAT> NextFrameVecPos;
	UINT64 NextFramePosTime;
	PacketType type() { return PE_S_NTF_CLIENTMOVESTART; }
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << ViewDirection;
		stream << MoveDirection;
		stream << MoveStartVecPos;
		stream << MoveStartPosTime;
		stream << NextFrameVecPos;
		stream << NextFramePosTime;
	}
	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &ViewDirection;
		stream >> &MoveDirection;
		stream >> &MoveStartVecPos;
		stream >> &MoveStartPosTime;
		stream >> &NextFrameVecPos;
		stream >> &NextFramePosTime;
	}
};
class PK_S_NTF_MOVESTOP : public Packet
{
public:
	INT64 uid;
	std::vector<FLOAT> StopVecPos;
	std::vector<FLOAT> StopDirection;
	UINT64 StopKeyTime;
	PacketType type() { return PE_S_NTF_CLIENTMOVEEND; }
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << StopVecPos;
		stream << StopDirection;
		stream << StopKeyTime;

	}
	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &StopVecPos;
		stream >> &StopDirection;
		stream >> &StopKeyTime;
	}
};
class PK_S_NTF_MOVEUPDATE : public Packet
{
public:
	INT64 uid;
	std::vector<FLOAT> ViewDirection;
	std::vector<FLOAT> MoveDirection;
	std::vector<FLOAT> MoveStartVecPos;
	UINT64 MoveStartPosTime;
	std::vector<FLOAT> NextFrameVecPos;
	UINT64 NextFramePosTime;
	PacketType type() { return PE_S_NTF_CLIENTMOVEUPDATE; }
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << ViewDirection;
		stream << MoveDirection;
		stream << MoveStartVecPos;
		stream << MoveStartPosTime;
		stream << NextFrameVecPos;
		stream << NextFramePosTime;
	}
	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &ViewDirection;
		stream >> &MoveDirection;
		stream >> &MoveStartVecPos;
		stream >> &MoveStartPosTime;
		stream >> &NextFrameVecPos;
		stream >> &NextFramePosTime;
	}
};
class PK_S_NTY_LATENCYCHECK : public Packet
{
public:
	PacketType type() { return PE_S_NTY_LATENCYCHECK; }
	UINT64 STime;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << STime;
	}

	void decode(Stream &stream) {
		stream >> &STime;
	}
};

class PK_S_NTY_PLAYERPOS : public Packet
{
public:
	PacketType type() { return PE_S_NTY_PLAYERPOS; }
	std::vector<INT64> uid;				// 유저 UID
	std::vector<float> px;				// 유저 
	std::vector<float> py;
	std::vector<float> pz;				//
	std::vector<float> LastLookvect;
	std::vector<float> LastRightvect;
	std::vector<INT32> LastDirection;
	std::vector<UINT64> LastUpdateTime;
	std::vector<UINT64> LastRecvTime;
	std::vector<INT32>	AnimationType;
	std::vector<INT32>	GeneratorNumber;				// 발전기 넘버
	std::vector<INT8>	GeneratorP;						// 발전기 퍼센트
	std::vector<INT8>	Playerupdate;							// 리플리케이션 업데이트 정보
	std::queue<INT8>	hp;								// 체력
	std::queue<INT8>	statusUI;						// 상태 UI
	std::queue<INT8>	trapCount;						// 트랩 갯수

	std::queue<uint64_t>	trapId;
	std::queue<uint8_t>		Tupdate;
	std::queue<uint64_t>	catchId;
	std::queue<float>		trapx;
	std::queue<float>		trapy;

	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << AnimationType;
		stream << px;
		stream << py;
		stream << pz;
		stream << LastLookvect;
		stream << LastRightvect;
		stream << LastDirection;
		stream << LastUpdateTime;
		stream << LastRecvTime;
		stream << GeneratorNumber;
		stream << GeneratorP;
		stream << Playerupdate;
		stream << hp;
		stream << statusUI;
		stream << trapCount;


		stream << trapId;
		stream << Tupdate;
		stream << catchId;
		stream << trapx;
		stream << trapy;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &AnimationType;
		stream >> &px;
		stream >> &py;
		stream >> &pz;
		stream >> &LastLookvect;
		stream >> &LastRightvect;
		stream >> &LastDirection;
		stream >> &LastUpdateTime;
		stream >> &LastRecvTime;
		stream >> &GeneratorNumber;
		stream >> &GeneratorP;
		stream >> &Playerupdate;
		stream >> &hp;
		stream >> &statusUI;
		stream >> &trapCount;

		stream >> &trapId;
		stream >> &Tupdate;
		stream >> &catchId;
		stream >> &trapx;
		stream >> &trapy;
	}
};


class PK_C_NTY_MOVESTAMPT : public Packet
{
public:
	PacketType type() { return PE_C_NTY_MOVESTAMPT; }
	INT64 uid;
	INT64 roomNumber;
	std::vector<float>	lookvector;
	std::vector<float>	rightvector;
	std::vector<UINT64> StamptStartTime;
	std::vector<float>	deltaTIme;
	std::vector<INT32>	nowDirection;
	std::vector<INT32>	LastDirection;
	std::vector<double> Velocity;
	int genNumber;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << roomNumber;
		stream << lookvector;
		stream << rightvector;
		stream << StamptStartTime;
		stream << deltaTIme;
		stream << nowDirection;
		stream << LastDirection;
		stream << genNumber;
		stream << Velocity;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &roomNumber;
		stream >> &lookvector;
		stream >> &rightvector;
		stream >> &StamptStartTime;
		stream >> &deltaTIme;
		stream >> &nowDirection;
		stream >> &LastDirection;
		stream >> &genNumber;
		stream >> &Velocity;
	}
};

class PK_S_ANS_HELLO : public Packet
{
public:
	PacketType type() { return PE_S_ANS_HELLO; }
	UINT64 ClientTime;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << ClientTime;
	}

	void decode(Stream &stream) {
		stream >> &ClientTime;
	}

};

class PK_C_NTY_BUILDOBJECTSUCC : public Packet
{
public:
	PacketType type() { return PE_C_NTY_BUILDOBJECTSUCC; }
	INT64 uid;
	INT64 roomNumber;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << uid;
		stream << roomNumber;
	}

	void decode(Stream &stream) {
		stream >> &uid;
		stream >> &roomNumber;
	}

};

class PK_S_NTY_GOTOINGAME : public Packet
{
public:
	PacketType type() { return PE_S_NTY_GOTOINGAME; }
};

class PK_S_NTY_LASTMOVETIMESTAMP : public Packet
{
public:
	PacketType type() { return PE_S_NTY_LASTMOVETIMESTAMP; }
	UINT64 lastTime;
	void encode(Stream &stream) {
		stream << (INT64)this->type();
		stream << lastTime;
	}

	void decode(Stream &stream) {
		stream >> &lastTime;
	}
};


class PK_C_ANS_CHITTING : public Packet
{
public:
	PacketType type() { return PE_C_ANS_CHITTING; }
};