#pragma once
#include"NetworkLib.h"

// CNetwork  : 접속에 필요한 기본적인 정보들 추출
class CNetwork
{
protected:
	WSADATA _wsa;
	DWORD _port;
	char _ip[16];
public:
	CNetwork();					// initalize() call
	virtual bool initalize();	// 네트워크 작업 초기화(WSAStartup, IP, PORT Read)
	char * Ip() {
		return _ip;
	}
	DWORD & Port() {
		return _port;
	}
	bool ReadFromFile(const char* filename, char* buffer, int len); // FIle Read Member Function
	virtual ~CNetwork();
};