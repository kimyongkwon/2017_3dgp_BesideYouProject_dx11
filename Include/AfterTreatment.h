#pragma once
#include"stdafx.h"
class AfterTreatment
{
protected:
	typedef void(*Runfunc)(Packet * rowPacket);
	std::unordered_map<PacketType, Runfunc> _runFuncTable;
public:
	AfterTreatment() {}
	virtual ~AfterTreatment() { _runFuncTable.clear(); }
	virtual void transport(Packet * rowPacket) = 0;
};