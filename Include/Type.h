#pragma once
#include"NetworkLib.h"
#include<atomic>

typedef int32_t					packet_size_t;
typedef INT64					Int64;

typedef std::string				str_t;
typedef std::wstring			wstr_t;
#define SIZE_8				8
#define SIZE_64				64
#define SIZE_128			128
#define SIZE_256			256
#define SIZE_1024			1024
#define SIZE_4096			4096
#define SIZE_8192			8192

enum {
	READ_BUF,
	WRITE_BUF,
	MAX_BUF,
	SOCKET_BUF_SIZE = 10240
};
inline void StrConvW2A(WCHAR *src, CHAR *dest, size_t destLen) {
	if (destLen < 1) {
		return;
	}
	WideCharToMultiByte(CP_ACP, 0, src, -1, dest, (int)destLen, NULL, FALSE);
}

#undef	SAFE_DELETE
#define SAFE_DELETE(obj)						\
{												\
	if ((obj)) delete(obj);		    			\
    (obj) = 0L;									\
}