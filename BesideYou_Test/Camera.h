#pragma once

struct VS_CB_CAMERA
{
	XMFLOAT4X4 m_xmf4x4View;
	XMFLOAT4X4 m_xmf4x4Projection;
};
class CPlayer;
//------------------------------------------------------------------
//
// Class : CCamera
//
// Desc : 
//
//------------------------------------------------------------------
class CCamera
{
	//-------------------------------------
	//data for Camera
	//-------------------------------------
protected:
	XMFLOAT4X4				m_xmf4x4View;
	XMFLOAT4X4				m_xmf4x4Projection;
	D3D11_VIEWPORT			m_d3dViewport;
	ID3D11Buffer*			m_pd3dcbCamera;

	//player  pointer
	CPlayer*				m_pPlayer;

	XMFLOAT3				m_xmf3Position;
	XMFLOAT3				m_xmf3Right;
	XMFLOAT3				m_xmf3Up;
	XMFLOAT3				m_xmf3Look;
	
	//x,y,z angle
	float					m_fPitch;
	float					m_fRoll;
	float					m_fYaw;

	DWORD					m_nMode;

	//플레이어와 카메라의 오프셋을 나타내는 벡터이다. 주로 3인칭 카메라에서 사용된다.
	XMFLOAT3				m_xmf3Offset;
	float					m_fTimeLag;

	//-------------------------------------
	//function for Camera
	//-------------------------------------
public:
	CCamera(CCamera *pCamera);
	virtual ~CCamera();

	void SetMode(DWORD nMode)			{ m_nMode = nMode; }
	DWORD GetMode()						{ return(m_nMode); }

	D3D11_VIEWPORT GetViewport()		{ return(m_d3dViewport); }
	XMFLOAT4X4 GetViewMatrix()			{ return(m_xmf4x4View); }
	XMFLOAT4X4 GetProjectionMatrix()	{ return(m_xmf4x4Projection); }
	ID3D11Buffer *GetCameraConstantBuffer() { return(m_pd3dcbCamera); }

	void SetPosition(XMFLOAT3 xmf3Position)		{ m_xmf3Position = xmf3Position; }
	XMFLOAT3& GetPosition()						{ return(m_xmf3Position); }

	XMFLOAT3& GetRightVector()			{ return(m_xmf3Right); }
	XMFLOAT3& GetUpVector()				{ return(m_xmf3Up); }
	XMFLOAT3& GetLookVector()			{ return(m_xmf3Look); }

	float& GetPitch()					{ return(m_fPitch); }
	float& GetRoll()					{ return(m_fRoll); }
	float& GetYaw()						{ return(m_fYaw); }

	void SetOffset(XMFLOAT3 xmf3Offset) { m_xmf3Offset = xmf3Offset; m_xmf3Position = Vector3::Add(m_xmf3Position, xmf3Offset);}
	XMFLOAT3& GetOffset()				{ return(m_xmf3Offset); }

	void SetTimeLag(float fTimeLag)		{ m_fTimeLag = fTimeLag; }
	float GetTimeLag()					{ return(m_fTimeLag); }

	virtual void Move(const XMFLOAT3& xmf3Shift) { m_xmf3Position = Vector3::Add(m_xmf3Position, xmf3Shift); }
	virtual void Rotate(float fPitch = 0.0f, float fYaw = 0.0f, float fRoll = 0.0f) { }
	virtual void Update(XMFLOAT3& xmf3LookAt, float fTimeElapsed) { }
	virtual void SetLookAt(XMFLOAT3& xmf3LookAt) { }

	void SetPlayer(CPlayer *pPlayer)	{ m_pPlayer = pPlayer; }
	CPlayer *GetPlayer()				{ return(m_pPlayer); }

	void SetViewport(ID3D11DeviceContext *pd3dDeviceContext, DWORD xStart, DWORD yStart, DWORD nWidth, DWORD nHeight, float fMinZ = 0.0f, float fMaxZ = 1.0f);
	void GenerateViewMatrix();
	void RegenerateViewMatrix();
	void GenerateProjectionMatrix(float fNearPlaneDistance, float fFarPlaneDistance, float fAspectRatio, float fFOVAngle);

	//상수 버퍼를 생성하고 내용을 갱신하는 멤버 함수를 선언한다.
	void CreateShaderVariables(ID3D11Device *pd3dDevice);
	void UpdateShaderVariables(ID3D11DeviceContext *pd3dDeviceContext);

	//-------------------------------------
	//data and function for Frustum Culling 
	//-------------------------------------
protected:
	//절두체의 6개 평면(월드 좌표계)을 나타낸다.
	XMFLOAT4				m_pxmfFrustumPlanes[6];
public:
	void CalculateFrustumPlanes();
	bool IsInFrustum(XMFLOAT3& xmf3Minimum, XMFLOAT3& xmf3Maximum);
	bool IsInFrustum(AABB *pAABB);
};


//------------------------------------------------------------------
//
// Class : CSpaceShipCamera
//
// Desc : 
//
//------------------------------------------------------------------
class CSpaceShipCamera : public CCamera
{
public:
	CSpaceShipCamera(CCamera *pCamera);
	virtual ~CSpaceShipCamera() { }

	virtual void Rotate(float fPitch = 0.0f, float fYaw = 0.0f, float fRoll = 0.0f);
};


//------------------------------------------------------------------
//
// Class : CFirstPersonCamera
//
// Desc : 
//
//------------------------------------------------------------------
class CFirstPersonCamera : public CCamera
{
public:
	CFirstPersonCamera(CCamera *pCamera);
	virtual ~CFirstPersonCamera() { }

	virtual void Rotate(float fPitch = 0.0f, float fYaw = 0.0f, float fRoll = 0.0f);
};


//------------------------------------------------------------------
//
// Class : CThirdPersonCamera
//
// Desc : 
//
//------------------------------------------------------------------
class CThirdPersonCamera : public CCamera
{
public:
	CThirdPersonCamera(CCamera *pCamera);
	virtual ~CThirdPersonCamera() { }

	virtual void Update(XMFLOAT3& xmf3LookAt, float fTimeScale);
	virtual void SetLookAt(XMFLOAT3& xmf3LookAt);
};
