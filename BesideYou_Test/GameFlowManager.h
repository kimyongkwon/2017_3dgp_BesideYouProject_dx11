#pragma once
#include"stdafx.h"

typedef enum
{
	FLOW_LOGIN,
	FLOW_JOIN,
	FLOW_ROBBY,
	FLOW_SEARCHING,
	FLOW_ROOM,
	FLOW_INGAME,
	FLOW_RESULT,
	FLOW_NULL,
}FLOWTYPE;

class GameFlowManager : public Singleton<GameFlowManager>
{
private:
	FLOWTYPE _type;		// 현재 FLOWTYPE
	FLOWTYPE _btype;	// 이전의 FLOWTYPE
	int role;
public:
	bool win;

	bool fastoutgame;
	uint64_t mTimer;
	uint64_t mNowTIme;
	GameFlowManager();
	virtual ~GameFlowManager();
	
	void ENTER(FLOWTYPE type);
	void LEAVE();
	bool wasFtype();
	FLOWTYPE & type();
	void Called();
	void ENTER(FLOWTYPE type, bool nwin, int nrole);
};