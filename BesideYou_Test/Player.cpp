#include "stdafx.h"
#include "Player.h"
//#include "Player.h"

CPlayerInfo::CPlayerInfo()
{
	STATUS_UI m_PlayerStatus;
	ROLE m_PlayerRole;

	m_ix64UserID = 0;
	m_ix64RoomNum = 0;
	m_uix64PreTime = 0;
	m_uix64CurrentTime = 0;
	m_uix64NextTime = 0;
	m_uix64TimeGap = 0;
	m_byteHP = 0;
	m_ix32TrapCount = 0;
	m_uix64LastPacketTime = 0;

	m_wsUserName = { NULL };

	m_xmf3PrePosition = { 0.0f, 0.0f, 0.0f };
	m_xmf3NextPosition = { 0.0f, 0.0f, 0.0f };
	m_xmf3FixPosition = { 0.0f, 0.0f, 0.0f };
	
	m_bOwn = true;
	m_bGapSign = true;

	m_PlayerStatus = IDLE_STATE;
	m_PlayerRole = KILLER;
	m_EventType = EVENT_NONE;
	m_AnimationType = TYPE_IDEL;
}

CPlayerInfo::CPlayerInfo(int shaderIndex)
{
	STATUS_UI m_PlayerStatus;
	ROLE m_PlayerRole;

	m_ix64UserID = 0;
	m_ix64RoomNum = 0;
	m_uix64PreTime = 0;
	m_uix64CurrentTime = 0;
	m_uix64NextTime = 0;
	m_uix64TimeGap = 0;
	m_byteHP = 0;
	m_ix32TrapCount = 0;
	m_uix64LastPacketTime = 0;

	m_wsUserName = { NULL };

	m_xmf3PrePosition = { 0.0f, 0.0f, 0.0f };
	m_xmf3NextPosition = { 0.0f, 0.0f, 0.0f };
	m_xmf3FixPosition = { 0.0f, 0.0f, 0.0f };

	m_bOwn = true;
	m_bGapSign = true;

	m_PlayerStatus = IDLE_STATE;
	m_PlayerRole = KILLER;
	m_EventType = EVENT_NONE;
	m_AnimationType = TYPE_IDEL;
	m_ix3ShaderIndex = shaderIndex;
}

CPlayerInfo::~CPlayerInfo()
{
}

void CPlayerInfo::Read(PK_S_NTY_PLAYERPOS * inPacket, uint8_t inPlayerUpdateData)
{
	if (inPlayerUpdateData & PUPDATE::UpdateHP)
	{
		this->SetHP(inPacket->hp.front());
		printf("%d", GetHP());
		inPacket->hp.pop();
	}
	if (inPlayerUpdateData & PUPDATE::UpdateTrap)
	{
		this->SetTrap(inPacket->trapCount.front());
		inPacket->trapCount.pop();
	}
	if (inPlayerUpdateData & PUPDATE::UpdateStatus)
	{
		this->SetStatusUI(static_cast<STATUS_UI>(inPacket->statusUI.front()));
		inPacket->statusUI.pop();
	}
}

void CPlayerInfo::SetAnimationType (ANIMATION_TYPE any, bool no)
{
	if (m_AnimationType == TYPE_FORWARDRUN || m_AnimationType == TYPE_BACKRUN || m_AnimationType == TYPE_LEFTRUN || m_AnimationType == TYPE_RIGHTRUN) return;
	if (any != m_AnimationType)
	{
		switch (any)
		{
		case TYPE_IDEL:
		{
			if (m_PlayerRole == KILLER) CGameFramework::getInstance().m_ppPlayerShaders[GetShaderIndex()]->GetFBXMesh->SetAnimation(BIDLE);
			if (m_PlayerRole == SURVIVOR) CGameFramework::getInstance().m_ppPlayerShaders[GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
		}
		break;
		case TYPE_INTERACTION:
		{
			if (m_PlayerRole == KILLER) return;
			if (m_PlayerRole == SURVIVOR) CGameFramework::getInstance().m_ppPlayerShaders[GetShaderIndex()]->GetFBXMesh->SetAnimation(SGEN);
		}
		break;
		case TYPE_ATTACK:
		{
			if (m_PlayerRole == KILLER) CGameFramework::getInstance().m_ppPlayerShaders[GetShaderIndex()]->GetFBXMesh->SetAnimation(BIDLE);
			if (m_PlayerRole == SURVIVOR) return;
		}
		break;
		case TYPE_HITTED:
		{
			if (m_PlayerRole == KILLER) return;
			if (m_PlayerRole == SURVIVOR)
			{
				CGameFramework::getInstance().m_ppPlayerShaders[GetShaderIndex()]->GetFBXMesh->SetAnimation(SREACT);
				SoundEngine::getInstance().execute(HITTED, true, false);
			}
		}
		break;
		default:
			break;
		}
		m_AnimationType = any;
	}
}

void CPlayerInfo::SetReplayflag(bool flag, UINT64 lastpacketTime, XMFLOAT3 pos)
{
	m_bReplayFlag = flag;
	m_uix64LastPacketTime = lastpacketTime;
	m_xmf3PacketPos = pos;
}

bool CPlayerInfo::IsLastPacketTimeCheck(UINT64 tick)
{
	if (tick < m_uix64LastPacketTime) {
		return false;
	}
	return true;
}

CPlayer::CPlayer(int nMeshes) : CGameObject(nMeshes)
{
	m_pCamera = NULL;

	m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_xmf3Right = XMFLOAT3(1.0f, 0.0f, 0.0f);
	m_xmf3Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	m_xmf3Look = XMFLOAT3(0.0f, 0.0f, 1.0f);

	m_xmf3Velocity = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_xmf3Gravity = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_fMaxVelocityXZ = 0.0f;
	m_fMaxVelocityY = 0.0f;
	m_fFriction = 0.0f;

	m_fPitch = 0.0f;
	m_fRoll = 0.0f;
	m_fYaw = 0.0f;

	m_pPlayerUpdatedContext = NULL;
	m_pCameraUpdatedContext = NULL;

	m_pPlayerInfo = new CPlayerInfo();
}

CPlayer::CPlayer(int nMeshes, int nPlayerShaderIndex ) : CGameObject(nMeshes)
{
	m_pCamera = NULL;

	m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_xmf3Right = XMFLOAT3(1.0f, 0.0f, 0.0f);
	m_xmf3Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	m_xmf3Look = XMFLOAT3(0.0f, 0.0f, 1.0f);

	m_xmf3Velocity = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_xmf3Gravity = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_fMaxVelocityXZ = 0.0f;
	m_fMaxVelocityY = 0.0f;
	m_fFriction = 0.0f;

	m_fPitch = 0.0f;
	m_fRoll = 0.0f;
	m_fYaw = 0.0f;

	m_pPlayerUpdatedContext = NULL;
	m_pCameraUpdatedContext = NULL;

	m_pPlayerInfo = new CPlayerInfo(nPlayerShaderIndex);
}

CPlayer::~CPlayer()
{
	if (m_pCamera) delete m_pCamera;
}

void CPlayer::CreateShaderVariables(ID3D11Device *pd3dDevice)
{
}

void CPlayer::UpdateShaderVariables(ID3D11DeviceContext *pd3dDeviceContext)
{
	//플레이어의 현재 카메라의 UpdateShaderVariables() 멤버 함수를 호출한다.
	if (m_pCamera) m_pCamera->UpdateShaderVariables(pd3dDeviceContext);
}

/*플레이어의 위치를 변경하는 함수이다. 플레이어의 위치는 기본적으로 사용자가 플레이어를 이동하기 위한 키보드를 누를 때 변경된다. 플레이어의 이동 방향(dwDirection)에 따라 플레이어를 fDistance 만큼 이동한다.*/
void CPlayer::Move( DWORD dwDirection, float fDistance, bool bUpdateVelocity)
{
	if (dwDirection)
	{
		XMFLOAT4X4 xmf4x4Rotate;
		XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);
		//화살표 키 ‘↑’를 누르면 로컬 z-축 방향으로 이동(전진)한다. ‘↓’를 누르면 반대 방향으로 이동한다.
		if (dwDirection & DIR_FORWARD)
			xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Look, fDistance, true));
		if (dwDirection & DIR_BACKWARD) 
			xmf3Shift = Vector3::Subtract(xmf3Shift, Vector3::ScalarProduct(m_xmf3Look, fDistance, true));
		//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.
		if (dwDirection & DIR_RIGHT)
			xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Right, fDistance, true));
		if (dwDirection & DIR_LEFT) 
			xmf3Shift = Vector3::Subtract(xmf3Shift, Vector3::ScalarProduct(m_xmf3Right, fDistance, true));
		//‘Page Up’을 누르면 로컬 y-축 방향으로 이동한다. ‘Page Down’을 누르면 반대 방향으로 이동한다.

		////(8)

		//플레이어를 현재 위치 벡터에서 d3dxvShift 벡터 만큼 이동한다.
		Move(xmf3Shift, bUpdateVelocity);
	}
}

bool CPlayer::Move(DWORD dwDirection, float fDistance, bool bUpdateVelocity, vector<CShader*>& vtInstancingShader)
{
	if (dwDirection)
	{
		XMFLOAT4X4 xmf4x4Rotate;
		XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);
		//화살표 키 ‘↑’를 누르면 로컬 z-축 방향으로 이동(전진)한다. ‘↓’를 누르면 반대 방향으로 이동한다.
		if (dwDirection & DIR_FORWARD)
			xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Look, fDistance, true));
		if (dwDirection & DIR_BACKWARD)
			xmf3Shift = Vector3::Subtract(xmf3Shift, Vector3::ScalarProduct(m_xmf3Look, fDistance, true));
		//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.
		if (dwDirection & DIR_RIGHT)
			xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Right, fDistance, true));
		if (dwDirection & DIR_LEFT)
			xmf3Shift = Vector3::Subtract(xmf3Shift, Vector3::ScalarProduct(m_xmf3Right, fDistance, true));
		//‘Page Up’을 누르면 로컬 y-축 방향으로 이동한다. ‘Page Down’을 누르면 반대 방향으로 이동한다.

		BoundingBox box;
		box.Center = Vector3::Add(m_xmAABB.Center, XMFLOAT3(xmf3Shift.x, 0.0f, xmf3Shift.z));

		//float fLength = sqrtf(m_xmf3Velocity.x * m_xmf3Velocity.x + m_xmf3Velocity.z * m_xmf3Velocity.z);
		//float fMaxVelocityXZ = m_fMaxVelocityXZ * fDistance;
		//if (fLength > fMaxVelocityXZ)
		//{
		//	m_xmf3Velocity.x *= (fMaxVelocityXZ / fLength);
		//	m_xmf3Velocity.z *= (fMaxVelocityXZ / fLength);
		//}

		if (CollisionCheck(box, vtInstancingShader)) {
			xmf3Shift = { 0.f,0.f,0.f };
			Move(xmf3Shift, bUpdateVelocity);
			return false;
		}
		else Move(xmf3Shift, bUpdateVelocity);
		//플레이어를 현재 위치 벡터에서 d3dxvShift 벡터 만큼 이동한다.
		return true;
	}
	return false;
}

void CPlayer::Move(XMFLOAT3& xmf3Shift, bool bUpdateVelocity)
{
	//bUpdateVelocity가 참이면 플레이어를 이동하지 않고 속도 벡터를 변경한다.
	if (bUpdateVelocity)
	{
		m_xmf3Velocity = xmf3Shift;
	}
	else{
			XMFLOAT3 xmf3Position = Vector3::Add(m_xmf3Position, xmf3Shift);
			m_xmf3Position = xmf3Position;

			m_xmAABB.Center.x = m_xmf4x4Translate._41 = m_xmf3Position.x;
			m_xmAABB.Center.y = m_xmf4x4Translate._42 = m_xmf3Position.y;
			m_xmAABB.Center.z = m_xmf4x4Translate._43 = m_xmf3Position.z;

			//플레이어의 위치가 변경되었으므로 카메라의 위치도 d3dxvShift 벡터 만큼 이동한다.
			m_pCamera->Move(xmf3Shift);
	}
}

bool CPlayer::CollisionCheck(BoundingBox& boundingbox, vector<CShader*>& ppInstancingShader)
{
	int nObjects;
	CGameObject ** ppObjects;

	for (int i = 2; i < 13; ++i) {
		nObjects = ppInstancingShader[i]->GetObjectNum();
		ppObjects = ppInstancingShader[i]->GetppObjects();
		for (int j = 0; j < nObjects; ++j) {
			ContainmentType containType = boundingbox.Contains(ppObjects[j]->GetAABB());
			switch (containType)
			{
			case DISJOINT:
			{
				continue;
			}
			case INTERSECTS:
			{
				cout << "INTERSECTS" << endl;
				return true;
			}
			case CONTAINS: {
				cout << "CONTAINS" << endl;
				return true;
			}
			}
		}
	}
	return false;
}

//플레이어를 로컬 x-축, y-축, z-축을 중심으로 회전한다.
void CPlayer::Rotate(float x, float y, float z)
{
	XMFLOAT4X4 xmf4x4Rotate;
	DWORD nCurrentCameraMode = m_pCamera->GetMode();

	//1인칭 카메라 또는 3인칭 카메라의 경우 플레이어의 회전은 약간의 제약이 따른다.
	if ((nCurrentCameraMode == FIRST_PERSON_CAMERA) || (nCurrentCameraMode == THIRD_PERSON_CAMERA))
	{
		/*로컬 x-축을 중심으로 회전하는 것은 고개를 앞뒤로 숙이는 동작에 해당한다. 그러므로 x-축을 중심으로 회전하는 각도는 -89.0~+89.0도 사이로 제한한다. x는 현재의 m_fPitch에서 실제 회전하는 각도이므로 x만큼 회전한 다음 Pitch가 +89도 보다 크거나 -89도 보다 작으면 m_fPitch가 +89도 또는 -89도가 되도록 회전각도(x)를 수정한다.*/
		if (x != 0.0f)
		{
			m_fPitch += x;
			if (m_fPitch > +89.0f) { x -= (m_fPitch - 89.0f); m_fPitch = +89.0f; }
			if (m_fPitch < -89.0f) { x -= (m_fPitch + 89.0f); m_fPitch = -89.0f; }

		}
		//로컬 y-축을 중심으로 회전하는 것은 몸통을 돌리는 것이므로 회전 각도의 제한이 없다.
		if (y != 0.0f)
		{
			m_fYaw += y;
			if (m_fYaw > 360.0f) m_fYaw -= 360.0f;
			if (m_fYaw < 0.0f) m_fYaw += 360.0f;
		}
		/*로컬 z-축을 중심으로 회전하는 것은 몸통을 좌우로 기울이는 것이므로 회전 각도는 -20.0~+20.0도 사이로 제한된다. z는 현재의 m_fRoll에서 실제 회전하는 각도이므로 z만큼 회전한 다음 m_fRoll이 +20도 보다 크거나 -20도보다 작으면 m_fRoll이 +20도 또는 -20도가 되도록 회전각도(z)를 수정한다.*/
		if (z != 0.0f)
		{
			m_fRoll += z;
			if (m_fRoll > +20.0f) { z -= (m_fRoll - 20.0f); m_fRoll = +20.0f; }
			if (m_fRoll < -20.0f) { z -= (m_fRoll + 20.0f); m_fRoll = -20.0f; }

		}
		//카메라를 x, y, z 만큼 회전한다. 플레이어를 회전하면 카메라가 회전하게 된다.
		m_pCamera->Rotate(x, y, z);

		/*플레이어를 회전한다. 1인칭 카메라 또는 3인칭 카메라에서 플레이어의 회전은 로컬 y-축에서만 일어난다. 플레이어의 로컬 y-축(Up 벡터)을 기준으로 로컬 z-축(Look 벡터)와 로컬 x-축(Right 벡터)을 회전시킨다. 기본적으로 Up 벡터를 기준으로 회전하는 것은 플레이어가 똑바로 서있는 것을 가정한다는 의미이다.*/
		if (y != 0.0f)
		{
			xmf4x4Rotate = Matrix4x4::RotationAxis(m_xmf3Up, y);
			m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4Rotate);
			m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4Rotate);
		}
	}
	else if (nCurrentCameraMode == SPACESHIP_CAMERA)
	{
		/*스페이스-쉽 카메라에서 플레이어의 회전은 회전 각도의 제한이 없다. 그리고 모든 축을 중심으로 회전을 할 수 있다.*/
		m_pCamera->Rotate(x, y, z);
		if (x != 0.0f)
		{
			xmf4x4Rotate = Matrix4x4::RotationAxis(m_xmf3Right, x);
			m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4Rotate);
			m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4Rotate);
		}
		if (y != 0.0f)
		{
			xmf4x4Rotate = Matrix4x4::RotationAxis(m_xmf3Up, y);
			m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4Rotate);
			m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4Rotate);
		}
		if (z != 0.0f)
		{
			xmf4x4Rotate = Matrix4x4::RotationAxis(m_xmf3Look, z);
			m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4Rotate);
			m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4Rotate);
		}
	}

	/*회전으로 인해 플레이어의 로컬 x-축, y-축, z-축이 서로 직교하지 않을 수 있으므로 z-축(LookAt 벡터)을 기준으로 하여 서로 직교하고 단위벡터가 되도록 한다.*/
	m_xmf3Look = Vector3::Normalize(m_xmf3Look);
	m_xmf3Right = Vector3::CrossProduct(m_xmf3Up, m_xmf3Look);
	m_xmf3Right = Vector3::Normalize(m_xmf3Right);
	m_xmf3Up = Vector3::CrossProduct(m_xmf3Look, m_xmf3Right);
	m_xmf3Up = Vector3::Normalize(m_xmf3Up);
}

void CPlayer::Update(float fTimeElapsed, int nPlayer, vector<CShader*> vtInstancingShader)
{
	if (nPlayer == MYPLAYER) {
		/*플레이어의 속도 벡터를 중력 벡터와 더한다. 중력 벡터에 fTimeElapsed를 곱하는 것은 중력을 시간에 비례하도록 적용한다는 의미이다.*/
		m_xmf3Velocity = Vector3::Add(m_xmf3Velocity, Vector3::ScalarProduct(m_xmf3Gravity, fTimeElapsed, false));

		/*플레이어의 속도 벡터의 XZ-성분의 크기를 구한다. 이것이 XZ-평면의 최대 속력보다 크면 속도 벡터의 x와 z-방향 성분을 조정한다.*/
		float fLength = sqrtf(m_xmf3Velocity.x * m_xmf3Velocity.x + m_xmf3Velocity.z * m_xmf3Velocity.z);
		float fMaxVelocityXZ = m_fMaxVelocityXZ * fTimeElapsed;
		if (fLength > fMaxVelocityXZ)
		{
			m_xmf3Velocity.x *= (fMaxVelocityXZ / fLength);
			m_xmf3Velocity.z *= (fMaxVelocityXZ / fLength);
		}

		/*플레이어의 속도 벡터의 Y-성분의 크기를 구한다. 이것이 Y 축 방향의 최대 속력보다 크면 속도 벡터의 y-방향 성분을 조정한다.*/
		fLength = sqrtf(m_xmf3Velocity.y * m_xmf3Velocity.y);
		float fMaxVelocityY = m_fMaxVelocityY * fTimeElapsed;
		if (fLength > fMaxVelocityY) m_xmf3Velocity.y *= (fMaxVelocityY / fLength);
		//플레이어를 속도 벡터 만큼 실제로 이동한다(카메라도 이동될 것이다). 
		if (m_keyflag == false) {
			m_xmf3Velocity.x = 0; m_xmf3Velocity.y = 0; m_xmf3Velocity.z = 0;
		}


		Move(m_xmf3Velocity, false);

		/*플레이어의 위치가 변경될 때 추가로 수행할 작업을 수행한다. 예를 들어, 플레이어의 위치가 변경되었지만
		플레이어 객체에는 지형(Terrain)의 정보가 없다. 플레이어의 새로운 위치가 유효한 위치가 아닐 수도 있고
		또는 플레이어의 충돌 검사 등을 수행할 필요가 있다.
		이러한 상황에서 플레이어의 위치를 유효한 위치로 다시 변경할 수 있다.*/
		if (m_pPlayerUpdatedContext && m_pPlayerInfo->GetROLE() == KILLER) {
			if(CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == BIDLE
				|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == BRUN
				|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == BPUNCH)OnPlayerUpdated(fTimeElapsed, 12.0f);
			else OnPlayerUpdated(fTimeElapsed);

		}
		if (m_pPlayerUpdatedContext && m_pPlayerInfo->GetROLE() == SURVIVOR)
		{
			if (CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SINSTALLTRAP
				|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SWALK
				|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SINJUREWALK
				|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SINJUREDIDLE
				|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SSTANDINGTOCROUCH
				|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SCROUCHIDLE
				|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SCROUCHTOSTANDING
				|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SCROUCHWALK
				|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SGEN
				|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SSTAPONTRAP) OnPlayerUpdated(fTimeElapsed, -1.0f);
			else OnPlayerUpdated(fTimeElapsed, 1.0f);
		}

		/*if (m_pPlayerUpdatedContext && (nPlayer == FIRST)) OnPlayerUpdated(fTimeElapsed);
		if (m_pPlayerUpdatedContext && (nPlayer == SECOND)) OnPlayerUpdated(fTimeElapsed, 1.0f);*/

		DWORD nCurrentCameraMode = m_pCamera->GetMode();
		//플레이어의 위치가 변경되었으므로 카메라의 상태를 갱신한다.
		if (nCurrentCameraMode == THIRD_PERSON_CAMERA) m_pCamera->Update(m_xmf3Position, fTimeElapsed);
		//카메라의 위치가 변경될 때 추가로 수행할 작업을 수행한다. 
		if (m_pCameraUpdatedContext) OnCameraUpdated(fTimeElapsed);
		//카메라가 3인칭 카메라이면 카메라가 변경된 플레이어 위치를 바라보도록 한다.
		if (nCurrentCameraMode == THIRD_PERSON_CAMERA) m_pCamera->SetLookAt(m_xmf3Position);
		//카메라의 카메라 변환 행렬을 다시 생성한다.
		m_pCamera->RegenerateViewMatrix();

		/*플레이어의 속도 벡터가 마찰력 때문에 감속이 되어야 한다면 감속 벡터를 생성한다.
		속도 벡터의 반대 방향 벡터를 구하고 단위 벡터로 만든다.
		마찰 계수를 시간에 비례하도록 하여 마찰력을 구한다.
		단위 벡터에 마찰력을 곱하여 감속 벡터를 구한다.
		속도 벡터에 감속 벡터를 더하여 속도 벡터를 줄인다. 마찰력이 속력보다 크면 속력은 0이 될 것이다.*/

		//D3DXVECTOR3 d3dxvDeceleration = -m_d3dxvVelocity;
		//D3DXVec3Normalize(&d3dxvDeceleration, &d3dxvDeceleration);
		//fLength = D3DXVec3Length(&m_d3dxvVelocity);
		//float fDeceleration = (m_fFriction * fTimeElapsed);
		//if (fDeceleration > fLength) fDeceleration = fLength;
		//m_d3dxvVelocity += d3dxvDeceleration * fDeceleration;
	}
	else
	{
		switch (m_pPlayerInfo->GetEventType())
		{
		case EVENT_NONE:
			if (m_pPlayerInfo->GetROLE() == KILLER && m_pPlayerInfo->AnimationType() != TYPE_IDEL && CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() != BPUNCH) {
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BIDLE);
				m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
			}
			if (m_pPlayerInfo->GetROLE() == SURVIVOR && m_pPlayerInfo->AnimationType() != TYPE_CROUSHON && m_pPlayerInfo->AnimationType() != TYPE_CROUSHIDLE
				&& m_pPlayerInfo->AnimationType() != TYPE_CROUSHOFF && m_pPlayerInfo->AnimationType() != TYPE_TRAPINSTALL && m_pPlayerInfo->AnimationType() != TYPE_STEPONTRAP)
			{
			if (m_pPlayerInfo->GetROLE() == SURVIVOR && m_pPlayerInfo->AnimationType() != TYPE_IDEL && m_pPlayerInfo->AnimationType() != TYPE_INTERACTION
				&& CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() != SREACT && m_pPlayerInfo->GetHP() > 1) {
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
				m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
			}
			else if (m_pPlayerInfo->GetROLE() == SURVIVOR && m_pPlayerInfo->AnimationType() != TYPE_INJUREDIDLE && m_pPlayerInfo->AnimationType() != TYPE_INTERACTION
				&& CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() != SREACT && m_pPlayerInfo->GetHP() == 1) {
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREDIDLE);
				m_pPlayerInfo->SetAnimationType(TYPE_INJUREDIDLE);
			}
			}
			

			if (m_pPlayerUpdatedContext && m_pPlayerInfo->GetROLE() == KILLER) {
				if(CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == BIDLE
					|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == BRUN
					|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == BPUNCH)OnPlayerUpdated(fTimeElapsed, 12.0f);
				else 
					OnPlayerUpdated(fTimeElapsed);
			}
			if (m_pPlayerUpdatedContext && m_pPlayerInfo->GetROLE() == SURVIVOR)
			{
				if (CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SINSTALLTRAP
					|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SWALK
					|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SINJUREWALK
					|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SINJUREDIDLE
					|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SSTANDINGTOCROUCH
					|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SCROUCHIDLE
					|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SCROUCHTOSTANDING
					|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SCROUCHWALK
					|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SGEN
					|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SSTAPONTRAP) OnPlayerUpdated(fTimeElapsed, -1.0f);
				else OnPlayerUpdated(fTimeElapsed, 1.0f);
			}

			// 멈쳐 있는 경우로 생각하고 생략
			break;
		case EVENT_MOVESTART:
			// 이동을 시작한 상태
			// prev의 값과 nextpos와 시간을 이용하여 이동을 해 주어야 한다.
		{
			// 임시 변수 tempTime -> NextTime - prevTIme 의 차이 UINT64

			auto tempTime = this->m_pPlayerInfo->GetNextTIME() - this->m_pPlayerInfo->GetPreTime();
			// type_cast() INT64 -> float  GepTime
			float GapTime = static_cast<float> (tempTime);
			// tempTime -> 현 시간 -> 서버 기준 UINT64
			INT64 nowTime = GetTickCount64();
			// (서버기준)현 시간보다 NextTime이 클 경우 의 경우 선형보간을 그대로 적용
			if (this->m_pPlayerInfo->GetNextTIME() > nowTime)
			{
				//printf("GetNextPosition  %.2f %.2f %.2f\n", this->m_pPlayerInfo->GetNextPosition().x, this->m_pPlayerInfo->GetNextPosition().y, this->m_pPlayerInfo->GetNextPosition().z);
				// 현시간을 Float형으로 변환
				if (m_pPlayerInfo->GetROLE() == KILLER && CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == BIDLE) {
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN);
				}
				else if (m_pPlayerInfo->GetROLE() == SURVIVOR && CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SIDLE) {
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN);
				}
				float iter = static_cast<float>(this->m_pPlayerInfo->GetNextTIME() - nowTime);
				float result = 1.f - (iter / GapTime);
				//ssert(result > 0 && "result is minus!!, %f, %f", iter, GapTime);

				this->SetPosition(Vector3::Lerp(this->m_pPlayerInfo->GetPrePosition(), this->m_pPlayerInfo->GetFixPosition(), result));
				if (m_pPlayerUpdatedContext && m_pPlayerInfo->GetROLE() == KILLER) {
					if (CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == BIDLE
						|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == BRUN
						|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == BPUNCH)OnPlayerUpdated(fTimeElapsed, 12.0f);
					else
						OnPlayerUpdated(fTimeElapsed);
				}
				if (m_pPlayerUpdatedContext && m_pPlayerInfo->GetROLE() == SURVIVOR)
				{
					if (CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SINSTALLTRAP
						|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SWALK
						|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SINJUREWALK
						|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SINJUREDIDLE
						|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SSTANDINGTOCROUCH
						|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SCROUCHIDLE
						|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SCROUCHTOSTANDING
						|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SCROUCHWALK
						|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SGEN
						|| CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SSTAPONTRAP) OnPlayerUpdated(fTimeElapsed, -1.0f);
					else OnPlayerUpdated(fTimeElapsed, 1.0f);
				}
			}
			else
			{
				// tempTime 이 NextTime을 넘어갔을 경우 
				this->SetPosition(Vector3::Lerp(this->m_pPlayerInfo->GetPrePosition(), this->m_pPlayerInfo->GetFixPosition(), 1));
				if (m_pPlayerUpdatedContext && m_pPlayerInfo->GetROLE() == KILLER) OnPlayerUpdated(fTimeElapsed);
				if (m_pPlayerUpdatedContext && m_pPlayerInfo->GetROLE() == SURVIVOR) OnPlayerUpdated(fTimeElapsed, 1.0f);
				this->m_pPlayerInfo->SetEventType(EVENT_NONE);
			}
		}
		break;
		case EVENT_MOVESTOP:
			this->SetPosition(this->m_pPlayerInfo->GetNextPosition());
			//printf("[ 멈춤 ] : curr [x %.2f][z %.2f], \n\n", Currentpos_.x, Currentpos_.z);
			this->m_pPlayerInfo->SetEventType(EVENT_NONE);
			break;
		case EVENT_DEADRECKNING:
		{
			// 임시 변수 tempTime -> NextTime - prevTIme 의 차이 UINT64
			auto tempTime = this->m_pPlayerInfo->GetNextTIME() - this->m_pPlayerInfo->GetPreTime();
			// type_cast() INT64 -> float  GepTime
			float GapTime = static_cast<float> (tempTime);
			// tempTime -> 현 시간 -> 서버 기준 UINT64
			tempTime = GetTickCount64();
			// (서버기준)현 시간보다 NextTime이 클 경우 의 경우 선형보간을 그대로 적용
			if (this->m_pPlayerInfo->GetNextTIME() > tempTime)
			{
				// 현시간을 Float형으로 변환
				float iter = static_cast<float>(this->m_pPlayerInfo->GetNextTIME() - tempTime);
				float result = 1.f - (iter / GapTime);
				this->SetPosition(Vector3::Lerp(this->m_pPlayerInfo->GetPrePosition(), this->m_pPlayerInfo->GetNextPosition(), result));
			}
			else
			{
				// tempTime 이 NextTime을 넘어갔을 경우 
				this->m_pPlayerInfo->SetPreTIme(this->m_pPlayerInfo->GetNextTIME());
				this->m_pPlayerInfo->SetNextTIME(this->m_pPlayerInfo->GetNextTIME() + 83);
				float iterTime = static_cast<float>(this->m_pPlayerInfo->GetNextTIME() - tempTime);
				this->m_pPlayerInfo->SetPrePosition(this->m_pPlayerInfo->GetNextPosition());
				this->m_pPlayerInfo->SetNextPosition(Vector3::Add(this->m_pPlayerInfo->GetPrePosition(), Vector3::ScalarProduct(this->GetVelocity(), 83)));
				//위험요소
				float result = 1.f - (static_cast<float>(iterTime) / 83.f);
				//assert(result > 0 && "result is minus!!, %f, %f", iter, GapTime);
				this->SetPosition(Vector3::Lerp(this->m_pPlayerInfo->GetPrePosition(), this->m_pPlayerInfo->GetNextPosition(), result));
			}
		}
		break;
		case EVENT_INTERPOLATION:
			// 카드뮬 보간 
			// 인자 FixPos(변경이 일어났을때의 현재 위치) , 
			//NextFramePos(다음 프레임까지의 위치), PrevTime(처음 입력 시간), 
			//NextTime(다음 프레임 도착 시간), RotateV(바라보는 방향)
			//FLT_EPSILON
			//static float FixTime = GetTickCount64() + (UINT64)CSgap() + 60;   
			// 다른 플레이어의 이동은 4번의 렌더링 후로 바뀌니 움직임의 동기화가 깨지는 문제점 발생
			//      0.06?? = 60ms 4번의 렌더링 후 해당 시간과 같아짐
			//      60fps =>>>>>> 16.6666666
			// 경우의 수 1. INTERPOLATION이 발생 도중 또 다른 변경이 이루어 졌을때는?
		{
			UINT64 nowTick = GetTickCount64();
			if (this->m_pPlayerInfo->GetNextTIME() < nowTick)
			{
				nowTick = nowTick - this->m_pPlayerInfo->GetNextTIME();
				this->SetPosition(Vector3::CatmullRom(this->m_pPlayerInfo->GetPrePosition(), this->GetPosition(),
					this->m_pPlayerInfo->GetFixPosition(), this->m_pPlayerInfo->GetNextPosition(), 1.0f));

				this->SetPosition(Vector3::Add(this->GetPosition(), Vector3::ScalarProduct(this->GetVelocity(), nowTick)));
				//위험요소
				//Currentpos_ += RotateMoveDir_ * nowTick * Velocity_F();
				this->m_pPlayerInfo->SetPrePosition(this->GetPosition());
				INT64 iter = 83;
				this->m_pPlayerInfo->SetNextPosition(Vector3::Add(this->m_pPlayerInfo->GetPrePosition(), Vector3::ScalarProduct(this->GetVelocity(), iter)));
				this->m_pPlayerInfo->SetPreTIme(GetTickCount64());
				this->m_pPlayerInfo->SetNextTIME(GetTickCount64() + iter);
				cout << " EVENT_INTERPOLATION " << this->m_pPlayerInfo->GetNextTIME() << endl;
				this->m_pPlayerInfo->SetEventType(EVENT_DEADRECKNING);
				//cnt = 0.f;
			}
			else
			{
				auto nexttime = this->m_pPlayerInfo->GetNextTIME();
				auto tempTime = nexttime - nowTick;
				auto cnt = static_cast<float>(1.f - (tempTime / ClientNetworkInterface::getInstance().Latency()));
				//this->SetPosition(Vector3::CatmullRom(this->m_pPlayerInfo->GetPrePosition(), this->GetPosition(),
				//   this->m_pPlayerInfo->GetFixPosition(), this->m_pPlayerInfo->GetNextPosition(), cnt));
			}
		}
		break;
		case EVENT_ATT:
			break;
		case EVENT_OPERATORON:
			break;
		case EVENT_TRAPON:
			break;
		default:
			break;
		}
	}
}

CCamera *CPlayer::OnChangeCamera(ID3D11Device *pd3dDevice, DWORD nNewCameraMode, DWORD nCurrentCameraMode)
{
	CCamera *pNewCamera = NULL;
	//새로운 카메라의 모드에 따라 카메라를 새로 생성한다.
	switch (nNewCameraMode)
	{
	case FIRST_PERSON_CAMERA:
		pNewCamera = new CFirstPersonCamera(m_pCamera);
		break;
	case THIRD_PERSON_CAMERA:
		pNewCamera = new CThirdPersonCamera(m_pCamera);
		break;
	case SPACESHIP_CAMERA:
		pNewCamera = new CSpaceShipCamera(m_pCamera);
		break;
	}
	/*현재 카메라의 모드가 스페이스-쉽 모드의 카메라이고 새로운 카메라가 1인칭 또는 3인칭 카메라이면 플레이어의 Up 벡터를 월드좌표계의 y-축 방향 벡터(0, 1, 0)이 되도록 한다. 즉, 똑바로 서도록 한다. 그리고 스페이스-쉽 카메라의 경우 플레이어의 이동에는 제약이 없다. 특히, y-축 방향의 움직임이 자유롭다. 그러므로 플레이어의 위치는 공중(위치 벡터의 y-좌표가 0보다 크다)이 될 수 있다. 이때 새로운 카메라가 1인칭 또는 3인칭 카메라이면 플레이어의 위치는 지면이 되어야 한다. 그러므로 플레이어의 Right 벡터와 Look 벡터의 y 값을 0으로 만든다. 이제 플레이어의 Right 벡터와 Look 벡터는 단위벡터가 아니므로 정규화한다.*/
	if (nCurrentCameraMode == SPACESHIP_CAMERA)
	{
		m_xmf3Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
		m_xmf3Right.y = 0.0f;
		m_xmf3Look.y = 0.0f;
		m_xmf3Right = Vector3::Normalize(m_xmf3Right);
		m_xmf3Look = Vector3::Normalize(m_xmf3Look);
		m_fPitch = 0.0f;
		m_fRoll = 0.0f;
		/*Look 벡터와 월드좌표계의 z-축(0, 0, 1)이 이루는 각도(내적=cos)를 계산하여 플레이어의 y-축의 회전 각도 m_fYaw로 설정한다.*/
		m_fYaw = (float)D3DXToDegree(acosf(Vector3::DotProduct(XMFLOAT3(0.0f, 0.0f, 1.0f), m_xmf3Look)));
		if (m_xmf3Look.x < 0.0f) m_fYaw = -m_fYaw;
	}
	else if ((nNewCameraMode == SPACESHIP_CAMERA) && m_pCamera)
	{
		/*새로운 카메라의 모드가 스페이스-쉽 모드의 카메라이고 현재 카메라 모드가 1인칭 또는 3인칭 카메라이면 플레이어의 로컬 축을 현재 카메라의 로컬 축과 같게 만든다.*/
		m_xmf3Right = m_pCamera->GetRightVector();
		m_xmf3Up = m_pCamera->GetUpVector();
		m_xmf3Look = m_pCamera->GetLookVector();
	}

	if (pNewCamera)
	{
		//기존 카메라가 없으면 새로운 카메라를 위한 쉐이더 변수를 생성한다.
		if (!m_pCamera) pNewCamera->CreateShaderVariables(pd3dDevice);
		pNewCamera->SetMode(nNewCameraMode);
		//현재 카메라를 사용하는 플레이어 객체를 설정한다.
		pNewCamera->SetPlayer(this);
	}

	if (m_pCamera) delete m_pCamera;

	return(pNewCamera);
}

void CPlayer::OnPlayerUpdated(float fTimeElapsed, float fRegulateHeight)
{
}

void CPlayer::OnCameraUpdated(float fTimeElapsed)
{
}

void CPlayer::ChangeCamera(ID3D11Device *pd3dDevice, DWORD nNewCameraMode, float fTimeElapsed)
{
}

void CPlayer::SetPlayerRotationPYR(float Pitch, float Yaw, float Roll)
{
	m_xmf4x4Rotate = Matrix4x4::RotationYawPitchRoll(Pitch, Yaw, Roll);
	
}

void CPlayer::SetObjectScale(int Modelidx)
{
	m_xmf4x4Scale._11 *= GetMesh(Modelidx)->GetFBXModelSize();
	m_xmf4x4Scale._12 *= GetMesh(Modelidx)->GetFBXModelSize();
	m_xmf4x4Scale._13 *= GetMesh(Modelidx)->GetFBXModelSize();
	m_xmf4x4Scale._21 *= GetMesh(Modelidx)->GetFBXModelSize();
	m_xmf4x4Scale._22 *= GetMesh(Modelidx)->GetFBXModelSize();
	m_xmf4x4Scale._23 *= GetMesh(Modelidx)->GetFBXModelSize();
	m_xmf4x4Scale._31 *= GetMesh(Modelidx)->GetFBXModelSize();
	m_xmf4x4Scale._32 *= GetMesh(Modelidx)->GetFBXModelSize();
	m_xmf4x4Scale._33 *= GetMesh(Modelidx)->GetFBXModelSize();
}

void CPlayer::OnPrepareRender()
{
	m_xmf4x4Translate._11 = m_xmf3Right.x;
	m_xmf4x4Translate._12 = m_xmf3Right.y;
	m_xmf4x4Translate._13 = m_xmf3Right.z;
	m_xmf4x4Translate._21 = m_xmf3Up.x;
	m_xmf4x4Translate._22 = m_xmf3Up.y;
	m_xmf4x4Translate._23 = m_xmf3Up.z;
	m_xmf4x4Translate._31 = m_xmf3Look.x;
	m_xmf4x4Translate._32 = m_xmf3Look.y;
	m_xmf4x4Translate._33 = m_xmf3Look.z;

	m_xmf4x4World = Matrix4x4::Multiply(Matrix4x4::Multiply(m_xmf4x4Scale, m_xmf4x4Rotate), m_xmf4x4Translate);
}

//(5) 플레이어는 절두체 컬링을 하지않는다.
void CPlayer::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	OnPrepareRender();

	CShader::UpdateShaderVariable(pd3dDeviceContext, &m_xmf4x4World);

	if (m_pMaterial) CIlluminatedShader::UpdateShaderVariable(pd3dDeviceContext, &m_pMaterial->m_Material);

	if (m_pTexture) m_pTexture->UpdateShaderVariable(pd3dDeviceContext);

	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
			m_ppMeshes[i]->Render(pd3dDeviceContext);
	}
}

void CPlayer::SetMotionAABB(void)
{
	//캐릭터의 look벡터 방향쪽으로 boundingbox를 만든다.
	XMFLOAT3 xmf3Shift = Vector3::ScalarProduct(m_xmf3Look, 3.0f, true);
	m_motionAABB = BoundingBox(Vector3::Add(m_xmf3Position, xmf3Shift), XMFLOAT3(10.0f, 10.0f, 10.0f));
}

void CPlayer::PlayerPYRUpdate(INT32 keydirection, ANIMATION_TYPE any)
{
	auto player = CPlayerManager::getInstance().players();

	// 공격을 눌렀으며 킬러일 경우
	if (any == TYPE_ATTACK && m_pPlayerInfo->GetROLE() == KILLER) {
		// 현재 공격 애니메이션이 아닐경우 펀치애니메이션을 날림
		if (m_pPlayerInfo->AnimationType() != TYPE_ATTACK) {
			CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BPUNCH);
			m_pPlayerInfo->SetAnimationType(TYPE_ATTACK);
			//살인자가 근처에서 공격하면 그 사운드를 나(생존자가)가 들을 수 있다.
			if (isNearOther::isNearOther(m_xmf3Position, (*player).front()->GetPosition(), 15.0f))
				SoundEngine::getInstance().execute(ATT, true, false);
		}
		return;
	}
	// 생존자 일때
	if (m_pPlayerInfo->GetROLE() == SURVIVOR)
	{
		switch (any)
		{
		case TYPE_INTERACTION:
			// 상호작용 애니메이션이 들어왓을때  && 현재 애니메이션 타입이 상호작용이 아닐경우
			// 상호작용을 하도록 애니메이션 수정이 필요.
			if (m_pPlayerInfo->AnimationType() != TYPE_INTERACTION)
			{
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SGEN);
				m_pPlayerInfo->SetAnimationType(TYPE_INTERACTION);
				//생존자가 근처에서 발전기를 가동시키는 중이면 그 사운드를 나(생존자 OR 살인자)가 들을 수 있다.
				if (isNearOther::isNearOther(m_xmf3Position, (*player).front()->GetPosition(), 30.0f))
					SoundEngine::getInstance().execute(GENERATING, true, false);
				return;
			}
			break;
		case TYPE_TRAPINSTALL:
			// 받은 애니메이션 트랩설치 인데 현재 애니메이션이 트랩 설치가 아닌 경우
			// 트랩설치 애니메이션으로 변경 해주어야 한다.
			if (m_pPlayerInfo->AnimationType() != TYPE_TRAPINSTALL)
			{
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SINSTALLTRAP);
				m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_TRAPINSTALL);
				//1
				if (isNearOther::isNearOther(m_xmf3Position, (*player).front()->GetPosition(), 30.0f))
					SoundEngine::getInstance().execute(INSTALLTRAP, true, false);
				return;
			}
			break;
		case TYPE_STEPONTRAP:
			// 받은 애니메이션이 트랩을 밟은 것인데 현재 애니메이션이 고통스러워 하는 것이 아니라면..
			// 고통스러운  애니메이션으로 변경 해줘야 한다.
			if (m_pPlayerInfo->AnimationType() != TYPE_STEPONTRAP)
			{
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SSTAPONTRAP);
				m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_STEPONTRAP);
				return;
			}
			break;
		case TYPE_HITTED:
			if (m_pPlayerInfo->AnimationType() != TYPE_HITTED)
			{
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SREACT);
				m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_HITTED);
				//생존자가 근처에 있으면 피격당하는 사운드를 나(생존자)가 들을 수 있다.
				if (isNearOther::isNearOther(m_xmf3Position, (*player).front()->GetPosition(), 15.0f))
					SoundEngine::getInstance().execute(HITTED, true, false);
				return;
			}
		case TYPE_INJUREDIDLE:
			if (m_pPlayerInfo->AnimationType() != TYPE_INJUREDIDLE)
			{
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SINJUREDIDLE);
				m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_INJUREDIDLE);
			}
			break;
		case TYPE_CROUSHON:
			// 원격 플레이어가 앉으려고 하는 상태가 아니고 이미 앉아 있는 상태도 아닌 경우
			if (m_pPlayerInfo->AnimationType() != TYPE_CROUSHON &&
				m_pPlayerInfo->AnimationType() != TYPE_CROUSHIDLE)
			{
				// 일단 앉게 시킨다.
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SSTANDINGTOCROUCH);
				m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHON);
			}
			break;
		case TYPE_CROUSHOFF:
			// 원격 플레이어가 컨트롤 키를 땟을경우 일어난 애니메이션을 보여아 햔다.
			if (m_pPlayerInfo->AnimationType() != TYPE_CROUSHOFF)
			{
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SCROUCHTOSTANDING);
				m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHOFF);
			}
			break;
		default:
			// 현재 애니메이션이 상호작용이나 받은것들이 전혀 상관 없는 경우
			// 상호작용을 그만하게 애니메이션 수정이 필요.
			// ERROR... 아마 발생하면 이곳에서 말썽을 일으킬확률이 높다
			if (m_pPlayerInfo->AnimationType() == TYPE_INTERACTION)
			{
				if (m_pPlayerInfo->GetHP() == 1)
				{
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SINJUREDIDLE);
					m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_INJUREDIDLE);
				}
				else
				{
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
					m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
				}
			}
			break;
		}
	}

	// 입력키가 있으며 현재 애니메이션이 공격이 아닌경우 -> 이동키 눌림.
	if (keydirection != 0 && m_pPlayerInfo->AnimationType() != TYPE_ATTACK)
	{
		double fDistance = (m_pPlayerInfo->GetROLE() == KILLER) ? 0.0537 * (double)ClientNetworkInterface::getInstance().Latency() : 0.0515f * (double)ClientNetworkInterface::getInstance().Latency();
		XMFLOAT4X4 xmf4x4Rotate;
		XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);
		//화살표 키 ‘↑’를 누르면 로컬 z-축 방향으로 이동(전진)한다. ‘↓’를 누르면 반대 방향으로 이동한다.
		if (keydirection & DIR_FORWARD) {
			// 살인자이면서 앞키가 안눌러져 있으면 이동 애니메이션을 넣음.
			if (m_pPlayerInfo->GetROLE() == KILLER && m_pPlayerInfo->AnimationType() != TYPE_FORWARDRUN) {
				SetPlayerRotationPYR(180.0f, 0.0f, 0.0f);
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN, true, true);
				m_pPlayerInfo->SetAnimationType(TYPE_FORWARDRUN);
			}
			// 생존자 이면서 애니메이션이 적용이 안되 있을 경우
			else if (m_pPlayerInfo->GetROLE() == SURVIVOR)
			{
				if (keydirection & DIR_CROUCH)
				{
					fDistance = 0.05 * (double)ClientNetworkInterface::getInstance().Latency();
					if (m_pPlayerInfo->AnimationType() != TYPE_CROUSHFORWORDWALK)
					{
						SetPlayerRotationPYR(90.0f, -90.0f, 90.0f);
						CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SCROUCHWALK);
						m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHFORWORDWALK);
					}
				}
				else if (keydirection & DIR_WALK)
				{
					fDistance = 0.05 * (double)ClientNetworkInterface::getInstance().Latency();
					if (m_pPlayerInfo->AnimationType() != TYPE_FORWORDWALK)
					{
						SetPlayerRotationPYR(90.0f, -90.0f, 90.0f);
						CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SWALK);
						m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_FORWORDWALK);
					}
				}
				else if(m_pPlayerInfo->AnimationType() != TYPE_INJUREDFORWORDWALK && m_pPlayerInfo->GetHP() == 1)
				{
					SetPlayerRotationPYR(90.0f, -90.0f, 90.0f);
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SINJUREWALK);
					m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_INJUREDFORWORDWALK);
				}
				else if (m_pPlayerInfo->AnimationType() != TYPE_FORWARDRUN && m_pPlayerInfo->GetHP() > 1)
				{
					SetPlayerRotationPYR(90.0f, -90.0f, 90.0f);
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN, true, true);
					m_pPlayerInfo->SetAnimationType(TYPE_FORWARDRUN);
				}
				SetPlayerRotationPYR(90.0f, -90.0f, 90.0f);
			}
			xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Look, fDistance, true));
		}

		if (keydirection & DIR_BACKWARD) {
			if (m_pPlayerInfo->GetROLE() == KILLER && m_pPlayerInfo->AnimationType() != TYPE_BACKRUN) {
				SetPlayerRotationPYR(360.0f, 0.0f, 0.0f);
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN, true, true);
				m_pPlayerInfo->SetAnimationType(TYPE_BACKRUN);
			}
			else if (m_pPlayerInfo->GetROLE() == SURVIVOR)
			{
				if (keydirection & DIR_CROUCH)
				{
					fDistance = 0.05 * (double)ClientNetworkInterface::getInstance().Latency();
					if (m_pPlayerInfo->AnimationType() != TYPE_CROUSHBACKWALK)
					{
						SetPlayerRotationPYR(90.0f, -90.0f, -90.0f);
						CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SCROUCHWALK);
						m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHBACKWALK);
					}
				}
				else if (keydirection & DIR_WALK)
				{
					fDistance = 0.05 * (double)ClientNetworkInterface::getInstance().Latency();
					if (m_pPlayerInfo->AnimationType() != TYPE_BACKWALK)
					{
						SetPlayerRotationPYR(90.0f, -90.0f, -90.0f);
						CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SWALK);
						m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_BACKWALK);
					}
				}
				else if (m_pPlayerInfo->AnimationType() != TYPE_INJUREDBACKWALK && m_pPlayerInfo->GetHP() == 1)
				{
					SetPlayerRotationPYR(90.0f, -90.0f, -90.0f);
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SINJUREWALK);
					m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_INJUREDBACKWALK);
				}
				else if (m_pPlayerInfo->AnimationType() != TYPE_BACKRUN && m_pPlayerInfo->GetHP() > 1)
				{
					SetPlayerRotationPYR(90.0f, -90.0f, -90.0f);
					//CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN, isNearOther(m_xmf3Position, (*player).front()->GetPosition(), 30.0f));
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN, true, true);
					m_pPlayerInfo->SetAnimationType(TYPE_BACKRUN);
				}
			}
			xmf3Shift = Vector3::Subtract(xmf3Shift, Vector3::ScalarProduct(m_xmf3Look, fDistance, true));
		}
		//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.
		if (keydirection & DIR_RIGHT) {
			if (m_pPlayerInfo->GetROLE() == KILLER && m_pPlayerInfo->AnimationType() != TYPE_RIGHTRUN) {
				SetPlayerRotationPYR(-90.0f, 0.0f, 0.0f);
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN, true, true);
				m_pPlayerInfo->SetAnimationType(TYPE_RIGHTRUN);
			}
			else if (m_pPlayerInfo->GetROLE() == SURVIVOR)
			{
				if (keydirection & DIR_CROUCH)
				{
					fDistance = 0.05 * (double)ClientNetworkInterface::getInstance().Latency();
					if (m_pPlayerInfo->AnimationType() != TYPE_CROUSHRIGHTWALK)
					{
						SetPlayerRotationPYR(90.0f, -90.0f, 180.0f);
						CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SCROUCHWALK);
						m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHRIGHTWALK);
					}
				}
				else if (keydirection & DIR_WALK)
				{
					fDistance = 0.05 * (double)ClientNetworkInterface::getInstance().Latency();
					if (m_pPlayerInfo->AnimationType() != TYPE_RIGHTWALK)
					{
						SetPlayerRotationPYR(90.0f, -90.0f, 180.0f);
						CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SWALK);
						m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_RIGHTWALK);
					}
				}
				else if (m_pPlayerInfo->AnimationType() != TYPE_INJUREDRIGHTWALK && m_pPlayerInfo->GetHP() == 1)
				{
					SetPlayerRotationPYR(90.0f, -90.0f, 180.0f);
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SINJUREWALK);
					m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_INJUREDRIGHTWALK);
				}
				else if (m_pPlayerInfo->AnimationType() != TYPE_RIGHTRUN && m_pPlayerInfo->GetHP() > 1)
				{
					SetPlayerRotationPYR(90.0f, -90.0f, 180.0f);
					//CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN, isNearOther(m_xmf3Position, (*player).front()->GetPosition(), 30.0f));
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN, true ,true);
					m_pPlayerInfo->SetAnimationType(TYPE_RIGHTRUN);
				}
			}
			xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Right, fDistance, true));
		}
		if (keydirection & DIR_LEFT) {
			if (m_pPlayerInfo->GetROLE() == KILLER && m_pPlayerInfo->AnimationType() != TYPE_LEFTRUN) {
				SetPlayerRotationPYR(90.0f, 0.0f, 0.0f);
				CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN, true, true);
				m_pPlayerInfo->SetAnimationType(TYPE_LEFTRUN);
			}
			else if (m_pPlayerInfo->GetROLE() == SURVIVOR)
			{
				if (keydirection & DIR_CROUCH)
				{
					fDistance = 0.05 * (double)ClientNetworkInterface::getInstance().Latency();
					if (m_pPlayerInfo->AnimationType() != TYPE_CROUSHLEFTWALK)
					{
						SetPlayerRotationPYR(90.0f, -90.0f, 0.0f);
						CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SCROUCHWALK);
						m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHLEFTWALK);
					}
				}
				else if (keydirection & DIR_WALK)
				{
					fDistance = 0.05 * (double)ClientNetworkInterface::getInstance().Latency();
					if (m_pPlayerInfo->AnimationType() != TYPE_LEFTWALK)
					{
						SetPlayerRotationPYR(90.0f, -90.0f, 0.0f);
						CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SWALK);
						m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_LEFTWALK);
					}
				}
				else if (m_pPlayerInfo->AnimationType() != TYPE_INJUREDLEFTWALK && m_pPlayerInfo->GetHP() == 1)
				{
					SetPlayerRotationPYR(90.0f, -90.0f, 0.0f);
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SurvivorAnimation::SINJUREWALK);
					m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_INJUREDLEFTWALK);
				}
				else if (m_pPlayerInfo->AnimationType() != TYPE_LEFTRUN && m_pPlayerInfo->GetHP() > 1)
				{
					SetPlayerRotationPYR(90.0f, -90.0f, 0.0f);
					//CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN, isNearOther(m_xmf3Position, (*player).front()->GetPosition(), 30.0f));
					CGameFramework::getInstance().m_ppPlayerShaders[this->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN, true, true);
					m_pPlayerInfo->SetAnimationType(TYPE_LEFTRUN);
				}
			}
			xmf3Shift = Vector3::Subtract(xmf3Shift, Vector3::ScalarProduct(m_xmf3Right, fDistance, true));
		}
		//printf("Ani?? %d  \n", m_pPlayerInfo->AnimationType());
		float fLength = sqrtf(xmf3Shift.x * xmf3Shift.x + xmf3Shift.z * xmf3Shift.z);
		float fMaxVelocityXZ = 300.f * (double)ClientNetworkInterface::getInstance().Latency();
		if (fLength > fMaxVelocityXZ)
		{
			xmf3Shift.x *= (fMaxVelocityXZ / fLength);
			xmf3Shift.z *= (fMaxVelocityXZ / fLength);
		}
		auto futurePos = Vector3::Add(m_pPlayerInfo->GetFixPosition(), xmf3Shift);
		if (!Vector3::Equal(m_xmf3Position, futurePos)) {
			XMFLOAT3 Fixpos = m_pPlayerInfo->GetFixPosition();
			m_pPlayerInfo->SetFixPosition(Vector3::Add(Fixpos, xmf3Shift));
			m_pPlayerInfo->SetPrePosition(m_xmf3Position);
			m_pPlayerInfo->SetPreTIme(GetTickCount64());
			auto nextTime = GetTickCount64() + 100 + ClientNetworkInterface::getInstance().Latency();
			m_pPlayerInfo->SetNextTIME(nextTime);
			// 시뮬레이션 중이지 않을경우 이벤트를 이동 시뮬레이션으로 변경
			if (m_pPlayerInfo->GetEventType() == EVENT_NONE) m_pPlayerInfo->SetEventType(EVENT_MOVESTART);
		}
	}
}

void CPlayer::PlayerObjectReplay(void)
{
	auto replayPos = this->m_pPlayerInfo->PacketPos();
	auto savedMove = ClientNetworkInterface::getInstance().moveList();
	auto movelist = savedMove.movelist();
	UINT64 LastPacketTime = this->m_pPlayerInfo->LastPacketTime();
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);
	for (auto iter = movelist.begin(); iter != movelist.end(); ++iter)
	{
		if (iter->MoveTimeTampt() > LastPacketTime)
		{
			xmf3Shift = XMFLOAT3(0, 0, 0);
			auto dwDirection = iter->nowDirection();
			double velocity = iter->Velocity();
			double fDistance = velocity * iter->DetaTime();
			auto look = iter->LookVector();
			auto right = iter->RightVector();
			XMFLOAT4X4 xmf4x4Rotate;

			//화살표 키 ‘↑’를 누르면 로컬 z-축 방향으로 이동(전진)한다. ‘↓’를 누르면 반대 방향으로 이동한다.
			if (dwDirection & DIR_FORWARD)
				xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(look, fDistance, true));
			if (dwDirection & DIR_BACKWARD)
				xmf3Shift = Vector3::Subtract(xmf3Shift, Vector3::ScalarProduct(look, fDistance, true));
			//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.
			if (dwDirection & DIR_RIGHT)
				xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(right, fDistance, true));
			if (dwDirection & DIR_LEFT)
				xmf3Shift = Vector3::Subtract(xmf3Shift, Vector3::ScalarProduct(right, fDistance, true));

			//auto delta = Vector3::ScalarProduct(xmf3Shift ,iter->DetaTime());
			/*(iter->MoveTimeTampt() - halfRTTTime) < ClientNetworkInterface::getInstance().Latency() ?
			iter->MoveTimeTampt() - halfRTTTime : iter->DetaTime();*/
			replayPos = Vector3::Add(replayPos, xmf3Shift);
		}
	}
	//auto delta = Vector3::ScalarProduct(xmf3Shift, latecny * 2);
	//replayPos = Vector3::Add(replayPos, delta);
	if (!Vector3::Equal(GetPosition(), replayPos)) SetPosition(replayPos);
}

void CPlayer::OthersReplay(UINT64 & latency, XMFLOAT3 & pos, XMFLOAT3 & inputKey)
{
	// 임시변수 position 현재의 위치를 가져온다.
	auto position = this->GetPosition();
	this->SetPosition(pos);
	return;

	// 현재 캐릭터의 이벤트가 없으나 위치가 전달받은 위치와 다른 경우 -> 이동이 시작 되었다.
	if (Vector3::Equal(position, pos)) return;
	if (this->m_pPlayerInfo->GetEventType() == EVENT_NONE && !Vector3::Equal(position, pos))
	{
		// 이전 시간은 현재로 맞추고
		this->m_pPlayerInfo->SetPreTIme(GetTickCount64());
		// 보간주기(IP)는 현재 시간 + 83 으로 고정
		this->m_pPlayerInfo->SetNextTIME(this->m_pPlayerInfo->GetPreTime() + 83);
		// 임시변수 nowPosition 
		auto nowPosition = this->GetPosition();
		// 이전 위치를 현재 위치로 설정해준다
		this->m_pPlayerInfo->SetPrePosition(nowPosition);
		// 시간 * 속력 인데 이것은 (1/2RTT + IP(83)) * 0.0537 이 된다.
		float RTT = static_cast<float>((latency + 83) * 0.0537);
		// 보간 + 외삽을 합친 다음 경로
		// 키 입력의 방향 * 시간 * 속력의 값을 현재 Position에
		auto NextPositon = Vector3::Add(pos, (Vector3::ScalarProduct(inputKey, RTT, false)));
		this->m_pPlayerInfo->SetNextPosition(NextPositon);
		this->m_pPlayerInfo->SetEventType(EVENT_MOVESTART);
		return;
	}
	/////////////////////////////////////////////////
	///// 외삽이 제대로 되고 있나 확인하는 곳	 ////
	///// 패킷이 알려주는 위치 정보				 ////

	// packetpos 패킷에 있는 위치
	auto packetpos = pos;
	// operationg Result = 받은 패킷을 갖고 방향(패킷이 준!!)에 맞게 외삽을 진행
	// TODO auto operatingresult = Vector3::ScalarProduct(GetVelocity(), latency);
	// 5.05 ->  auto operatingresult = Vector3::ScalarProduct(inputKey, latency); 변경
	float RTT = static_cast<float>(latency * 0.0537);
	auto operatingresult = Vector3::ScalarProduct(inputKey, RTT);
	// 패킷의 위치와 예상 거리를 더한다.
	auto tempPos = Vector3::Add(packetpos, operatingresult);
	// 외삽을 진행한 위치가 현재 위치와 같은가?
	if ((float_compare(tempPos.x, position.x) && float_compare(tempPos.z, position.z)) == false)
	{
		// 외삽법이 맞지 않는 경우! 
		// tempPos의 값을 기준으로 CurrentPos를 보간 해주어야 한다.
		// 방향을 키입력 방향으로 변경
		this->SetLookVector(inputKey);
		tempPos = Vector3::Add(tempPos, operatingresult);
		this->m_pPlayerInfo->SetFixPosition(tempPos);
		cout << "OthersReplay Func latency TIck" << latency << endl;
		this->m_pPlayerInfo->SetNextTIME(GetTickCount64() + latency);
		this->m_pPlayerInfo->SetEventType(EVENT_INTERPOLATION);
	}
}


CTerrainPlayer::CTerrainPlayer(int nMeshes) : CPlayer(nMeshes)
{
}

CTerrainPlayer::CTerrainPlayer(int nMeshes, int shaderIndex) : CPlayer(nMeshes, shaderIndex)
{
}

void CTerrainPlayer::OnPrepareRender()
{
	CPlayer::OnPrepareRender();
}

void CTerrainPlayer::ChangeCamera(ID3D11Device * pd3dDevice, DWORD nNewCameraMode, float fTimeElapsed)
{
	DWORD nCurrentCameraMode = (m_pCamera) ? m_pCamera->GetMode() : 0x00;
	if (nCurrentCameraMode == nNewCameraMode) return;
	switch (nNewCameraMode)
	{
	case FIRST_PERSON_CAMERA:
		//마찰력 설정
		SetFriction(250.0f);
		//1인칭 카메라일 때 플레이어에 y-축 방향으로 중력이 작용한다.
		SetGravity(XMFLOAT3(0.0f, -300.0f, 0.0f));
		//?
		SetMaxVelocityXZ(300.0f);
		SetMaxVelocityY(400.0f);
		m_pCamera = OnChangeCamera(pd3dDevice, FIRST_PERSON_CAMERA, nCurrentCameraMode);
		m_pCamera->SetTimeLag(0.0f);
		m_pCamera->SetOffset(XMFLOAT3(0.0f, 20.0f, 0.0f));
		m_pCamera->GenerateProjectionMatrix(1.01f, 50000.0f, ASPECT_RATIO, 60.0f);
		break;
	case SPACESHIP_CAMERA:
		SetFriction(125.0f);
		//스페이스 쉽 카메라일 때 플레이어에 중력이 작용하지 않는다.
		SetGravity(XMFLOAT3(0.0f, 0.0f, 0.0f));
		SetMaxVelocityXZ(300.0f);
		SetMaxVelocityY(400.0f);
		m_pCamera = OnChangeCamera(pd3dDevice, SPACESHIP_CAMERA, nCurrentCameraMode);
		m_pCamera->SetTimeLag(0.0f);
		m_pCamera->SetOffset(XMFLOAT3(0.0f, 0.0f, 0.0f));
		m_pCamera->GenerateProjectionMatrix(1.01f, 50000.0f, ASPECT_RATIO, 60.0f);
		break;
	case THIRD_PERSON_CAMERA:
		SetFriction(250.0f);
		//3인칭 카메라일 때 플레이어에 y-축 방향으로 중력이 작용한다.
		SetGravity(XMFLOAT3(0.0f, 0.f, 0.0f));
		SetMaxVelocityXZ(53.7);
		SetMaxVelocityY(400.0f);
		m_pCamera = OnChangeCamera(pd3dDevice, THIRD_PERSON_CAMERA, nCurrentCameraMode);
		m_pCamera->SetTimeLag(0.25f);
		m_pCamera->SetOffset(XMFLOAT3(0.0f, 20.0f, -50.0f));
		m_pCamera->GenerateProjectionMatrix(1.01f, 5000.0f, ASPECT_RATIO, 60.0f);
		break;
	default:
		break;
	}
	//if (PlayerManager.m_vtPlayers.size() > 0 )
		vector<CShader*> tmp;
		Update(fTimeElapsed, MYPLAYER, tmp);
		Update(fTimeElapsed, SECONDPLAYER, tmp);
}

void CTerrainPlayer::OnPlayerUpdated(float fTimeElapsed, float fRegulateHeight)
{
	CHeightMapTerrain *pTerrain = (CHeightMapTerrain *)m_pPlayerUpdatedContext;
	XMFLOAT3 xmf3Scale = pTerrain->GetScale();
	XMFLOAT3 xmf3PlayerPosition = GetPosition();
	int z = (int)(xmf3PlayerPosition.z / xmf3Scale.z);
	bool bReverseQuad = ((z % 2) != 0);
	/*높이 맵에서 플레이어의 현재 위치 (x, z)의 y 값을 구한다. 그리고 플레이어 메쉬의 높이가 12이고
	플레이어의 중심이 직육면체의 가운데이므로 y 값에 메쉬의 높이의 절반을 더하면 플레이어의 위치가 된다.*/
	float fHeight = pTerrain->GetHeight(xmf3PlayerPosition.x, xmf3PlayerPosition.z, bReverseQuad) + fRegulateHeight;
	/*플레이어의 속도 벡터의 y-값이 음수이면(예를 들어, 중력이 적용되는 경우)
	플레이어의 위치 벡터의 y-값이 점점 작아지게 된다.
	이때 플레이어의 현재 위치의 y 값이 지형의 높이(실제로 지형의 높이 + 6)보다 작으면
	플레이어가 땅속에 있게 되므로 플레이어의 속도 벡터의 y 값을 0으로 만들고
	플레이어의 위치 벡터의 y-값을 지형의 높이로 설정한다. 그러면 플레이어는 지형 위에 있게 된다.*/
	if (xmf3PlayerPosition.y < fHeight)
	{
		XMFLOAT3 xmf3PlayerVelocity = GetVelocity();
		xmf3PlayerVelocity.y = 0.0f;
		SetVelocity(xmf3PlayerVelocity);
		xmf3PlayerPosition.y = fHeight;
		SetPosition(xmf3PlayerPosition);
	}
	else if (xmf3PlayerPosition.y > fHeight)
	{
		xmf3PlayerPosition.y = fHeight;
		SetPosition(xmf3PlayerPosition);
	}
}

void CTerrainPlayer::OnCameraUpdated(float fTimeElapsed)
{
	CHeightMapTerrain *pTerrain = (CHeightMapTerrain *)m_pCameraUpdatedContext;
	XMFLOAT3 xmf3Scale = pTerrain->GetScale();
	CCamera *pCamera = GetCamera();
	XMFLOAT3 d3dxvCameraPosition = pCamera->GetPosition();
	int z = (int)(d3dxvCameraPosition.z / xmf3Scale.z);
	bool bReverseQuad = ((z % 2) != 0);
	/*높이 맵에서 카메라의 현재 위치 (x, z)의 높이(y 값)를 구한다
	. 이 값이 카메라의 위치에 해당하는 지형의 높이 보다 작으면 카메라가 땅속에 있게 된다.
	이렇게 되면 <그림 4>의 왼쪽과 같이 지형이 그려지지 않는 경우가 발생한다(카메라가 지형 안에 있으므로 와인딩 순서가 바뀐다).
	이러한 경우가 발생하지 않도록 카메라의 위치의 최소값은 (지형의 높이 + 5)로 설정한다.
	카메라의 위치의 최소값은 지형의 모든 위치에서 카메라가 지형 아래에 위치하지 않도록 설정한다.*/
	float fHeight = pTerrain->GetHeight(d3dxvCameraPosition.x, d3dxvCameraPosition.z, bReverseQuad) + 5.0f;
	if (d3dxvCameraPosition.y < fHeight)
	{
		d3dxvCameraPosition.y = fHeight;
		pCamera->SetPosition(d3dxvCameraPosition);
		//3인칭 카메라의 경우 카메라의 y-위치가 변경되었으므로 카메라가 플레이어를 바라보도록 한다.
		if (pCamera->GetMode() == THIRD_PERSON_CAMERA)
		{
			CThirdPersonCamera *p3rdPersonCamera = (CThirdPersonCamera *)pCamera;
			p3rdPersonCamera->SetLookAt(GetPosition());
		}
	}
}