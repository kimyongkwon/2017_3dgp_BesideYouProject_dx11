#include"stdafx.h"
#include"GameObject.h"

CGameObject::CGameObject() : _pos(0.0f, 0.0f, 0.0f)
{
	_name.assign(L"None Name");
	_vUp = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
}

CGameObject::CGameObject(wstring name, D3DXVECTOR3 pos)
{
	_name.assign(name.begin(), name.end());
	_pos = pos;
	_vUp = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
}

CGameObject::CGameObject(const CGameObject & o)
{
	_name.assign(o._name);
	_pos = o._pos;
	_vUp = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
}

const D3DXVECTOR3 & CGameObject::GetPos()
{
	return _pos;
}

BOOL CGameObject::SetPos(D3DXVECTOR3 & v)
{
	_pos = v;
	return true;
}
// Update
// 업데이트에는 오므젝트의 변동 사항을 업데이트 한다.
// 각도에 따른 변화
// 이동 연산
// 상태 변화
void CPlayer::MoveSulCulate()
{
	_MoveTime.second += GetTickCount64() - _MoveTime.first;
	_MoveTime.first = GetTickCount64();
	D3DXVECTOR3 v = { 0.0f, 0.0f, 0.0f };
	D3DXMATRIX iter;
	D3DXMatrixIdentity(&iter);
	D3DXVec3TransformCoord(&v, &_Scale, &_Rotate);

	float swap = v.z;
	v.z = v.x;
	v.x = swap;

	v.x *= _MoveTime.second;
	v.z *= _MoveTime.second;

	_pos += v;
	// FIXIT : 버그 발생 확률 있음.
	_MoveTime.second = 0;
	return;
	//_MoveTime.second += GetTickCount64() - _MoveTime.first;
	//_MoveTime.first = GetTickCount64();
	//Vector2D v;
	//U2DMETRIX position;
	//position.SetScale(_pos.GetVec_x(), _pos.GetVec_y());
	//_Metrixptr.reset(_Scale * _Rotate);
	//_Metrixptr->TimeMultiple(_MoveTime.second);
	//_Metrixptr.reset(*_Metrixptr * position);
	//_pos = *(v * *_Metrixptr);
	//// FIXIT : 버그 발생 확률 있음.
	//_MoveTime.second = 0;
}
void CPlayer::MoveOn()
{
	if (!_move) {
		_move = true;
		_MoveTime.first = GetTickCount64();
		_MoveTime.second = 0;
		return;
	}
}
void CPlayer::MoveOff()
{
	_move = false;
}
INT32 CPlayer::trapCount()
{
	return _trapCount;
}
void CPlayer::userTrap()
{
	if (_trapCount < 1) return;
	--_trapCount;
}
void CPlayer::setTrapCount(INT32 trap)
{
	_trapCount = trap;
}
void CPlayer::sethp(INT32 hp)
{
	_hp = hp;
}
void CPlayer::hited()
{
	if (_hp > 0) --_hp;
}
INT32 CPlayer::hp()
{
	return _hp;
}
FLOAT CPlayer::degree()
{
	return _degree;
}
void CPlayer::setDegree(FLOAT degree)
{
	_degree = degree;
}
void CPlayer::setstatusbar(STATE_BAR state)
{
	_statusbar = state;
}
STATE_BAR CPlayer::statusbar()
{
	return _statusbar;
}
void CPlayer::Render(CDisplay & display, Gdiplus::Matrix * mat)
{
}
void CPlayer::otherRender(CDisplay & display, double x, double y)
{
	
}
void CPlayer::setSCgap(UINT64 gap, bool Sign)
{
	SCgap = gap;
	SCgapSign = Sign;
}
INT64 CPlayer::CSgap()
{
	if (SCgapSign) return SCgap;
	return -(INT64)SCgap;
}
bool CPlayer::CSgapsign()
{
	return SCgapSign;
}
Gdiplus::Matrix * CPlayer::Update(CDisplay & display)
{
	
}

ObjectManager::ObjectManager() : _ownPlayer(nullptr)
{
	_playerList.clear();
	_objectList.clear();
}

ObjectManager::~ObjectManager()
{
	for (CPlayer * obj : _playerList) {
		SAFE_DELETE(obj);
	}
	_playerList.clear();
	for (CGameObject * obj : _objectList) {
		SAFE_DELETE(obj);
	}
	_objectList.clear();
	if(_ownPlayer != nullptr)
		SAFE_DELETE(_ownPlayer);
	
}

bool ObjectManager::addOwnplayer(CPlayer * _own)
{
	if (_own == nullptr || _ownPlayer != nullptr) {
		return false;
	}
	_ownPlayer = _own;
	return true;
}

bool ObjectManager::addPlayer(CPlayer * player)
{
	/*auto iter = std::find(_playerList.begin(), _playerList.end(), player);
	if (iter == _playerList.end()) return false;
	_playerList.push_back(player);
	return true;*/
	DWORD uid =  player->uid();
	auto iter = std::find_if(_playerList.begin(), _playerList.end(), [uid](CPlayer * m)-> bool {return m->uid() == uid; });
	if (iter != _playerList.end())
		return false;
	_playerList.push_back(player);
	return true;
}

bool ObjectManager::removePlayer(DWORD oid)
{
	auto iter = std::find_if(_playerList.begin(), _playerList.end(), [oid](CPlayer * m)-> bool {return m->uid() == oid; });
	if (iter == _playerList.end()) return false;
	_playerList.erase(iter);
	return true;
}

size_t ObjectManager::Pcount()
{
	return _playerList.size();
}

CPlayer * ObjectManager::at_own()
{
	if (_ownPlayer == nullptr)
		return nullptr;
	return _ownPlayer;
}

CPlayer * ObjectManager::at_Player(DWORD uid)
{
	auto iter = std::find_if(_playerList.begin(), _playerList.end(), [uid](CPlayer * p)-> bool {return p->uid() == uid; });
	if (iter == _playerList.end()) return nullptr;
	return *iter;
}

CPlayer * ObjectManager::at_Object(DWORD oid)
{
	return nullptr;
}

vector<CPlayer*> * ObjectManager::Playerinfo()
{
	if (_playerList.size() == 0) return nullptr;
	std::vector<CPlayer *> * info = &_playerList;
	return info;
}

void ObjectManager::ownRoomEnter(INT64 roomNumber)
{
	if (_ownPlayer == nullptr) return;
	_ownPlayer->EnterRoom(roomNumber);
}

void ObjectManager::ownRoomLeave()
{
	if (_ownPlayer == nullptr) return;
	_ownPlayer->LeaveRoom();
}