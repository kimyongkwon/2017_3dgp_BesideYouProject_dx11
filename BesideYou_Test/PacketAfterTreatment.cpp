#include "stdafx.h"
#include "RBPlayerObject.h"
#include "PacketAfterTreatment.h"

extern ClientNetwork Network;
extern HWND Ghwnd;

void PacketAfterTreatment::initalize()
{
	_runFuncTable.insert(make_pair(PE_S_ANS_JOINSUCC, &PacketAfterTreatment::SPacket_JOINSUCC));
	_runFuncTable.insert(make_pair(PE_S_ANS_JOINFAIL, &PacketAfterTreatment::SPacket_JOINFAIL));

	_runFuncTable.insert(make_pair(PE_S_ANS_ENTERROOMSUCC, &PacketAfterTreatment::SPacket_ENTERROOMSUCC));
	_runFuncTable.insert(make_pair(PE_S_ANS_ENTERROOMFAIL, &PacketAfterTreatment::SPacket_ENTERROOMFAIL));

	_runFuncTable.insert(make_pair(PE_S_ANS_OUTROOMSUCC, &PacketAfterTreatment::SPacket_OUTROOMSECC));

	_runFuncTable.insert(make_pair(PE_S_ANS_LOGINSUCC, &PacketAfterTreatment::SPacket_LOGINSUCC));
	_runFuncTable.insert(make_pair(PE_S_ANS_LOGINFAIL, &PacketAfterTreatment::SPacket_LOGINFAIL));

	_runFuncTable.insert(make_pair(PE_S_ANS_SELECTPARTSUCC, &PacketAfterTreatment::SPacket_SELECTPARTSUCC));
	_runFuncTable.insert(make_pair(PE_S_NTF_ROOMSEARCHNOTROLE, &PacketAfterTreatment::SPacket_ROOMSEARCHNOTROLE));
	_runFuncTable.insert(make_pair(PE_S_BRD_ROOMSTATE, &PacketAfterTreatment::SPacket_ROOMSTATE));

	_runFuncTable.insert(make_pair(PE_S_BRD_GAMESTARTSUCC, &PacketAfterTreatment::SPacket_GAMESTARTSUCC));
	_runFuncTable.insert(make_pair(PE_C_REQ_FIRSTSIGNAL, &PacketAfterTreatment::NPacket_FIRSTSIGNAL)); // 내부 패킷 전송용 함수
	_runFuncTable.insert(make_pair(PE_S_NTY_FIRSTUSERDATA, &PacketAfterTreatment::SPacket_FIRSTUSERDATA)); // 내부 패킷 전송용 함수

	_runFuncTable.insert(make_pair(PE_S_REQ_LATENCYCHECK, &PacketAfterTreatment::SPacket_LATENCTCHECK));
	_runFuncTable.insert(make_pair(PE_S_NTY_LATENCYCHECK, &PacketAfterTreatment::SPacket_LATENCY));
	_runFuncTable.insert(make_pair(PE_S_NTY_PLAYERPOS, &PacketAfterTreatment::SPacket_PLAYERSPOS));
	_runFuncTable.insert(make_pair(PE_S_ANS_HELLO, &PacketAfterTreatment::SPacket_HELLO));
	//update
	_runFuncTable.insert(make_pair(PE_S_NTY_GOTOINGAME, &PacketAfterTreatment::SPacket_GOTOINGAME));
	_runFuncTable.insert(make_pair(PE_S_NTY_LASTMOVETIMESTAMP, &PacketAfterTreatment::SPacket_LASTMOVETIMESTAMP));

	_runFuncTable.insert(make_pair(PE_S_ANS_WINSTATE, &PacketAfterTreatment::SPacket_WIN));
	_runFuncTable.insert(make_pair(PE_S_ANS_DEFEATSTATE, &PacketAfterTreatment::SPacket_DEFEAT));
}

PacketAfterTreatment::PacketAfterTreatment()
{
	this->initalize();
}

PacketAfterTreatment::~PacketAfterTreatment()
{
	_runFuncTable.clear();
}

void PacketAfterTreatment::transport(Packet * rowPacket)
{
	PacketType type = rowPacket->type();
	Runfunc runFunction = _runFuncTable.at(type);
	if (runFunction == nullptr) {
		MessageBox(NULL, L"NOTIFY", L"수샇한 패킷이 도착", NULL);
		return;
	}
	runFunction(rowPacket);
	delete rowPacket;
}

/////////////////////////// After Treatment Function ///////////////////////////

// 룸 상태 변화 작업 완료
void PacketAfterTreatment::SPacket_ROOMSTATE(Packet * rowPacket)
{
	PK_S_BRD_ROOMSTATE * packet = (PK_S_BRD_ROOMSTATE *)rowPacket;
	int _update  = packet->update;
	INT64 _uid = packet->uid;
	INT64 roomNumber = packet->roomNumber;
	switch (_update)
	{
		// 타 유저의 방 나가기
	case 2:
		//if (!ObjectManager::getInstance().removePlayer(_uid)) {
		if(RBPlayerManager::getInstance().removePlayer(_uid)){
			MessageBox(NULL, L"(ROOMSTATE_ERROR)서버 개발자에게 통보해주세요", L"특급 에러 발생", NULL);
		}
		break;
		// 준비 상태 on
	case 3: 
		{
			//if (_uid == ObjectManager::getInstance().at_own()->uid()) {
			if(_uid == RBPlayerManager::getInstance().at_own()->uid()){
				RBPlayerManager::getInstance().at_own()->ready();
				break;
			}
			auto user = RBPlayerManager::getInstance().at_Player(_uid);
			user->ready();
		}
		break;
		// 준비 상태 off
	case 4:
		{
			if (_uid == RBPlayerManager::getInstance().at_own()->uid()) {
				RBPlayerManager::getInstance().at_own()->unready();
				break;
			}
			auto user = RBPlayerManager::getInstance().at_Player(_uid);
			user->unready();
		}
		break;
		// 살인자 입장 시
	case 10:
	{
		/*ObjectManager::getInstance().addPlayer(new CPlayer(packet->id.c_str(),
			Vector2D(0.0f, 0.0f), OTHER_PLAYER, _uid, KILLER, roomNumber));*/
		auto other = new RBPlayerObject();
		other->setname(packet->id);
		other->setuid(packet->uid);
		other->setrole(KILLER);
		other->setroomnumber(roomNumber);
		other->setidenti(OTHERPLAYER);
		RBPlayerManager::getInstance().addPlayer(other);
	}
		break;
		// 생존자 입장 시
	case 11:
	{
		auto other = new RBPlayerObject();
		other->setname(packet->id);
		other->setuid(packet->uid);
		other->setrole(SURVIVOR);
		other->setroomnumber(roomNumber);
		other->setidenti(OTHERPLAYER);
		RBPlayerManager::getInstance().addPlayer(other);
		//ObjectManager::getInstance().addPlayer(new CPlayer(packet->id.c_str(),			// 처음 등록 되어질 유저의 경우 새로 Cplayer를 만들어 넣어준다.
		//	Vector2D(0.0f, 0.0f), OTHER_PLAYER, _uid, SURVIVOR, roomNumber));
	}
		break;
		// Error page......
	default:
		MessageBox(NULL, L"예외 처리 발생", L"경고", NULL);
		break;
	}
#if TESTCLIENT
	WPARAM _wparam = MAKEWPARAM(ROOMUPDATE, 0);
	SendMessage(roomhandle_, WM_COMMAND, _wparam, NULL);
#endif
}

// 역할 미설정 방서치 실패
void PacketAfterTreatment::SPacket_ROOMSEARCHNOTROLE(Packet * rowPacket)
{
	PK_S_NTF_ROOMSEARCHNOTROLE * packet = (PK_S_NTF_ROOMSEARCHNOTROLE *)rowPacket;
	MessageBox(NULL, L"역활을 지정후 방 입장이 가능 합니다.", L"방 입장 실패", NULL);
}
// 역할 변경 성공 완료
void PacketAfterTreatment::SPacket_SELECTPARTSUCC(Packet * rowPacket)
{
	PK_S_ANS_SELECTPARTSUCC * packet = (PK_S_ANS_SELECTPARTSUCC *)rowPacket;
	RBPlayerManager::getInstance().at_own()->setrole((ROLE)packet->role);
	GameFlowManager::getInstance().Called();
}
// 가입 성공 시  작업완료
void PacketAfterTreatment::SPacket_JOINSUCC(Packet * rowPacket)
{
	PK_S_ANS_JOINSUCC * packet = (PK_S_ANS_JOINSUCC *)rowPacket;
	MessageBox(NULL, L"회원 가입 성공", L"알림", NULL);
}
// 가입 실패 시// 작업완료
void PacketAfterTreatment::SPacket_JOINFAIL(Packet * rowPacket)
{
	PK_S_ANS_JOINFAIL * packet = (PK_S_ANS_JOINFAIL *)rowPacket;
	RBPlayerManager::getInstance().at_own()->setname(L"");
	MessageBox(NULL, L"사용중인 아이디 입니다", L"가입 실패", NULL);
}
// 로그인 성공 시!  작업완료
void PacketAfterTreatment::SPacket_LOGINSUCC(Packet * rowPacket)
{
	// 씬에 캐릭터가 붙어있는게 문제............................................
	PK_S_ANS_LOGINSUCC * packet = (PK_S_ANS_LOGINSUCC *)rowPacket;
	// ObjectManager::getInstance().at_own()->setuid(packet->uid);
	RBPlayerManager::getInstance().at_own()->setuid(packet->uid);
	RBPlayerManager::getInstance().at_own()->setidenti(OWNPLAYER);
	GameFlowManager::getInstance().ENTER(FLOW_ROBBY);
#if 0
	HWND loginHandle_ = FindWindow(L"#32770", L"로그인창");
	WPARAM _wparam = MAKEWPARAM(IDCANCEL,0);
	SendMessage(loginHandle_, WM_COMMAND, _wparam, NULL);
#endif
	
}
// 로그인 실패 시 작업완료
void PacketAfterTreatment::SPacket_LOGINFAIL(Packet * rowPacket)
{
	RBPlayerManager::getInstance().at_own()->setname(L"name");
	PK_S_ANS_LOGINFAIL * packet = (PK_S_ANS_LOGINFAIL *)rowPacket;
	if (packet->errcode == 1) {
		MessageBox(NULL, L"아이디 오류 입니다.", L"로그인 실패", NULL);
		return;
	}
	if(packet->errcode == 2)
		MessageBox(NULL, L"비밀번호 오류입니다.", L"로그인 실패", NULL);
}
// 방 나가기 성공 작업완료
void PacketAfterTreatment::SPacket_OUTROOMSECC(Packet * rowPacket)
{
	PK_S_ANS_OUTROOMSUCC * packet = (PK_S_ANS_OUTROOMSUCC *)rowPacket;
	if (RBPlayerManager::getInstance().at_own()->uid() != packet->uid) {
		MessageBox(NULL, L"잘못 도착한 패킷 정보", L"알림", NULL);
	}
	RBPlayerManager::getInstance().ownRoomLeave();
	GameFlowManager::getInstance().ENTER(FLOW_ROBBY);
}

// 방 입장 성공 작업 완료
void PacketAfterTreatment::SPacket_ENTERROOMSUCC(Packet * rowPacket)
{
	PK_S_ANS_ENTERROOMSUCC * packet = (PK_S_ANS_ENTERROOMSUCC *)rowPacket;
	INT64 roomNumber = packet->roomNumber;
	BYTE count_ = packet->id.size();															// 방정보 (방에 있는 유저들)
	RBPlayerObject * Oplayer = nullptr;															// 유저 접근 포인터
	for (auto index = 0; index < count_; ++index) {											// 한명도 입장하지 않는다면 해당 for문은 돌지 않을 것 이다.
		// TODO : FIX TO Cycle....
		//Oplayer = RBPlayerManager::getInstance().at_Player(packet->uid[index]);				// 방정보 유저 접근
		/*if (Oplayer == nullptr) {*/
			if (RBPlayerManager::getInstance().at_own()->uid() == packet->uid[index]) {		// at_Player안에 없으나 그게 자기 자신일 경우
				RBPlayerManager::getInstance().ownRoomEnter(packet->roomNumber);		// own의 방입장에 대한 번호 변경
				continue;																	
			}
			RBPlayerObject * other = new RBPlayerObject();
			// 타인의 플레이어 생성
			other->setuid(packet->uid[index]);
			other->setrole((ROLE)packet->role[index]);
			other->setname(packet->id[index]);
			other->setidenti(OTHERPLAYER);
			other->setroomnumber(packet->roomNumber);
			// 플레이어 넣기.
			RBPlayerManager::getInstance().addPlayer(other);
			continue;
		//}
		//if (nullptr != Oplayer) {															// 이미 등록 되어져 있는 유저 일경우 역할 정보만 renew 해준다
		//	Oplayer->setRole((ROLE)packet->role[index]);
		//	continue;
		//}
	}
	GameFlowManager::getInstance().ENTER(FLOW_ROOM);
	SendMessage(Ghwnd, WM_LBUTTONDOWN, NULL, NULL);
#if TESTCLIENT
	HWND robbyHandle_ = FindWindow(L"#32770", L"Robby");
	WPARAM _wparam = MAKEWPARAM(IDCANCEL, 0);
	SendMessage(robbyHandle_, WM_COMMAND, _wparam, NULL);
#endif
	
}
// 방 입장 실패
void PacketAfterTreatment::SPacket_ENTERROOMFAIL(Packet * rowPacket)
{
	PK_S_ANS_ENTERROOMFAIL * packet = (PK_S_ANS_ENTERROOMFAIL *)rowPacket;
	MessageBox(NULL, L"방 입장에 실패하였습니다. 다시 시도해 주세요", L"입장 실패", NULL);
}

void PacketAfterTreatment::SPacket_GAMESTARTSUCC(Packet * rowPacket)
{
	PK_S_BRD_GAMESTARTSUCC *packet = (PK_S_BRD_GAMESTARTSUCC *)rowPacket;
	char ip[16] = {};
	strcpy_s(ip, packet->IP.c_str());

	ClientSession * RoomSession = new ClientSession();
	Network.SetupWSAAsyncSelect(Ghwnd, RoomSession, ip, packet->PORT);
}

void PacketAfterTreatment::NPacket_FIRSTSIGNAL(Packet * rowPacket)
{
	PK_C_REQ_FIRSTSIGNAL packet;
	packet.roomNumber = RBPlayerManager::getInstance().at_own()->roomnumber();
	packet.uid = RBPlayerManager::getInstance().at_own()->uid();
	packet.ClientTime = GetTickCount64();
	Network._Roomsession->sendPacket(&packet);
}

// update
void PacketAfterTreatment::SPacket_FIRSTUSERDATA(Packet * rowPacket)
{
	UINT64 ClientTick = GetTickCount64();		//시간측정
	PK_S_NTY_FIRSTUSERDATA * packet = (PK_S_NTY_FIRSTUSERDATA*)rowPacket;

	PK_C_NTY_LATENCYCHECK Cpacket;
	Cpacket.STime = packet->STime;
	Network._Roomsession->sendPacket(&Cpacket);
	Sleep(100);
	auto uid = RBPlayerManager::getInstance().at_own()->uid();
	auto roomnumber = RBPlayerManager::getInstance().at_own()->roomnumber();
	for (int i = 0; i < packet->generatorNumber_.size(); ++i)
	{
		CPlayerManager::getInstance().tempGenList[i]->SetNumber(i);
		CPlayerManager::getInstance().m_mpGenlist.insert(pair<int, CGenerator *>(packet->generatorNumber_[i], CPlayerManager::getInstance().tempGenList[i]));
	}
	//CPlayerManager::getInstance().tempGenList.clear();
	for (auto iter = 0; iter < packet->uid.size(); ++iter)
	{
		if (packet->role[iter] == NONE) continue;
		if (packet->uid[iter] == uid)
		{
			FactoryPlayer::getInstance().BuildObject((ROLE)packet->role[iter]);
			auto players = CPlayerManager::getInstance().players();
			size_t cnt = (*players).size();
			(*players).front()->m_pPlayerInfo->SetOwn(true);
			if (packet->role[iter] != KILLER) {
				(*players).front()->m_pPlayerInfo->SetHP(packet->hp[iter]);
				(*players).front()->m_pPlayerInfo->SetStatusUI(static_cast<STATUS_UI>(packet->statusbar[iter]));
			}
			(*players).front()->m_pPlayerInfo->SetUserNAME(packet->id[iter]);
			(*players).front()->m_pPlayerInfo->SetUserID(packet->uid[iter]);
			(*players).front()->m_pPlayerInfo->SetRoomNUM(roomnumber);
			XMFLOAT3 position = { packet->px[iter], packet->py[iter], packet->pz[iter] };
			(*players).front()->SetPosition(position);
			(*players).front()->m_pPlayerInfo->SetTrap(packet->trapcount);
			(*players).front()->SetLookVector(XMFLOAT3(packet->degree[(iter * 3)], packet->degree[(iter * 3) + 1], packet->degree[(iter * 3) + 2]));
			(*players).front()->m_pPlayerInfo->SetROLE(static_cast<ROLE>(packet->role[iter]));
			//ObjectManager::getInstance().addPlayer(ObjectManager::getInstance().at_own());
			if (packet->STime  > ClientTick)
			{
				(*players).front()->m_pPlayerInfo->setSCgap(packet->STime - ClientTick, true);
			}
			else
			{
				(*players).front()->m_pPlayerInfo->setSCgap(ClientTick - packet->STime, false);
			}
			continue;
		}
		FactoryPlayer::getInstance().BuildObject((ROLE)packet->role[iter], 1);
		auto players = CPlayerManager::getInstance().players();
		size_t cnt = (*players).size();
		(*players).back()->SetPosition(XMFLOAT3(packet->px[iter], packet->py[iter], packet->pz[iter]));
		(*players).back()->m_pPlayerInfo->SetOwn(false);
		if (packet->role[iter] != KILLER) {
			(*players).back()->m_pPlayerInfo->SetHP(packet->hp[iter]);
			(*players).back()->m_pPlayerInfo->SetStatusUI(static_cast<STATUS_UI>(packet->statusbar[iter]));
		}
		(*players).back()->m_pPlayerInfo->SetUserNAME(packet->id[iter]);
		(*players).back()->m_pPlayerInfo->SetUserID(packet->uid[iter]);
		(*players).back()->m_pPlayerInfo->SetRoomNUM(roomnumber);
		(*players).back()->m_pPlayerInfo->SetTrap(0);
		(*players).back()->SetLookVector(XMFLOAT3(packet->degree[(iter * 3)], packet->degree[(iter * 3) + 1], packet->degree[(iter * 3) + 2]));
		(*players).back()->m_pPlayerInfo->SetROLE(static_cast<ROLE>(packet->role[iter]));
		
	}
	PK_C_NTY_BUILDOBJECTSUCC Bpacket;
	Bpacket.uid = uid;
	Bpacket.roomNumber = roomnumber;
	Network._Roomsession->sendPacket(&Bpacket);
	//GameFlowManager::getInstance().ENTER(FLOW_INGAME);
}

//
//void PacketAfterTreatment::SPacket_OTHERMOVEDATA(Packet * rowPacket)
//{
//	PK_S_ANS_USERMOVE * packet = (PK_S_ANS_USERMOVE *)rowPacket;
//	auto player = ObjectManager::getInstance().at_Player(packet->uid);
//	if (player == nullptr) return;
//		player->SetPos(D3DXVECTOR3(packet->pos[0], packet->pos[1], packet->pos[2]));
//		player->setDegree(packet->degree);
//	//printf("uid  : [%d] | pos x [%f] | pos y [%f] |  pos z[%f]\n", packet->uid, packet->pos[0], packet->pos[1], packet->pos[2]);
//}

void PacketAfterTreatment::SPacket_LATENCTCHECK(Packet * rowPacket)
{
	UINT64 ClientTick = GetTickCount64();
	PK_S_REQ_LATENCYCHECK * packet = (PK_S_REQ_LATENCYCHECK *)rowPacket;
	PK_C_NTY_LATENCYCHECK Cpacket;
	Cpacket.STime = packet->STime;
	Network._Roomsession->sendPacket(&Cpacket);

	// 서버의 시간을 다시 되돌려 준뒤, 서버와 클라이언트의 시간차(시간 동기화)를 업로드 해준다
	//if (packet->STime > ClientTick)
	//{
	//	ObjectManager::getInstance().at_own()->setSCgap(packet->STime - ClientTick, true);
	//}
	//else
	//{
	//	ObjectManager::getInstance().at_own()->setSCgap(ClientTick - packet->STime, false);
	//}
}

void PacketAfterTreatment::SPacket_LATENCY(Packet * rowPacket)
{
	PK_S_NTY_LATENCYCHECK * packet = (PK_S_NTY_LATENCYCHECK*)rowPacket;
	ClientNetworkInterface::getInstance().SetLatencyTime(packet->STime);
	cout <<"PK_latency TIme : " <<packet->STime << endl;
}

void PacketAfterTreatment::SPacket_PLAYERSPOS(Packet * rowPacket)
{
	auto NowTIme = GetTickCount64();   // 현재 패킷이 온 시간
	UINT64 Latency = ClientNetworkInterface::getInstance().Latency();
	INT64 halfRTTTime = NowTIme - Latency;
	// NowTime은 1/2RTT의 시간의 시간이 될 것. 이라고 추정
	// 이 NowTime의 기준으로 TimeTampt안을 뒤져서 되새김을 한다. 되새김 함수 필요( void OjbectReply())
	// 리플레이가 끝난 데이터를 currnetPos로 해버림.
	PK_S_NTY_PLAYERPOS * packet = (PK_S_NTY_PLAYERPOS *)rowPacket;
	int index = packet->uid.size();
	auto playerlist = CPlayerManager::getInstance().players();
	auto me = (*playerlist).front();
	if (me == nullptr) return;
	for (int i = 0; i < 10; ++i)
	{
		auto iter = CPlayerManager::getInstance().m_mpGenlist.find(packet->GeneratorNumber[i]);
		iter->second->Read(packet->GeneratorP[i]);

	}
	auto TupdateCount = packet->Tupdate.size();
	for (int index = 0; index != TupdateCount; ++index)
	{
		uint8_t update = packet->Tupdate.front();
		packet->Tupdate.pop();
		if (update & install)
		{
			XMFLOAT3 position{ packet->trapx.front() , 0.f, packet->trapy.front() };
			uint64_t TrapNumber = packet->trapId.front();
			uint64_t CatchPlayerId = packet->catchId.front();
			packet->trapx.pop();
			packet->trapy.pop();
			packet->trapId.pop();
			packet->catchId.pop();
			TrapJob job;
			job.inData(install, TrapNumber, CatchPlayerId, position);
			CPlayerManager::getInstance().mJob.push(job);
		}
		if(update & invoked)
		{
			uint64_t TrapNumber  = packet->trapId.front();
			uint64_t CatchPlayerId = packet->catchId.front();
			packet->trapId.pop();
			packet->catchId.pop();
			TrapJob job;
			job.inData(invoked, TrapNumber, CatchPlayerId);
			CPlayerManager::getInstance().mJob.push(job);
		}
		if (update & toDelete)
		{

		}
	}
	for (int iter = 0; iter < index; ++iter)
	{
		if (packet->uid[iter] != me->m_pPlayerInfo->GetUserID()) continue;
		UINT64 LastRecvTime = (GetTickCount64() - packet->LastRecvTime[iter]) / 2;
		ClientNetworkInterface::getInstance().SetLatencyTime(LastRecvTime);
		break;
	}
	for (int iter = 0; iter < index; ++iter)
	{
		UINT64 lastpacketTime = packet->LastUpdateTime[iter];


		XMFLOAT3 lookvect{ packet->LastLookvect[iter * 3], packet->LastLookvect[(iter * 3) + 1] , packet->LastLookvect[(iter * 3) + 2] };
		XMFLOAT3 rightvect{ packet->LastRightvect[iter * 3], packet->LastRightvect[(iter * 3) + 1] , packet->LastRightvect[(iter * 3) + 2] };
		XMFLOAT3 pos{ packet->px[iter], packet->py[iter], packet->pz[iter] };
		INT32 direction = 0;
		uint8_t playerUpdateData = 0;
		if(!packet->LastDirection.empty())
			direction = packet->LastDirection[iter];
		// 플레이어 업데이트 정보
		if(!packet->Playerupdate.empty())
			playerUpdateData = packet->Playerupdate[iter];
		
		// 나의 정보 일 경우
		if (packet->uid[iter] == me->m_pPlayerInfo->GetUserID())
		{
			if (me->m_pPlayerInfo->IsLastPacketTimeCheck(lastpacketTime)) {
				/*if (playerUpdateData & PUPDATE::UpdateHP)
				{
					me->m_pPlayerInfo->SetHP(packet->hp.front());
					packet->hp.pop();
				}
				if (playerUpdateData & PUPDATE::UpdateTrap)
				{
					me->m_pPlayerInfo->SetTrap(packet->trapCount.front());
					packet->trapCount.pop();
				}
				if (playerUpdateData & PUPDATE::UpdateStatus)
				{
					me->m_pPlayerInfo->SetStatusUI(static_cast<STATUS_UI>(packet->statusUI.front()));
					packet->statusUI.pop();
				}*/
				me->m_pPlayerInfo->Read(packet, playerUpdateData);
				
				me->m_pPlayerInfo->SetReplayflag(true, lastpacketTime, pos);
				
				if ((ANIMATION_TYPE)packet->AnimationType[iter] == TYPE_HITTED)
				{
					me->m_pPlayerInfo->SetAnimationType((ANIMATION_TYPE)packet->AnimationType[iter], true);

				}
				// me->PlayerObjectReplay(tampt, lastpacketTime, pos, keyvector, Latency);
				ClientNetworkInterface::getInstance().MoveListremove(lastpacketTime);
			}
		}
		// 상대방의 정보
		else
		{
			for (auto i = (*playerlist).begin(); i != (*playerlist).end(); ++i) {
				if (me->m_pPlayerInfo->GetUserID() == (*i)->m_pPlayerInfo->GetUserID())continue;
				if ((*i)->m_pPlayerInfo->GetUserID() == packet->uid[iter]) {
					(*i)->m_pPlayerInfo->Read(packet, playerUpdateData);

					(*i)->SetLookVector(lookvect);
					(*i)->SetRightVector(rightvect);
					(*i)->m_pPlayerInfo->SetFixPosition(pos);
					(*i)->PlayerPYRUpdate(direction, (ANIMATION_TYPE)packet->AnimationType[iter]);
				}
			}
		}
	}
}

void PacketAfterTreatment::SPacket_HELLO(Packet * rowPacket)
{
	PK_S_ANS_HELLO * packet = (PK_S_ANS_HELLO*)rowPacket;
	auto rtt = GetTickCount64() - packet->ClientTime;
	ClientNetworkInterface::getInstance().SetLatencyTime(rtt / 2);
	cout << "SPacket_HELLO" <<rtt / 2 << endl;
}
//update
void PacketAfterTreatment::SPacket_GOTOINGAME(Packet * rowPacket)
{
	GameFlowManager::getInstance().ENTER(FLOW_INGAME);
}

void PacketAfterTreatment::SPacket_LASTMOVETIMESTAMP(Packet * rowPacket)
{
	PK_S_NTY_LASTMOVETIMESTAMP * packet = (PK_S_NTY_LASTMOVETIMESTAMP *)rowPacket;
	INT64 RTT = GetTickCount64() - packet->lastTime;
	ClientNetworkInterface::getInstance().SetLatencyTime(RTT / 2);
}

bool ones = false;
void PacketAfterTreatment::SPacket_WIN(Packet * rowPacket)
{
	PK_S_ANS_WINSTATE * packet = (PK_S_ANS_WINSTATE*)rowPacket;
	if (!ones) {
		GameFlowManager::getInstance().ENTER(FLOW_RESULT, true, packet->role);
		GameFlowManager::getInstance().mTimer = GetTickCount64();
		if (CPlayerManager::getInstance().players()->front()->m_pPlayerInfo->GetROLE() == SURVIVOR)
			GameFlowManager::getInstance().fastoutgame = true;
		// TODO : 게임에 필요없는 오브젝트들 삭제 필요
		// 2. 룸서버 소켓 닫기 필요
		ones = true;
		//Network.IngameEnd();
	}

}

void PacketAfterTreatment::SPacket_DEFEAT(Packet * rowPacket)
{
	PK_S_ANS_DEFEATSTATE * packet = (PK_S_ANS_DEFEATSTATE*)rowPacket;
	if (!ones) {
		GameFlowManager::getInstance().ENTER(FLOW_RESULT, false, packet->role);
		GameFlowManager::getInstance().mTimer = GetTickCount64();
		auto playerinfo = CPlayerManager::getInstance().players();
		if (CPlayerManager::getInstance().players()->front()->m_pPlayerInfo->GetROLE() == KILLER)
			GameFlowManager::getInstance().fastoutgame = true;
		ones = true;
		//Network.IngameEnd();
	}
}