#include "stdafx.h"

CFBXInstancingShader::CFBXInstancingShader()
{
	m_nModelType = 0;
}

CFBXInstancingShader::~CFBXInstancingShader()
{
	if (m_pd3dInstanceMatrixBuffer) m_pd3dInstanceMatrixBuffer->Release();
}

void CFBXInstancingShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "INSTANCEPOS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 3, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCEPOS", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 3, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCEPOS", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 3, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCEPOS", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 3, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);

	CreateVertexShaderFromCompiledFile(pd3dDevice, L"VSFBXInstance.fxo", d3dInputElements, nElements);
	CreatePixelShaderFromCompiledFile(pd3dDevice, L"PSFBXInstance.fxo");
}

void CFBXInstancingShader::BuildInstanceObjects(ID3D11Device * pd3dDevice, ModelContainer* pFBXDatas, CCSVData * pCSVData, CHeightMapTerrain * pHeightMapTerrain, CScene * pScene)
{
	m_nInstanceMatrixBufferStride = sizeof(XMFLOAT4X4);
	m_nInstanceMatrixBufferOffset = 0;

	int i = 0;

	m_nObjects = pCSVData->m_nDataNum;
	m_ppObjects = new CGameObject*[m_nObjects];

	float fTerrainWidth = pHeightMapTerrain->GetWidth();
	float fTerrainLengtH = pHeightMapTerrain->GetLength();
	XMFLOAT3 xmf3TerrainScale = pHeightMapTerrain->GetScale();

	CMaterial *pNormalMaterial = new CMaterial();
	pNormalMaterial->m_Material.m_xmcDiffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	pNormalMaterial->m_Material.m_xmcAmbient = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	pNormalMaterial->m_Material.m_xmcSpecular = XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f);
	pNormalMaterial->m_Material.m_xmcEmissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	m_pMaterial = pNormalMaterial;
	if (pNormalMaterial)
		pNormalMaterial->AddRef();

	XMFLOAT3 xmf3RotateAxis;
	CGameObject * pFBXInstanceObject = NULL;
	for (int j = 0; j < m_nObjects; j++)
	{
		pFBXInstanceObject = new CGameObject(1);
		m_nModelType = pCSVData->m_ipDataType[j];
		pFBXInstanceObject->SetMesh((pFBXDatas)->m_pModelMesh);
		m_pTexture[m_nModelType] = (pFBXDatas)->m_pModelTexture;
		if (pFBXDatas->m_pModelTexture)
			(pFBXDatas)->m_pModelTexture->AddRef();
		float fHeight = pHeightMapTerrain->GetHeight(pCSVData->m_ipDataPositionX[j], pCSVData->m_ipDataPositionZ[j]);
		XMFLOAT3 xmf3SurfaceNormal = pHeightMapTerrain->GetNormal(pCSVData->m_ipDataPositionX[j], pCSVData->m_ipDataPositionZ[j]);
		xmf3RotateAxis = Vector3::CrossProduct(XMFLOAT3(0.0f, 1.0f, 0.0f), xmf3SurfaceNormal, true);
		float fAngle = acos(Vector3::DotProduct(XMFLOAT3(0.0f, 1.0f, 0.0f), xmf3SurfaceNormal));
		pFBXInstanceObject->Rotate(&xmf3RotateAxis, (float)D3DXToDegree(fAngle));

		//m_fpDataRotationY가 X값에 적용됐다. 주의해라
		if (m_nModelType == Car1)
		{
			pFBXInstanceObject->SetRotationYawPitchRoll(pCSVData->m_fpDataRotationX[j], 0.0f, 0.0f);
			pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight, pCSVData->m_ipDataPositionZ[j]);
			pFBXInstanceObject->SetAABB(
				XMFLOAT3(0.0f, 0.0f, 0.0f),
				Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
				);
		}

		else if (m_nModelType == Tree1)
		{
			pFBXInstanceObject->SetRotationYawPitchRoll(90.0f, -90.0f, 0.0f);
			pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 40.0f, pCSVData->m_ipDataPositionZ[j]);
		}

		else if (m_nModelType == Fence)
		{
			if (m_nModelType == Fence && pCSVData->m_fpDataRotationY[j] == 0.f) pFBXInstanceObject->SetRotationYawPitchRoll(0.0f, -90.0f, 0.0f);
			if (m_nModelType == Fence && pCSVData->m_fpDataRotationY[j] == 90.0f) pFBXInstanceObject->SetRotationYawPitchRoll(-270.0f, -90.0f, 0.0f);
			if (m_nModelType == Fence && pCSVData->m_fpDataRotationY[j] == 178.6538f) pFBXInstanceObject->SetRotationYawPitchRoll(-180.0f, -90.0f, 0.0f);
			if (m_nModelType == Fence && pCSVData->m_fpDataRotationY[j] == 268.8717f) pFBXInstanceObject->SetRotationYawPitchRoll(-90.0f, -90.0f, 0.0f);

			pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight, pCSVData->m_ipDataPositionZ[j]);

			pFBXInstanceObject->SetAABB(
				XMFLOAT3(-60.0f, 0.0f, 0.0f),
				Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
				);
		}

		else if (m_nModelType == Drum)
		{
			pFBXInstanceObject->SetRotationYawPitchRoll(0.0f, 180.0f, 0.0f);
			pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight, pCSVData->m_ipDataPositionZ[j]);
			pFBXInstanceObject->SetAABB(
				XMFLOAT3(0.0f, 0.0f, 0.0f),
				Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
				);
		}

		else if (m_nModelType == Bus)
		{
			pFBXInstanceObject->SetRotationYawPitchRoll(0.0f, 0.0f, 0.0f);
			pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight, pCSVData->m_ipDataPositionZ[j]);
			pFBXInstanceObject->SetAABB(
				XMFLOAT3(0.0f, 0.0f, 0.0f),
				Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
				);
		}

		else if (m_nModelType == Truck)
		{
			pFBXInstanceObject->SetRotationYawPitchRoll(0.0f, 0.0f, 0.0f);
			pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight, pCSVData->m_ipDataPositionZ[j]);
			pFBXInstanceObject->SetAABB(
				XMFLOAT3(0.0f, 0.0f, 0.0f),
				Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
				);
		}

		else if (m_nModelType == Generator) {
			CPlayerManager::getInstance().tempGenList.push_back(new CGenerator(-1, XMFLOAT3(pCSVData->m_ipDataPositionX[j], fHeight, pCSVData->m_ipDataPositionZ[j]), pScene));
			pFBXInstanceObject->SetRotationYawPitchRoll(90.0f, -90.0f, 0.0f);
			if (i == 0)
			{
				pFBXInstanceObject->SetAABB(
					XMFLOAT3(-23.0f, 0.0f, 0.0f),
					Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.6f, false)
					);
				pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 1.0f, pCSVData->m_ipDataPositionZ[j]);
			}
			else if (i == 1) {
				pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 3, pCSVData->m_ipDataPositionZ[j]);
				pFBXInstanceObject->SetAABB(
					XMFLOAT3(-23.0f, 0.0f, 0.0f),
					Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.6f, false)
					);
			}
			else if (i == 2) {
				pFBXInstanceObject->SetAABB(
					XMFLOAT3(-18.0f, 0.0f, 0.0f),
					Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.7f, false)
					);
				pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 3, pCSVData->m_ipDataPositionZ[j]);
			}
			else if (i == 3) {
				pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 1.0f, pCSVData->m_ipDataPositionZ[j]);
				pFBXInstanceObject->SetAABB(
					XMFLOAT3(-18.0f, 0.0f, 0.0f),
					Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.6f, false)
					);
			}
			else if (i == 4) {
				pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight - 1.0f, pCSVData->m_ipDataPositionZ[j]);
				pFBXInstanceObject->SetAABB(
					XMFLOAT3(-18.0f, 0.0f, 0.0f),
					Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
					);
			}
			else if (i == 5) {
				pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 1.0f, pCSVData->m_ipDataPositionZ[j]);
				pFBXInstanceObject->SetAABB(
					XMFLOAT3(-18.0f, 0.0f, 0.0f),
					Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.8f, false)
					);
			}
			else if (i == 6) {
				pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 3, pCSVData->m_ipDataPositionZ[j]);
				pFBXInstanceObject->SetAABB(
					XMFLOAT3(-23.0f, 0.0f, 0.0f),
					Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.6f, false)
					);
			}
			else if (i == 7) {
				pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 3, pCSVData->m_ipDataPositionZ[j]);
				pFBXInstanceObject->SetAABB(
					XMFLOAT3(-23.0f, 0.0f, 0.0f),
					Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.6f, false)
					);
			}
			else if (i == 8) {
				pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 3, pCSVData->m_ipDataPositionZ[j]);
				pFBXInstanceObject->SetAABB(
					XMFLOAT3(-23.0f, 0.0f, 0.0f),
					Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.6f, false)
					);
			}
			else if (i == 9) {
				pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 3, pCSVData->m_ipDataPositionZ[j]);
				pFBXInstanceObject->SetAABB(
					XMFLOAT3(-18.0f, 0.0f, 0.0f),
					Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.8f, false)
					);
			}
			else {
				pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 3, pCSVData->m_ipDataPositionZ[j]);
				pFBXInstanceObject->SetAABB(
					XMFLOAT3(-18.0f, 0.0f, 0.0f),
					Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
					);
			}
		}

		else if (m_nModelType == Bark)
		{
			pFBXInstanceObject->SetRotationYawPitchRoll(90.0f, 0.0f, 0.0f);
			pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 43.0f, pCSVData->m_ipDataPositionZ[j]);
			pFBXInstanceObject->SetAABB(
				XMFLOAT3(0.0f, 0.0f, 0.0f),
				Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
				);
		}

		else if (m_nModelType == Escapedoor1) {
			pFBXInstanceObject->SetRotationYawPitchRoll(0.0f, 0.0f, 0.0f);
			pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 15.0f, pCSVData->m_ipDataPositionZ[j]);
			pFBXInstanceObject->SetAABB(
				XMFLOAT3(0.0f, 0.0f, 0.0f),
				Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
				);
		}

		else if (m_nModelType == Escapedoor2) {
			pFBXInstanceObject->SetRotationYawPitchRoll(90.0f, 0.0f, 0.0f);
			pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 23.0f, pCSVData->m_ipDataPositionZ[j]);
			pFBXInstanceObject->SetAABB(
				XMFLOAT3(0.0f, 0.0f, 0.0f),
				Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
				);
		}

		else if (m_nModelType == Stone)
		{
			pFBXInstanceObject->SetRotationYawPitchRoll(180.0f, 0.0f, 0.0f);
			pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight + 5.0f, pCSVData->m_ipDataPositionZ[j]);
			pFBXInstanceObject->SetAABB(
				XMFLOAT3(0.0f, 0.0f, 0.0f),
				Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.6f, false)
				);
		}

		else if (m_nModelType == StreetLight)
		{
			pFBXInstanceObject->SetRotationYawPitchRoll(0.0f, 0.0f, 0.0f);
			pFBXInstanceObject->SetPosition(pCSVData->m_ipDataPositionX[j], fHeight, pCSVData->m_ipDataPositionZ[j]);
			pFBXInstanceObject->SetAABB(
				XMFLOAT3(0.0f, 0.0f, 0.0f),
				Vector3::ScalarProduct((pFBXDatas)->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
				);
		}

		pFBXInstanceObject->SetAABBPosition();

		m_ppObjects[i++] = pFBXInstanceObject;
	}

	m_pd3dInstanceMatrixBuffer = CreateInstanceBuffer(pd3dDevice, m_nObjects, m_nInstanceMatrixBufferStride, NULL);
	//인스턴싱 버퍼를 모델메쉬의 정점버퍼에 추가한다. 이제 메쉬는 정점데이터와 인스턴스 데이터를 가진다.
	(pFBXDatas)->m_pModelMesh->AssembleToVertexBuffer(1, &m_pd3dInstanceMatrixBuffer, &m_nInstanceMatrixBufferStride, &m_nInstanceMatrixBufferOffset);
}

ID3D11Buffer * CFBXInstancingShader::CreateInstanceBuffer(ID3D11Device *pd3dDevice, int nObjects, UINT nBufferStride, void *pBufferData)
{
	ID3D11Buffer *pd3dInstanceBuffer = NULL;
	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	/*버퍼의 초기화 데이터가 없으면 동적 버퍼로 생성한다. 즉, 나중에 매핑을 하여 내용을 채우거나 변경한다.*/
	d3dBufferDesc.Usage = (pBufferData) ? D3D11_USAGE_DEFAULT : D3D11_USAGE_DYNAMIC;
	d3dBufferDesc.ByteWidth = nBufferStride * nObjects;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = (pBufferData) ? 0 : D3D11_CPU_ACCESS_WRITE;

	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = pBufferData;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, (pBufferData) ? &d3dBufferData : NULL, &pd3dInstanceBuffer);
	return(pd3dInstanceBuffer);

}

void CFBXInstancingShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	OnPrepareRender(pd3dDeviceContext);

	if (m_pMaterial) CIlluminatedShader::UpdateShaderVariable(pd3dDeviceContext, &m_pMaterial->m_Material);

	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	pd3dDeviceContext->Map(m_pd3dInstanceMatrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	XMFLOAT4X4 *pxmf4x4Models = (XMFLOAT4X4 *)d3dMappedResource.pData;
	for (int i = 0; i < m_nObjects; i++)
	{
		m_ppObjects[i]->OnPrepareRender();
		m_pTexture[m_nModelType]->UpdateShaderVariable(pd3dDeviceContext);
		pxmf4x4Models[i] = Matrix4x4::Transpose(m_ppObjects[i]->GetmtxWorld());
	}
	pd3dDeviceContext->Unmap(m_pd3dInstanceMatrixBuffer, 0);

	CMesh *pModelMesh = m_ppObjects[m_nObjects - 1]->GetMesh();
	pModelMesh->RenderInstanced(pd3dDeviceContext, m_nObjects, 0);
}