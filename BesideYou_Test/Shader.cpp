#include "stdafx.h"
//#include "Shader.h"

//월드 변환 행렬을 위한 상수 버퍼는 쉐이더 객체의 정적(static) 데이터 멤버이다.
ID3D11Buffer *CShader::m_pd3dcbWorldMatrix = NULL;
ID3D11Buffer *CIlluminatedShader::m_pd3dcbMaterial = NULL;

//dx11 compiler
BYTE *ReadCompiledEffectFile(WCHAR *pszFileName, int *pnReadBytes)
{
	FILE *pFile = NULL;
	HRESULT hResult;
	::_wfopen_s(&pFile, pszFileName, L"rb");
	::fseek(pFile, 0, SEEK_END);
	int nFileSize = ::ftell(pFile);
	BYTE *pByteCode = new BYTE[nFileSize];
	::rewind(pFile);
	*pnReadBytes = ::fread(pByteCode, sizeof(BYTE), nFileSize, pFile);
	::fclose(pFile);
	return(pByteCode);
}

//dx11 compiler
void CShader::CreateVertexShaderFromCompiledFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName, D3D11_INPUT_ELEMENT_DESC *pd3dInputLayout, UINT nElements)
{
	int nReadBytes = 0;
	BYTE *pByteCode = ReadCompiledEffectFile(pszFileName, &nReadBytes);
	HRESULT hResult = pd3dDevice->CreateVertexShader(pByteCode, nReadBytes, NULL, &m_pd3dVertexShader);
	pd3dDevice->CreateInputLayout(pd3dInputLayout, nElements, pByteCode, nReadBytes, &m_pd3dVertexLayout);

	if (pByteCode) delete[] pByteCode;
}

//dx11 compiler
//컴파일된 쉐이더 코드에서 픽셀 쉐이더를 생성한다.
void CShader::CreatePixelShaderFromCompiledFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName)
{
	int nReadBytes = 0;
	BYTE *pByteCode = ReadCompiledEffectFile(pszFileName, &nReadBytes);
	HRESULT hResult = pd3dDevice->CreatePixelShader(pByteCode, nReadBytes, NULL, &m_pd3dPixelShader);

	if (pByteCode) delete[] pByteCode;
}

CShader::CShader(CHeightMapTerrain *pHeightMapTerrain, vector<ModelContainer*> pvtFBXDatas, CCSVData * pCSVData)
{
	m_pHeightMapTerrain = pHeightMapTerrain;
	m_pvtFBXDatas = pvtFBXDatas;
	m_pCSVData = pCSVData;

	m_ppObjects = NULL;
	m_nObjects = 0;

	if (m_nObjects > 0)
	{
		m_ppObjects = new CGameObject*[m_nObjects];
		for (int i = 0; i < m_nObjects; i++) m_ppObjects[i] = NULL;
	}

	m_pd3dVertexShader = NULL;
	m_pd3dVertexLayout = NULL;
	m_pd3dPixelShader = NULL;
}

CShader::~CShader()
{
	

	if (m_pd3dVertexShader) m_pd3dVertexShader->Release();
	if (m_pd3dVertexLayout) m_pd3dVertexLayout->Release();
	if (m_pd3dPixelShader) m_pd3dPixelShader->Release();
}

void CShader::BuildObjects(ID3D11Device *pd3dDevice)
{
	m_nObjects = m_pCSVData->m_nDataNum;
	m_ppObjects = new CGameObject*[m_nObjects];

	CCubeMesh *pBoundingCubeMesh;
	CGameObject * pGameObject = NULL;
	XMFLOAT3 xmf3BoxSize;
	XMFLOAT3 xmf3RotateAxis;

	int type, i = 0;

	//collision fake box render
	for (int j = 0; j < m_nObjects; j++)
	{
		pGameObject = new CGameObject(1);
		type = m_pCSVData->m_ipDataType[j];
		xmf3BoxSize = m_pvtFBXDatas[type]->m_pModelMesh->GetBoundingSizes();
		pBoundingCubeMesh = new CCubeMesh(pd3dDevice, xmf3BoxSize.x, xmf3BoxSize.y, xmf3BoxSize.z, XMFLOAT4(1.0f, 0.0f, 0.0f, 0.0f));
		pGameObject->SetMesh(pBoundingCubeMesh);
		float fHeight = m_pHeightMapTerrain->GetHeight(m_pCSVData->m_ipDataPositionX[j], m_pCSVData->m_ipDataPositionZ[j]);

		XMFLOAT3 xmf3SurfaceNormal = m_pHeightMapTerrain->GetNormal(m_pCSVData->m_ipDataPositionX[j], m_pCSVData->m_ipDataPositionZ[j]);
		xmf3RotateAxis = Vector3::CrossProduct(XMFLOAT3(0.0f, 1.0f, 0.0f), xmf3SurfaceNormal, true);
		float fAngle = acos(Vector3::DotProduct(XMFLOAT3(0.0f, 1.0f, 0.0f), xmf3SurfaceNormal));
		pGameObject->Rotate(&xmf3RotateAxis, (float)D3DXToDegree(fAngle));
		if (type == Car1) { pGameObject->SetRotationYawPitchRoll(m_pCSVData->m_fpDataRotationY[j], 0.0f, 0.0f); };
		if (type == Tree1) { pGameObject->SetRotationYawPitchRoll(90.0f, -90.0f, 0.0f); };
		if (type == Fence && m_pCSVData->m_ipDataNum[j] >= 0 && m_pCSVData->m_ipDataNum[j] <= 19) { pGameObject->SetRotationYawPitchRoll(-90.0f, -90.0f, 0.0f); };
		if (type == Fence && m_pCSVData->m_ipDataNum[j] >= 20 && m_pCSVData->m_ipDataNum[j] <= 39) { pGameObject->SetRotationYawPitchRoll(-180.0f, -90.0f, 0.0f); };
		if (type == Fence && m_pCSVData->m_ipDataNum[j] >= 40 && m_pCSVData->m_ipDataNum[j] <= 59) { pGameObject->SetRotationYawPitchRoll(-270.0f, -90.0f, 0.0f); };
		if (type == Fence && m_pCSVData->m_ipDataNum[j] >= 60 && m_pCSVData->m_ipDataNum[j] <= 79) { pGameObject->SetRotationYawPitchRoll(0.0f, -90.0f, 0.0f); };
		if (type == Drum) { pGameObject->SetRotationYawPitchRoll(0.0f, 180.0f, 0.0f); };
		if (type == Bus) { pGameObject->SetRotationYawPitchRoll(0.0f, 0.0f, 0.0f); };
		if (type == Truck) {};

		if (type == Generator) {
			pGameObject->SetRotationYawPitchRoll(90.0f, -90.0f, 0.0f);
			pGameObject->SetPosition(m_pCSVData->m_ipDataPositionX[j], fHeight + Vector3::ScalarProduct(xmf3BoxSize, 0.5, false).y, m_pCSVData->m_ipDataPositionZ[j] + 18);
		};

		if (type != Generator)
			pGameObject->SetPosition(m_pCSVData->m_ipDataPositionX[j], fHeight + Vector3::ScalarProduct(xmf3BoxSize, 0.5, false).y, m_pCSVData->m_ipDataPositionZ[j]);

		m_ppObjects[i++] = pGameObject;
	}
}

void CShader::ReleaseObjects()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++) if (m_ppObjects[j]) delete m_ppObjects[j];
		delete[] m_ppObjects;
	}
}

void CShader::AnimateObjects(float fTimeElapsed)
{
	for (int j = 0; j < m_nObjects; j++)
	{
		m_ppObjects[j]->Animate(fTimeElapsed);
	}
}

void CShader::OnPrepareRender(ID3D11DeviceContext *pd3dDeviceContext)
{
	pd3dDeviceContext->IASetInputLayout(m_pd3dVertexLayout);
	pd3dDeviceContext->VSSetShader(m_pd3dVertexShader, NULL, 0);
	pd3dDeviceContext->PSSetShader(m_pd3dPixelShader, NULL, 0);
}

void CShader::CreateVertexShaderFromFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderModel, ID3D11VertexShader **ppd3dVertexShader, D3D11_INPUT_ELEMENT_DESC *pd3dInputLayout, UINT nElements, ID3D11InputLayout **ppd3dVertexLayout)
{
	HRESULT hResult;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif
	ID3DBlob *pd3dShaderBlob = NULL, *pd3dErrorBlob = NULL;
	/*파일(pszFileName)에서 쉐이더 함수(pszShaderName)를 컴파일하여 컴파일된 쉐이더 코드의 메모리 주소(pd3dShaderBlob)를 반환한다.*/
	if (SUCCEEDED(hResult = D3DX11CompileFromFile(pszFileName, NULL, NULL, pszShaderName, pszShaderModel, dwShaderFlags, 0, NULL, &pd3dShaderBlob, &pd3dErrorBlob, NULL)))
	{
		//컴파일된 쉐이더 코드의 메모리 주소에서 정점-쉐이더를 생성한다. 
		pd3dDevice->CreateVertexShader(pd3dShaderBlob->GetBufferPointer(), pd3dShaderBlob->GetBufferSize(), NULL, ppd3dVertexShader);
		//컴파일된 쉐이더 코드의 메모리 주소와 입력 레이아웃에서 정점 레이아웃을 생성한다. 
		pd3dDevice->CreateInputLayout(pd3dInputLayout, nElements, pd3dShaderBlob->GetBufferPointer(), pd3dShaderBlob->GetBufferSize(), ppd3dVertexLayout);
		pd3dShaderBlob->Release();
	}
}

void CShader::CreatePixelShaderFromFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderModel, ID3D11PixelShader **ppd3dPixelShader)
{
	HRESULT hResult;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif
	ID3DBlob *pd3dShaderBlob = NULL, *pd3dErrorBlob = NULL;
	/*파일(pszFileName)에서 쉐이더 함수(pszShaderName)를 컴파일하여 컴파일된 쉐이더 코드의 메모리 주소(pd3dShaderBlob)를 반환한다.*/
	if (SUCCEEDED(hResult = D3DX11CompileFromFile(pszFileName, NULL, NULL, pszShaderName, pszShaderModel, dwShaderFlags, 0, NULL, &pd3dShaderBlob, &pd3dErrorBlob, NULL)))
	{
		//컴파일된 쉐이더 코드의 메모리 주소에서 픽셀-쉐이더를 생성한다. 
		pd3dDevice->CreatePixelShader(pd3dShaderBlob->GetBufferPointer(), pd3dShaderBlob->GetBufferSize(), NULL, ppd3dPixelShader);
		pd3dShaderBlob->Release();
	}
}

void CShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputLayout);
	//파일 “Effect.fx”에서 정점-쉐이더의 시작 함수가 "VS"인 정점-쉐이더를 생성한다. 
	CreateVertexShaderFromFile(pd3dDevice, L"Effect.fx", "VS", "vs_5_0", &m_pd3dVertexShader, d3dInputLayout, nElements, &m_pd3dVertexLayout);
	//파일 “Effect.fx”에서 픽셀-쉐이더의 시작 함수가 "PS"인 픽셀-쉐이더를 생성한다. 
	CreatePixelShaderFromFile(pd3dDevice, L"Effect.fx", "PS", "ps_5_0", &m_pd3dPixelShader);

	//0720 에서 아마 없어도 되지 않을까싶다. 임의로 지웠음
	CreateShaderVariables(pd3dDevice);
}

void CShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	OnPrepareRender(pd3dDeviceContext);

	for (int j = 0; j < m_nObjects; j++){
		if (m_ppObjects[j]){
			if (m_ppObjects[j]->IsVisible(pCamera)){
				m_ppObjects[j]->Render(pd3dDeviceContext, pCamera);
			}
			m_ppObjects[j]->Render(pd3dDeviceContext, pCamera);
		}
	}
}

void CShader::CreateShaderVariables(ID3D11Device *pd3dDevice)
{
	//월드 변환 행렬을 위한 상수 버퍼를 생성한다.
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(VS_CB_WORLD_MATRIX);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	pd3dDevice->CreateBuffer(&bd, NULL, &m_pd3dcbWorldMatrix);
}

void CShader::ReleaseShaderVariables()
{
	//월드 변환 행렬을 위한 상수 버퍼를 반환한다.
	if (m_pd3dcbWorldMatrix) m_pd3dcbWorldMatrix->Release();
}

void CShader::UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, XMFLOAT4X4 *pxmf4x4World)
{
	//월드 변환 행렬을 상수 버퍼에 복사한다.
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	pd3dDeviceContext->Map(m_pd3dcbWorldMatrix, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	VS_CB_WORLD_MATRIX *pcbWorldMatrix = (VS_CB_WORLD_MATRIX *)d3dMappedResource.pData;
	pcbWorldMatrix->m_xmf4x4World = Matrix4x4::Transpose(pxmf4x4World);
	//D3DXMatrixTranspose(&pcbWorldMatrix->m_xmf4x4World, pxmf4x4World);
	pd3dDeviceContext->Unmap(m_pd3dcbWorldMatrix, 0);

	//상수 버퍼를 디바이스의 슬롯(VS_SLOT_WORLD_MATRIX)에 연결한다.
	pd3dDeviceContext->VSSetConstantBuffers(VS_SLOT_WORLD_MATRIX, 1, &m_pd3dcbWorldMatrix);
}

CPlayerShader::CPlayerShader()
{
	m_pMaterial = NULL;
}

CPlayerShader::~CPlayerShader()
{
	if (m_pMaterial) m_pMaterial->Release();
}

void CPlayerShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 3, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONEINDICES", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 4, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);

	/*CreateVertexShaderFromFile(pd3dDevice, L"CharacterSkinned.fx", "VSSkinned", "vs_5_0", &m_pd3dVertexShader, d3dInputElements, nElements, &m_pd3dVertexLayout);
	CreatePixelShaderFromFile(pd3dDevice, L"CharacterSkinned.fx", "PSSkinned", "ps_5_0", &m_pd3dPixelShader);*/

	CreateVertexShaderFromCompiledFile(pd3dDevice, L"VSCharacterSkinned.fxo", d3dInputElements, nElements);
	CreatePixelShaderFromCompiledFile(pd3dDevice, L"PSCharacterSkinned.fxo");

}

void CPlayerShader::BuildObjects(ID3D11Device *pd3dDevice, ROLE role, int playerShaderindex)
{
	m_nObjects = 1;
	m_ppObjects = new CGameObject*[m_nObjects];

	CMaterial *pNormalMaterial = new CMaterial();
	pNormalMaterial->m_Material.m_xmcDiffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	pNormalMaterial->m_Material.m_xmcAmbient = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	pNormalMaterial->m_Material.m_xmcSpecular = XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f);
	pNormalMaterial->m_Material.m_xmcEmissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	ID3D11SamplerState *pd3dSamplerState = NULL;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;
	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dSamplerState);

	ID3D11ShaderResourceView *pd3dsrvTexture = NULL;
	CTexture *pDrayerTexture = new CTexture(1, 1, 0, 0);
	if (role == KILLER) D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("../Data/BesideYouData/butcher/Butcher_Albedo.jpg"), NULL, NULL, &pd3dsrvTexture, NULL);
	if (role == SURVIVOR) D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("../Data/BesideYouData/survivor/survivor1.png"), NULL, NULL, &pd3dsrvTexture, NULL);
	pDrayerTexture->SetTexture(0, pd3dsrvTexture);
	pDrayerTexture->SetSampler(0, pd3dSamplerState);
	pd3dsrvTexture->Release();
	pd3dsrvTexture = NULL;

	m_pTexture = pDrayerTexture;
	if (m_pTexture) m_pTexture->AddRef();

	CMesh * pTestMesh;
	if (KILLER == role) pTestMesh = new CFBXMesh(pd3dDevice, "../Data/BesideYouData/butcher/Butcher_Animation.data", 0.3);
	if (SURVIVOR == role) pTestMesh = new CFBXMesh(pd3dDevice, "../Data/BesideYouData/survivor/survivor1Animation.data", 0.1);
	CTerrainPlayer * pPlayer = new CTerrainPlayer(1, playerShaderindex);
	if (KILLER == role) pPlayer->SetPlayerRotationPYR(180.0f, 0.0f, 0.0f);
	if (SURVIVOR == role) pPlayer->SetPlayerRotationPYR(90.0f, -90.0f, 90.0f);
	pPlayer->SetMesh(pTestMesh);
	pPlayer->SetObjectScale(0);
	pPlayer->SetTexture(pDrayerTexture);
	pPlayer->SetMaterial(pNormalMaterial);
	pPlayer->ChangeCamera(pd3dDevice, THIRD_PERSON_CAMERA, 0.0f);

	// 플레이어의 AABB바운딩 박스를 만든다.
	/*if (KILLER == role) {
		pPlayer->SetAABB(
			XMFLOAT3(0.0f, 0.0f, 0.0f),
			XMFLOAT3(pTestMesh->GetBoundingSizes().x * 0.01, pTestMesh->GetBoundingSizes().y * 0.01, pTestMesh->GetBoundingSizes().z * 0.01)
		);
	}
	else {
		pPlayer->SetAABB(
			XMFLOAT3(0.0f, 0.0f, 0.0f),
			XMFLOAT3(pTestMesh->GetBoundingSizes().x * 0.01, pTestMesh->GetBoundingSizes().y * 0.01, pTestMesh->GetBoundingSizes().z * 0.01)
		);
	}*/

	pPlayer->SetAABB(
		XMFLOAT3(0.0f, 0.0f, 0.0f),
		XMFLOAT3(pTestMesh->GetBoundingSizes().x * 0.01, pTestMesh->GetBoundingSizes().y * 0.01, pTestMesh->GetBoundingSizes().z * 0.01)
		);

	pPlayer->SetAABBPosition();

	//pPlayer->SetPosition(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_ppObjects[0] = pPlayer;
}

void CPlayerShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera)
{
	//3인칭 카메라일 때 플레이어를 렌더링한다.
	DWORD nCameraMode = (pCamera) ? pCamera->GetMode() : 0x00;
	if (nCameraMode == THIRD_PERSON_CAMERA)
	{
		//Player는 절두체 컬링을 하지않는다.
		OnPrepareRender(pd3dDeviceContext);

		for (int j = 0; j < m_nObjects; j++)
		{
			if (m_ppObjects[j])
			{
				m_ppObjects[j]->Render(pd3dDeviceContext, pCamera);
			}
		}

		//Player도 절두체 컬링을 한다.
		//CShader::Render(pd3dDeviceContext, pCamera);
	}
}

CTerrainShader::CTerrainShader()
{
}

CTerrainShader::~CTerrainShader()
{
}

void CTerrainShader::BuildObjects(ID3D11Device *pd3dDevice)
{
	m_nObjects = 1;
	m_ppObjects = new CGameObject*[m_nObjects];

	{
		//지형은 텍스쳐가 2개이므로 2개의 샘플러 객체가 필요하다. 
		ID3D11SamplerState *pd3dBaseSamplerState = NULL;
		D3D11_SAMPLER_DESC d3dSamplerDesc;
		ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
		d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		d3dSamplerDesc.MinLOD = 0;
		d3dSamplerDesc.MaxLOD = 0;
		pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dBaseSamplerState);

		ID3D11SamplerState *pd3dDetailSamplerState = NULL;
		d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dDetailSamplerState);

		CTexture *pTerrainTexture = new CTexture(2, 2, 0, 0);
		ID3D11ShaderResourceView *pd3dsrvBaseTexture = NULL;
		D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("../Assets/Image/Terrain/Base_Texture.jpg"), NULL, NULL, &pd3dsrvBaseTexture, NULL);
		pTerrainTexture->SetTexture(0, pd3dsrvBaseTexture);
		pTerrainTexture->SetSampler(0, pd3dBaseSamplerState);
		pd3dsrvBaseTexture->Release();
		pd3dBaseSamplerState->Release();

		ID3D11ShaderResourceView *pd3dsrvDetailTexture = NULL;
		D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("../Assets/Image/Terrain/Detail_Texture_7.jpg"), NULL, NULL, &pd3dsrvDetailTexture, NULL);
		pTerrainTexture->SetTexture(1, pd3dsrvDetailTexture);
		pTerrainTexture->SetSampler(1, pd3dDetailSamplerState);
		pd3dsrvDetailTexture->Release();
		pd3dDetailSamplerState->Release();

		CMaterial *pTerrainMaterial = new CMaterial();
		pTerrainMaterial->m_Material.m_xmcDiffuse = XMFLOAT4(0.8f, 1.0f, 0.2f, 1.0f);
		pTerrainMaterial->m_Material.m_xmcAmbient = XMFLOAT4(0.1f, 0.3f, 0.1f, 1.0f);
		pTerrainMaterial->m_Material.m_xmcSpecular = XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f);
		pTerrainMaterial->m_Material.m_xmcEmissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

		XMFLOAT3 xmf3Scale(10.0f, 2.0f, 10.0f);
		XMFLOAT4 xmcColor(0.0f, 0.2f, 0.1f, 0.0f);
		m_ppObjects[0] = new CHeightMapTerrain(pd3dDevice, 257, 257, 257, 257, _T("../Data/Terrain/finalterrain.raw"), xmf3Scale, xmcColor);
		m_ppObjects[0]->SetTexture(pTerrainTexture);
		m_ppObjects[0]->SetMaterial(pTerrainMaterial);
	}
}

CHeightMapTerrain *CTerrainShader::GetTerrain() const
{
	return ((CHeightMapTerrain*)m_ppObjects[0]);
}


CIlluminatedShader::CIlluminatedShader()
{
}

CIlluminatedShader::~CIlluminatedShader()
{
}

void CIlluminatedShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Effect.fx", "VSLightingColor", "vs_5_0", &m_pd3dVertexShader, d3dInputElements, nElements, &m_pd3dVertexLayout);
	CreatePixelShaderFromFile(pd3dDevice, L"Effect.fx", "PSLightingColor", "ps_5_0", &m_pd3dPixelShader);
}

void CIlluminatedShader::CreateShaderVariables(ID3D11Device *pd3dDevice)
{
	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	d3dBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	d3dBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	d3dBufferDesc.ByteWidth = sizeof(MATERIAL);
	pd3dDevice->CreateBuffer(&d3dBufferDesc, NULL, &m_pd3dcbMaterial);
}

void CIlluminatedShader::ReleaseShaderVariables()
{
	if (m_pd3dcbMaterial) m_pd3dcbMaterial->Release();
}

void CIlluminatedShader::UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, MATERIAL *pMaterial)
{
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	pd3dDeviceContext->Map(m_pd3dcbMaterial, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	MATERIAL *pcbMaterial = (MATERIAL *)d3dMappedResource.pData;
	memcpy(pcbMaterial, pMaterial, sizeof(MATERIAL));
	pd3dDeviceContext->Unmap(m_pd3dcbMaterial, 0);
	pd3dDeviceContext->PSSetConstantBuffers(PS_SLOT_MATERIAL, 1, &m_pd3dcbMaterial);
}

CTexturedShader::CTexturedShader()
{
}

CTexturedShader::~CTexturedShader()
{
}

void CTexturedShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Effect.fx", "VSTexturedColor", "vs_5_0", &m_pd3dVertexShader, d3dInputElements, nElements, &m_pd3dVertexLayout);
	CreatePixelShaderFromFile(pd3dDevice, L"Effect.fx", "PSTexturedColor", "ps_5_0", &m_pd3dPixelShader);
}



CDetailTexturedShader::CDetailTexturedShader()
{
}

CDetailTexturedShader::~CDetailTexturedShader()
{
}

void CDetailTexturedShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 1, DXGI_FORMAT_R32G32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Effect.fx", "VSDetailTexturedColor", "vs_5_0", &m_pd3dVertexShader, d3dInputElements, nElements, &m_pd3dVertexLayout);
	CreatePixelShaderFromFile(pd3dDevice, L"Effect.fx", "PSDetailTexturedColor", "ps_5_0", &m_pd3dPixelShader);
}

CTexturedIlluminatedShader::CTexturedIlluminatedShader()
{
}

CTexturedIlluminatedShader::~CTexturedIlluminatedShader()
{
}

void CTexturedIlluminatedShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Effect.fx", "VSTexturedLightingColor", "vs_5_0", &m_pd3dVertexShader, d3dInputElements, nElements, &m_pd3dVertexLayout);
	CreatePixelShaderFromFile(pd3dDevice, L"Effect.fx", "PSTexturedLightingColor", "ps_5_0", &m_pd3dPixelShader);
}

CDetailTexturedIlluminatedShader::CDetailTexturedIlluminatedShader()
{
}

//2.27-2
CDetailTexturedIlluminatedShader::~CDetailTexturedIlluminatedShader()
{
}

//2.27-2
void CDetailTexturedIlluminatedShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 1, DXGI_FORMAT_R32G32_FLOAT, 3, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	/*CreateVertexShaderFromFile(pd3dDevice, L"Effect.fx", "VSDetailTexturedLightingColor", "vs_5_0", &m_pd3dVertexShader, d3dInputElements, nElements, &m_pd3dVertexLayout);
	CreatePixelShaderFromFile(pd3dDevice, L"Effect.fx", "PSDetailTexturedLightingColor", "ps_5_0", &m_pd3dPixelShader);*/

	CreateVertexShaderFromCompiledFile(pd3dDevice, L"VSDetailTexturedLightingColor.fxo", d3dInputElements, nElements);
	CreatePixelShaderFromCompiledFile(pd3dDevice, L"PSDetailTexturedLightingColor.fxo");

}

void CPlayerAABBShader::Render(ID3D11DeviceContext *pd3dDeviceContext, CPlayer ** ppPlayer)
{
	//캐릭터의 fakeboundingbox를 움직인다.
	m_ppObjects[0]->SetPosition(ppPlayer[0]->GetPosition());

	OnPrepareRender(pd3dDeviceContext);

	for (int j = 0; j < m_nObjects; j++)
	{
		if (m_ppObjects[j])
		{
			m_ppObjects[j]->Render(pd3dDeviceContext, NULL);
		}
	}
}

//trap
void CTrapShader::CreateShader(ID3D11Device *pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 3, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONEINDICES", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 4, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);

	CreateVertexShaderFromCompiledFile(pd3dDevice, L"VSCharacterSkinned.fxo", d3dInputElements, nElements);
	CreatePixelShaderFromCompiledFile(pd3dDevice, L"PSCharacterSkinned.fxo");
}

//trap
void CTrapShader::BuildObjects(ID3D11Device * pd3dDevice)
{
	m_ppObjects = new CGameObject*[1];

	CMaterial *pNormalMaterial = new CMaterial();
	pNormalMaterial->m_Material.m_xmcDiffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	pNormalMaterial->m_Material.m_xmcAmbient = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	pNormalMaterial->m_Material.m_xmcSpecular = XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f);
	pNormalMaterial->m_Material.m_xmcEmissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	ID3D11SamplerState * pd3dSamplerState = NULL;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;
	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dSamplerState);

	ID3D11ShaderResourceView *pd3dsrvTexture = NULL;
	CTexture *pTrapTexture = new CTexture(1, 1, 0, 0);
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, _T("../Data/BesideYouData/trap/trap.png"), NULL, NULL, &pd3dsrvTexture, NULL);
	pTrapTexture->SetTexture(0, pd3dsrvTexture);
	pTrapTexture->SetSampler(0, pd3dSamplerState);
	pd3dsrvTexture->Release();
	pd3dsrvTexture = NULL;

	CMesh * pTrapMesh;
	pTrapMesh = new CFBXMesh(pd3dDevice, "../Data/BesideYouData/trap/trap(Animation).data", 10.0f);

	CGameObject * pTrapObject = new CTrap();
	pTrapObject->SetMesh(pTrapMesh);
	pTrapObject->SetObjectScale(0);
	pTrapObject->SetTexture(pTrapTexture);
	pTrapObject->SetMaterial(pNormalMaterial);

	m_ppObjects[0] = pTrapObject;
}

void CTrapShader::Render(ID3D11DeviceContext * pd3dDeviceContext, CCamera * pCamera)
{
	OnPrepareRender(pd3dDeviceContext);

	m_ppObjects[0]->Render(pd3dDeviceContext, pCamera);
}

void CTrapShader::SetTrapPosition(CPlayer& player, CHeightMapTerrain& terrain)
{
	m_bTrapState = true;

	//Trap을 캐릭터의 앞쪽(Look방향)에 설치한다.
	XMFLOAT3 SetLength = Vector3::ScalarProduct(player.GetLookVector(), 15.0f, true);
	XMFLOAT3 SetTrapPosition = Vector3::Add(player.GetPosition(), SetLength);
	float fHeight = terrain.GetHeight(SetTrapPosition.x, SetTrapPosition.z, false) + 1.0f;
	m_ppObjects[0]->SetPosition(SetTrapPosition.x, fHeight, SetTrapPosition.z);

	m_ppObjects[0]->SetTrapAABBPosition();

}

void CTrapShader::SetTrapPosition(XMFLOAT3 & inPosition, CHeightMapTerrain & terrain)
{
	//m_bTrapState = true;
	float fHeight = terrain.GetHeight(inPosition.x, inPosition.z, false) + 1.0f;
	m_ppObjects[0]->SetPosition(inPosition.x, fHeight, inPosition.z);

	m_ppObjects[0]->SetTrapAABBPosition();
}

//trap
bool CTrapShader::CheckTrapOn(BoundingBox & playerAABB)
{
	BoundingBox& trapAABB = m_ppObjects[0]->GetAABB();
	ContainmentType containType = trapAABB.Contains(playerAABB);
	switch (containType)
	{
	case INTERSECTS: {
		cout << "트랩밟음" << endl;
		return true;
	}
	case CONTAINS: {
		cout << "트랩밟음" << endl;
		return true;
	}
	}
	return false;
}

void TrapJob::inData(int32_t inJobNumber, uint64_t inTrapid, uint64_t inCatchid, XMFLOAT3 inPosition)
{
	this->mJobNumber = inJobNumber;
	switch (inJobNumber)
	{
	case install:
		this->minPosition = inPosition;
		this->mTrapid = inTrapid;
		this->mCatchid = inCatchid;
		break;
	case invoked:
		this->mTrapid = inTrapid;
		this->mCatchid = inCatchid;
		break;
	default:
		break;
	}
}
