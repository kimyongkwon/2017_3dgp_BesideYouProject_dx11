#pragma once

struct LIGHT;
struct LIGHTS;

enum ButcherAnimation
{
	BIDLE, 
	BWALK, 
	BRUN, 
	BPUNCH, 
	BKICK,
};
enum SurvivorAnimation
{
	SIDLE, 
	SRUN, 
	SGEN, 
	SREACT, 
	SINSTALLTRAP, 
	SINJUREDIDLE, 
	SSTAPONTRAP, 
	SINJUREWALK,
	SDIE,
	SCROUCHIDLE = 9,
	SCROUCHWALK = 10,
	SSTANDINGTOCROUCH = 11,
	SCROUCHTOSTANDING = 12,
	SWALK = 13,
	SINJUREDRUN = 14,
};

//------------------------------------------------------------------
//
// Class : CScene (Base Scene)
//
// Desc : Shader Manage, Light, Player Interface, UI  
//
//------------------------------------------------------------------
class CScene
{
private:
	vector<CShader*>		m_vtShaders;

public:
	CScene();
	~CScene();

	virtual bool OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, POINT * cursor, ID2D1DeviceContext * pDeviceContext = NULL, CPlayerShader ** playerShader = NULL);
	virtual bool OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, CPlayerShader ** playerShader = NULL);

	void BuildObjects(ID3D11Device *pd3dDevice, vector<ModelContainer*> pvtFBXDatas, deque<CCSVData*> pCSVData);
	void ReleaseObjects();

	bool ProcessInput();
	void AnimateObjects(float fTimeElapsed);
	void Render(ID3D11DeviceContext*pd3dDeviceContext, CCamera *pCamera);

	vector<CShader*>&		GetInstancingShader(void);
	CHeightMapTerrain*		GetTerrain() const;

	//-------------------------------------
	//Camera, Light Data and Function
	//-------------------------------------
private:
	LIGHTS*					m_pLights;
	ID3D11Buffer*			m_pd3dcbLights;

	CCamera*				m_pCamera;
public:
	void CreateShaderVariables(ID3D11Device *pd3dDevice, CCSVData * pCSVDatas = NULL, CHeightMapTerrain * pHeightMapTerrain = NULL);
	void UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, LIGHTS *pLights);
	void ReleaseShaderVariables();
	LIGHTS* GetLights(void) const { return m_pLights; }
	void SetCamera(CCamera *pCamera) { m_pCamera = pCamera; }


	//-------------------------------------
	//Collision, InstallTrap Function
	//-------------------------------------
public:
	bool MotionCollision(CPlayer * pPlayer, vector<CShader*>& vtInstancingShader, CCSVData * pCsvData, XMFLOAT3 * pos);
	bool MotionCollision(CPlayer * ppPlayer, vector<CShader*>& vtInstancingShader, CCSVData * pCsvData);
	bool CanInstallTrap(CPlayer * pPlayer, vector<CShader*>& vtInstancingShader);


	//-------------------------------------
	//UI Data and Function
	//-------------------------------------
protected:
	vector<ID2D1Bitmap1*>		m_vtd2dBitmap;
public:
	bool LoadImageFromFile(_TCHAR *pszstrFileName, ID2D1Bitmap1 **ppd2dBitmap, D2D_RECT_U *pd2drcImage,
		UINT nWidth, UINT nHeight, WICBitmapTransformOptions nFlipRotation, IWICImagingFactory * pwicFactory, ID2D1DeviceContext * pd2dDeviceContext);
	virtual void Build2dObjects(IDWriteFactory * pdwFactory, IWICImagingFactory * pwicFactory, ID2D1DeviceContext * pd2dDeviceContext, ID2D1Factory1 * pd2dFactory);
	virtual void Render2D(ID2D1DeviceContext * pd2dDeviceContext, IDWriteFactory * pdwFactory, ID2D1Factory1 * pd2dFactory);
};

