#include "stdafx.h"

static bool bBoundingBoxRender = true;

CScene::CScene()
{

	m_pLights = NULL;
	m_pd3dcbLights = NULL;
}

CScene::~CScene()
{
}

void CScene::CreateShaderVariables(ID3D11Device *pd3dDevice, CCSVData * pCSVDatas, CHeightMapTerrain * pHeightMapTerrain)
{
	m_pLights = new LIGHTS;
	::ZeroMemory(m_pLights, sizeof(LIGHTS));
	m_pLights->m_xmcGlobalAmbient = XMFLOAT4(0.2f, 0.2f, 0.2f, 0.5f);

	//3개의 조명(점 광원, 스팟 광원, 방향성 광원)을 설정한다.
	m_pLights->m_pLights[0].m_bEnable = 0.0f;
	m_pLights->m_pLights[0].m_nType = POINT_LIGHT;
	m_pLights->m_pLights[0].m_fRange = 300.0f;
	m_pLights->m_pLights[0].m_xmcAmbient = XMFLOAT4(0.1f, 0.0f, 0.0f, 1.0f);
	m_pLights->m_pLights[0].m_xmcDiffuse = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	m_pLights->m_pLights[0].m_xmcSpecular = XMFLOAT4(0.0f, 0.1f, 0.1f, 0.0f);
	m_pLights->m_pLights[0].m_xmf3Position = XMFLOAT3(300.0f, 300.0f, 300.0f);
	m_pLights->m_pLights[0].m_xmf3Direction = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_pLights->m_pLights[0].m_xmf3Attenuation = XMFLOAT3(1.0f, 0.001f, 0.0001f);
	m_pLights->m_pLights[1].m_bEnable = 0.0f;
	m_pLights->m_pLights[1].m_nType = SPOT_LIGHT;
	m_pLights->m_pLights[1].m_fRange = 100.0f;
	m_pLights->m_pLights[1].m_xmcAmbient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	m_pLights->m_pLights[1].m_xmcDiffuse = XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);
	m_pLights->m_pLights[1].m_xmcSpecular = XMFLOAT4(0.1f, 0.1f, 0.1f, 0.0f);
	m_pLights->m_pLights[1].m_xmf3Position = XMFLOAT3(500.0f, 300.0f, 500.0f);
	m_pLights->m_pLights[1].m_xmf3Direction = XMFLOAT3(0.0f, 0.0f, 1.0f);
	m_pLights->m_pLights[1].m_xmf3Attenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
	m_pLights->m_pLights[1].m_fFalloff = 8.0f;
	m_pLights->m_pLights[1].m_fPhi = (float)cos(D3DXToRadian(40.0f));
	m_pLights->m_pLights[1].m_fTheta = (float)cos(D3DXToRadian(20.0f));
	m_pLights->m_pLights[2].m_bEnable = 1.0f;
	m_pLights->m_pLights[2].m_nType = DIRECTIONAL_LIGHT;
	m_pLights->m_pLights[2].m_xmcAmbient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	m_pLights->m_pLights[2].m_xmcDiffuse = XMFLOAT4(0.3f, 0.3f, 0.3f, 0.5f);
	m_pLights->m_pLights[2].m_xmcSpecular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	m_pLights->m_pLights[2].m_xmf3Direction = XMFLOAT3(0.0f, -1.0f, 0.0f);
	m_pLights->m_pLights[3].m_bEnable = 0.0f;
	m_pLights->m_pLights[3].m_nType = SPOT_LIGHT;
	m_pLights->m_pLights[3].m_fRange = 60.0f;
	m_pLights->m_pLights[3].m_xmcAmbient = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	m_pLights->m_pLights[3].m_xmcDiffuse = XMFLOAT4(0.5f, 0.0f, 0.0f, 1.0f);
	m_pLights->m_pLights[3].m_xmcSpecular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	m_pLights->m_pLights[3].m_xmf3Position = XMFLOAT3(500.0f, 300.0f, 500.0f);
	m_pLights->m_pLights[3].m_xmf3Direction = XMFLOAT3(0.0f, -1.0f, 0.0f);
	m_pLights->m_pLights[3].m_xmf3Attenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
	m_pLights->m_pLights[3].m_fFalloff = 20.0f;
	m_pLights->m_pLights[3].m_fPhi = (float)cos(D3DXToRadian(40.0f));
	m_pLights->m_pLights[3].m_fTheta = (float)cos(D3DXToRadian(15.0f));

	XMFLOAT3 xmf3RotateAxis;
	for (int i = 0; i < pCSVDatas->m_nDataNum; ++i) {

		m_pLights->m_pLights[i + 4].m_bEnable = 0.0f;
		m_pLights->m_pLights[i + 4].m_nType = POINT_LIGHT;
		m_pLights->m_pLights[i + 4].m_fRange = 50.0f;
		m_pLights->m_pLights[i + 4].m_xmcAmbient = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
		m_pLights->m_pLights[i + 4].m_xmcDiffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
		m_pLights->m_pLights[i + 4].m_xmcSpecular = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
		float fHeight = pHeightMapTerrain->GetHeight(pCSVDatas->m_ipDataPositionX[i], pCSVDatas->m_ipDataPositionZ[i]);

		if (i == 0) m_pLights->m_pLights[i + 4].m_xmf3Position = XMFLOAT3(pCSVDatas->m_ipDataPositionX[i] + 1.5f, fHeight + 40.0f, pCSVDatas->m_ipDataPositionZ[i] + 15.0f);
		else if (i == 1) m_pLights->m_pLights[i + 4].m_xmf3Position = XMFLOAT3(pCSVDatas->m_ipDataPositionX[i] + 1.5f, fHeight + 40.0f, pCSVDatas->m_ipDataPositionZ[i] + 19.0f);
		else if (i == 2) m_pLights->m_pLights[i + 4].m_xmf3Position = XMFLOAT3(pCSVDatas->m_ipDataPositionX[i] + 5.0f, fHeight + 40.0f, pCSVDatas->m_ipDataPositionZ[i] + 19.0f);
		else if (i == 3) m_pLights->m_pLights[i + 4].m_xmf3Position = XMFLOAT3(pCSVDatas->m_ipDataPositionX[i] + 1.5f, fHeight + 40.0f, pCSVDatas->m_ipDataPositionZ[i] + 15.0f);
		else if (i == 4) m_pLights->m_pLights[i + 4].m_xmf3Position = XMFLOAT3(pCSVDatas->m_ipDataPositionX[i] - 5.0f, fHeight + 40.0f, pCSVDatas->m_ipDataPositionZ[i] + 15.0f);
		else if (i == 5) m_pLights->m_pLights[i + 4].m_xmf3Position = XMFLOAT3(pCSVDatas->m_ipDataPositionX[i] + 1.5f, fHeight + 40.0f, pCSVDatas->m_ipDataPositionZ[i] + 30.0f);
		else if (i == 6 || i == 7 || i == 8) m_pLights->m_pLights[i + 4].m_xmf3Position = XMFLOAT3(pCSVDatas->m_ipDataPositionX[i] + 1.5f, fHeight + 40.0f, pCSVDatas->m_ipDataPositionZ[i] + 19.0f);
		else if (i == 9) m_pLights->m_pLights[i + 4].m_xmf3Position = XMFLOAT3(pCSVDatas->m_ipDataPositionX[i] + 10.0f, fHeight + 40.0f, pCSVDatas->m_ipDataPositionZ[i] + 25.0f);

		XMFLOAT3 xmf3SurfaceNormal = pHeightMapTerrain->GetNormal(pCSVDatas->m_ipDataPositionX[i], pCSVDatas->m_ipDataPositionZ[i]);
		m_pLights->m_pLights[i + 4].m_xmf3Direction = XMFLOAT3(-xmf3SurfaceNormal.x, -1.0f, -xmf3SurfaceNormal.z);
		m_pLights->m_pLights[i + 4].m_fFalloff = 1.0f;
		m_pLights->m_pLights[i + 4].m_fPhi = (float)cos(D3DXToRadian(20.0f));
		m_pLights->m_pLights[i + 4].m_xmf3Attenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
		m_pLights->m_pLights[i + 4].m_fTheta = (float)cos(D3DXToRadian(10.0f));
	}

	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(d3dBufferDesc));
	d3dBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	d3dBufferDesc.ByteWidth = sizeof(LIGHTS);
	d3dBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	d3dBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pLights;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dcbLights);
}

void CScene::ReleaseShaderVariables()
{
	if (m_pLights)
		delete m_pLights;
	if (m_pd3dcbLights)
		m_pd3dcbLights->Release();
}

void CScene::UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, LIGHTS *pLights)
{
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	pd3dDeviceContext->Map(m_pd3dcbLights, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	LIGHTS *pcbLight = (LIGHTS *)d3dMappedResource.pData;
	memcpy(pcbLight, pLights, sizeof(LIGHTS));
	pd3dDeviceContext->Unmap(m_pd3dcbLights, 0);
	pd3dDeviceContext->PSSetConstantBuffers(PS_SLOT_LIGHT, 1, &m_pd3dcbLights);
}

void CScene::BuildObjects(ID3D11Device *pd3dDevice, vector<ModelContainer*> pvtFBXDatas, deque<CCSVData*> pCSVData)
{
#ifdef FBX
	m_vtShaders.resize(15, nullptr);

	m_vtShaders[0] = new CSkyBoxShader();
	m_vtShaders[0]->CreateShader(pd3dDevice);
	m_vtShaders[0]->BuildObjects(pd3dDevice);

	m_vtShaders[1] = new CTerrainShader();
	m_vtShaders[1]->CreateShader(pd3dDevice);
	m_vtShaders[1]->BuildObjects(pd3dDevice);

	m_vtShaders[2] = new CFBXInstancingShader();
	m_vtShaders[2]->CreateShader(pd3dDevice);
	m_vtShaders[2]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[Car1], pCSVData[Car1], GetTerrain());

	m_vtShaders[3] = new CFBXInstancingShader();
	m_vtShaders[3]->CreateShader(pd3dDevice);
	m_vtShaders[3]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[Tree1], pCSVData[Tree1], GetTerrain());

	m_vtShaders[4] = new CFBXInstancingShader();
	m_vtShaders[4]->CreateShader(pd3dDevice);
	m_vtShaders[4]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[Fence], pCSVData[Fence], GetTerrain());

	m_vtShaders[5] = new CFBXInstancingShader();
	m_vtShaders[5]->CreateShader(pd3dDevice);
	m_vtShaders[5]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[Drum], pCSVData[Drum], GetTerrain());

	m_vtShaders[6] = new CFBXInstancingShader();
	m_vtShaders[6]->CreateShader(pd3dDevice);
	m_vtShaders[6]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[Bus], pCSVData[Bus], GetTerrain());

	m_vtShaders[7] = new CFBXInstancingShader();
	m_vtShaders[7]->CreateShader(pd3dDevice);
	m_vtShaders[7]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[Truck], pCSVData[Truck], GetTerrain());

	m_vtShaders[8] = new CFBXInstancingShader();
	m_vtShaders[8]->CreateShader(pd3dDevice);
	m_vtShaders[8]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[Generator], pCSVData[Generator], GetTerrain(), this);

	m_vtShaders[9] = new CFBXInstancingShader();
	m_vtShaders[9]->CreateShader(pd3dDevice);
	m_vtShaders[9]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[Bark], pCSVData[Bark], GetTerrain());

	m_vtShaders[10] = new CFBXInstancingShader();
	m_vtShaders[10]->CreateShader(pd3dDevice);
	m_vtShaders[10]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[Escapedoor1], pCSVData[Escapedoor1], GetTerrain());

	m_vtShaders[11] = new CFBXInstancingShader();
	m_vtShaders[11]->CreateShader(pd3dDevice);
	m_vtShaders[11]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[Escapedoor2], pCSVData[Escapedoor2], GetTerrain());

	m_vtShaders[12] = new CFBXInstancingShader();
	m_vtShaders[12]->CreateShader(pd3dDevice);
	m_vtShaders[12]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[Stone], pCSVData[Stone], GetTerrain());

	m_vtShaders[13] = new CFBXInstancingShader();
	m_vtShaders[13]->CreateShader(pd3dDevice);
	m_vtShaders[13]->BuildInstanceObjects(pd3dDevice, pvtFBXDatas[StreetLight], pCSVData[StreetLight], GetTerrain());

	m_vtShaders[14] = new CFogShader();
	m_vtShaders[14]->CreateShader(pd3dDevice);
	m_vtShaders[14]->BuildObjects(pd3dDevice);

#else
	//3.3
	m_nShaders = 1;
	m_ppShaders = new CShader*[m_nShaders];

	m_ppShaders[0] = new CTerrainShader();
	m_ppShaders[0]->CreateShader(pd3dDevice);
	m_ppShaders[0]->BuildObjects(pd3dDevice);
#endif

	//조명설정
	CreateShaderVariables(pd3dDevice, pCSVData[Generator], GetTerrain());
}

vector<CShader*>& CScene::GetInstancingShader(void) 
{
	return m_vtShaders;
}

void CScene::ReleaseObjects()
{
	ReleaseShaderVariables();

	for (int j = 0; j < m_vtShaders.size(); j++)
	{
		if (m_vtShaders[j]) m_vtShaders[j]->ReleaseObjects();
		if (m_vtShaders[j]) delete m_vtShaders[j];
	}
}

bool CScene::LoadImageFromFile(_TCHAR *pszstrFileName, ID2D1Bitmap1 **ppd2dBitmap, D2D_RECT_U *pd2drcImage, UINT nWidth, UINT nHeight,
	WICBitmapTransformOptions nFlipRotation, IWICImagingFactory * pwicFactory, ID2D1DeviceContext * pd2dDeviceContext) {
	HRESULT hResult;
	IWICBitmapDecoder *pwicBitmapDecoder = NULL;
	if (FAILED(hResult = pwicFactory->CreateDecoderFromFilename(pszstrFileName, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pwicBitmapDecoder))) return(false);
	IWICBitmapFrameDecode *pwicBitmapFrameDecode = NULL;
	if (FAILED(hResult = pwicBitmapDecoder->GetFrame(0, &pwicBitmapFrameDecode))) return(false);
	IWICBitmapSource *pwicSource = pwicBitmapFrameDecode;
	UINT nImageWidth, nImageHeight;
	if (FAILED(hResult = pwicSource->GetSize(&nImageWidth, &nImageHeight))) return(false);
	IWICFormatConverter   *pwicFormatConverter = NULL;
	IWICBitmapScaler *pwicScaler = NULL;
	IWICBitmapClipper *pwicClipper = NULL;
	IWICBitmapFlipRotator *pwicFlipRotator = NULL;
	if (pd2drcImage)
	{
		if (pd2drcImage->left < 0) pd2drcImage->left = 0;
		if (pd2drcImage->top < 0) pd2drcImage->top = 0;
		if (pd2drcImage->right > nImageWidth) pd2drcImage->right = nImageWidth;
		if (pd2drcImage->bottom > nImageHeight) pd2drcImage->bottom = nImageHeight;
		WICRect wicRect = { pd2drcImage->left, pd2drcImage->top, (pd2drcImage->right - pd2drcImage->left), (pd2drcImage->bottom - pd2drcImage->top) };
		if (FAILED(hResult = pwicFactory->CreateBitmapClipper(&pwicClipper))) return(false);
		if (FAILED(hResult = pwicClipper->Initialize(pwicSource, &wicRect))) return(false);
		pwicSource = pwicClipper;
	}
	if ((nWidth != 0) || (nHeight != 0))
	{
		if (nWidth == 0) nWidth = UINT(float(nHeight) / float(nImageHeight) * float(nImageWidth));
		if (nHeight == 0) nHeight = UINT(float(nWidth) / float(nImageWidth) * float(nImageHeight));
		if (FAILED(hResult = pwicFactory->CreateBitmapScaler(&pwicScaler))) return(false);
		if (FAILED(hResult = pwicScaler->Initialize(pwicSource, nWidth, nHeight, WICBitmapInterpolationModeCubic))) return(false);
		pwicSource = pwicScaler;
	}
	if (nFlipRotation != WICBitmapTransformRotate0)
	{
		if (FAILED(hResult = pwicFactory->CreateBitmapFlipRotator(&pwicFlipRotator))) return(false);
		if (FAILED(hResult = pwicFlipRotator->Initialize(pwicSource, nFlipRotation))) return(false);
		pwicSource = pwicFlipRotator;
	}
	if (FAILED(hResult = pwicFactory->CreateFormatConverter(&pwicFormatConverter))) return(false);
	if (FAILED(hResult = pwicFormatConverter->Initialize(pwicSource, GUID_WICPixelFormat32bppPBGRA, WICBitmapDitherTypeNone, NULL, 0.f, WICBitmapPaletteTypeMedianCut))) return(false);
	if (FAILED(hResult = pd2dDeviceContext->CreateBitmapFromWicBitmap(pwicFormatConverter, NULL, ppd2dBitmap))) return(false);

	if (pwicBitmapFrameDecode) pwicBitmapFrameDecode->Release();
	if (pwicBitmapDecoder) pwicBitmapDecoder->Release();
	if (pwicFormatConverter) pwicFormatConverter->Release();
	if (pwicClipper) pwicClipper->Release();
	if (pwicScaler) pwicScaler->Release();
	if (pwicFlipRotator) pwicFlipRotator->Release();

	return(true);
}

bool CScene::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, POINT * cursor, ID2D1DeviceContext * pDeviceContext, CPlayerShader ** playerShader)
{
	return(false);
}

bool CScene::CanInstallTrap(CPlayer * pPlayer, vector<CShader*>& vtInstancingShader)
{
	BoundingBox motionAABB = pPlayer->GetMotionAABB();

	for (int i = 2; i < 13; i++)
	{
		CGameObject ** ppObjects = vtInstancingShader[i]->GetppObjects();
		for (int j = 0; j < vtInstancingShader[i]->GetObjectNum(); j++)
		{
			ContainmentType containType = motionAABB.Contains(ppObjects[j]->GetAABB());
			switch (containType)
			{
			case DISJOINT: {
				continue;
			}
			case INTERSECTS: {

				return true;
			}
			case CONTAINS: {

				return true;
			}
			}
		}
	}
	return false;
}

bool CScene::MotionCollision(CPlayer * pPlayer, vector<CShader*>& ppInstancingShader, CCSVData * pCsvData)
{
	BoundingBox motionAABB = pPlayer->GetMotionAABB();
	CGameObject ** ppObjects = ppInstancingShader[8]->GetppObjects();
	int nObjects = pCsvData->m_nDataNum;

	if (pPlayer->GetMotioning()) {
		for (int i = 0; i < nObjects; i++)
		{
			ContainmentType containType = motionAABB.Contains(ppObjects[i]->GetAABB());
			switch (containType)
			{
			case DISJOINT: {
				continue;
			}
			case INTERSECTS: {
				cout << "GEN INTERSECTS" << endl;
				return true;
			}
			case CONTAINS: {
				cout << "GEN CONTAINS" << endl;
				return true;
			}
			default:
				break;
			}
		}
	}
	return false;
}

bool CScene::MotionCollision(CPlayer * pPlayer, vector<CShader*>& ppInstancingShader, CCSVData * pCsvData, XMFLOAT3 * pos)
{
	BoundingBox motionAABB = pPlayer->GetMotionAABB();
	CGameObject ** ppObjects = ppInstancingShader[8]->GetppObjects();
	int nObjects = pCsvData->m_nDataNum;

	for (int i = 0; i < nObjects; i++)
	{
		ContainmentType containType = motionAABB.Contains(ppObjects[i]->GetAABB());
		switch (containType)
		{
		case DISJOINT: {
			continue;
		}
		case INTERSECTS: {
			cout << "GEN INTERSECTS" << endl;
			pos->x = ppObjects[i]->GetPosition().x;
			pos->y = ppObjects[i]->GetPosition().y;
			pos->z = ppObjects[i]->GetPosition().z;
			return true;
		}
		case CONTAINS: {
			cout << "GEN CONTAINS" << endl;
			pos->x = ppObjects[i]->GetPosition().x;
			pos->y = ppObjects[i]->GetPosition().y;
			pos->z = ppObjects[i]->GetPosition().z;
			return true;
		}
		default:
			break;
		}
	}
	return false;
}

bool CScene::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, CPlayerShader ** playerShader)
{
	CPlayer * pMy = playerShader[0]->GetPlayer(0);
	auto role = pMy->m_pPlayerInfo->GetROLE();

	switch (nMessageID)
	{
	case WM_KEYDOWN:
		break;
	case WM_KEYUP:
	{
		switch (wParam) {
		case '9':
			bBoundingBoxRender = (bBoundingBoxRender + 1) % 2;
			break;
		}
	}
	default:
		break;
	}

	return(false);
}

bool CScene::ProcessInput()
{
	return(false);
}

void CScene::AnimateObjects(float fTimeElapsed)
{
	if (m_pLights && m_pd3dcbLights)
	{
		//현재 카메라의 위치 벡터를 조명을 나타내는 상수 버퍼에 설정한다.
		XMFLOAT3 xmf3CameraPosition = m_pCamera->GetPosition();
		m_pLights->m_xmf4CameraPosition = XMFLOAT4(xmf3CameraPosition.x, xmf3CameraPosition.y, xmf3CameraPosition.z, 1.0f);

		//점 조명이 지형의 중앙을 중심으로 회전하도록 설정한다.
		CHeightMapTerrain *pTerrain = GetTerrain();
		static XMFLOAT3 xmf3Rotated = XMFLOAT3(pTerrain->GetWidth()*0.3f, 0.0f, 0.0f);
		XMFLOAT4X4 xmf4x4Rotate;
		xmf4x4Rotate = Matrix4x4::RotationYawPitchRoll((float)D3DXToRadian(30.0f*fTimeElapsed), 0.0f, 0.0f);
		xmf3Rotated = Vector3::TransformCoord(xmf3Rotated, xmf4x4Rotate);
		XMFLOAT3 xmf3TerrainCenter = XMFLOAT3(pTerrain->GetWidth() * 0.5f, pTerrain->GetPeakHeight() + 10.0f, pTerrain->GetLength()*0.5f);

		m_pLights->m_pLights[0].m_xmf3Position = Vector3::Add(xmf3TerrainCenter, xmf3Rotated);
		m_pLights->m_pLights[0].m_fRange = pTerrain->GetPeakHeight();

		/*두 번째 조명은 플레이어가 가지고 있는 손전등(스팟 조명)이다. 그러므로 플레이어의 위치와 방향이 바뀌면 현재 플레이어의 위치와 z-축 방향 벡터를 스팟 조명의 위치와 방향으로 설정한다.*/
		CPlayer *pPlayer = m_pCamera->GetPlayer();
		//캐릭터가 가지고 있는 스팟조명
		m_pLights->m_pLights[1].m_xmf3Position = pPlayer->GetPosition();
		m_pLights->m_pLights[1].m_xmf3Direction = pPlayer->GetLookVector();

		//위에서 캐릭터를 향하는 스팟조명
		m_pLights->m_pLights[3].m_xmf3Position = Vector3::Add(pPlayer->GetPosition(), XMFLOAT3(0.0f, 40.0f, 0.0f));
	}

	for (int i = 0; i < m_vtShaders.size(); i++)
	{
		m_vtShaders[i]->AnimateObjects(fTimeElapsed);
	}
}

void CScene::Render(ID3D11DeviceContext*pd3dDeviceContext, CCamera *pCamera)
{
	if (m_pLights && m_pd3dcbLights) 
		UpdateShaderVariable(pd3dDeviceContext, m_pLights);

	for (int i = 0; i < m_vtShaders.size(); i++)
	{
		m_vtShaders[i]->Render(pd3dDeviceContext, pCamera);
	}
}

void CScene::Build2dObjects(IDWriteFactory * pdwFactory, IWICImagingFactory * pwicFactory, ID2D1DeviceContext * pd2dDeviceContext, ID2D1Factory1 * pd2dFactory)
{
	HRESULT result;

	m_vtd2dBitmap.resize(19,nullptr);

	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameSurvivorHP.png", &m_vtd2dBitmap[0], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);;

	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameNum0.png", &m_vtd2dBitmap[1], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameSlash.png", &m_vtd2dBitmap[2], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameNum10.png", &m_vtd2dBitmap[3], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);

	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameTrap.png", &m_vtd2dBitmap[4], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);

	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameNum1.png", &m_vtd2dBitmap[5], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameNum2.png", &m_vtd2dBitmap[6], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameNum3.png", &m_vtd2dBitmap[7], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameNum4.png", &m_vtd2dBitmap[8], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameNum5.png", &m_vtd2dBitmap[9], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameNum6.png", &m_vtd2dBitmap[10], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameNum7.png", &m_vtd2dBitmap[11], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameNum8.png", &m_vtd2dBitmap[12], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGameNum9.png", &m_vtd2dBitmap[13], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);

	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGamePlayerState.png", &m_vtd2dBitmap[14], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);

	result = LoadImageFromFile(L"../Data/BesideYouData/UI/Survivor.png", &m_vtd2dBitmap[15], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/Murderer.png", &m_vtd2dBitmap[16], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);

	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGamePlayerStateInjured.png", &m_vtd2dBitmap[17], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);

	result = LoadImageFromFile(L"../Data/BesideYouData/UI/InGamePlayerTraped.png", &m_vtd2dBitmap[18], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);

}

void CScene::Render2D(ID2D1DeviceContext * pd2dDeviceContext, IDWriteFactory * pdwFactory, ID2D1Factory1 * pd2dFactory)
{
	pd2dDeviceContext->BeginDraw();

	float width = pd2dDeviceContext->GetSize().width;
	float height = pd2dDeviceContext->GetSize().height;

	ID2D1SolidColorBrush * pd2dsbrWhiteColor;
	IDWriteTextFormat * dwMyChattingFormat;

	pd2dDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White, 1.0f), &pd2dsbrWhiteColor);

	pdwFactory->CreateTextFormat(L"맑은고딕", nullptr, DWRITE_FONT_WEIGHT_EXTRA_BLACK, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 20.0f, L"ko-ko", &dwMyChattingFormat);

	auto players = CPlayerManager::getInstance().players();
	size_t cnt = (*players).size();

	int genNum = 0;

	if ((*players).front()->m_pPlayerInfo->GetROLE() == SURVIVOR) {
		for (auto j = CPlayerManager::getInstance().m_mpGenlist.begin(); j != CPlayerManager::getInstance().m_mpGenlist.end(); ++j)
		{
			if ((j->second->IsComplete())) genNum++;
		}
		if (genNum == 0) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[1], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 1) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[5], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 2) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[6], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 3) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[7], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 4) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[8], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 5) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[9], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 6) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[10], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 7) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[11], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 8) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[12], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 9) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[13], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 10) {
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[3], D2D1::RectF(NormalCoordX(0.81, width), NormalCoordY(0.039, height), NormalCoordX(0.9, width), NormalCoordY(0.169, height)), 0.9f);
		}

		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[3], D2D1::RectF(NormalCoordX(0.898, width), NormalCoordY(0.039, height), NormalCoordX(0.996, width), NormalCoordY(0.169, height)), 0.9f);
		//Slash
		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[2], D2D1::RectF(NormalCoordX(0.888, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);

		//SurvivorHP
		if ((*players).front()->m_pPlayerInfo->GetHP() == 1) {
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[0], D2D1::RectF(NormalCoordX(0.009, width), NormalCoordY(0.039, height), NormalCoordX(0.078, width), NormalCoordY(0.13, height)), 0.9f);
			//Injured상태 출력
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[17], D2D1::RectF(NormalCoordX(0.019, width), NormalCoordY(0.82, height), NormalCoordX(0.149, width), NormalCoordY(0.911, height)), 0.9f);
		}
		if ((*players).front()->m_pPlayerInfo->GetHP() == 2) {
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[0], D2D1::RectF(NormalCoordX(0.009, width), NormalCoordY(0.039, height), NormalCoordX(0.078, width), NormalCoordY(0.13, height)), 0.9f);
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[0], D2D1::RectF(NormalCoordX(0.087, width), NormalCoordY(0.039, height), NormalCoordX(0.156, width), NormalCoordY(0.13, height)), 0.9f);
		}
		if ((*players).front()->m_pPlayerInfo->GetHP() == 3) {
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[0], D2D1::RectF(NormalCoordX(0.009, width), NormalCoordY(0.039, height), NormalCoordX(0.078, width), NormalCoordY(0.13, height)), 0.9f);
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[0], D2D1::RectF(NormalCoordX(0.087, width), NormalCoordY(0.039, height), NormalCoordX(0.156, width), NormalCoordY(0.13, height)), 0.9f);
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[0], D2D1::RectF(NormalCoordX(0.166, width), NormalCoordY(0.039, height), NormalCoordX(0.234, width), NormalCoordY(0.13, height)), 0.9f);
		}
		
		//트랩 밟았을때
		if ((*players).front()->m_pPlayerInfo->AnimationType() == TYPE_STEPONTRAP)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[18], D2D1::RectF(NormalCoordX(0.019, width), NormalCoordY(0.82, height), NormalCoordX(0.149, width), NormalCoordY(0.911, height)), 0.9f);

		//Trap부분
		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[4], D2D1::RectF(NormalCoordX(0.849, width), NormalCoordY(0.781, height), NormalCoordX(0.957, width), NormalCoordY(0.898, height)), 0.8f);
		if ((*players).front()->m_pPlayerInfo->GetTrap() == 5)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[9], D2D1::RectF(NormalCoordX(0.917, width), NormalCoordY(0.872, height), NormalCoordX(0.976, width), NormalCoordY(0.924, height)), 0.9f);
		if ((*players).front()->m_pPlayerInfo->GetTrap() == 4)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[8], D2D1::RectF(NormalCoordX(0.917, width), NormalCoordY(0.872, height), NormalCoordX(0.976, width), NormalCoordY(0.924, height)), 0.9f);
		if ((*players).front()->m_pPlayerInfo->GetTrap() == 3)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[7], D2D1::RectF(NormalCoordX(0.917, width), NormalCoordY(0.872, height), NormalCoordX(0.976, width), NormalCoordY(0.924, height)), 0.9f);
		if ((*players).front()->m_pPlayerInfo->GetTrap() == 2)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[6], D2D1::RectF(NormalCoordX(0.917, width), NormalCoordY(0.872, height), NormalCoordX(0.976, width), NormalCoordY(0.924, height)), 0.9f);
		if ((*players).front()->m_pPlayerInfo->GetTrap() == 1)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[5], D2D1::RectF(NormalCoordX(0.917, width), NormalCoordY(0.872, height), NormalCoordX(0.976, width), NormalCoordY(0.924, height)), 0.9f);
		if ((*players).front()->m_pPlayerInfo->GetTrap() == 0)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[1], D2D1::RectF(NormalCoordX(0.917, width), NormalCoordY(0.872, height), NormalCoordX(0.976, width), NormalCoordY(0.924, height)), 0.9f);

		//PlayerState1부분
		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[14], D2D1::RectF(NormalCoordX(0.009, width), NormalCoordY(0.924, height), NormalCoordX(0.126, width), NormalCoordY(0.95, height)), 0.5f);
		//PlayerID 렌더링
		pd2dDeviceContext->DrawTextW(RBPlayerManager::getInstance().at_own()->name(), RBPlayerManager::getInstance().at_own()->sizeOfName(), dwMyChattingFormat, D2D1::RectF(NormalCoordX(0.012, width), NormalCoordY(0.924, height), NormalCoordX(0.2, width), NormalCoordY(0.95, height)), pd2dsbrWhiteColor);
		//PlayerState2
		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[14], D2D1::RectF(NormalCoordX(0.141, width), NormalCoordY(0.924, height), NormalCoordX(0.258, width), NormalCoordY(0.95, height)), 0.5f);
		//PlayerState3
		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[14], D2D1::RectF(NormalCoordX(0.273, width), NormalCoordY(0.924, height), NormalCoordX(0.39, width), NormalCoordY(0.95, height)), 0.5f);
	}

	else {
		for (auto j = CPlayerManager::getInstance().m_mpGenlist.begin(); j != CPlayerManager::getInstance().m_mpGenlist.end(); ++j)
		{
			if ((j->second->IsComplete())) genNum++;
		}
		if (genNum == 0) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[1], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 1) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[5], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 2) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[6], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 3) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[7], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 4) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[8], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 5) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[9], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 6) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[10], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 7) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[11], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 8) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[12], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 9) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[13], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		if (genNum == 10) {
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[3], D2D1::RectF(NormalCoordX(0.82, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);
		}
		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[3], D2D1::RectF(NormalCoordX(0.898, width), NormalCoordY(0.039, height), NormalCoordX(0.996, width), NormalCoordY(0.169, height)), 0.9f);
		//Slash
		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[2], D2D1::RectF(NormalCoordX(0.888, width), NormalCoordY(0.039, height), NormalCoordX(0.917, width), NormalCoordY(0.169, height)), 0.9f);

		auto player = CPlayerManager::getInstance().players();
		for (auto start = (*players).begin(); start != (*players).end(); ++start)
		{
			
			//PlayerState1
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[14], D2D1::RectF(NormalCoordX(0.009, width), NormalCoordY(0.924, height), NormalCoordX(0.126, width), NormalCoordY(0.95, height)), 0.5f);
			if ( (*start)->m_pPlayerInfo->GetROLE() == SURVIVOR)
				//PlayerID 렌더링
				pd2dDeviceContext->DrawTextW((*start)->m_pPlayerInfo->GetUserNAME().c_str(), (*start)->m_pPlayerInfo->GetUserNAME().size(), dwMyChattingFormat, D2D1::RectF(NormalCoordX(0.012, width), NormalCoordY(0.924, height), NormalCoordX(0.2, width), NormalCoordY(0.95, height)), pd2dsbrWhiteColor);
			//PlayerState2
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[14], D2D1::RectF(NormalCoordX(0.141, width), NormalCoordY(0.924, height), NormalCoordX(0.258, width), NormalCoordY(0.95, height)), 0.5f);
			//PlayerState3
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[14], D2D1::RectF(NormalCoordX(0.273, width), NormalCoordY(0.924, height), NormalCoordX(0.39, width), NormalCoordY(0.95, height)), 0.5f);
		}
	}

	pd2dDeviceContext->EndDraw();
}

CHeightMapTerrain* CScene::GetTerrain() const
{
	CTerrainShader* pTerrainShader = (CTerrainShader*)m_vtShaders[1];
	return (pTerrainShader->GetTerrain());
}


