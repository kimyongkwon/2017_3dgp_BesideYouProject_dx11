#include"stdafx.h"
#include "SoundUtil.h"

SoundUtil SoundEngine::mSoundUtil;

SoundUtil::SoundUtil() {}

SoundUtil::~SoundUtil()
{
	mFmodSystem->release();
	mFmodSystem->close();
}

void SoundUtil::Initalize()
{
	mResult = System_Create(&mFmodSystem);
	ErrorCheck(mResult);

	mResult = mFmodSystem->init(MAX_SOUND_COUNT, FMOD_INIT_NORMAL, NULL);
	ErrorCheck(mResult);
	mVolume = 0.5f;

	for (int index = 0; index < MAX_SOUND_COUNT; ++index)
	{
		mFmodChannel[index]->setVolume(mVolume);
	}
}

void SoundUtil::Loading(const char* inFileName, FMOD_MODE inMode, int inSoundType)
{
	mResult = mFmodSystem->createSound(inFileName, inMode, NULL, &mSound[inSoundType]);
	ErrorCheck(mResult);
}
void SoundUtil::LoopLoading(const char* inFileName, FMOD_MODE inMode, int inSoundType)
{
	mResult = mFmodSystem->createSound(inFileName, inMode, NULL, &mSound[inSoundType]);
	ErrorCheck(mResult);
}

void SoundUtil::Play(int32_t inSoundIndex, int32_t inSoundChannel)
{
	mFmodSystem->update();
	mResult = mFmodSystem->playSound(FMOD_CHANNEL_FREE, mSound[inSoundIndex], false, &mFmodChannel[inSoundChannel]);
	ErrorCheck(mResult);
}

void SoundUtil::Stop(int32_t inChannel)
{
	mFmodChannel[inChannel]->stop();
}

void SoundUtil::Paused(int32_t inChannel)
{
	bool pause;
	mFmodChannel[inChannel]->getPaused(&pause);
	if (pause)
		mFmodChannel[inChannel]->setPaused(false);
	else
		mFmodChannel[inChannel]->setPaused(true);
}

bool SoundUtil::isPlaying(int32_t inChannel)
{
	bool isplaying;
	mResult = mFmodChannel[inChannel]->isPlaying(&isplaying);
	ErrorCheck(mResult);

	//플레이중이다.
	if (isplaying)
		return true;
	//플레이 중 아님
	else
		return false;
}

void SoundUtil::ErrorCheck(FMOD_RESULT inResult)
{
	if (inResult != FMOD_OK)
	{
		TCHAR szStr[256] = { 0 };
		MultiByteToWideChar(CP_ACP, NULL, FMOD_ErrorString(inResult), -1, (LPWSTR)szStr, 256);
	}
}

SoundEngine::SoundEngine()
{
	mSoundUtil.Initalize();
	this->initalize();
}

void SoundEngine::initalize()
{
	mSoundUtil.Loading("../Data/Sound/Lobby_sound.mp3", FMOD_DEFAULT, 0);
	mSoundUtil.Loading("../Data/Sound/Ingame_sound.mp3", FMOD_DEFAULT, 1);
	mSoundUtil.Loading("../Data/Sound/Shot_sound.mp3", FMOD_DEFAULT, 2);
	mSoundUtil.Loading("../Data/Sound/Attacked(short)_sound.mp3", FMOD_DEFAULT, 3);
	mSoundUtil.Loading("../Data/Sound/StartGeneratorON_sound.mp3", FMOD_DEFAULT, 4);
	mSoundUtil.Loading("../Data/Sound/GeneratingON_sound.mp3", FMOD_DEFAULT, 5);
	mSoundUtil.Loading("../Data/Sound/FootStep_sound.mp3", FMOD_DEFAULT, 6);
	mSoundUtil.Loading("../Data/Sound/InstallTrap_sound.mp3", FMOD_DEFAULT, 7);
	mSoundUtil.Loading("../Data/Sound/StepOnTrap_sound.wav", FMOD_DEFAULT, 8);
	mSoundUtil.Loading("../Data/Sound/AfterGeneratorON_sound.mp3", FMOD_DEFAULT, 9);

	mSoundFunc.insert(make_pair(static_cast<int>(MAIN_SOUND), &SoundEngine::mainOn));
	mSoundFunc.insert(make_pair(static_cast<int>(INGAME_SOUND), &SoundEngine::inGameOn));
	mSoundFunc.insert(make_pair(static_cast<int>(HITTED), &SoundEngine::HITTEDOn));
	mSoundFunc.insert(make_pair(static_cast<int>(ATT), &SoundEngine::ATTOn));
	mSoundFunc.insert(make_pair(static_cast<int>(STARTGENON), &SoundEngine::startGeneratorOn));
	mSoundFunc.insert(make_pair(static_cast<int>(GENERATING), &SoundEngine::Generating));
	mSoundFunc.insert(make_pair(static_cast<int>(RUN), &SoundEngine::Run));
	mSoundFunc.insert(make_pair(static_cast<int>(INSTALLTRAP), &SoundEngine::InstallTrap));
	mSoundFunc.insert(make_pair(static_cast<int>(STEPONTRAP), &SoundEngine::StepOnTrap));
	mSoundFunc.insert(make_pair(static_cast<int>(AFTERGENON), &SoundEngine::AfterGeneratorOn));

	mSoundFunc.insert(make_pair(static_cast<int>(WIN), &SoundEngine::WINOn));
	mSoundFunc.insert(make_pair(static_cast<int>(DEF), &SoundEngine::DEFOn));
	mSoundFunc.insert(make_pair(static_cast<int>(OPENDOOR), &SoundEngine::OPENDOOROn));
	mSoundFunc.insert(make_pair(static_cast<int>(EMERGENCY), &SoundEngine::EMERGENCYOn));
	mSoundFunc.insert(make_pair(static_cast<int>(TRAPED), &SoundEngine::TRAPEDOn));
	mSoundFunc.insert(make_pair(static_cast<int>(NEARONE), &SoundEngine::FNEARONEOn));
	mSoundFunc.insert(make_pair(static_cast<int>(NEARTWO), &SoundEngine::FNEARTWOOn));
	mSoundFunc.insert(make_pair(static_cast<int>(NEARTHR), &SoundEngine::FNEARTHREEOn));
}

void SoundEngine::execute(int32_t inSoundKey, bool isOn, bool isLoop)
{
	auto result = mSoundFunc[inSoundKey];
	result(isLoop, isOn);
}

void SoundEngine::mainOn(bool isloop, bool ison)
{
	ison ? mSoundUtil.Play(0, 0) : mSoundUtil.Stop(0);
}

void SoundEngine::inGameOn(bool isloop, bool ison)
{
	ison ? mSoundUtil.Play(1, 1) : mSoundUtil.Stop(1);
}

void SoundEngine::ATTOn(bool isloop, bool ison)
{
	ison ? mSoundUtil.Play(2, 2) : mSoundUtil.Stop(2);
}

void SoundEngine::HITTEDOn(bool isloop, bool ison)
{
	ison ? mSoundUtil.Play(3, 3) : mSoundUtil.Stop(3);
}

void SoundEngine::startGeneratorOn(bool isloop, bool ison)
{
	ison ? mSoundUtil.Play(4, 4) : mSoundUtil.Stop(4);
}

void SoundEngine::Generating(bool isloop, bool ison)
{
	ison ? mSoundUtil.Play(5, 5) : mSoundUtil.Stop(5);
}

void SoundEngine::Run(bool isloop, bool ison)
{
	ison ? mSoundUtil.Play(6, 6) : mSoundUtil.Stop(6);
}

void SoundEngine::InstallTrap(bool isloop, bool ison)
{
	ison ? mSoundUtil.Play(7, 7) : mSoundUtil.Stop(7);
}

void SoundEngine::StepOnTrap(bool isloop, bool ison)
{
	ison ? mSoundUtil.Play(8, 8) : mSoundUtil.Stop(8);
}

void SoundEngine::AfterGeneratorOn(bool isloop, bool ison)
{
	ison ? mSoundUtil.Play(9, 9) : mSoundUtil.Stop(9);
}

void SoundEngine::WINOn(bool isloop, bool ison)
{
}

void SoundEngine::DEFOn(bool isloop, bool ison)
{
}

void SoundEngine::OPENDOOROn(bool isloop, bool ison)
{
}

void SoundEngine::EMERGENCYOn(bool isloop, bool ison)
{
}

void SoundEngine::TRAPEDOn(bool isloop, bool ison)
{
}

void SoundEngine::FNEARONEOn(bool isloop, bool ison)
{
}

void SoundEngine::FNEARTWOOn(bool isloop, bool ison)
{
}

void SoundEngine::FNEARTHREEOn(bool isloop, bool ison)
{
}
