#pragma once

//애니메이션이 없는 데이터
class CAssetMesh : public CMeshTexturedIlluminated
{
	unsigned int m_uitype;
public:
	enum { Common = 0, CharacterWeapon = 1 };
	CAssetMesh(ID3D11Device *pd3dDevice, const std::string& filename, unsigned int type = CAssetMesh::Common);
	virtual ~CAssetMesh();
};
