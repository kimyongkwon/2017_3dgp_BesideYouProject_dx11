#pragma once
#include"stdafx.h"

class PacketAfterTreatment : public AfterTreatment
{
private:
	void initalize();
public:
	PacketAfterTreatment();
	~PacketAfterTreatment();
	void transport(Packet * rowPacket);
private:
	static void SPacket_ROOMSTATE(Packet * rowPacket);
	static void SPacket_ROOMSEARCHNOTROLE(Packet * rowPacket);
	static void SPacket_SELECTPARTSUCC(Packet * rowPacket);
	static void SPacket_JOINSUCC(Packet * rowPacket);
	static void SPacket_JOINFAIL(Packet * rowPacket);
	static void SPacket_LOGINSUCC(Packet * rowPacket);
	static void SPacket_LOGINFAIL(Packet * rowPacket);
	static void SPacket_OUTROOMSECC(Packet * rowPacket);
	static void SPacket_ENTERROOMSUCC(Packet * rowPacket);
	static void SPacket_ENTERROOMFAIL(Packet * rowPacket);
	static void SPacket_GAMESTARTSUCC(Packet * rowPacket);
	static void NPacket_FIRSTSIGNAL(Packet * rowPacket);
	static void SPacket_FIRSTUSERDATA(Packet * rowPacket);
	static void SPacket_LATENCTCHECK(Packet * rowPacket);
	static void SPacket_LATENCY(Packet * rowPacket);
	static void SPacket_PLAYERSPOS(Packet * rowPacket);
	static void SPacket_HELLO(Packet * rowPacket);

	// UPDATE
	static void SPacket_GOTOINGAME(Packet * rowPacket);
	static void SPacket_LASTMOVETIMESTAMP(Packet * rowPacket);

	static void SPacket_WIN(Packet * rowPacket);
	static void SPacket_DEFEAT(Packet * rowPacket);
	
};