#include "stdafx.h"

bool bMurdererSelectOK = false;
bool bSurvivorSelectOK = false;
bool bLobbyNextOK = false;

CLobbyScene::CLobbyScene()
{
}

CLobbyScene::~CLobbyScene()
{
}

bool CLobbyScene::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, POINT * cursorPos, ID2D1DeviceContext * pDeviceContext, CPlayerShader ** playerShader)
{
	float width = pDeviceContext->GetSize().width;
	float height = pDeviceContext->GetSize().height;

	switch (nMessageID)
	{
	case WM_MOUSEMOVE:
		if ((bMurdererSelectOK || bSurvivorSelectOK) && (cursorPos->x > NormalCoordX(0.346, width) && cursorPos->x < NormalCoordX(0.649, width) && cursorPos->y > NormalCoordY(0.813, height) && cursorPos->y < NormalCoordY(0.93, height)))
			bLobbyNextOK = true;
		else
			bLobbyNextOK = false;
		break;
	case WM_LBUTTONDOWN:
	{
#if NETWORK
		{
			if (GameFlowManager::getInstance().type() == FLOW_ROOM) return true;

			if (cursorPos->x > NormalCoordX(0.014, width) && cursorPos->x < NormalCoordX(0.463, width) && cursorPos->y > NormalCoordY(0.13, height) && cursorPos->y < NormalCoordY(0.8, height)) {
				if (RBPlayerManager::getInstance().at_own()->role() == KILLER) {
					bMurdererSelectOK = true;
					bSurvivorSelectOK = false;
				}
				else ClientNetworkInterface::getInstance().ClientSelectROLE(KILLER);
			}
			else if (cursorPos->x > NormalCoordX(0.532, width) && cursorPos->x < NormalCoordX(0.966, width) && cursorPos->y > NormalCoordY(0.13, height) && cursorPos->y < NormalCoordY(0.8, height)) {
				if (RBPlayerManager::getInstance().at_own()->role() == SURVIVOR) {
					bSurvivorSelectOK = true;
					bMurdererSelectOK = false;
				}
				else ClientNetworkInterface::getInstance().ClientSelectROLE(SURVIVOR);
			}
			else { 
				bMurdererSelectOK = false; bSurvivorSelectOK = false; 
			};

			if (bLobbyNextOK) {
				ClientNetworkInterface::getInstance().ClinetReqRoomEnter();
			}
		}
#else
		if (cursorPos->x > 150 && cursorPos->x < 610 && cursorPos->y > 100 && cursorPos->y < 615) {
			bMurdererSelectOK = true;
			bSurvivorSelectOK = false;
		}
		else if (cursorPos->x > 680 && cursorPos->x < 1125 && cursorPos->y > 100 && cursorPos->y < 600) {
			bSurvivorSelectOK = true;
			bMurdererSelectOK = false;
		}
		else { bMurdererSelectOK = false; bSurvivorSelectOK = false; };
		if (bLobbyNextOK) {
			return (true);
		}
#endif

		break;
	}
	}
	return(false);
}

bool CLobbyScene::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, CPlayerShader ** playerShader)
{
	return (false);
}

void CLobbyScene::Build2dObjects(IDWriteFactory * pdwFactory, IWICImagingFactory * pwicFactory, ID2D1DeviceContext * pd2dDeviceContext, ID2D1Factory1 * pd2dFactory)
{
	HRESULT result;

	m_vtd2dBitmap.resize(7, nullptr);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/LobbyImage.png", &m_vtd2dBitmap[0], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/LobbyMurderer.png", &m_vtd2dBitmap[1], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/LobbyMurdererOK.png", &m_vtd2dBitmap[2], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/LobbyCitizen.png", &m_vtd2dBitmap[3], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/LobbyCitizenOK.png", &m_vtd2dBitmap[4], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/LobbyNext.png", &m_vtd2dBitmap[5], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/LobbyNextOK.png", &m_vtd2dBitmap[6], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
}

void CLobbyScene::Render2D(ID2D1DeviceContext * pd2dDeviceContext, IDWriteFactory * pdwFactory, ID2D1Factory1 * pd2dFactory)
{
	pd2dDeviceContext->BeginDraw();

	float width = pd2dDeviceContext->GetSize().width;
	float height = pd2dDeviceContext->GetSize().height;

	////2d�̹��� ����
	pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[0], D2D1::RectF(0.f, 0.f, width, height), 0.9f);
	pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[1], D2D1::RectF(NormalCoordX(0.014, width), NormalCoordY(0.13, height), NormalCoordX(0.463, width), NormalCoordY(0.8, height)), 0.9f);
	pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[3], D2D1::RectF(NormalCoordX(0.532, width), NormalCoordY(0.13, height), NormalCoordX(0.966, width), NormalCoordY(0.8, height)), 0.9f);
	pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[5], D2D1::RectF(NormalCoordX(0.346, width), NormalCoordY(0.813, height), NormalCoordX(0.649, width), NormalCoordY(0.93, height)), 0.9f);

	if (bMurdererSelectOK) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[2], D2D1::RectF(NormalCoordX(0.014, width), NormalCoordY(0.13, height), NormalCoordX(0.463, width), NormalCoordY(0.8, height)), 0.9f);
	if (bSurvivorSelectOK) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[4], D2D1::RectF(NormalCoordX(0.532, width), NormalCoordY(0.13, height), NormalCoordX(0.966, width), NormalCoordY(0.8, height)), 0.9f);
	if (bLobbyNextOK)  pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[6], D2D1::RectF(NormalCoordX(0.346, width), NormalCoordY(0.813, height), NormalCoordX(0.649, width), NormalCoordY(0.93, height)), 0.9f);

	pd2dDeviceContext->EndDraw();
}

void CLobbyScene::ReleaseObjects()
{
	if (!m_vtd2dBitmap.empty()) {
		for (auto i = 0; i < m_vtd2dBitmap.size(); ++i) {
			m_vtd2dBitmap[i]->Release();
			delete m_vtd2dBitmap[i];
		}
	}

	CScene::ReleaseObjects();
}