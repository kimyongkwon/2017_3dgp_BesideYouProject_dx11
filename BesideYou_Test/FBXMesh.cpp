#include "stdafx.h"

CFBXMesh::CFBXMesh(ID3D11Device *pd3dDevice, char *pszFileName, float fSize) : CMeshTexturedIlluminated(pd3dDevice)
{
	m_fFBXModelSize = fSize;
	m_fFBXAnimationTime = 0.0f;
	m_nFBXAnimationNum = 0;
	m_nFBXNowFrameNum = 0;
	m_nFBXFrameState = 0;
	m_RunAnimation = false;
	m_isAnimationOther = false;

	ifstream fin;

	int filename_length = strlen(pszFileName);
	char* ch = new char[200];
	char* sToken = new char[50];
	char* temp;
	fin.open(pszFileName);
	if (!fin.fail())
	{
		// 데이터를 읽어와 필요한 정점, 인덱스, 본, 애니메이션 수 파악
		fin.getline(ch, 200);	// [MESH_DATA]
		fin.getline(ch, 200);
		sToken = strtok_s(ch, " ", &temp);
		sToken = strtok_s(NULL, " ", &temp);
		m_nVertices = stoi(sToken);
		fin.getline(ch, 200);
		sToken = strtok_s(ch, " ", &temp);
		sToken = strtok_s(NULL, " ", &temp);
		m_nIndices = stoi(sToken);
		fin.getline(ch, 200);
		sToken = strtok_s(ch, " ", &temp);
		sToken = strtok_s(NULL, " ", &temp);
		m_nBoneCount = stoi(sToken);
		fin.getline(ch, 200);
		sToken = strtok_s(ch, " ", &temp);
		sToken = strtok_s(NULL, " ", &temp);
		m_nAnimationClip = stoi(sToken);

		fin.getline(ch, 1); // 한줄띄워쓰기

		XMFLOAT3 xmf3MinTmp = XMFLOAT3(FLT_MAX, FLT_MAX, FLT_MAX);
		XMFLOAT3 xmf3MaxTmp = XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

		// 정점 데이터를 저장
		fin.getline(ch, 200);	 // [VERTEX_DATA]
		m_pxmf3Positions = new XMFLOAT3[m_nVertices];
		m_pxmf3Normals = new XMFLOAT3[m_nVertices];
		m_pxmf2TexCoords = new XMFLOAT2[m_nVertices];
		if (m_nBoneCount)
		{
			m_pxmf4BoneIndices = new XMFLOAT4[m_nVertices];
			m_pxmf4BoneWeights = new XMFLOAT4[m_nVertices];
		}

		for (int i = 0; i < m_nVertices; i++)
		{
			// Vertice
			fin.getline(ch, 200);
			sToken = strtok_s(ch, " ", &temp);	
			sToken = strtok_s(NULL, " ", &temp); m_pxmf3Positions[i].x = stof(sToken);
			sToken = strtok_s(NULL, " ", &temp); m_pxmf3Positions[i].y = stof(sToken);
			sToken = strtok_s(NULL, " ", &temp); m_pxmf3Positions[i].z = stof(sToken);

			//모델의 최소점, 최대점 찾기
			if (xmf3MinTmp.x > m_pxmf3Positions[i].x) xmf3MinTmp.x = m_pxmf3Positions[i].x;
			if (xmf3MinTmp.y > m_pxmf3Positions[i].y) xmf3MinTmp.y = m_pxmf3Positions[i].y;
			if (xmf3MinTmp.z > m_pxmf3Positions[i].z) xmf3MinTmp.z = m_pxmf3Positions[i].z;
			if (xmf3MaxTmp.x < m_pxmf3Positions[i].x) xmf3MaxTmp.x = m_pxmf3Positions[i].x;
			if (xmf3MaxTmp.y < m_pxmf3Positions[i].y) xmf3MaxTmp.y = m_pxmf3Positions[i].y;
			if (xmf3MaxTmp.z < m_pxmf3Positions[i].z) xmf3MaxTmp.z = m_pxmf3Positions[i].z;

			// Normal
			fin.getline(ch, 200);
			sToken = strtok_s(ch, " ", &temp);
			sToken = strtok_s(NULL, " ", &temp); m_pxmf3Normals[i].x = stof(sToken);
			sToken = strtok_s(NULL, " ", &temp); m_pxmf3Normals[i].y = stof(sToken);
			sToken = strtok_s(NULL, " ", &temp); m_pxmf3Normals[i].z = stof(sToken);
			// UV
			fin.getline(ch, 200);
			sToken = strtok_s(ch, " ", &temp);  
			sToken = strtok_s(NULL, " ", &temp); m_pxmf2TexCoords[i].x = stof(sToken);
			sToken = strtok_s(NULL, " ", &temp); m_pxmf2TexCoords[i].y = stof(sToken);
			if (m_nBoneCount)
			{			// BoneIndice
				fin.getline(ch, 200);
				sToken = strtok_s(ch, " ", &temp); sToken = strtok_s(NULL, " ", &temp);	 m_pxmf4BoneIndices[i].x = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4BoneIndices[i].y = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4BoneIndices[i].z = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4BoneIndices[i].w = stof(sToken);
				// BoneWeight
				fin.getline(ch, 200);
				sToken = strtok_s(ch, " ", &temp); sToken = strtok_s(NULL, " ", &temp);  m_pxmf4BoneWeights[i].x = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4BoneWeights[i].y = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4BoneWeights[i].z = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4BoneWeights[i].w = stof(sToken);
			}
			//BesideYou 모델은 이걸 추가해야 작동한다.
			fin.getline(ch, 200);
		}

		//최소점과 최대점을 저장한다.
		m_bcBoundingCube.SetBoundingCube(xmf3MinTmp, xmf3MaxTmp);

		//최소점과 최대점을 이용해서 충돌박스에 중심점, 크기, 중심과 꼭지점까지의 거리를 구한다.
		m_xmf3BoundingSizes = XMFLOAT3(Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).x - 30, Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).y, Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).z);
		//박스의 사이즈를 반으로 나누면 물체의 중심점을 알 수 있다.
		m_xmf3BoundingCenterPos = XMFLOAT3(m_xmf3BoundingSizes.x / 2.0f, m_xmf3BoundingSizes.y / 2.0f, m_xmf3BoundingSizes.z / 2.0f);
		m_xmf3BoundingExtents = XMFLOAT3(m_xmf3BoundingSizes.x / 2.0f, m_xmf3BoundingSizes.y / 2.0f, m_xmf3BoundingSizes.z / 2.0f);

		fin.getline(ch, 1);	//파일에서는 두줄띄워져있는데 하나만 써도 두줄이 읽히나보다

		// 인덱스 데이터 저장
		fin.getline(ch, 200);		//[INDEX_DATA]
		m_pnIndices = new UINT[m_nIndices];

		for (int i = 0; i < m_nIndices; i += 3)
		{
			fin.getline(ch, 200);
			sToken = strtok_s(ch, " ", &temp);   m_pnIndices[i] = stoi(sToken);
			sToken = strtok_s(NULL, " ", &temp); m_pnIndices[i + 1] = stoi(sToken);
			sToken = strtok_s(NULL, " ", &temp); m_pnIndices[i + 2] = stoi(sToken);
		}

		fin.getline(ch, 1);	//띄어쓰기

		// (애니메이션을 포함한 메쉬일 경우) 본 정보와 애니메이션 정보 저장
		if (m_nBoneCount)
		{
			fin.getline(ch, 200);		//[BONE_HIERARCHY]
			m_pBoneHierarchy = new UINT[m_nBoneCount];
			m_pxmf4x4BoneOffsets = new XMFLOAT4X4[m_nBoneCount];
			m_pxmf4x4SQTTransform = new XMFLOAT4X4[m_nBoneCount];
			m_pxmf4x4FinalBone = new XMFLOAT4X4[m_nBoneCount];

			// 부모 뼈대를 가리키는 BoneHierarchy를 저장
			for (int i = 0; i < m_nBoneCount; i++)
			{
				fin.getline(ch, 200);
				sToken = strtok_s(ch, " ", &temp); 
				sToken = strtok_s(NULL, " ", &temp);
				m_pBoneHierarchy[i] = stoi(sToken);
			}

			fin.getline(ch, 1);	//한줄 띄어쓰기

			// 뼈대 자체의 오프셋 행렬을 저장
			fin.getline(ch, 200);		//[OFFSET_MATRIX]
			for (int i = 0; i < m_nBoneCount; i++)
			{
				fin.getline(ch, 200);
				sToken = strtok_s(ch, " ", &temp);  sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._11 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._12 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._13 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._14 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._21 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._22 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._23 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._24 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._31 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._32 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._33 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._34 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._41 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._42 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._43 = stof(sToken);
				sToken = strtok_s(NULL, " ", &temp); m_pxmf4x4BoneOffsets[i]._44 = stof(sToken);
			}

			fin.getline(ch, 1);	//띄어쓰기

			// 여기에서부터 애니메이션을 담는다.
			m_ppBoneAnimationData = new BoneAnimationData*[m_nAnimationClip];

			BoneAnimationData *pBoneAnimationData;
			for (int k = 0; k < m_nAnimationClip; k++)
			{
				pBoneAnimationData = new BoneAnimationData[m_nBoneCount];

				fin.getline(ch, 200);	//	[ANIMATION_CLIPS]
				fin.getline(ch, 200);	//	[AnimationClip Take_001]
				fin.getline(ch, 20);	//  {
				for (int i = 0; i < m_nBoneCount; i++)
				{
					fin.getline(ch, 200);
					sToken = strtok_s(ch, " ", &temp);
					sToken = strtok_s(NULL, " ", &temp);
					sToken = strtok_s(NULL, " ", &temp);
					pBoneAnimationData[i].m_nFrameCount = stoi(sToken);		// 프레임카운트가 들어간다
					pBoneAnimationData[i].m_pxmf3Translate = new XMFLOAT3[pBoneAnimationData[i].m_nFrameCount];
					pBoneAnimationData[i].m_pxmf3Scale = new XMFLOAT3[pBoneAnimationData[i].m_nFrameCount];
					pBoneAnimationData[i].m_pxmf4Quaternion = new XMFLOAT4[pBoneAnimationData[i].m_nFrameCount];
					pBoneAnimationData[i].m_pfAniTime = new float[pBoneAnimationData[i].m_nFrameCount];
					fin.getline(ch, 20);	// {
					for (int j = 0; j < pBoneAnimationData[i].m_nFrameCount; j++)
					{
						
						fin.getline(ch, 200);
						////BesideYou 모델은 이걸 추가해야 작동한다.
						sToken = strtok_s(ch, "                ", &temp);	//Time:
						sToken = strtok_s(NULL, " ", &temp);
						pBoneAnimationData[i].m_pfAniTime[j] = stof(sToken);
						sToken = strtok_s(NULL, " ", &temp);				// T:
						sToken = strtok_s(NULL, " ", &temp);
						pBoneAnimationData[i].m_pxmf3Translate[j].x = stof(sToken);
						sToken = strtok_s(NULL, " ", &temp);
						pBoneAnimationData[i].m_pxmf3Translate[j].y = stof(sToken);
						sToken = strtok_s(NULL, " ", &temp);
						pBoneAnimationData[i].m_pxmf3Translate[j].z = stof(sToken);
						sToken = strtok_s(NULL, " ", &temp);				// S:
						sToken = strtok_s(NULL, " ", &temp);
						pBoneAnimationData[i].m_pxmf3Scale[j].x = stof(sToken);
						sToken = strtok_s(NULL, " ", &temp);
						pBoneAnimationData[i].m_pxmf3Scale[j].y = stof(sToken);
						sToken = strtok_s(NULL, " ", &temp);
						pBoneAnimationData[i].m_pxmf3Scale[j].z = stof(sToken);
						sToken = strtok_s(NULL, " ", &temp);				// Q:
						sToken = strtok_s(NULL, " ", &temp);
						pBoneAnimationData[i].m_pxmf4Quaternion[j].x = stof(sToken);
						sToken = strtok_s(NULL, " ", &temp);
						pBoneAnimationData[i].m_pxmf4Quaternion[j].y = stof(sToken);
						sToken = strtok_s(NULL, " ", &temp);
						pBoneAnimationData[i].m_pxmf4Quaternion[j].z = stof(sToken);
						sToken = strtok_s(NULL, " ", &temp);
						pBoneAnimationData[i].m_pxmf4Quaternion[j].w = stof(sToken);
					}
					fin.getline(ch, 20);	// }
				}
				m_ppBoneAnimationData[k] = pBoneAnimationData;
				fin.getline(ch, 20);	//  }
			}
		}
	}
	fin.close();

	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	m_pd3dPositionBuffer = CreateBuffer(pd3dDevice, sizeof(XMFLOAT3), m_nVertices, m_pxmf3Positions, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0);
	m_pd3dNormalBuffer = CreateBuffer(pd3dDevice, sizeof(XMFLOAT3), m_nVertices, m_pxmf3Normals, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0);
	m_pd3dTexCoordBuffer = CreateBuffer(pd3dDevice, sizeof(XMFLOAT2), m_nVertices, m_pxmf2TexCoords, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0);
	m_pd3dWeightBuffer = CreateBuffer(pd3dDevice, sizeof(XMFLOAT4), m_nVertices, m_pxmf4BoneWeights, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0);
	m_pd3dBoneIndiceBuffer = CreateBuffer(pd3dDevice, sizeof(XMFLOAT4), m_nVertices, m_pxmf4BoneIndices, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0);
	ID3D11Buffer *pd3dBuffers[5] = { m_pd3dPositionBuffer, m_pd3dNormalBuffer, m_pd3dTexCoordBuffer, m_pd3dWeightBuffer, m_pd3dBoneIndiceBuffer };
	UINT pnBufferStrides[5] = { sizeof(XMFLOAT3), sizeof(XMFLOAT3), sizeof(XMFLOAT2), sizeof(XMFLOAT4), sizeof(XMFLOAT4) };
	UINT pnBufferOffsets[5] = { 0, 0, 0, 0, 0 };
	AssembleToVertexBuffer(5, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	m_pd3dIndexBuffer = CreateBuffer(pd3dDevice, sizeof(UINT), m_nIndices, m_pnIndices, D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT, 0);

	CreateRasterizerState(pd3dDevice);
	CreateConstantBuffer(pd3dDevice);

	// idle 상태의 프레임 값을 체크.
	if (m_nBoneCount)
		m_nFBXMaxFrameNum = m_ppBoneAnimationData[0][0].m_nFrameCount - 1;
}

CFBXMesh::~CFBXMesh()
{
	if (m_pd3dWeightBuffer) m_pd3dWeightBuffer->Release();
	if (m_pd3dBoneIndiceBuffer) m_pd3dBoneIndiceBuffer->Release();
	if (m_pd3dcbBones) m_pd3dcbBones->Release();

	if (m_pxmf3Positions) delete[] m_pxmf3Positions;
	if (m_pxmf3Normals) delete[] m_pxmf3Normals;
	if (m_pxmf2TexCoords) delete[] m_pxmf2TexCoords;
	if (m_pxmf4BoneWeights) delete[] m_pxmf4BoneWeights;
	if (m_pxmf4BoneIndices) delete[] m_pxmf4BoneIndices;
	if (m_ppBoneAnimationData) delete[] m_ppBoneAnimationData;
	if (m_pxmf4x4FinalBone) delete[] m_pxmf4x4FinalBone;
	if (m_pxmf4x4SQTTransform) delete[] m_pxmf4x4SQTTransform;
	if (m_pBoneHierarchy) delete[] m_pBoneHierarchy;
	if (m_pxmf4x4BoneOffsets) delete[] m_pxmf4x4BoneOffsets;

}

void CFBXMesh::UpdateBoneTransform(ID3D11DeviceContext *pd3dDeviceContext, int nAnimationNum, int nNowFrame)
{
	for (int i = 0; i < m_nBoneCount; i++)
	{
		MakeBoneMatrix(nNowFrame, nAnimationNum, i, *(m_pxmf4x4SQTTransform + i));
	}

	// 마지막으로 본의 기본 오프셋행렬을 곱해주어 최종 행렬을 만들어준다.
	for (int i = 0; i < m_nBoneCount; i++)
	{
		XMFLOAT4X4 xmf4x4Offset = m_pxmf4x4BoneOffsets[i];
		XMFLOAT4X4 xmf4x4ToRoot = m_pxmf4x4SQTTransform[i];
		m_pxmf4x4FinalBone[i] = Matrix4x4::Multiply(xmf4x4Offset, xmf4x4ToRoot);
	}

	// 상수버퍼로 최종 행렬값을 넘겨주자.
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	pd3dDeviceContext->Map(m_pd3dcbBones, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	VS_CB_SKINNED *pcbBones = (VS_CB_SKINNED*)d3dMappedResource.pData;
	for (int i = 0; i < m_nBoneCount; i++)
	{
		pcbBones->m_xmf4x4Bone[i] = Matrix4x4::Transpose(m_pxmf4x4FinalBone[i]);
		//D3DXMatrixTranspose(&pcbBones->m_d3dxmtxBone[i], &m_pd3dxmtxFinalBone[i]);
	}
	pd3dDeviceContext->Unmap(m_pd3dcbBones, 0);

	//상수 버퍼를 슬롯(VS_SLOT_SKINNEDBONE)에 설정한다.
	pd3dDeviceContext->VSSetConstantBuffers(VS_SLOT_SKINNEDBONE, 1, &m_pd3dcbBones);
}

void CFBXMesh::MakeBoneMatrix(int nNowframe, int nAnimationNum, int nBoneNum, XMFLOAT4X4& BoneMatrix)
{
	// XMAffine 함수에서는 scale의 VECTOR3을 쓰지만
	// D3DXAffine 함수에서는 scale의 계수를 사용한다.
	if (m_ppBoneAnimationData[nAnimationNum][nBoneNum].m_nFrameCount != 0)
	{
		XMFLOAT3 xmf3Scale;
		xmf3Scale = XMFLOAT3(m_ppBoneAnimationData[nAnimationNum][nBoneNum].m_pxmf3Scale[nNowframe].z, 1.0f, 1.0f);

		XMFLOAT3 xmf3Translate = m_ppBoneAnimationData[nAnimationNum][nBoneNum].m_pxmf3Translate[nNowframe];
		XMFLOAT4 xmf4Quaternion = m_ppBoneAnimationData[nAnimationNum][nBoneNum].m_pxmf4Quaternion[nNowframe];
		XMFLOAT3 xmf3Zero = { 0.0f, 0.0f, 0.0f };

		BoneMatrix = Matrix4x4::MatrixAffineTransformation(xmf3Scale, xmf3Zero, xmf4Quaternion, xmf3Translate);
		//D3DXMatrixAffineTransformation(&BoneMatrix, fScale, &d3dxvZero, &d3dxvQuaternion, &d3dxvTranslate);
	}
	else // 해당 본에 애니메이션 프레임이 아예 없을 경우 단위행렬을 리턴하자.
	{
		BoneMatrix = Matrix4x4::Identity();
		//D3DXMatrixIdentity(&BoneMatrix);
	}
}

void CFBXMesh::CreateConstantBuffer(ID3D11Device *pd3dDevice)
{
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(VS_CB_SKINNED);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	pd3dDevice->CreateBuffer(&bd, NULL, &m_pd3dcbBones);
}

// 다음 애니메이션을 위한 프레임으로 넘긴다.
// 추가적인 애니메이션 관리를 위해 마지막 프레임일 경우 true를 리턴한다.
bool CFBXMesh::FBXFrameAdvance(float fTimeElapsed, int32_t inFbx, XMFLOAT3 mypos, XMFLOAT3 otherpos)
{
	m_fFBXAnimationTime += fTimeElapsed;

	float AniFrameTime = 0;

	if (m_nFBXFrameState == 0) AniFrameTime = 0.0333333f;
	if (m_nFBXFrameState == 1) AniFrameTime = 0.0222222f;	//애니메이션을 조금 빠르게

	
	//트랩 애니메이션일 경우에는 트랩아구가 딱 접혔을떄만 표현한다.
	if (inFbx == FBX_TRAP)
	{
		if (m_fFBXAnimationTime > AniFrameTime)	// 0.0333333f초가 지나면 1프레임 올리자.
		{
			if (m_nFBXNowFrameNum < m_nFBXMaxFrameNum - 20)
			{
				m_nFBXNowFrameNum++;
				m_fFBXAnimationTime = 0.0f;
				return false;
			}
			else
				return true;
		}
		else
			return false;
	}
	else if (inFbx == FBX_DIE)
	{
		if (m_fFBXAnimationTime > AniFrameTime)	// 0.0333333f초가 지나면 1프레임 올리자.
		{
			if (m_nFBXNowFrameNum < m_nFBXMaxFrameNum - 1)
			{
				m_nFBXNowFrameNum++;
				m_fFBXAnimationTime = 0.0f;
				return false;
			}
			else
			{
				m_nFBXNowFrameNum = m_nFBXMaxFrameNum - 1;
				m_fFBXAnimationTime = 0.0f;
				return true;
			}
			return false;
		}
	
	}
	else {
		if (m_fFBXAnimationTime > AniFrameTime)	// 0.0333333f초가 지나면 1프레임 올리자.
		{
			if (m_nFBXNowFrameNum < m_nFBXMaxFrameNum - 1)
			{
				m_nFBXNowFrameNum++;
				
				if (m_RunAnimation && m_isAnimationOther) {	
					if (isNearOther::isNearOther(mypos, otherpos, 100.0f) && (m_nFBXNowFrameNum == 8 || m_nFBXNowFrameNum == 19)) {
						cout << "상대방 사운드소리" << endl;
						SoundEngine::getInstance().execute(RUN, true, false);
					}
				}
				else if ( m_RunAnimation )
				{
					if (m_nFBXNowFrameNum == 8 || m_nFBXNowFrameNum == 19) { cout << "내 사운드소리" << endl; SoundEngine::getInstance().execute(RUN, true, false); }
				}

				m_fFBXAnimationTime = 0.0f;
				return false;
			}
			else
			{
				m_nFBXNowFrameNum = 0;
				m_fFBXAnimationTime = 0.0f;
				return true;
			}
		}
		else
			return false;
	}
}

void CFBXMesh::SetAnimation(int nFBXAnimation , bool run, bool other)
{
	m_nFBXAnimationNum = nFBXAnimation;
	m_nFBXNowFrameNum = 0;
	m_nFBXMaxFrameNum = m_ppBoneAnimationData[nFBXAnimation][0].m_nFrameCount;
	m_fFBXAnimationTime = 0.0f;
	m_RunAnimation = run;
	m_isAnimationOther = other;
}

