#include "stdafx.h"


CGenerator::CGenerator(int index, XMFLOAT3 Position, CScene * IngameScene) :
	Position_(Position), generatorNumber_(index), LocalPersent_(0.f), m_pInGameScene(IngameScene),
	ServerPersent_(0), genState_(GENERATIOR_STOP) 
{
	Sound = false; 
	StartSound = false;
}

bool CGenerator::IsComplete() const 
{
	if (genState_ == GENERATIOR_END)
		return true;
	return false;
}

void CGenerator::clear()
{
	genState_ = GENERATIOR_STOP;
	ServerPersent_ = 0;
	LocalPersent_ = 0.f;
}

bool CGenerator::compare(XMFLOAT3 Position) const
{
	if (Position_.x == Position.x && Position_.z == Position.z)
		return true;
	return false;
}

void CGenerator::tick()
{
	if (genState_ != GENERATIOR_START) return;
}

void CGenerator::Read(INT8 ServerPersent)
{
	ServerPersent_ = ServerPersent;
	if (ServerPersent_ >= 100) {
		genState_ = GENERATIOR_END;
		OnLights();
	}
	else OffLights();
}

void CGenerator::OnLights()
{
	LIGHTS * Lights = m_pInGameScene->GetLights();
	Lights->m_pLights[generatorNumber_ + 4].m_bEnable = 1.0f;
}

void CGenerator::OffLights()
{
	LIGHTS * Lights = m_pInGameScene->GetLights();
	Lights->m_pLights[generatorNumber_ + 4].m_bEnable = 0.0f;
}