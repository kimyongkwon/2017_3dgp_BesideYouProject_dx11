#pragma once

//------------------------------------------------------------------
//
// Class : CCSVData
//
// Desc : static model data to representing class
//
//------------------------------------------------------------------

class CCSVData
{
public:
	CCSVData(const std::string& filename, int DataNum);
	~CCSVData();

	int m_nDataNum;

	int*		m_ipDataType;
	int*		m_ipDataNum;
	int*		m_ipDataPositionX;
	int*		m_ipDataPositionY;
	int*		m_ipDataPositionZ;
	float*		m_fpDataRotationY;
	float*		m_fpDataRotationX;
	float*		m_fpDataRotationZ;
	float*		m_fpDataScaleX;
	float*		m_fpDataScaleY;
	float*		m_fpDataScaleZ;
};