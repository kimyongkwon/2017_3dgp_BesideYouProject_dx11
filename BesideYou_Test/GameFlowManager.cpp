#include"stdafx.h"
#include"GameFlowManager.h"

extern HWND Ghwnd;
GameFlowManager::GameFlowManager() : _type(FLOW_LOGIN)
{
	_btype = FLOW_NULL;
	fastoutgame = false;
}

GameFlowManager::~GameFlowManager()
{
}

void GameFlowManager::ENTER(FLOWTYPE type)
{
	_type = type;
	SendMessage(Ghwnd, WM_LBUTTONDOWN, NULL, NULL);
}

void GameFlowManager::LEAVE()
{
}

bool GameFlowManager::wasFtype()
{
	if (_btype == FLOW_NULL)return false;
	return true;
}

FLOWTYPE & GameFlowManager::type()
{
	return _type;
}

void GameFlowManager::Called()
{
	switch (_type)
	{
	case FLOW_ROBBY:
		SendMessage(Ghwnd, WM_LBUTTONDOWN, NULL, NULL);
		break;
	default:
		break;
	}
}

void GameFlowManager::ENTER(FLOWTYPE type, bool nwin, int nrole)
{
	_type = type;
	if (type == FLOW_RESULT)
	{
		win = nwin;
		role = nrole;
	}
}