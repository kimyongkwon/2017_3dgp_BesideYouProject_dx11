#pragma once

//------------------------------------------------------------------
//
// Class : CSkyBoxShader
//
// Desc : Shaders for Creating and Rendering Skyboxes
//
//------------------------------------------------------------------
class CSkyBoxShader : public CTexturedShader
{
public:
	CSkyBoxShader();
	virtual ~CSkyBoxShader();

	virtual void BuildObjects(ID3D11Device *pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
};