#include "Light.fx"
//
// 상수 버퍼
///////////////////////////////////////////////////////////////////////////
cbuffer cbViewProjectionMatrix : register(b0)
{
	matrix gmtxView : packoffset(c0);
	matrix gmtxProjection : packoffset(c4);
}

cbuffer cbWorldMatrix : register(b1)
{
	matrix gmtxWorld : packoffset(c0);

}

cbuffer cbSkinned : register(b2)
{
	// 한 캐릭터 당 최대 뼈대 개수는 96
	matrix gBoneTransform[96];// : packoffset(c0);
};


//
// 쉐이더 변수
///////////////////////////////////////////////////////////////////////////
Texture2D gtxtTexture : register(t0);
SamplerState gSamplerState : register(s0);

struct VS_SKINNED_OUTPUT
{
	float4 position : SV_POSITION;
	float3 positionW : POSITION;
	float3 normalW : NORMAL;
	float2 texCoord : TEXCOORD0;
};


float4 PSCharacterSkinned(VS_SKINNED_OUTPUT input) : SV_Target
{
	input.normalW = normalize(input.normalW);
float4 cIllumination = Lighting(input.positionW, input.normalW);
float4 cColor = gtxtTexture.Sample(gSamplerState, input.texCoord) * cIllumination;

return(cColor);
}