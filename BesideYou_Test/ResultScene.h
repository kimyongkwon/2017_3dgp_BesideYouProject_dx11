#pragma once

//------------------------------------------------------------------
//
// Class : CResultScene
//
// Desc : final scene, Build and Render UI to check whether I won or lost
//		    
//------------------------------------------------------------------

class CResultScene : public CScene
{
public:
	CResultScene();
	~CResultScene();

	virtual void Build2dObjects(IDWriteFactory *pdwFactory, IWICImagingFactory * pwicFactory,
		ID2D1DeviceContext * pd2dDeviceContext, ID2D1Factory1 * pd2dFactory);
	virtual bool OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, POINT * cursor, ID2D1DeviceContext * pDeviceContext = NULL, CPlayerShader ** playerShader = NULL);
	virtual void ReleaseObjects();
	virtual void Render2D(ID2D1DeviceContext * pd2dDeviceContext, IDWriteFactory * pdwFactory, ID2D1Factory1 * pd2dFactory);
};