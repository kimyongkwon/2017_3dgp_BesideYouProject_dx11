#include"stdafx.h"
#include"ClientNetwork.h"
#include"RBPlayerObject.h"

extern ClientNetwork Network;

const wchar_t * RBPlayerObject::name()
{
	return _name.c_str();
}

void RBPlayerObject::setname(wstring name)
{
	_name = name;
}

INT64 & RBPlayerObject::uid()
{
	return _uid;
}

void RBPlayerObject::setuid(INT64 uid)
{
	_uid = uid;
}

//UINT64 & RBPlayerObject::oid()
//{
//	return _oid;
//}
//
//void RBPlayerObject::setoid(UINT64 oid)
//{
//	_oid = oid;
//}

ROLE & RBPlayerObject::role()
{
	return _role;
}

void RBPlayerObject::setrole(ROLE role)
{
	_role = role;
}

IDENTI & RBPlayerObject::identi()
{
	return _ident;
}

void RBPlayerObject::setidenti(IDENTI iden)
{
	_ident = iden;
}

INT64 RBPlayerObject::roomnumber()
{
	return _roomNumber;
}

void RBPlayerObject::setroomnumber(INT64 roomnumber)
{
	_roomNumber = roomnumber;
}

void RBPlayerObject::setPlayer(wstring name, INT64 uid, ROLE role, IDENTI iden)
{
	this->setname(name);
	this->setuid(uid);
	//this->setoid(oid);
	this->setrole(role);
	this->setidenti(iden);
}

void RBPlayerObject::ready()
{
	_roomready = true;
}

void RBPlayerObject::unready()
{
	_roomready = false;
}

bool RBPlayerObject::isReady()
{
	return _roomready;
}

RBPlayerManager::RBPlayerManager()
{
	_ownPlayer = new RBPlayerObject();
	_playerVector.clear();
	_playerVector.emplace_back(_ownPlayer);
}

RBPlayerManager::~RBPlayerManager()
{
	for (RBPlayerObject * obj : _playerVector) {
		if (obj == _ownPlayer)continue;
		SAFE_DELETE(obj);
	}
	_playerVector.clear();
	SAFE_DELETE(_ownPlayer);
}


bool RBPlayerManager::addPlayer(RBPlayerObject * player)
{
	INT64 uid = player->uid();
	auto iter = std::find_if(_playerVector.begin(), _playerVector.end(), [uid](RBPlayerObject * m)-> bool {return m->uid() == uid; });
	if (iter != _playerVector.end())
		return false;
	_playerVector.push_back(player);
	return true;
}

bool RBPlayerManager::removePlayer(DWORD uid)
{
	auto iter = std::find_if(_playerVector.begin(), _playerVector.end(), [uid](RBPlayerObject * m)-> bool {return m->uid() == uid; });
	if (iter == _playerVector.end()) return false;
	SAFE_DELETE(*iter);
	_playerVector.erase(iter);
	return true;
}

size_t RBPlayerManager::Pcount()
{
	return _playerVector.size();
}

RBPlayerObject * RBPlayerManager::at_own()
{
	if (_ownPlayer == nullptr)
		return nullptr;
	return _ownPlayer;
}

RBPlayerObject * RBPlayerManager::at_Player(DWORD uid)
{
	auto iter = std::find_if(_playerVector.begin(), _playerVector.end(), [uid](RBPlayerObject * p)-> bool {return p->uid() == uid; });
	if (iter == _playerVector.end()) return nullptr;
	return *iter;
}

vector<RBPlayerObject*>* RBPlayerManager::Playerinfo()
{
	std::vector<RBPlayerObject *> * info = &_playerVector;
	return info;
}

void RBPlayerManager::ownRoomEnter(INT64 roomNumber)
{
	if (_ownPlayer == nullptr) return;
	_ownPlayer->setroomnumber(roomNumber);
}

void RBPlayerManager::ownRoomLeave()
{
	if (_ownPlayer == nullptr) return;
	_ownPlayer->setroomnumber(10000);
	_ownPlayer->setrole(NONE);
	_ownPlayer->unready();
	for (auto iter = _playerVector.begin(); iter != _playerVector.end();)
	{
		if (*iter == _ownPlayer) {
			++iter;
			continue;
		}
		else
		{
			auto ptr = *iter;
			iter = _playerVector.erase(iter);
			SAFE_DELETE(ptr);
		}
	}
}

void ClientNetworkInterface::ClientLogin(wstring _idInput, wstring _pwinput)
{
	if (lstrlenW(_idInput.data()) > 20) {
		MessageBoxW(NULL, L"id 문자열 20 초과", L"문자열 20 초과", NULL);
		return;
	}
	if (lstrlenW(_pwinput.data()) > 20) {
		MessageBoxW(NULL, L"pw 문자열 20 초과", L"문자열 20 초과", NULL);
		return;
	}
	RBPlayerManager::getInstance().at_own()->setname(_idInput);
	PK_C_REQ_LOGIN cPacket;
	cPacket.id = _idInput;
	cPacket.pw = _pwinput;
	Network._session->sendPacket(&cPacket);
}

void ClientNetworkInterface::ClientJOIN(wstring id, wstring pw, wstring email, wstring name)
{
	string transfromid_;
	/*wchar_t _pwconfirminput[30] = { L"", };
	StrConvW2A(&id.front(), &transfromid_.front(), transfromid_.size());
	wchar_t _name[15] = { L"", };
	wchar_t _email[50] = { L"", };
	if (!JoinNameCheak(&id.front(), &transfromid_)) {
		return;
	}
	if (!JoinPasswordCheak(&pw.front(), _pwconfirminput)) {
		return;
	}
	if (!OthersCheak(_name, _email)) {
		return;
	}*/
	PK_C_REQ_JOIN cPacket;
	cPacket.id = id;
	cPacket.pw = pw;
	cPacket.email = email;
	cPacket.name = name;
	Network._session->sendPacket(&cPacket);
}

void ClientNetworkInterface::ClientSelectROLE(ROLE role)
{
	if (role == SURVIVOR) 
	{
		PK_C_REQ_SELECTPART cPacket;
		cPacket.role = (BYTE)SURVIVOR;
		cPacket.uid = RBPlayerManager::getInstance().at_own()->uid();
		Network._session->sendPacket(&cPacket);
	}
	else if (role == KILLER)
	{
		PK_C_REQ_SELECTPART cPacket;
		cPacket.role = (BYTE)KILLER;
		cPacket.uid = RBPlayerManager::getInstance().at_own()->uid();
		Network._session->sendPacket(&cPacket);
	}
}

void ClientNetworkInterface::ClinetReqRoomEnter()
{
	if (RBPlayerManager::getInstance().at_own()->role() == NONE) {
		MessageBoxW(NULL, L"역할을 설정후 방 입장을 클릭 해 주세요.", L"경고", NULL);
		return;
	}
	PK_C_REQ_SEARCHROOM cPacket;
	cPacket.uid = RBPlayerManager::getInstance().at_own()->uid();
	Network._session->sendPacket(&cPacket);
}

void ClientNetworkInterface::ClientRoomReady()
{
	if (!RBPlayerManager::getInstance().at_own()->isReady()) {
		PK_C_REQ_GAMEREADYON cPacket;
		cPacket.uid = RBPlayerManager::getInstance().at_own()->uid();
		Network._session->sendPacket(&cPacket);
	}
	else {
		PK_C_REQ_GAMEREADYOFF cPacket;
		cPacket.uid = RBPlayerManager::getInstance().at_own()->uid();
		Network._session->sendPacket(&cPacket);
	}
}

void ClientNetworkInterface::ClientPos(float posx, float posy, float posz, float degree, INT32 roomnumber, INT64 uid)
{
	/*static INT64 count = 1;
	PK_C_REQ_OWNMOVE packet;
	packet.count = count++;
	packet.degree = degree;
	packet.pos.push_back(posx);
	packet.pos.push_back(posy);
	packet.pos.push_back(posz);
	packet.roomnumber = roomnumber;
	packet.uid = uid;
	packet.time = GetTickCount64() + ObjectManager::getInstance().at_own()->CSgap();
	
	Network._Roomsession->sendPacket(&packet);*/
}

void ClientNetworkInterface::ClientMoveStart()
{
	// 움직임 보내기
	// 필요한 것은?
	// 방향!
	// 움직인 위치
	//Packet packet;	// TEST Packet
	/*움직임 시작 패킷 packet;
	packet.도착시 예상패킷x;
	packet.도착시 예상패킷y;
	packet.도착시 예상패킷z;
	packet.방향;*/
	//Network._session->sendPacket(&packet);
}

void ClientNetworkInterface::ClientLogout()
{
	PK_C_REQ_GAMEOUT packet;
	packet.uid = RBPlayerManager::getInstance().at_own()->uid();
	Network._session->sendPacket(&packet);
	Sleep(100);
}

//void ClientNetworkInterface::SendinputPacket(MoveList & moveSTampt_)
//{
//	if (Network._Roomsession == nullptr) return;
//	if (moveSTampt_.HasMoves())
//	{
//		int moveCount = moveSTampt_.size();
//		int firstindex = moveCount - 3;
//		if (firstindex < 3) firstindex = 0;
//
//		auto move = moveSTampt_.movelist();
//		auto moved = move.begin() + firstindex;
//		PK_C_NTY_MOVESTAMPT cPacket;
//
//		auto players = CPlayerManager::getInstance().players();
//
//		cPacket.roomNumber = (*players)[0]->m_pPlayerInfo->GetRoomNUM();
//		cPacket.uid = (*players)[0]->m_pPlayerInfo->GetUserID();
//		for (; moved != moveSTampt_.end(); ++moved)
//		{
//			cPacket.StamptStartTime.emplace_back(moved->MoveTimeTampt());
//			cPacket.deltaTIme.emplace_back(moved->DetaTime());
//			cPacket.Key.emplace_back(moved->Keystate().GetKeyResult().x);
//			cPacket.Key.emplace_back(moved->Keystate().GetKeyResult().y);
//			cPacket.Key.emplace_back(moved->Keystate().GetKeyResult().z);
//			cPacket.rotate.emplace_back(moved->Keystate()._direction.x);
//			cPacket.rotate.emplace_back(moved->Keystate()._direction.y);
//			cPacket.rotate.emplace_back(moved->Keystate()._direction.z);
//			cPacket.rotate.emplace_back(moved->Keystate()._direction.w);
//		}
//		Network._Roomsession->sendPacket(&cPacket);
//	}
//}

// Change
void MoveList::SendinputPacket()
{
	{
		if (Network._Roomsession == nullptr) return;
		if (HasMoves())
		{
			int moveCount = moveList_.size();
			int firstindex = moveCount - 5;
			if (firstindex < 5) firstindex = 0;

			//auto move = moveList_.movelist();
			auto moved = moveList_.begin() + firstindex;
			PK_C_NTY_MOVESTAMPT cPacket;

			auto playerlist = CPlayerManager::getInstance().players();
			auto me = (*playerlist).front();

			cPacket.roomNumber = (me)->m_pPlayerInfo->GetRoomNUM();
			cPacket.uid = (me)->m_pPlayerInfo->GetUserID();
			for (; moved != moveList_.end(); ++moved)
			{
				auto lookvector = moved->LookVector();
				auto rightvector = moved->RightVector();
				auto nowdirction = moved->nowDirection();
				auto lastdirection = moved->lastDirection();
				cPacket.genNumber = moved->GeneratorNumber();
				cPacket.lookvector.push_back(lookvector.x);
				cPacket.lookvector.push_back(lookvector.y);
				cPacket.lookvector.push_back(lookvector.z);
				cPacket.rightvector.push_back(rightvector.x);
				cPacket.rightvector.push_back(rightvector.y);
				cPacket.rightvector.push_back(rightvector.z);
				cPacket.nowDirection.push_back(nowdirction);
				cPacket.LastDirection.push_back(lastdirection);
				cPacket.StamptStartTime.emplace_back(moved->MoveTimeTampt());
				cPacket.deltaTIme.emplace_back(moved->DetaTime());
				cPacket.Velocity.push_back(moved->Velocity());
			}
			
			Network._Roomsession->sendPacket(&cPacket);
		}
	}

}

void ClientNetworkInterface::ClientChitt()
{
	PK_C_ANS_CHITTING packet;
	Network._Roomsession->sendPacket(&packet);
}