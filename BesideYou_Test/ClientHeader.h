#pragma once

//Debugging
//#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 

#include "targetver.h"
#pragma comment (lib,"fmodex64_vc.lib")

//Network
#include "NetworkLib.h"
#include "packetAfterTreatment.h"
#include "ClientNetwork.h"
#include "GameFlowManager.h"

#include <windows.h>

// C 런타임 헤더 파일
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <stdio.h>
#include <vector>
#include <array>
#include <map>
#include <time.h>
#include <thread>
#include <chrono>

#include <iostream>
#include <fstream>
#include <string>

#include <d3d11.h>
#include <d3dx11.h>
#include <mmsystem.h>
#include <math.h>
#include <d3dcompiler.h>  	//쉐이더 컴파일 함수를 사용하기 위한 헤더 파일
#include <D3DX10Math.h>		//Direct3D 수학 함수를 사용하기 위한 헤더 파일
#include <D3D9Types.h>

#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <DirectXColors.h>
#include <DirectXCollision.h>
#include "inc/fmod.hpp"

using namespace std;
using namespace DirectX;
using namespace DirectX::PackedVector;
using namespace FMOD;

typedef enum
{
	UpdateNot = 0,
	UpdateHP = 1 << 0,
	UpdateTrap = 1 << 1,
	UpdateStatus = 1 << 2,
	UpdateAll = UpdateHP | UpdateTrap | UpdateStatus,
}PUPDATE;
typedef enum TUPDATE
{
	none = 0,
	install = 1 << 0,
	invoked = 1 << 1,
	toDelete = 1 << 2,
};

namespace Vector3
{
	inline XMFLOAT3 XMVectorToFloat3(XMVECTOR& xmvVector)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, xmvVector);
		return(xmf3Result);
	}

	inline XMFLOAT3 ScalarProduct(XMFLOAT3& xmf3Vector, float fScalar, bool bNormalize = true)
	{
		XMFLOAT3 xmf3Result;
		if (bNormalize)
			XMStoreFloat3(&xmf3Result, XMVector3Normalize(XMLoadFloat3(&xmf3Vector)) * fScalar);
		else
			XMStoreFloat3(&xmf3Result, XMLoadFloat3(&xmf3Vector) * fScalar);
		return(xmf3Result);
	}

	inline XMFLOAT3 Add(const XMFLOAT3& xmf3Vector1, const XMFLOAT3& xmf3Vector2)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, XMLoadFloat3(&xmf3Vector1) + XMLoadFloat3(&xmf3Vector2));
		return(xmf3Result);
	}

	inline XMFLOAT3 Add(XMFLOAT3& xmf3Vector1, XMFLOAT3& xmf3Vector2, float fScalar)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, XMLoadFloat3(&xmf3Vector1) + (XMLoadFloat3(&xmf3Vector2) * fScalar));
		return(xmf3Result);
	}

	inline XMFLOAT3 Subtract(const XMFLOAT3& xmf3Vector1, XMFLOAT3& xmf3Vector2)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, XMLoadFloat3(&xmf3Vector1) - XMLoadFloat3(&xmf3Vector2));
		return(xmf3Result);
	}

	inline float DotProduct(XMFLOAT3& xmf3Vector1, XMFLOAT3& xmf3Vector2)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, XMVector3Dot(XMLoadFloat3(&xmf3Vector1), XMLoadFloat3(&xmf3Vector2)));
		return(xmf3Result.x);
	}

	inline float DotProduct(XMFLOAT3* xmf3Vector1, XMFLOAT3* xmf3Vector2)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, XMVector3Dot(XMLoadFloat3(xmf3Vector1), XMLoadFloat3(xmf3Vector2)));
		return(xmf3Result.x);
	}

	inline XMFLOAT3 CrossProduct(XMFLOAT3& xmf3Vector1, XMFLOAT3& xmf3Vector2, bool bNormalize = true)
	{
		XMFLOAT3 xmf3Result;
		if (bNormalize)
			XMStoreFloat3(&xmf3Result, XMVector3Normalize(XMVector3Cross(XMLoadFloat3(&xmf3Vector1), XMLoadFloat3(&xmf3Vector2))));
		else
			XMStoreFloat3(&xmf3Result, XMVector3Cross(XMLoadFloat3(&xmf3Vector1), XMLoadFloat3(&xmf3Vector2)));
		return(xmf3Result);
	}

	inline XMFLOAT3 Normalize(XMFLOAT3& xmf3Vector)
	{
		XMFLOAT3 m_xmf3Normal;
		XMStoreFloat3(&m_xmf3Normal, XMVector3Normalize(XMLoadFloat3(&xmf3Vector)));
		return(m_xmf3Normal);
	}

	inline float Length(XMFLOAT3& xmf3Vector)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, XMVector3Length(XMLoadFloat3(&xmf3Vector)));
		return(xmf3Result.x);
	}

	inline float Angle(XMVECTOR& xmvVector1, XMVECTOR& xmvVector2)
	{
		XMVECTOR xmvAngle = XMVector3AngleBetweenNormals(xmvVector1, xmvVector2);
		return(XMConvertToDegrees(acosf(XMVectorGetX(xmvAngle))));
	}

	inline float Angle(XMFLOAT3& xmf3Vector1, XMFLOAT3& xmf3Vector2)
	{
		return(Angle(XMLoadFloat3(&xmf3Vector1), XMLoadFloat3(&xmf3Vector2)));
	}

	inline XMFLOAT3 TransformNormal(XMFLOAT3& xmf3Vector, XMMATRIX& xmxm4x4Transform)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, XMVector3TransformNormal(XMLoadFloat3(&xmf3Vector), xmxm4x4Transform));
		return(xmf3Result);
	}

	inline XMFLOAT3 TransformNormal(XMFLOAT3& xmf3Vector, XMFLOAT4X4& xmmtx4x4Matrix)
	{
		return(TransformNormal(xmf3Vector, XMLoadFloat4x4(&xmmtx4x4Matrix)));
	}

	inline XMFLOAT3 TransformNormal(XMFLOAT3 xmf3Vector, XMFLOAT4X4* xmmtx4x4Matrix)
	{
		return(TransformNormal(xmf3Vector, XMLoadFloat4x4(xmmtx4x4Matrix)));
	}

	inline XMFLOAT3 TransformCoord(XMFLOAT3& xmf3Vector, XMMATRIX& xmxm4x4Transform)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, XMVector3TransformCoord(XMLoadFloat3(&xmf3Vector), xmxm4x4Transform));
		return(xmf3Result);
	}

	/*inline XMFLOAT3 TransformCoord(XMFLOAT3 xmf3Vector, XMFLOAT4X4* xmxm4x4Transform)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, XMVector3TransformCoord(XMLoadFloat3(&xmf3Vector), XMLoadFloat4x4(xmxm4x4Transform)));
		return(xmf3Result);
	}*/

	inline XMFLOAT3 TransformCoord(XMFLOAT3& xmf3Vector, XMFLOAT4X4& xmmtx4x4Matrix)
	{
		return(TransformCoord(xmf3Vector, XMLoadFloat4x4(&xmmtx4x4Matrix)));
	}

	inline XMFLOAT3 Lerp(XMFLOAT3& xmf3Vector1, XMFLOAT3& xmf3Vector2, float t)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, XMVectorLerp(XMLoadFloat3(&xmf3Vector1), XMLoadFloat3(&xmf3Vector2), t));
		return xmf3Result;
	}

	inline XMFLOAT3 CatmullRom(XMFLOAT3& xmf3Vector1, XMFLOAT3& xmf3Vector2, XMFLOAT3& xmf3Vector3, XMFLOAT3& xmf3Vector4, float f)
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3(&xmf3Result, XMVectorCatmullRom(XMLoadFloat3(&xmf3Vector1), XMLoadFloat3(&xmf3Vector2),
			XMLoadFloat3(&xmf3Vector3), XMLoadFloat3(&xmf3Vector4), f));
		return xmf3Result;
	}
	// Update 5.5
	inline bool Equal(XMFLOAT3& xmf4Vector1, XMFLOAT3& xmf4Vector2)
	{
		XMFLOAT3 xmf4Result;
		XMStoreFloat3(&xmf4Result, XMVectorEqual(XMLoadFloat3(&xmf4Vector1), XMLoadFloat3(&xmf4Vector2)));
		if (xmf4Result.x == 0)return false;
		//if (xmf4Result.y == 0)return false;
		if (xmf4Result.z == 0)return false;
		return true;
	}
}
namespace Vector4
{
	inline XMFLOAT4 Add(XMFLOAT4& xmf4Vector1, XMFLOAT4& xmf4Vector2)
	{
		XMFLOAT4 xmf4Result;
		XMStoreFloat4(&xmf4Result, XMLoadFloat4(&xmf4Vector1) + XMLoadFloat4(&xmf4Vector2));
		return(xmf4Result);
	}

	inline bool Equal(XMFLOAT4& xmf4Vector1, XMFLOAT4& xmf4Vector2)
	{
		XMFLOAT4 xmf4Result;
		XMStoreFloat4(&xmf4Result, XMVectorEqual(XMLoadFloat4(&xmf4Vector1), XMLoadFloat4(&xmf4Vector2)));
		if (xmf4Result.x == 0)return false;
		if (xmf4Result.y == 0)return false;
		if (xmf4Result.z == 0)return false;
		return true;
	}
}
namespace Matrix4x4
{
	inline XMFLOAT4X4 Identity()
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixIdentity());
		return(xmmtx4x4Result);
	}

	inline XMFLOAT4X4 Multiply(XMFLOAT4X4& xmmtx4x4Matrix1, XMFLOAT4X4& xmmtx4x4Matrix2)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMLoadFloat4x4(&xmmtx4x4Matrix1) * XMLoadFloat4x4(&xmmtx4x4Matrix2));
		return(xmmtx4x4Result);
	}

	inline XMFLOAT4X4 Multiply(XMFLOAT4X4& xmmtx4x4Matrix1, XMMATRIX& xmmtxMatrix2)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMLoadFloat4x4(&xmmtx4x4Matrix1) * xmmtxMatrix2);
		return(xmmtx4x4Result);
	}

	inline XMFLOAT4X4 Multiply(XMMATRIX& xmmtxMatrix1, XMFLOAT4X4& xmmtx4x4Matrix2)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, xmmtxMatrix1 * XMLoadFloat4x4(&xmmtx4x4Matrix2));
		return(xmmtx4x4Result);
	}

	inline XMFLOAT4X4 Scaling(XMFLOAT4X4& xmf4x4Matrix, float x, float y, float z)
	{
		XMFLOAT4X4 xm4x4Result;
		XMStoreFloat4x4(&xm4x4Result, (XMLoadFloat4x4(&xmf4x4Matrix) * XMMatrixScaling(x, y, z)));
		return(xm4x4Result);
	}

	inline XMFLOAT4X4 RotationYawPitchRoll(float fPitch, float fYaw, float fRoll)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixRotationRollPitchYaw(XMConvertToRadians(fYaw), XMConvertToRadians(fPitch), XMConvertToRadians(fRoll)));
		return(xmmtx4x4Result);
	}

	inline XMFLOAT4X4 RotationAxis(XMFLOAT3& xmf3Axis, float fAngle)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixRotationAxis(XMLoadFloat3(&xmf3Axis), XMConvertToRadians(fAngle)));
		return(xmmtx4x4Result);
	}

	inline XMFLOAT4X4 Inverse(XMFLOAT4X4& xmmtx4x4Matrix)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixInverse(NULL, XMLoadFloat4x4(&xmmtx4x4Matrix)));
		return(xmmtx4x4Result);
	}

	inline XMFLOAT4X4 Transpose(XMFLOAT4X4& xmmtx4x4Matrix)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixTranspose(XMLoadFloat4x4(&xmmtx4x4Matrix)));
		return(xmmtx4x4Result);
	}

	inline XMFLOAT4X4 Transpose(XMFLOAT4X4* xmmtx4x4Matrix)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixTranspose(XMLoadFloat4x4(xmmtx4x4Matrix)));
		return(xmmtx4x4Result);
	}

	inline XMFLOAT4X4 MatrixAffineTransformation(XMFLOAT3& xmf3Scale, XMFLOAT3& xmf3RotationOrigin, XMFLOAT4& xmf4qRotation, XMFLOAT3& xmf3Translation)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixAffineTransformation(XMLoadFloat3(&xmf3Scale), XMLoadFloat3(&xmf3RotationOrigin), XMLoadFloat4(&xmf4qRotation), XMLoadFloat3(&xmf3Translation)));
		return(xmmtx4x4Result);
	}

	inline XMFLOAT4X4 PerspectiveFovLH(float fFovAngleY, float fAspectRatio, float fNearZ, float fFarZ)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixPerspectiveFovLH(XMConvertToRadians(fFovAngleY), fAspectRatio, fNearZ, fFarZ));
		return(xmmtx4x4Result);
	}

	inline XMFLOAT4X4 LookAtLH(XMFLOAT3& xmf3EyePosition, XMFLOAT3& xmf3LookAtPosition, XMFLOAT3& xmf3UpDirection)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixLookAtLH(XMLoadFloat3(&xmf3EyePosition), XMLoadFloat3(&xmf3LookAtPosition), XMLoadFloat3(&xmf3UpDirection)));
		return(xmmtx4x4Result);
	}
}
namespace Triangle
{
	inline bool Intersect(XMFLOAT3& xmf3RayPosition, XMFLOAT3& xmf3RayDirection, XMFLOAT3& v0, XMFLOAT3& v1, XMFLOAT3& v2, float& fHitDistance)
	{
		return(TriangleTests::Intersects(XMLoadFloat3(&xmf3RayPosition), XMLoadFloat3(&xmf3RayDirection), XMLoadFloat3(&v0), XMLoadFloat3(&v1), XMLoadFloat3(&v2), fHitDistance));
	}
}
namespace Plane
{
	inline XMFLOAT4 Normalize(XMFLOAT4& xmf4Plane)
	{
		XMFLOAT4 xmf4Result;
		XMStoreFloat4(&xmf4Result, XMPlaneNormalize(XMLoadFloat4(&xmf4Plane)));
		return(xmf4Result);
	}
}
namespace isNearOther
{
	inline bool isNearOther(XMFLOAT3& myPos, XMFLOAT3& otherPos, float distance)
	{
		float result = Vector3::Length(Vector3::Subtract(myPos, otherPos));
		return (result < distance) ? true : false;
	}
}
inline float NormalCoordX(float point, float width)
{
	return width * point;
}
inline float NormalCoordY(float point, float height)
{
	return height * point;
}

//---------------------------------------------------------------------------------------------

//instancing max Datas
const int MIXED_GRASS = 4;
const int CAR1NUM = 6;
const int TREE1NUM = 187;
const int FENCENUM = 78;
const int DRUMNUM = 10;
const int BUSNUM = 2;
const int TRUCKNUM = 1;
const int GENERATORNUM = 10;
const int BARKNUM = 190;
const int ESCAPEDOOR1 = 4;
const int ESCAPEDOOR2 = 4;
const int STONE = 13;
const int STREETLIGHT = 10;

const int MAX_LIGHTS = 14;
const float POINT_LIGHT = 1.0f;
const float SPOT_LIGHT = 2.0f;
const float DIRECTIONAL_LIGHT = 3.0f;

const int FRAME_BUFFER_WIDTH = 2000;
const int FRAME_BUFFER_HEIGHT = 1600;

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.

#define VS_SLOT_CAMERA					0x00
#define VS_SLOT_WORLD_MATRIX			0x01
#define VS_SLOT_FOG						0x02

#define PS_SLOT_COLOR					0x00
#define PS_SLOT_LIGHT					0x00
#define PS_SLOT_MATERIAL				0x01
#define PS_SLOT_FOG_SAMPLER_STATE		0x02

//camera
#define FIRST_PERSON_CAMERA				0x01
#define SPACESHIP_CAMERA				0x02
#define THIRD_PERSON_CAMERA				0x03
#define ASPECT_RATIO					(float(FRAME_BUFFER_WIDTH) / float(FRAME_BUFFER_HEIGHT))

//move
#define DIR_FORWARD						1 << 0
#define DIR_BACKWARD					1 << 1
#define DIR_LEFT						1 << 2
#define DIR_RIGHT						1 << 3
#define DIR_RIGHT_AND_FORWARD 

#define DIR_CROUCH						1 << 4
#define DIR_WALK						1 << 5
#define DIR_ATTACT						1 << 6
#define DIR_INTERACTION					0x40
#define DIR_TRAP						1 << 7
#define DIR_CROUCHUP					1 << 8
#define DIR_TRAPINSTALL					1 << 9

#define DIR_RIGHT_AND_FORWARD			1 << 10
#define DIR_LEFT_AND_FORWARD			1 << 11

//텍스쳐와 샘플러 상태를 설정하기 위한 쉐이더의 슬롯 번호를 정의한다. 
#define PS_SLOT_TEXTURE					0x00
#define PS_SLOT_SAMPLER_STATE			0x00

//animation
#define VS_SLOT_SKINNEDBONE				0x02
#define GetFBXMesh						GetGameObject(0)->GetMesh(0)
const float ANIFRAMETIME = 0.0333333f;

#define FBX
#define NETWORK 1

enum GameSceneState { Login = 0, Lobby = 1, Room = 2, InGame = 3, Result = 4, };
enum GameStaticModel { Car1, Tree1, Fence, Drum, Bus, Truck, Generator, Bark, Escapedoor1, Escapedoor2, Stone, StreetLight };
enum GamePlayer { MYPLAYER = 0, SECONDPLAYER = 1, };

static int NowPlayer = MYPLAYER;
static int SceneNumber = Login;
static bool killer = false;
static bool win = false;
static bool defeat = false;

//---------------------------------------------------------------------------------------------

#include "RBPlayerObject.h"
#include "SoundUtil.h"

//D2D
#include <d2d1_2.h>
#include <d2d1_2helper.h>
#include <dwrite.h>
#include <wincodec.h>
#include <d3d11_2.h>
#include <dxgi1_3.h>
#include <DxErr.h>

#include "BesideYou_Test.h"
#include "Timer.h"

#include "Mesh.h"
#include "FBXMesh.h"
#include "AssetMesh.h"
#include "SkyBoxMesh.h"

#include "Camera.h"
#include "Object.h"

#include "Player.h"
#include "Generator.h"

#include "Shader.h"
#include "FBXInstancingShader.h"
#include "CharacterShader.h"
#include "SkyBoxShader.h"
#include "FogShader.h"

#include "Light.h"
#include "CSVData.h"
#include "Scene.h"
#include "RoomScene.h"
#include "LobbyScene.h"
#include "LoginScene.h"
#include "ResultScene.h"

#include "GameFramework.h"
#include "FactoryPlayer.h"





