#include "stdafx.h"

ID3D11Buffer * CFogShader::m_pd3dcbFog = NULL;

CFogShader::CFogShader() : CShader()
{
	m_pd3dSamplerState = NULL;
}

CFogShader::~CFogShader()
{
	if (m_pd3dcbFog) m_pd3dcbFog->Release();
	if (m_pd3dSamplerState) m_pd3dSamplerState->Release();
}

void CFogShader::CreateShader(ID3D11Device * pd3dDevice)
{
	D3D11_INPUT_ELEMENT_DESC d3dInputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT nElements = ARRAYSIZE(d3dInputElements);
	CreateVertexShaderFromFile(pd3dDevice, L"Effect.fx", "FogVertexShader", "vs_5_0", &m_pd3dVertexShader, d3dInputElements, nElements, &m_pd3dVertexLayout);
	CreatePixelShaderFromFile(pd3dDevice, L"Effect.fx", "FogPixelShader", "ps_5_0", &m_pd3dPixelShader);
}

void CFogShader::BuildObjects(ID3D11Device * pd3dDevice)
{
	m_nObjects = 1;
	m_ppObjects = new CGameObject*[m_nObjects];

	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	d3dSamplerDesc.BorderColor[0] = 0;
	d3dSamplerDesc.BorderColor[1] = 0;
	d3dSamplerDesc.BorderColor[2] = 0;
	d3dSamplerDesc.BorderColor[3] = 0;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;
	pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &m_pd3dSamplerState);

	/*CCubeMesh *pCubeMesh;
	CFog * pGameObject = NULL;

	pGameObject = new CFog();
	pCubeMesh = new CCubeMesh(pd3dDevice, 1200.0f, 500.0f, 1200.0f, XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	pGameObject->SetMesh(pCubeMesh);*/

	CSphereMesh * pSphereMesh;
	CFog * pGameObject = NULL;
	pGameObject = new CFog();
	pSphereMesh = new CSphereMesh(pd3dDevice, 400.0f, 30, 30);
	pGameObject->SetMesh(pSphereMesh);

	m_ppObjects[0] = pGameObject;
}

void CFogShader::Render(ID3D11DeviceContext * pd3dDeviceContext, CCamera *pCamera)
{
	CShader::OnPrepareRender(pd3dDeviceContext);

	pd3dDeviceContext->PSSetSamplers(PS_SLOT_FOG_SAMPLER_STATE, 1, &m_pd3dSamplerState);

	CPlayer * player = pCamera->GetPlayer();
	/*XMFLOAT3 SetLength = Vector3::ScalarProduct(player->GetPosition(), 200.0f, true);
	XMFLOAT3 FogPosition = Vector3::Add(player->GetPosition(), SetLength);*/
	m_ppObjects[0]->SetPosition(player->GetPosition().x, player->GetPosition().y, player->GetPosition().z);

	m_ppObjects[0]->Render(pd3dDeviceContext, NULL);
}

void CFogShader::CreateShaderVariables(ID3D11Device *pd3dDevice)
{
	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	d3dBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	d3dBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	d3dBufferDesc.ByteWidth = sizeof(FogBufferType);
	pd3dDevice->CreateBuffer(&d3dBufferDesc, NULL, &m_pd3dcbFog);
}

void CFogShader::UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, float fogStart, float fogEnd)
{
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	pd3dDeviceContext->Map(m_pd3dcbFog, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	FogBufferType *pcbFog = (FogBufferType *)d3dMappedResource.pData;
	pcbFog->fogStart = fogStart;
	pcbFog->fogEnd = fogEnd;
	pd3dDeviceContext->Unmap(m_pd3dcbFog, 0);
	pd3dDeviceContext->VSSetConstantBuffers(VS_SLOT_FOG, 1, &m_pd3dcbFog);
}