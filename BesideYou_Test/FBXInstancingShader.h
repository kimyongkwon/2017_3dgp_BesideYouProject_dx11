#pragma once

#include "stdafx.h"

class CFBXInstancingShader : public CTexturedIlluminatedShader
{
private:
	CMaterial*				m_pMaterial;
	CTexture*				m_pTexture[12];

	int						m_nModelType;

	//인스턴싱 버퍼의 원소 크기와 버퍼 오프셋이다.
	UINT					m_nInstanceMatrixBufferStride;
	UINT					m_nInstanceMatrixBufferOffset;

	ID3D11Buffer*			m_pd3dInstanceMatrixBuffer;
public:
	CFBXInstancingShader();
	~CFBXInstancingShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);
	virtual void BuildInstanceObjects(ID3D11Device *pd3dDevice, ModelContainer * pFBXDatas, CCSVData * pCSVData, CHeightMapTerrain * pHeightMapTerrain, CScene * pSCene = NULL);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
	ID3D11Buffer * CreateInstanceBuffer(ID3D11Device *pd3dDevice, int nObjects, UINT nBufferStride, void *pBufferData);
};
