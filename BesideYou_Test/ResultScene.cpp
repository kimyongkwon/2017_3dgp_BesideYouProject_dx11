#include "stdafx.h"

bool bExitOK = false;

CResultScene::CResultScene() : CScene()
{
	//m_nBitmaps = 0;
}

CResultScene::~CResultScene()
{
}

bool CResultScene::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, POINT * cursorPos, ID2D1DeviceContext * pDeviceContext, CPlayerShader ** playerShader)
{
	float width = pDeviceContext->GetSize().width;
	float height = pDeviceContext->GetSize().height;

	switch (nMessageID)
	{
		//마우스 커서가 로그인이미지에 들어가면 활성화시킨다.
	case WM_MOUSEMOVE:
		if (cursorPos->x > NormalCoordX(0.878, width) && cursorPos->x < NormalCoordX(0.976, width) && cursorPos->y > NormalCoordY(0.013, height) && cursorPos->y < NormalCoordY(0.156, height)) bExitOK = true;
		else bExitOK = false;
		break;
	case WM_LBUTTONDOWN:
		if (bExitOK) {
			if (GameFlowManager::getInstance().type() == FLOW_RESULT) return true;
		}
		break;
	}
	return(false);
}

void CResultScene::Build2dObjects(IDWriteFactory *pdwFactory, IWICImagingFactory * pwicFactory, ID2D1DeviceContext * pd2dDeviceContext, ID2D1Factory1 * pd2dFactory)
{
	HRESULT result;

	//m_nBitmaps = 8;
	//m_vtd2dBitmap = new ID2D1Bitmap1*[m_nBitmaps];
	m_vtd2dBitmap.resize(8, nullptr);
	

	result = LoadImageFromFile(L"../Data/BesideYouData/UI/ResultSceneDiedBack.png", &m_vtd2dBitmap[0], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/ResultSceneExit.png", &m_vtd2dBitmap[1], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/ResultSceneExitOK.png", &m_vtd2dBitmap[2], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);

	result = LoadImageFromFile(L"../Data/BesideYouData/UI/ResultSceneVictoryBack.png", &m_vtd2dBitmap[3], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/ResultSceneExitBlue.png", &m_vtd2dBitmap[4], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/ResultSceneExitOKBlue.png", &m_vtd2dBitmap[5], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);

	result = LoadImageFromFile(L"../Data/BesideYouData/UI/ResultSceneKillerDefeat.png", &m_vtd2dBitmap[6], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/ResultSceneKillerVictory.png", &m_vtd2dBitmap[7], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
}

void CResultScene::Render2D(ID2D1DeviceContext * pd2dDeviceContext, IDWriteFactory * pdwFactory, ID2D1Factory1 * pd2dFactory)
{
	pd2dDeviceContext->BeginDraw();

	float width = pd2dDeviceContext->GetSize().width;
	float height = pd2dDeviceContext->GetSize().height;

	auto players = CPlayerManager::getInstance().players();
	size_t cnt = (*players).size();

	if (!GameFlowManager::getInstance().win){
		if ((*players).front()->m_pPlayerInfo->GetROLE() == SURVIVOR) {
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[0], D2D1::RectF(0.f, 0.f, width, height), 0.9f);
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[1], D2D1::RectF(NormalCoordX(0.878, width), NormalCoordY(0.013, height), NormalCoordX(0.976, width), NormalCoordY(0.156, height)), 0.9f);
			if (bExitOK) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[2], D2D1::RectF(NormalCoordX(0.878, width), NormalCoordY(0.013, height), NormalCoordX(0.976, width), NormalCoordY(0.156, height)), 0.9f);
		}
		else {
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[6], D2D1::RectF(0.f, 0.f, width, height), 0.9f);
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[4], D2D1::RectF(NormalCoordX(0.878, width), NormalCoordY(0.013, height), NormalCoordX(0.976, width), NormalCoordY(0.156, height)), 0.9f);
			if (bExitOK) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[5], D2D1::RectF(NormalCoordX(0.878, width), NormalCoordY(0.013, height), NormalCoordX(0.976, width), NormalCoordY(0.156, height)), 0.9f);
		}
	}

	else if (GameFlowManager::getInstance().win) {
		if ((*players).front()->m_pPlayerInfo->GetROLE() == SURVIVOR) {
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[3], D2D1::RectF(0.f, 0.f, width, height), 0.9f);
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[4], D2D1::RectF(NormalCoordX(0.878, width), NormalCoordY(0.013, height), NormalCoordX(0.976, width), NormalCoordY(0.156, height)), 0.9f);
			if (bExitOK) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[5], D2D1::RectF(NormalCoordX(0.878, width), NormalCoordY(0.013, height), NormalCoordX(0.976, width), NormalCoordY(0.156, height)), 0.9f);
		}
		else {
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[7], D2D1::RectF(0.f, 0.f, width, height), 0.9f);
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[1], D2D1::RectF(NormalCoordX(0.878, width), NormalCoordY(0.013, height), NormalCoordX(0.976, width), NormalCoordY(0.156, height)), 0.9f);
			if (bExitOK) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[2], D2D1::RectF(NormalCoordX(0.878, width), NormalCoordY(0.013, height), NormalCoordX(0.976, width), NormalCoordY(0.156, height)), 0.9f);
		}
	}
	

	pd2dDeviceContext->EndDraw();
}

void CResultScene::ReleaseObjects()
{
	if (!m_vtd2dBitmap.empty()) {
		for (auto i = 0; i < m_vtd2dBitmap.size(); ++i) {
			m_vtd2dBitmap[i]->Release();
				delete m_vtd2dBitmap[i];
		}
	}
	//delete[]m_vtd2dBitmap;

	CScene::ReleaseObjects();
}