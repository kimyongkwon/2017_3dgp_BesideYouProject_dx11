#include "stdafx.h"
#include<thread>

extern ClientNetwork Network;
extern HWND Ghwnd;
extern bool ones;

static bool bBoundingBoxRender = false;

CTexture* CreateTexture(ID3D11Device* pd3dDevice, WCHAR* ptrstring, ID3D11ShaderResourceView** pd3dsrvTexture, ID3D11SamplerState** pd3dSamplerState, int nTextureStartSlot, int nSamplerStartSlot)
{
	CTexture* pTexture = new CTexture(1, 1, nTextureStartSlot, nSamplerStartSlot);
	D3DX11CreateShaderResourceViewFromFile(pd3dDevice, ptrstring, NULL, NULL, pd3dsrvTexture, NULL);
	pTexture->SetTexture(0, *pd3dsrvTexture);
	pTexture->SetSampler(0, *pd3dSamplerState);
	(*pd3dsrvTexture)->Release();
	return pTexture;
}

CGameFramework::CGameFramework()
{
	m_nWndClientWidth = FRAME_BUFFER_WIDTH;
	m_nWndClientHeight = FRAME_BUFFER_HEIGHT;

	//씬, 캐릭터 영역 초기화
	m_vtScenes.resize(5,nullptr);
	m_vtPlayers.resize(4, nullptr);

	_tcscpy_s(m_pszBuffer, _T("BesideYou"));

	m_nCameras = 4;
	m_ppCamera = new CCamera*[m_nCameras];
	for (int i = 0; i < m_nCameras; i++)
		m_ppCamera[i] = NULL;

	m_nPlayerShaders = 2;
	m_ppPlayerShaders = new CPlayerShader*[m_nPlayerShaders];
	for (int i = 0; i < m_nPlayerShaders; i++)
		m_ppPlayerShaders[i] = NULL;

	m_nPlayerBoxShaders = 2;
	m_ppPlayerBoxShaders = new CPlayerAABBShader*[m_nPlayerBoxShaders];
	for (int i = 0; i < m_nPlayerBoxShaders; i++)
		m_ppPlayerBoxShaders[i] = NULL;

	OpenDoorlock = true;
}

CGameFramework::~CGameFramework()
{
}

//다음 함수는 응용 프로그램이 실행되면 호출된다는 것에 유의하라. 
bool CGameFramework::OnCreate(HINSTANCE hInstance, HWND hMainWnd)
{
	m_hInstance = hInstance;
	m_hWnd = hMainWnd;
	
	//Direct3D 디바이스, 디바이스 컨텍스트, 스왑 체인 등을 생성하는 함수를 호출한다. 
	if (!CreateDirect3DDisplay()) return(false);

	//렌더링할 객체(게임 월드 객체)를 생성한다. 
	BuildObjects();

	return(true);
}

bool CGameFramework::CreateRenderTargetDepthStencilView()
{
	HRESULT hResult = S_OK;

	ID3D11Texture2D *pd3dBackBuffer;
	if (FAILED(hResult = m_pDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID *)&pd3dBackBuffer))) return(false);
	if (FAILED(hResult = m_pd3dDevice->CreateRenderTargetView(pd3dBackBuffer, NULL, &m_pd3dRenderTargetView))) return(false);
	if (pd3dBackBuffer) pd3dBackBuffer->Release();

	//렌더 타겟과 같은 크기의 깊이 버퍼(Depth Buffer)를 생성한다.
	D3D11_TEXTURE2D_DESC d3dDepthStencilBufferDesc;
	ZeroMemory(&d3dDepthStencilBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));
	d3dDepthStencilBufferDesc.Width = m_nWndClientWidth;
	d3dDepthStencilBufferDesc.Height = m_nWndClientHeight;
	d3dDepthStencilBufferDesc.MipLevels = 1;
	d3dDepthStencilBufferDesc.ArraySize = 1;
	d3dDepthStencilBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	d3dDepthStencilBufferDesc.SampleDesc.Count = 1;
	d3dDepthStencilBufferDesc.SampleDesc.Quality = 0;
	d3dDepthStencilBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dDepthStencilBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	d3dDepthStencilBufferDesc.CPUAccessFlags = 0;
	d3dDepthStencilBufferDesc.MiscFlags = 0;
	if (FAILED(hResult = m_pd3dDevice->CreateTexture2D(&d3dDepthStencilBufferDesc, NULL, &m_pd3dDepthStencilBuffer))) return(false);

	//생성한 깊이 버퍼(Depth Buffer)에 대한 뷰를 생성한다.
	D3D11_DEPTH_STENCIL_VIEW_DESC d3dViewDesc;
	ZeroMemory(&d3dViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	d3dViewDesc.Format = d3dDepthStencilBufferDesc.Format;
	d3dViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	d3dViewDesc.Texture2D.MipSlice = 0;
	if (FAILED(hResult = m_pd3dDevice->CreateDepthStencilView(m_pd3dDepthStencilBuffer, &d3dViewDesc, &m_pd3dDepthStencilView))) return(false);

	m_pd3dDeviceContext->OMSetRenderTargets(1, &m_pd3dRenderTargetView, m_pd3dDepthStencilView);

	//d2d
	float fdpiX, fdpiY;
	m_pd2dFactory->GetDesktopDpi(&fdpiX, &fdpiY);
	D2D1_BITMAP_PROPERTIES1 bitmapProperties =
		D2D1::BitmapProperties1(
			D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
			D2D1::PixelFormat(DXGI_FORMAT_R8G8B8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED)
			, fdpiX
			, fdpiY
			);
	IDXGISurface2 *dxgiBackBuffer;
	hResult = m_pDXGISwapChain->GetBuffer(0, IID_PPV_ARGS(&dxgiBackBuffer));
	hResult = m_pd2dDeviceContext->CreateBitmapFromDxgiSurface(dxgiBackBuffer, &bitmapProperties, &m_pd2dBitmapBackBuffer);
	m_pd2dDeviceContext->SetTarget(m_pd2dBitmapBackBuffer);
	dxgiBackBuffer->Release();

	return(true);
}

bool CGameFramework::CreateDirect3DDisplay()
{
	RECT rcClient;
	::GetClientRect(m_hWnd, &rcClient);
	m_nWndClientWidth = rcClient.right - rcClient.left;
	m_nWndClientHeight = rcClient.bottom - rcClient.top;

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	::ZeroMemory(&dxgiSwapChainDesc, sizeof(dxgiSwapChainDesc));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = m_nWndClientWidth;
	dxgiSwapChainDesc.BufferDesc.Height = m_nWndClientHeight;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = m_hWnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;
	dxgiSwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH/*0*/;

	UINT dwCreateDeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
#ifdef _DEBUG
	dwCreateDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE
	};
	UINT nDriverTypes = sizeof(d3dDriverTypes) / sizeof(D3D_DRIVER_TYPE);

	D3D_FEATURE_LEVEL pd3dFeatureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0
	};
	UINT nFeatureLevels = sizeof(pd3dFeatureLevels) / sizeof(D3D_FEATURE_LEVEL);

	D3D_DRIVER_TYPE nd3dDriverType = D3D_DRIVER_TYPE_NULL;
	D3D_FEATURE_LEVEL nd3dFeatureLevel = D3D_FEATURE_LEVEL_11_0;
	HRESULT hResult = S_OK;
#ifdef _WITH_DEVICE_AND_SWAPCHAIN
	for (UINT i = 0; i < nDriverTypes; i++)
	{
		nd3dDriverType = d3dDriverTypes[i];
		if (SUCCEEDED(hResult = D3D11CreateDeviceAndSwapChain(NULL, nd3dDriverType, NULL, dwCreateDeviceFlags, pd3dFeatureLevels, nFeatureLevels, D3D11_SDK_VERSION, &dxgiSwapChainDesc, &m_pDXGISwapChain, &m_pd3dDevice, &nd3dFeatureLevel, &m_pd3dDeviceContext))) break;
	}
#else
	for (UINT i = 0; i < nDriverTypes; i++)
	{
		nd3dDriverType = d3dDriverTypes[i];
		if (SUCCEEDED(hResult = D3D11CreateDevice(NULL, nd3dDriverType, NULL, dwCreateDeviceFlags, pd3dFeatureLevels, nFeatureLevels, D3D11_SDK_VERSION, &m_pd3dDevice, &nd3dFeatureLevel, &m_pd3dDeviceContext))) break;
	}
	if (!m_pd3dDevice) return(false);

	if (FAILED(hResult = m_pd3dDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &m_n4xMSAAQualities))) return(false);
#ifdef _WITH_MSAA4_MULTISAMPLING
	dxgiSwapChainDesc.SampleDesc.Count = 4;
	dxgiSwapChainDesc.SampleDesc.Quality = m_n4xMSAAQualities - 1;
#else
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
#endif
	IDXGIFactory1 *pdxgiFactory = NULL;
	if (FAILED(hResult = CreateDXGIFactory1(__uuidof(IDXGIFactory1), (void **)&pdxgiFactory))) return(false);
	IDXGIDevice2 *pdxgiDevice = NULL;
	if (FAILED(hResult = m_pd3dDevice->QueryInterface(__uuidof(IDXGIDevice2), (void **)&pdxgiDevice))) return(false);
	if (FAILED(hResult = pdxgiFactory->CreateSwapChain(pdxgiDevice, &dxgiSwapChainDesc, &m_pDXGISwapChain))) return(false);

	D2D1_FACTORY_OPTIONS options;
	ZeroMemory(&options, sizeof(D2D1_FACTORY_OPTIONS));
	options.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;

	hResult = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, __uuidof(ID2D1Factory1), &options, reinterpret_cast<LPVOID*>(&m_pd2dFactory));
	hResult = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), (IUnknown **)&m_pdwFactory);
	CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
	hResult = CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&m_pwicFactory));
	hResult = m_pd2dFactory->CreateDevice(pdxgiDevice, &m_pd2dDevice);
	hResult = m_pd2dDevice->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE, &m_pd2dDeviceContext);

	if (pdxgiDevice) pdxgiDevice->Release();
	if (pdxgiFactory) pdxgiFactory->Release();
#endif

	if (!CreateRenderTargetDepthStencilView()) return(false);

	return(true);
}

void CGameFramework::CreateAllShaderVariable()
{
	CShader::CreateShaderVariables(m_pd3dDevice);

	CIlluminatedShader::CreateShaderVariables(m_pd3dDevice);

	CFogShader::CreateShaderVariables(m_pd3dDevice);

}

void CGameFramework::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	bool NextSceneOK;
	switch (nMessageID)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		SetCapture(hWnd);
		GetCursorPos(&m_ptInGameSceneOldCursorPos);
		if (SceneNumber == Login || SceneNumber == Lobby || SceneNumber == Room)
			ScreenToClient(hWnd, &m_ptInGameSceneOldCursorPos);         //현재 실행되고있는 클라이언트 영역으로 커서포인트를 바꿔준다.
		if (SceneNumber == Login) {
			NextSceneOK = m_vtScenes[Login]->OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam, &m_ptInGameSceneOldCursorPos, m_pd2dDeviceContext);
			if (NextSceneOK) SceneNumber = Lobby;						//로그인버튼을 누르면 Lobby씬으로 넘어간다.
		}
		if (SceneNumber == Lobby) {
			NextSceneOK = m_vtScenes[Lobby]->OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam, &m_ptInGameSceneOldCursorPos, m_pd2dDeviceContext);
			if (NextSceneOK) SceneNumber = Room;
		}
		if (SceneNumber == Room) {
			NextSceneOK = m_vtScenes[Room]->OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam, &m_ptInGameSceneOldCursorPos, m_pd2dDeviceContext);
			if (NextSceneOK)
			{
				SoundEngine::getInstance().execute(MAIN_SOUND, false, false);
				SoundEngine::getInstance().execute(INGAME_SOUND, true, false);
				SceneNumber = InGame;
			}
		}

		//엔딩창에서 다시 로비화면으로 돌아간다.
		if (SceneNumber == Result) {
			NextSceneOK = m_vtScenes[Result]->OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam, &m_ptInGameSceneOldCursorPos, m_pd2dDeviceContext);
			if (NextSceneOK) {
				GameFlowManager::getInstance().ENTER(FLOW_ROBBY);
				SceneNumber = Lobby;
				SoundEngine::getInstance().execute(INGAME_SOUND, false, false);
				SoundEngine::getInstance().execute(MAIN_SOUND, true, false);
			}
		}
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		if (SceneNumber == InGame || SceneNumber == Login || SceneNumber == Lobby || SceneNumber == Room || SceneNumber == Result)
			ReleaseCapture();
		break;
	case WM_MOUSEMOVE:
		GetCursorPos(&m_ptMoveCursorPos);
		ScreenToClient(hWnd, &m_ptMoveCursorPos); //현재 실행되고있는 클라이언트 영역으로 커서포인트를 바꿔준다.
		if (SceneNumber == Login) m_vtScenes[Login]->OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam, &m_ptMoveCursorPos, m_pd2dDeviceContext);
		if (SceneNumber == Lobby) m_vtScenes[Lobby]->OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam, &m_ptMoveCursorPos, m_pd2dDeviceContext);
		if (SceneNumber == Room) m_vtScenes[Room]->OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam, &m_ptMoveCursorPos, m_pd2dDeviceContext);
		if (SceneNumber == Result) m_vtScenes[Result]->OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam, &m_ptMoveCursorPos, m_pd2dDeviceContext);
		break;
	default:
		break;
	}
}

void CGameFramework::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_CHAR:
		if (SceneNumber == Login)
			m_vtScenes[Login]->OnProcessingKeyboardMessage(hWnd, nMessageID, wParam, lParam);
		break;
	case WM_KEYUP:
		switch (wParam)
		{
			/*‘F1’ 키를 누르면 1인칭 카메라, ‘F2’ 키를 누르면 스페이스-쉽 카메라로 변경한다, ‘F3’ 키를 누르면 3인칭 카메라로 변경한다.*/
		case VK_F1:
		case VK_F2:
		case VK_F3:
		{
			if (m_vtPlayers[MYPLAYER]) m_vtPlayers[MYPLAYER]->ChangeCamera(m_pd3dDevice, (wParam - VK_F1 + 1), m_GameTimer.GetTimeElapsed());
			m_ppCamera[MYPLAYER] = m_vtPlayers[MYPLAYER]->GetCamera();
			//씬에 현재 카메라를 설정한다.
			m_vtScenes[InGame]->SetCamera(m_ppCamera[MYPLAYER]);
		}
			break;
			case VK_F9:
		{
			BOOL bFullScreenState = FALSE;
			m_pDXGISwapChain->GetFullscreenState(&bFullScreenState, NULL);
			if (!bFullScreenState)
			{
				DXGI_MODE_DESC dxgiTargetParameters;
				dxgiTargetParameters.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				dxgiTargetParameters.Width = m_nWndClientWidth;
				dxgiTargetParameters.Height = m_nWndClientHeight;
				dxgiTargetParameters.RefreshRate.Numerator = 60;
				dxgiTargetParameters.RefreshRate.Denominator = 1;
				dxgiTargetParameters.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
				dxgiTargetParameters.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
				m_pDXGISwapChain->ResizeTarget(&dxgiTargetParameters);
			}
			m_pDXGISwapChain->SetFullscreenState(!bFullScreenState, NULL);

			OnResizeBackBuffer();

			break;
		}
		case '1':
			if (SceneNumber == InGame) 
			{
				ClientNetworkInterface::getInstance().ClientChitt();
			}
			break;
		case '2':
			break;
		case '3':
			break;
		case '4':
			break;
		case '6':
			OnOffLights();
			break;
		case '0':
			break;
		case '9':
			break;
		case VK_ESCAPE:
			::PostQuitMessage(0);
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
}

LRESULT CALLBACK CGameFramework::OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{

	switch (nMessageID)
	{
		/*윈도우의 크기가 변경될 때(현재는 “Alt+Enter“ 전체 화면 모드와 윈도우 모드로 전환될 때) 스왑 체인의 후면버퍼 크기를 조정하고 후면버퍼에 대한 렌더 타겟 뷰를 다시 생성한다. */
	case WM_SIZE:
	{
		m_nWndClientWidth = LOWORD(lParam);
		m_nWndClientHeight = HIWORD(lParam);

		m_pd3dDeviceContext->OMSetRenderTargets(0, NULL, NULL);

		OnResizeBackBuffer();

		if (GameFlowManager::getInstance().type() == FLOW_INGAME) {
			CCamera *pCamera = m_vtPlayers[0]->GetCamera();
			if (pCamera) pCamera->SetViewport(m_pd3dDeviceContext, 0, 0, m_nWndClientWidth, m_nWndClientHeight, 0.0f, 1.0f);
		}

		break;
	}
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
		OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam);
		break;
	case WM_KEYDOWN:
	case WM_KEYUP:
	case WM_CHAR:
		OnProcessingKeyboardMessage(hWnd, nMessageID, wParam, lParam);
		break;
	}
	return(0);
}

void CGameFramework::OnResizeBackBuffer(void)
{
	if (m_pd3dRenderTargetView) m_pd3dRenderTargetView->Release();
	if (m_pd3dDepthStencilBuffer) m_pd3dDepthStencilBuffer->Release();
	if (m_pd3dDepthStencilView) m_pd3dDepthStencilView->Release();

	if (m_pd2dBitmapBackBuffer) m_pd2dBitmapBackBuffer->Release();
	m_pd2dDeviceContext->SetTarget(NULL);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	m_pDXGISwapChain->GetDesc(&dxgiSwapChainDesc);
	m_pDXGISwapChain->ResizeBuffers(2, m_nWndClientWidth, m_nWndClientHeight, dxgiSwapChainDesc.BufferDesc.Format, dxgiSwapChainDesc.Flags);

	CreateRenderTargetDepthStencilView();
}

//다음 함수는 응용 프로그램이 종료될 때 호출된다는 것에 유의하라. 
void CGameFramework::OnDestroy()
{
	//게임 객체를 소멸한다. 
	ReleaseObjects();

	//Direct3D와 관련된 객체를 소멸한다. 
	if (m_pd3dDeviceContext) m_pd3dDeviceContext->ClearState();
	if (m_pd3dRenderTargetView) m_pd3dRenderTargetView->Release();

	if (m_pd3dDepthStencilBuffer) m_pd3dDepthStencilBuffer->Release();
	if (m_pd3dDepthStencilView) m_pd3dDepthStencilView->Release();

	if (m_pDXGISwapChain) m_pDXGISwapChain->Release();
	if (m_pd3dDeviceContext) m_pd3dDeviceContext->Release();
	if (m_pd3dDevice) m_pd3dDevice->Release();

	//다이렉트2
	if (m_pd2dFactory) m_pd2dFactory->Release();
	if (m_pd2dDevice) m_pd2dDevice->Release();
	if (m_pd2dDeviceContext) m_pd2dDeviceContext->Release();
	if (m_pdwFactory) m_pdwFactory->Release();
	if (m_pwicFactory) m_pwicFactory->Release();
	if (m_pd2dBitmapBackBuffer) m_pd2dBitmapBackBuffer->Release();

	//2dObject
	if (m_dwExplainFormat) m_dwExplainFormat->Release();
	if (m_dwMyChattingFormat) m_dwMyChattingFormat->Release();
	if (m_pd2drcBox) m_pd2drcBox->Release();
	if (m_pd2dsbrBeige) m_pd2dsbrBeige->Release();
	if (m_pd2dsbrGreenColor) m_pd2dsbrGreenColor->Release();
	if (m_pd2dsbrRedColor) m_pd2dsbrRedColor->Release();
}

void CGameFramework::BuildObjects()
{
	CreateAllShaderVariable();
	FBXModelDataLoad();

	CSVDataLoad();

	m_vtScenes[Login] = new CLoginScene();
	m_vtScenes[Lobby] = new CLobbyScene();
	m_vtScenes[Room] = new CRoomScene();
	m_vtScenes[Result] = new CResultScene();
	m_vtScenes[InGame] = new CScene();
	
	m_vtScenes[Login]->Build2dObjects(m_pdwFactory,  m_pwicFactory, m_pd2dDeviceContext, m_pd2dFactory);
	m_vtScenes[Lobby]->Build2dObjects(m_pdwFactory, m_pwicFactory, m_pd2dDeviceContext, m_pd2dFactory);
	m_vtScenes[Room]->Build2dObjects(m_pdwFactory, m_pwicFactory, m_pd2dDeviceContext, m_pd2dFactory);
	m_vtScenes[Result]->Build2dObjects(m_pdwFactory, m_pwicFactory, m_pd2dDeviceContext, m_pd2dFactory);
	m_vtScenes[InGame]->BuildObjects(m_pd3dDevice, m_vtFBXDatas, m_deqCSVDatas);
	m_vtScenes[InGame]->Build2dObjects(m_pdwFactory, m_pwicFactory, m_pd2dDeviceContext, m_pd2dFactory);

	m_vtInstancingShader = m_vtScenes[InGame]->GetInstancingShader();

	//---------------------------------------------------
	m_ppPlayerShaders[MYPLAYER] = new CPlayerShader();
	m_ppPlayerShaders[MYPLAYER]->CreateShader(m_pd3dDevice);

	m_ppPlayerShaders[SECONDPLAYER] = new CPlayerShader();
	m_ppPlayerShaders[SECONDPLAYER]->CreateShader(m_pd3dDevice);

	//테스트를 위한 player fake 바운딩 박스를 만든다.
	m_ppPlayerBoxShaders[MYPLAYER] = new CPlayerAABBShader();
	m_ppPlayerBoxShaders[MYPLAYER]->CreateShader(m_pd3dDevice);

	//trap
	for (int i = 0; i < 15; ++i)
	{
		CTrapShader * pTrapShader = new CTrapShader();
		pTrapShader->CreateShader(m_pd3dDevice);
		pTrapShader->BuildObjects(m_pd3dDevice);

		CPlayerManager::getInstance().m_ListTrapShader.push_back(pTrapShader);
	}

	//테스트를 위한 player fake 바운딩 박스를 만든다.
	m_ppPlayerBoxShaders[SECONDPLAYER] = new CPlayerAABBShader();
	m_ppPlayerBoxShaders[SECONDPLAYER]->CreateShader(m_pd3dDevice);

	//sound
	SoundEngine::getInstance().execute(MAIN_SOUND, true, false);
#if 0
	m_ppPlayerShaders[FIRST]->BuildObjects(m_pd3dDevice, 0);

	m_vtPlayers[FIRST] = m_ppPlayerShaders[FIRST]->GetPlayer();

	auto players = CPlayerManager::getInstance().players();
	(*players).push_back(m_vtPlayers[0]);

	CHeightMapTerrain * pTerrain = m_vtScenes[InGame]->GetTerrain();
	m_vtPlayers[FIRST]->SetPosition(XMFLOAT3(pTerrain->GetWidth() * 0.5f, pTerrain->GetPeakHeight(), pTerrain->GetLength() * 0.5f));
	//m_pPlayer->SetPosition(D3DXVECTOR3(0, pTerrain->GetPeakHeight() + 1000.0f, 0));
	//플레이어의 위치가 변경될 때 지형의 정보에 따라 플레이어의 위치를 변경할 수 있도록 설정한다.
	m_vtPlayers[FIRST]->SetPlayerUpdatedContext(pTerrain);
	//카메라의 위치가 변경될 때 지형의 정보에 따라 카메라의 위치를 변경할 수 있도록 설정한다.
	m_vtPlayers[FIRST]->SetCameraUpdatedContext(pTerrain);

	//이거 왜 있는거지?
	m_ppCamera[FIRST] = m_vtPlayers[FIRST]->GetCamera();
	m_ppCamera[FIRST]->SetViewport(m_pd3dDeviceContext, 0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
	m_ppCamera[FIRST]->GenerateViewMatrix();

	//----------------------------------------------------



	//----------------------------------------------------
	m_ppPlayerShaders[SECOND] = new CPlayerShader();
	m_ppPlayerShaders[SECOND]->CreateShader(m_pd3dDevice);
	m_ppPlayerShaders[SECOND]->BuildObjects(m_pd3dDevice, 1);

	m_vtPlayers[SECOND] = m_ppPlayerShaders[SECOND]->GetPlayer();

	//players = CPlayerManager::getInstance().players();
	(*players).push_back(m_vtPlayers[1]);

	pTerrain = m_vtScenes[InGame]->GetTerrain();
	m_vtPlayers[SECOND]->SetPosition(XMFLOAT3(pTerrain->GetWidth() * 0.5f, pTerrain->GetPeakHeight(), pTerrain->GetLength() * 0.5f));
	//m_pPlayer->SetPosition(D3DXVECTOR3(0, pTerrain->GetPeakHeight() + 1000.0f, 0));
	//플레이어의 위치가 변경될 때 지형의 정보에 따라 플레이어의 위치를 변경할 수 있도록 설정한다.
	m_vtPlayers[SECOND]->SetPlayerUpdatedContext(pTerrain);
	//카메라의 위치가 변경될 때 지형의 정보에 따라 카메라의 위치를 변경할 수 있도록 설정한다.
	m_vtPlayers[SECOND]->SetCameraUpdatedContext(pTerrain);

	//이거 왜 있는거지?
	m_ppCamera[SECOND] = m_vtPlayers[SECOND]->GetCamera();
	m_ppCamera[SECOND]->SetViewport(m_pd3dDeviceContext, 0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
	m_ppCamera[SECOND]->GenerateViewMatrix();
#endif
	//---------------------------------------------------
}

void CGameFramework::SubBuildObject(int who, ROLE role)
{
	m_ppPlayerShaders[who]->BuildObjects(m_pd3dDevice, role, who);

	m_vtPlayers[who] = m_ppPlayerShaders[who]->GetPlayer();

	auto players = CPlayerManager::getInstance().players();
	if (who == 0) 
		(*players).push_front(m_vtPlayers[who]);
	else 
		(*players).push_back(m_vtPlayers[who]);

	CHeightMapTerrain * pTerrain = m_vtScenes[InGame]->GetTerrain();
	m_vtPlayers[who]->SetPosition(XMFLOAT3(pTerrain->GetWidth() * 0.5f, pTerrain->GetPeakHeight(), pTerrain->GetLength() * 0.5f));
	//m_pPlayer->SetPosition(D3DXVECTOR3(0, pTerrain->GetPeakHeight() + 1000.0f, 0));
	//플레이어의 위치가 변경될 때 지형의 정보에 따라 플레이어의 위치를 변경할 수 있도록 설정한다.
	m_vtPlayers[who]->SetPlayerUpdatedContext(pTerrain);
	//카메라의 위치가 변경될 때 지형의 정보에 따라 카메라의 위치를 변경할 수 있도록 설정한다.
	m_vtPlayers[who]->SetCameraUpdatedContext(pTerrain);

	//이거 왜 있는거지?
	m_ppCamera[who] = m_vtPlayers[who]->GetCamera();
	m_ppCamera[who]->SetViewport(m_pd3dDeviceContext, 0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
	m_ppCamera[who]->GenerateViewMatrix();

	//player collision box shader rendering
	//일단 who를 0으로 놓고 한다. 5.11
	//1번플레이어(butcher만 playerAABBShader가 적용
	m_ppPlayerBoxShaders[who]->BuildObjects(m_pd3dDevice, m_vtPlayers, who);
}

void CGameFramework::ReleaseObjects()
{
	//CShader 클래스의 정적(static) 멤버 변수로 선언된 상수 버퍼를 반환한다.
	CShader::ReleaseShaderVariables();

	if (!m_vtScenes.empty()) {
		for (int i = 0; i < m_vtScenes.size(); i++) {
			m_vtScenes[i]->ReleaseObjects();
			delete m_vtScenes[i];
		}
	}

	if (!m_vtPlayers.empty()) {
		for (int i = 0; i < m_vtPlayers.size(); i++) {
			m_vtPlayers[i]->Release();
			delete m_vtPlayers[i];
		}
	}

	if (m_ppPlayerShaders) {
		for (int i = 0; i < m_nPlayerShaders; i++) {
			m_ppPlayerShaders[i]->ReleaseObjects();
			delete m_ppPlayerShaders[i];
		}
		delete m_ppPlayerShaders;
	}

	if (m_ppPlayerBoxShaders) {
		for (int i = 0; i < m_nPlayerBoxShaders; i++) {
			delete m_ppPlayerBoxShaders[i];
		}
		delete m_ppPlayerBoxShaders;
	}
}

void CGameFramework::ProcessInput()
{
	bool bProcessedByScene = false;

	if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->IsReplayflag()) {
		m_vtPlayers[MYPLAYER]->PlayerObjectReplay();
		m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetReplayflag(false);
	}
	if (!bProcessedByScene)
	{
		static UCHAR pKeyBuffer[256];
		DWORD dwDirection = 0;
		int getNumber = -1;
		// 속도에 대한 변수 PlayerSpeed 생성
		double PlayerSpeed = (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER) ? 53.7 : 50.f;
		//double PlayerSpeed = 53.7f;
		/*키보드의 상태 정보를 반환한다. 화살표 키(‘→’, ‘←’, ‘↑’, ‘↓’)를 누르면 플레이어를 오른쪽/왼쪽(로컬 x-축), 앞/뒤(로컬 z-축)로 이동한다. ‘Page Up’과 ‘Page Down’ 키를 누르면 플레이어를 위/아래(로컬 y-축)로 이동한다.*/
		if (GetKeyboardState(pKeyBuffer))
		{
			//if (m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->GetAnimation() == SINSTALLTRAP) return;

			if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR)
			{
				//4
				for (auto i : CPlayerManager::getInstance().m_mpGenlist) {
					if (i.second->IsComplete()) {
						if (i.second->GetStartSound() == false) {
							SoundEngine::getInstance().execute(STARTGENON, true, false);
							i.second->SetStartSound(true);
						}
						if (SoundEngine::getInstance().mSoundUtil.isPlaying(4) == false)
						{
							if (isNearOther::isNearOther(m_vtPlayers[MYPLAYER]->GetPosition(), i.second->GetPosition(), 200.0f) && (i.second->GetSound() == false))
							{
								SoundEngine::getInstance().execute(AFTERGENON, true, false);
								i.second->SetSound(true);
							}
							else if ((isNearOther::isNearOther(m_vtPlayers[MYPLAYER]->GetPosition(), i.second->GetPosition(), 200.0f) == false) && (i.second->GetSound()))
							{
								SoundEngine::getInstance().execute(AFTERGENON, false, false);
								i.second->SetSound(false);
							}
						}
					}
				}
			}

			// shift
			if (pKeyBuffer[VK_LSHIFT] & 0xF0 && (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR)) {
				// 걷는 키가 눌릴경우 속도 변경
				PlayerSpeed = 15.f;
				if ((m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_INTERACTION)
					&& (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_TRAPINSTALL)
					&& (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_STEPONTRAP)
					&& (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_HITTED))
				{
					if (pKeyBuffer['W'] & 0xF0)
					{
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != ANIMATION_TYPE::TYPE_FORWORDWALK) {
							m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_FORWORDWALK);
							m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 90.0f);
							m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SWALK);
						}
						dwDirection |= DIR_WALK | DIR_FORWARD;
						m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
					}
					if (pKeyBuffer['S'] & 0xF0)
					{
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != ANIMATION_TYPE::TYPE_BACKWALK) {
							m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_BACKWALK);
							m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, -90.0f);
							m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SWALK);
						}
						dwDirection |= DIR_WALK | DIR_BACKWARD;
						m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
					}
					if (pKeyBuffer['A'] & 0xF0)
					{
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != ANIMATION_TYPE::TYPE_LEFTWALK) {
							m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_LEFTWALK);
							m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 0.0f);
							m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SWALK);
						}
						dwDirection |= DIR_WALK | DIR_LEFT;
						m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
					}
					if (pKeyBuffer['D'] & 0xF0)
					{
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != ANIMATION_TYPE::TYPE_RIGHTWALK) {
							m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_RIGHTWALK);
							m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 180.0f);
							m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SWALK);
						}
						dwDirection |= DIR_WALK | DIR_RIGHT;
						m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
					}
				}
				
			}
			// ctrl
			else if (pKeyBuffer[VK_LCONTROL] & 0xF0 && (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR)) {
				if ((m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_INTERACTION)
					&& (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_TRAPINSTALL)
					&& (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_STEPONTRAP)
					&& (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_HITTED))
				{
					// 앉는 키가 눌릴경우 속도 변경
					dwDirection |= DIR_CROUCH;
					PlayerSpeed = 18.f;
					bool isMaxKey = false;
					if (0 == (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() & TYPE_CROUSHNOTOFF))
					{
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_CROUSHON)
						{
							m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHON);
							m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SSTANDINGTOCROUCH);
							
						}
					}
					else
					{
						if (pKeyBuffer['W'] & 0xF0)
						{
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != ANIMATION_TYPE::TYPE_CROUSHFORWORDWALK) {
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHFORWORDWALK);
								m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 90.0f);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SCROUCHWALK);
							}
							dwDirection |= DIR_FORWARD;
							isMaxKey = true;
							m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
						}
						if (pKeyBuffer['S'] & 0xF0)
						{
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != ANIMATION_TYPE::TYPE_CROUSHBACKWALK) {
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHBACKWALK);
								m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, -90.0f);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SCROUCHWALK);
							}
							isMaxKey = true;
							dwDirection |= DIR_BACKWARD;
							m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
						}
						if (pKeyBuffer['A'] & 0xF0)
						{
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != ANIMATION_TYPE::TYPE_CROUSHLEFTWALK) {
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHLEFTWALK);
								m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 0.0f);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SCROUCHWALK);
							}
							isMaxKey = true;
							dwDirection |= DIR_LEFT;
							m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
						}
						if (pKeyBuffer['D'] & 0xF0)
						{
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != ANIMATION_TYPE::TYPE_CROUSHRIGHTWALK) {
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHRIGHTWALK);
								m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 180.0f);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SCROUCHWALK);
							}
							isMaxKey = true;
							dwDirection |= DIR_RIGHT;
							m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
						}
						if (!isMaxKey)
						{
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != ANIMATION_TYPE::TYPE_CROUSHIDLE) {
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_CROUSHIDLE);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SCROUCHIDLE);
							}
						}
					}
				}
			}
			else
			{
				if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() & TYPE_CROUSHNOTOFF)
				{
					m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_CROUSHOFF);
					m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SCROUCHTOSTANDING);
					dwDirection |= DIR_CROUCHUP;

				}
				// test
			/*	else if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() & TYPE_CROUSHOFF)
				{
					dwDirection |= DIR_CROUCHUP;
				}*/
				if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR && ( 0 == (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() & (TYPE_HITTED | TYPE_CROUSHALL)))) {
					if (pKeyBuffer[2] & 0xF0) {
						m_vtPlayers[MYPLAYER]->SetMotionAABB();
						XMFLOAT3 xm;
						if (m_vtScenes[InGame]->MotionCollision(m_vtPlayers[MYPLAYER], m_vtInstancingShader, m_deqCSVDatas[Generator], &xm))
						{
							for (auto i : CPlayerManager::getInstance().m_mpGenlist)
							{
								if (!i.second->compare(xm)) continue;
								if (i.second->IsComplete()) {
									if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() > 1)
									{
										m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
										m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
									}
									else if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() == 1)
									{
										m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INJUREDIDLE);
										m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREDIDLE);
									}
									//발전기가 작동되면 소리를 끈다.
									SoundEngine::getInstance().execute(GENERATING, false, false);
									break;
								}
								dwDirection = 0;
								dwDirection |= DIR_INTERACTION;
								getNumber = i.first;
								if ((m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_INTERACTION))
								{
									m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INTERACTION);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SGEN);
									//생존자가 발전기를 가동시키는 중이면 소리를 킨다.
									SoundEngine::getInstance().execute(GENERATING, true, false);
								}
								break;
							}
						}
					}
					else if (pKeyBuffer['F'] & 0xF0)
					{
						/*
						생존자 - 공격을 맞고 있을때 트램을 설치 할 수 없다.
						생존자 - 트랩이 없을 경우 트랩을 설치 할 수 없다.
						생존자 - 발전기를 가동중일때 트랩을 설치 할 수 없다.
						*/
						//  TODO : 트랩이 다른 사물과 겹침이 없어야함. 
						//  CPlayer * player = m_vtPlayers[MYPLAYER];
						//  CHeightMapTerrain * pTerrain = m_vtScenes[InGame]->GetTerrain();
						//  for (auto TrapElem : m_mapTrapShader) {
						// 	if (TrapElem.second->GetTrapState() == false) {
						// 		TrapElem.second->SetTrapPosition(*player, *pTerrain);
						// 		break;
						// 	}
						//  }

						//8.16
						m_vtPlayers[MYPLAYER]->SetMotionAABB();
						if (!m_vtScenes[InGame]->CanInstallTrap(m_vtPlayers[MYPLAYER], m_vtInstancingShader) && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetTrap() > 0 && true && (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_TRAPINSTALL))
						{
							dwDirection = 0;
							dwDirection |= DIR_TRAP;
							m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_TRAPINSTALL);
							m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINSTALLTRAP);
						}
					}
				}
				if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER) {
					//4
					for (auto i : CPlayerManager::getInstance().m_mpGenlist) {
						if (i.second->IsComplete()) {
							if (isNearOther::isNearOther(m_vtPlayers[MYPLAYER]->GetPosition(), i.second->GetPosition(), 200.0f) && (i.second->GetSound() == false))
							{
								SoundEngine::getInstance().execute(AFTERGENON, true, false);
								i.second->SetSound(true);
							}
							else if ((isNearOther::isNearOther(m_vtPlayers[MYPLAYER]->GetPosition(), i.second->GetPosition(), 200.0f) == false) && (i.second->GetSound()))
							{
								SoundEngine::getInstance().execute(AFTERGENON, false, false);
								i.second->SetSound(false);
							}
						}
					}
					if (pKeyBuffer[2] & 0xF0 && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_ATTACK) {
						// 공격시 이동 무시
						dwDirection = 0;
						dwDirection |= DIR_ATTACT;
						//살인자가 공격을 하면 때리는 소리를 킨다.
						SoundEngine::getInstance().execute(ATT, true, false);
						m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BPUNCH);
						m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_ATTACK);
					}
				}
				if ((m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_INTERACTION)
					&& (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_TRAPINSTALL)
					&& (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_STEPONTRAP)
					&& (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_HITTED)
					&& (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_ATTACK)
					&& (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_CROUSHOFF))
				{
					if (pKeyBuffer['W'] & 0xF0) {
						m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
						dwDirection |= DIR_FORWARD;
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER)
						{
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_FORWARDRUN)
							{
								// 이동중이 아니였을 경우 앞으로.
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_FORWARDRUN);
								m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(180.0f, 0.0f, 0.0f);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN, true);
							}
						}
						// 생존자 이면
						else
						{
							// 체력이 실피 일 경우
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() == 1)
							{
								PlayerSpeed = 27.f;
								if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_INJUREDFORWORDWALK) {
									m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INJUREDFORWORDWALK);
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 90.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREWALK);
								}
							}
							else
							{
								if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_FORWARDRUN) {
									m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_FORWARDRUN);
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 90.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN, true);
								}
							}
						}
						/*if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_FORWARDRUN) {
							m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_FORWARDRUN);
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER) {
								m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(180.0f, 0.0f, 0.0f);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN);
							}
							else
							{
								m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 90.0f);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN);
							}
						}*/
					}
					if (pKeyBuffer['S'] & 0xF0) {
						m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
						dwDirection |= DIR_BACKWARD;
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER)
						{
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_BACKRUN)
							{
								// 이동중이 아니였을 경우 앞으로.
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_BACKRUN);
								m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(360.0f, 0.0f, 0.0f);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN, true);
							}
						}
						// 생존자 이면
						else
						{
							// 체력이 실피 일 경우
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() == 1)
							{
								PlayerSpeed = 27.f;
								if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_INJUREDBACKWALK) {
									m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INJUREDBACKWALK);
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, -90.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREWALK);
								}

							}
							else
							{
								if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_BACKRUN) {
									m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_BACKRUN);
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, -90.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN, true);
								}
							}
						}
						/*if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_BACKRUN) {
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_BACKRUN);
								if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER) {
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(360.0f, 0.0f, 0.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN);
								}
								else {
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, -90.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN);
								}
							}*/
					
					}
					if (pKeyBuffer['A'] & 0xF0) {
						m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
						dwDirection |= DIR_LEFT;
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER)
						{
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_LEFTRUN)
							{
								// 이동중이 아니였을 경우 앞으로.
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_LEFTRUN);
								m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, 0.0f, 0.0f);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN, true);
							}
						}
						// 생존자 이면
						else
						{
							// 체력이 실피 일 경우
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() == 1)
							{
								PlayerSpeed = 27.f;
								if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_INJUREDLEFTWALK) {
									m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INJUREDLEFTWALK);
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 0.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREWALK);
								}

							}
							else
							{
								if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_LEFTRUN) {
									m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_LEFTRUN);
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 0.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN, true);
								}
							}
						}
						/*	if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_LEFTRUN) {
									m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_LEFTRUN);
									if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER) {
										m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, 0.0f, 0.0f);
										m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN);
									}
									else {
										m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 0.0f);
										m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN);
									}
								}*/
					}
					if (pKeyBuffer['D'] & 0xF0) {
						m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
						dwDirection |= DIR_RIGHT;
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER)
						{
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_RIGHTRUN)
							{
								// 이동중이 아니였을 경우 앞으로.
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_RIGHTRUN);
								m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(-90.0f, 0.0f, 0.0f);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN, true);
							}
						}
						// 생존자 이면
						else
						{
							// 체력이 실피 일 경우
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() == 1)
							{
								PlayerSpeed = 27.f;
								if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_INJUREDRIGHTWALK) {
									m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INJUREDRIGHTWALK);
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 180.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREWALK);
								}

							}
							else
							{
								if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_RIGHTRUN) {
									m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_RIGHTRUN);
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 180.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN);
								}
							}
						}
						/*if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_RIGHTRUN) {
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_RIGHTRUN);
								if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER) {
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(-90.0f, 0.0f, 0.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BRUN);
								}
								else {
									m_vtPlayers[MYPLAYER]->SetPlayerRotationPYR(90.0f, -90.0f, 180.0f);
									m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SRUN);
								}
							}*/
					
					}
					if (0 == (dwDirection & (DIR_RIGHT | DIR_LEFT | DIR_FORWARD | DIR_BACKWARD)))
					{
						m_vtPlayers[MYPLAYER]->SetKeyFlag(false);
						// 살인자가 아무키도 입력되지 않았다면 아이들 상태로 되돌린다
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER)
						{
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_IDEL)
							{
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BIDLE);
							}
						}
						// 생존자 이나 아무키도 안눌렸을때
						else
						{
							if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() == 1 && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_INJUREDIDLE)
							{
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(ANIMATION_TYPE::TYPE_INJUREDIDLE);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREDIDLE);
							}
							else if(m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() > 1 && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_IDEL)
							{
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
							}
							else if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() == 0 && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_DIE)
							{
								m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_DIE);
								m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SDIE);
							}
						}
					}
					else m_vtPlayers[MYPLAYER]->SetKeyFlag(true);
				}
			}
			
			if (!m_vtPlayers[MYPLAYER]->Move(dwDirection, PlayerSpeed * m_GameTimer.GetTimeElapsed(), true, m_vtInstancingShader)) dwDirection = 0;

			/*if (dwDirection == 0) {
				if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_ATTACK)
				{
					if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_IDEL) {
						m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
						m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BIDLE);
					}
					else if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_IDEL) {
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_HITTED) {
							m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
							m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
						}
					}
				}
				m_vtPlayers[MYPLAYER]->SetKeyFlag(false);
			}
			else m_vtPlayers[MYPLAYER]->SetKeyFlag(true);*/

		}
			
#if NETWORK
		if (GameFlowManager::getInstance().type() == FLOW_INGAME) {
			ClientNetworkInterface::getInstance().moveList().insertTimeTempt(dwDirection, m_vtPlayers[MYPLAYER]->GetLookVector(), m_vtPlayers[MYPLAYER]->GetRightVector(), GetTickCount64(), m_GameTimer.GetTimeElapsed(), PlayerSpeed, getNumber);
		}
		else if (GameFlowManager::getInstance().type() == FLOW_RESULT) {
			ClientNetworkInterface::getInstance().moveList().clear();
		}
#endif

		float cxDelta = 0.0f, cyDelta = 0.0f;
		POINT ptCursorPos;
		/*마우스를 캡쳐했으면 마우스가 얼마만큼 이동하였는 가를 계산한다. 마우스 왼쪽 또는 오른쪽 버튼이 눌러질 때의 메시지(WM_LBUTTONDOWN, WM_RBUTTONDOWN)를 처리할 때 마우스를 캡쳐하였다. 그러므로 마우스가 캡쳐된 것은 마우스 버튼이 눌려진 상태를 의미한다. 마우스를 좌우 또는 상하로 움직이면 플레이어를 x-축 또는 y-축으로 회전한다.*/
		if (GetCapture() == m_hWnd)
		{
			//if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_INTERACTION && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() != TYPE_ATTACK) {
			//마우스 커서를 화면에서 없앤다(보이지 않게 한다).
			SetCursor(NULL);
			//현재 마우스 커서의 위치를 가져온다.
			GetCursorPos(&ptCursorPos);
			//마우스 버튼이 눌린 채로 이전 위치에서 현재 마우스 커서의 위치까지 움직인 양을 구한다.
			cxDelta = (float)(ptCursorPos.x - m_ptInGameSceneOldCursorPos.x) / 3.0f;
			cyDelta = (float)(ptCursorPos.y - m_ptInGameSceneOldCursorPos.y) / 3.0f;
			SetCursorPos(m_ptInGameSceneOldCursorPos.x, m_ptInGameSceneOldCursorPos.y);
			//}
		}
		//플레이어를 이동하거나(dwDirection) 회전한다(cxDelta 또는 cyDelta).
		if ((cxDelta != 0.0f) || (cyDelta != 0.0f))
		{
			if (cxDelta || cyDelta)
			{
				/*cxDelta는 y-축의 회전을 나타내고 cyDelta는 x-축의 회전을 나타낸다. 오른쪽 마우스 버튼이 눌려진 경우 cxDelta는 z-축의 회전을 나타낸다.*/
				if (NowPlayer == MYPLAYER) {
					if (pKeyBuffer[VK_RBUTTON] & 0xF0) m_vtPlayers[MYPLAYER]->Rotate(cyDelta, 0.0f, -cxDelta);
					else m_vtPlayers[MYPLAYER]->Rotate(cyDelta, cxDelta, 0.0f);

				}
			}
			/*플레이어를 dwDirection 방향으로 이동한다(실제로는 속도 벡터를 변경한다). 이동 거리는 시간에 비례하도록 한다. 플레이어의 이동 속력은 (50/초)로 가정한다. 만약 플레이어의 이동 속력이 있다면 그 값을 사용한다.*/
		}
	}
	//플레이어를 실제로 이동하고 카메라를 갱신한다. 중력과 마찰력의 영향을 속도 벡터에 적용한다.

	m_vtPlayers[MYPLAYER]->Update(m_GameTimer.GetTimeElapsed(), MYPLAYER, m_vtInstancingShader);
	m_vtPlayers[MYPLAYER]->SetKeyFlag(false);
	m_vtPlayers[SECONDPLAYER]->Update(m_GameTimer.GetTimeElapsed(), SECONDPLAYER, m_vtInstancingShader);
	m_vtPlayers[SECONDPLAYER]->SetKeyFlag(false);
}

void CGameFramework::AnimateObjects()
{
	if (SceneNumber == InGame) {
		m_vtScenes[InGame]->AnimateObjects(m_GameTimer.GetTimeElapsed());
		if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_DIE)
		{
			m_ppPlayerShaders[MYPLAYER]->GetFBXMesh->FBXFrameAdvance(m_GameTimer.GetTimeElapsed(), FBX_DIE);
		}
		//m_vtScenes[InGame]->MotionCollision(m_vtPlayers[MYPLAYER], m_ppInstancingShader, m_deqCSVDatas[Generator]);
		else if (m_ppPlayerShaders[MYPLAYER]->GetFBXMesh->FBXFrameAdvance(m_GameTimer.GetTimeElapsed(), 0, m_vtPlayers[MYPLAYER]->GetPosition(), m_vtPlayers[SECONDPLAYER]->GetPosition())) {
			if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_ATTACK && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == KILLER) {
				m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
				m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BIDLE);
			}
			if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_CROUSHON && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR)
			{
				m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_CROUSHIDLE);
				m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SCROUCHIDLE);
			}
			if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_CROUSHOFF && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR)
			{
				m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
				m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
			}
			if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_HITTED && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR) {
				if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() == 1) {
					m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INJUREDIDLE);
					m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREDIDLE);
				}
				// 생존자가 살인자에게 맞고 그 뒤 체력이 0이라는 사실을 알게됬을 때 죽는 애니메이션을 보여준다.
				else if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() == 0) {
					m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_DIE);
					m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SDIE);
				}
				else
				{
					m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
					m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
				}
			}
			if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_TRAPINSTALL && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR) {
				if (GameFlowManager::getInstance().type() == FLOW_INGAME) {
					ClientNetworkInterface::getInstance().moveList().insertTimeTempt(DIR_TRAPINSTALL, m_vtPlayers[MYPLAYER]->GetLookVector(), m_vtPlayers[MYPLAYER]->GetRightVector(), GetTickCount64(), m_GameTimer.GetTimeElapsed(), 0, -1);
				}
				if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() == 1) {
					m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INJUREDIDLE);
					m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREDIDLE);
				}
				else {
					m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
					m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
				}
			}
			if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_STEPONTRAP && m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR) {
				
				if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetHP() == 1) {
					m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INJUREDIDLE);
					m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREDIDLE);
				}
				else {
					m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
					m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
				}
			}
		}


		//////////////////////////////////


		if (m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_DIE)
		{
			m_ppPlayerShaders[SECONDPLAYER]->GetFBXMesh->FBXFrameAdvance(m_GameTimer.GetTimeElapsed(), FBX_DIE);
		}
		else if (m_ppPlayerShaders[SECONDPLAYER]->GetFBXMesh->FBXFrameAdvance(m_GameTimer.GetTimeElapsed(), 0, m_vtPlayers[MYPLAYER]->GetPosition(), m_vtPlayers[SECONDPLAYER]->GetPosition()))
		{
			if (m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_ATTACK && m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetROLE() == KILLER) {
				m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
				m_ppPlayerShaders[m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(BIDLE);
			}
			if (m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_CROUSHON && m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR)
			{
				m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_CROUSHIDLE);
				m_ppPlayerShaders[m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SCROUCHIDLE);
			}
			if (m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_CROUSHOFF && m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR)
			{
				m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
				m_ppPlayerShaders[m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
			}
			if (m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_HITTED && m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR) {
				if (m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetHP() == 1) {
					m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INJUREDIDLE);
					m_ppPlayerShaders[m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREDIDLE);
				}
				// 생존자가 살인자에게 맞고 그 뒤 체력이 0이라는 사실을 알게됬을 때 죽는 애니메이션을 보여준다.
				else if (m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetHP() == 0) {
					m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_DIE);
					m_ppPlayerShaders[m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SDIE);
				}
				else
				{
					m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
					m_ppPlayerShaders[m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
				}
			}
			if (m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_TRAPINSTALL && m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR) {
				if (m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetHP() == 1) {
					m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INJUREDIDLE);
					m_ppPlayerShaders[m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREDIDLE);
				}
				else {
					m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
					m_ppPlayerShaders[m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
				}
			}
			if (m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_STEPONTRAP && m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR) {
				if (m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetHP() == 1) {
					m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_INJUREDIDLE);
					m_ppPlayerShaders[m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SINJUREDIDLE);
				}
				else {
					m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
					m_ppPlayerShaders[m_vtPlayers[SECONDPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);
				}
			}
		}

		//trap

		while (!CPlayerManager::getInstance().mJob.empty())
		{
			auto job = CPlayerManager::getInstance().mJob.front();
			if (job.mJobNumber == install)
			{
				if (CPlayerManager::getInstance().m_ListTrapShader.front()->GetTrapState() == false)
				{
					auto ret = CPlayerManager::getInstance().m_ListTrapShader.front();
					ret->SetTrapNumber(job.mTrapid);
					ret->SetCatchPlayer(job.mCatchid);
					auto Trap = m_vtScenes[InGame]->GetTerrain();
					ret->SetTrapPosition(job.minPosition, *Trap);
					CPlayerManager::getInstance().m_ListTrapShader.pop_front();
					CPlayerManager::getInstance().mInstalledTrap[job.mTrapid] = ret;
					//CPlayerManager::getInstance().m_ListTrapShader.push_back(ret);
					//1
					if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_TRAPINSTALL)
						SoundEngine::getInstance().execute(INSTALLTRAP, true, false);
				}
			}
			else if (job.mJobNumber == invoked)
			{
				auto installtrap = CPlayerManager::getInstance().mInstalledTrap;//[job.mTrapid]->SetTrapState(true);
				installtrap[job.mTrapid]->SetTrapState(true);
			/*	m_vtPlayers[MYPLAYER]->m_pPlayerInfo->SetAnimationType(TYPE_IDEL);
				m_ppPlayerShaders[m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SIDLE);*/
				auto players = CPlayerManager::getInstance().players();
				for (auto start = (*players).begin(); start != (*players).end(); ++start)
				{
					if (((*start)->m_pPlayerInfo->GetUserID() == job.mCatchid) && ((*start)->m_pPlayerInfo->GetROLE() == SURVIVOR)) {
						m_ppPlayerShaders[(*start)->m_pPlayerInfo->GetShaderIndex()]->GetFBXMesh->SetAnimation(SSTAPONTRAP);
						(*start)->m_pPlayerInfo->SetAnimationType(TYPE_STEPONTRAP);
						//2
						if (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->AnimationType() == TYPE_STEPONTRAP)
						{
							SoundEngine::getInstance().execute(STEPONTRAP, true, false);
							SoundEngine::getInstance().execute(HITTED, true, false);
						}
					}
				}			
			}
			CPlayerManager::getInstance().mJob.pop();

		}
		/*{
			BoundingBox& PlayerAABB = (m_vtPlayers[MYPLAYER]->m_pPlayerInfo->GetROLE() == SURVIVOR) ? m_vtPlayers[MYPLAYER]->GetAABB() : m_vtPlayers[SECONDPLAYER]->GetAABB();
			for (auto TrapElem : CPlayerManager::getInstance().mInstalledTrap) {
				
				if (TrapElem.second->CheckTrapOn(PlayerAABB))
					TrapElem.second->GetFBXMesh->FBXFrameAdvance(m_GameTimer.GetTimeElapsed(), FBX_TRAP);
			}
		}*/
		for (auto & TrapElem : CPlayerManager::getInstance().mInstalledTrap) {
			if (TrapElem.second == nullptr) continue;
			if (TrapElem.second->GetTrapState() == true)
				if (TrapElem.second->GetFBXMesh->FBXFrameAdvance(m_GameTimer.GetTimeElapsed(), FBX_TRAP) == true)
				{
					TrapElem.second->SetTrapState(false);
					CPlayerManager::getInstance().m_ListTrapShader.push_back(TrapElem.second);
					TrapElem.second = nullptr;
				}
		}
		if (IsOpenDoor() && OpenDoorlock)
		{
			OpenDoor();
			OpenDoorlock = false;
		}	
	}
}

void CGameFramework::FrameAdvance()
{
	//타이머의 시간이 갱신되도록 하고 프레임 레이트를 계산한다. 
	m_GameTimer.Tick(50.f);

	//플레이어들에게 카메라를 세팅한다.
	if (NowPlayer == MYPLAYER) m_vtScenes[InGame]->SetCamera(m_ppCamera[MYPLAYER]);

	AnimateObjects();

	float fClearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

	m_pd3dDeviceContext->ClearRenderTargetView(m_pd3dRenderTargetView, fClearColor);
	if (m_pd3dDepthStencilView) m_pd3dDeviceContext->ClearDepthStencilView(m_pd3dDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);


	if (SceneNumber == InGame) {

		ProcessInput();

		m_vtPlayers[MYPLAYER]->UpdateShaderVariables(m_pd3dDeviceContext);
		m_vtScenes[InGame]->Render(m_pd3dDeviceContext, m_ppCamera[MYPLAYER]);
		m_ppPlayerShaders[MYPLAYER]->GetFBXMesh->UpdateBoneTransform(m_pd3dDeviceContext, m_ppPlayerShaders[MYPLAYER]->GetFBXMesh->GetFBXAnimationNum(), m_ppPlayerShaders[MYPLAYER]->GetFBXMesh->GetFBXNowFrameNum());
		m_ppPlayerShaders[MYPLAYER]->Render(m_pd3dDeviceContext, m_ppCamera[MYPLAYER]);

		m_ppPlayerShaders[SECONDPLAYER]->GetFBXMesh->UpdateBoneTransform(m_pd3dDeviceContext, m_ppPlayerShaders[SECONDPLAYER]->GetFBXMesh->GetFBXAnimationNum(), m_ppPlayerShaders[SECONDPLAYER]->GetFBXMesh->GetFBXNowFrameNum());
		m_ppPlayerShaders[SECONDPLAYER]->Render(m_pd3dDeviceContext, m_ppCamera[MYPLAYER]);


		for (auto TrapElem : CPlayerManager::getInstance().mInstalledTrap) {
			if (TrapElem.second == nullptr) continue;
			TrapElem.second->GetFBXMesh->UpdateBoneTransform(m_pd3dDeviceContext, TrapElem.second->GetFBXMesh->GetFBXAnimationNum(), TrapElem.second->GetFBXMesh->GetFBXNowFrameNum());
			TrapElem.second->Render(m_pd3dDeviceContext, m_ppCamera[MYPLAYER]);
		}
		

		m_vtScenes[InGame]->Render2D(m_pd2dDeviceContext, m_pdwFactory, m_pd2dFactory);
	}

	else {
		if (SceneNumber == Login) m_vtScenes[Login]->Render2D(m_pd2dDeviceContext, m_pdwFactory, m_pd2dFactory);
		if (SceneNumber == Lobby) m_vtScenes[Lobby]->Render2D(m_pd2dDeviceContext, m_pdwFactory, m_pd2dFactory);
		if (SceneNumber == Room) m_vtScenes[Room]->Render2D(m_pd2dDeviceContext, m_pdwFactory, m_pd2dFactory);
		if (SceneNumber == Result) m_vtScenes[Result]->Render2D(m_pd2dDeviceContext, m_pdwFactory, m_pd2dFactory);
	}

	if (GameFlowManager::getInstance().type() == FLOW_RESULT && SceneNumber != Result) {
		GameFlowManager::getInstance().mNowTIme = GetTickCount64() - GameFlowManager::getInstance().mTimer;
		if (GameFlowManager::getInstance().fastoutgame == true)
		{
			if (GameFlowManager::getInstance().mNowTIme > 2000)
			{
				Network.IngameEnd(Ghwnd);
				SceneNumber = Result;
				CPlayerManager::getInstance().m_mpGenlist.clear();
				for (auto iter : CPlayerManager::getInstance().tempGenList)
				{
					iter->clear();
				}
				OpenDoorlock = true;
				GameFlowManager::getInstance().fastoutgame = false;
				closeDoor();
				RBPlayerManager::getInstance().ownRoomLeave();
				ones = false;
			}
		}
		else
		{
			if (GameFlowManager::getInstance().mNowTIme > 5000)
			{
				Network.IngameEnd(Ghwnd);
				SceneNumber = Result;
				CPlayerManager::getInstance().m_mpGenlist.clear();
				for (auto iter : CPlayerManager::getInstance().tempGenList)
				{
					iter->clear();
				}
				OpenDoorlock = true;
				GameFlowManager::getInstance().fastoutgame = false;
				closeDoor();
				RBPlayerManager::getInstance().ownRoomLeave();
				ones = false;
			}
		}
	}
	//후면버퍼를 전면버퍼로 프리젠트한다. 
	m_pDXGISwapChain->Present(0, 0);

	m_GameTimer.GetFrameRate(m_pszBuffer + 12, 37);
	::SetWindowText(m_hWnd, m_pszBuffer);
}

void CGameFramework::FBXModelDataLoad(void)
{
	ID3D11SamplerState *pd3dSamplerState = NULL;
	D3D11_SAMPLER_DESC d3dSamplerDesc;
	ZeroMemory(&d3dSamplerDesc, sizeof(D3D11_SAMPLER_DESC));
	d3dSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	d3dSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	d3dSamplerDesc.MinLOD = 0;
	d3dSamplerDesc.MaxLOD = 0;
	m_pd3dDevice->CreateSamplerState(&d3dSamplerDesc, &pd3dSamplerState);
	auto pd3dDevice = m_pd3dDevice;

	ID3D11ShaderResourceView *pd3dsrvTexture = NULL;
	CTexture * pCar1_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/car/car1.png"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);
	CTexture * pTree_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/tree/Tree01.png"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);
	CTexture * pFence_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/fence/fence.png"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);
	CTexture * pDrum_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/drum/drum.png"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);
	CTexture * pTruck_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/truck/truck.png"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);
	CTexture * pBus_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/bus/Bus.png"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);
	CTexture * pGenerator_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/Generator/Generator.png"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);
	CTexture * pBark_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/tree/bark.jpg"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);
	CTexture * pEscapeDoor1_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/escapedoor/escapedoor1.png"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);
	CTexture * pEscapeDoor2_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/escapedoor/escapedoor2.png"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);
	CTexture * pStone_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/stone/stone.png"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);
	CTexture * pStreetLight_Texture = CreateTexture(m_pd3dDevice, _T("../Data/BesideYouData/streetlight/streetlight.jpg"), &pd3dsrvTexture, &pd3dSamplerState, 0, 0);


	pd3dSamplerState->Release();

	CMesh * pCar1_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/car/car1.data", Car1);
	CMesh * pTree_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/tree/FirTree.data", Tree1);
	CMesh * pFence_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/fence/fence.data", Fence);
	CMesh * pDrum_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/drum/drum.data", Drum);
	CMesh * pBus_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/bus/bus.data", Bus);
	CMesh * pTruck_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/truck/truck.data", Truck);
	CMesh * pGenerator_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/Generator/generator.data", Generator);
	CMesh * pBark_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/tree/bark.data", Bark);
	CMesh * pEscapeDoor1_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/escapedoor/escapedoor1.data", Escapedoor1);
	CMesh * pEscapeDoor2_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/escapedoor/escapedoor2.data", Escapedoor2);
	CMesh * pStone_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/stone/stone.data", Stone);
	CMesh * pStreetLight_Mesh = new CAssetMesh(m_pd3dDevice, "../Data/BesideYouData/streetlight/streetlight.data", StreetLight);


	m_vtFBXDatas.push_back(new ModelContainer{ "CarModel", pCar1_Mesh, pCar1_Texture });
	m_vtFBXDatas.push_back(new ModelContainer{ "TreeModel", pTree_Mesh, pTree_Texture });
	m_vtFBXDatas.push_back(new ModelContainer{ "FenceModel", pFence_Mesh, pFence_Texture });
	m_vtFBXDatas.push_back(new ModelContainer{ "DrumModel", pDrum_Mesh, pDrum_Texture });
	m_vtFBXDatas.push_back(new ModelContainer{ "BusModel", pBus_Mesh, pBus_Texture });
	m_vtFBXDatas.push_back(new ModelContainer{ "TruckModel", pTruck_Mesh, pTruck_Texture });
	m_vtFBXDatas.push_back(new ModelContainer{ "GeneratorModel", pGenerator_Mesh, pGenerator_Texture });
	m_vtFBXDatas.push_back(new ModelContainer{ "BarkModel", pBark_Mesh, pBark_Texture });
	m_vtFBXDatas.push_back(new ModelContainer{ "Escapedoor1Model", pEscapeDoor1_Mesh, pEscapeDoor1_Texture });
	m_vtFBXDatas.push_back(new ModelContainer{ "Escapedoor2Model", pEscapeDoor2_Mesh, pEscapeDoor2_Texture });
	m_vtFBXDatas.push_back(new ModelContainer{ "StoneModel", pStone_Mesh, pStone_Texture });
	m_vtFBXDatas.push_back(new ModelContainer{ "StreetLight", pStreetLight_Mesh, pStreetLight_Texture });

	for (auto& iter : m_vtFBXDatas) iter->AddRef();
}

void CGameFramework::CSVDataLoad(void)
{
	CCSVData * pCAR1CSVData = new CCSVData("../Data/BesideYouData/CSV/CarCSV.txt", CAR1NUM);
	CCSVData * pTREECSVData = new CCSVData("../Data/BesideYouData/CSV/TreeCSV.txt", TREE1NUM);
	CCSVData * pFENCECSVData = new CCSVData("../Data/BesideYouData/CSV/FenceCSV.txt", FENCENUM);
	CCSVData * pDRUMCSVData = new CCSVData("../Data/BesideYouData/CSV/DrumCSV.txt", DRUMNUM);
	CCSVData * pBUSCSVData = new CCSVData("../Data/BesideYouData/CSV/BusCSV.txt", BUSNUM);
	CCSVData * pTRUCKCSVData = new CCSVData("../Data/BesideYouData/CSV/TruckCSV.txt", TRUCKNUM);
	CCSVData * pGENERATORCSVData = new CCSVData("../Data/BesideYouData/CSV/GeneratorCSV.txt", GENERATORNUM);
	CCSVData * pBARKCSVData = new CCSVData("../Data/BesideYouData/CSV/BarkCSV.txt", BARKNUM);
	CCSVData * pESCAPEDOOR1CSVData = new CCSVData("../Data/BesideYouData/CSV/Escapedoor1CSV.txt", ESCAPEDOOR1);
	CCSVData * pESCAPEDOOR2CSVData = new CCSVData("../Data/BesideYouData/CSV/Escapedoor2CSV.txt", ESCAPEDOOR2);
	CCSVData * pStoneCSVData = new CCSVData("../Data/BesideYouData/CSV/StoneCSV.txt", STONE);
	CCSVData * pStreetLightCSVData = new CCSVData("../Data/BesideYouData/CSV/StreetLightCSV.txt", STREETLIGHT);

	m_deqCSVDatas.push_back(pCAR1CSVData);
	m_deqCSVDatas.push_back(pTREECSVData);
	m_deqCSVDatas.push_back(pFENCECSVData);
	m_deqCSVDatas.push_back(pDRUMCSVData);
	m_deqCSVDatas.push_back(pBUSCSVData);
	m_deqCSVDatas.push_back(pTRUCKCSVData);
	m_deqCSVDatas.push_back(pGENERATORCSVData);
	m_deqCSVDatas.push_back(pBARKCSVData);
	m_deqCSVDatas.push_back(pESCAPEDOOR1CSVData);
	m_deqCSVDatas.push_back(pESCAPEDOOR2CSVData);
	m_deqCSVDatas.push_back(pStoneCSVData);
	m_deqCSVDatas.push_back(pStreetLightCSVData);
};

void CGameFramework::OnOffLights(void) const
{
	static int nLights = 4;
	LIGHTS * Lights = m_vtScenes[InGame]->GetLights();
	if (nLights >= 4 && nLights < 14) Lights->m_pLights[nLights].m_bEnable = 1.0f;
	nLights++;
}

bool CGameFramework::IsOpenDoor(void) const
{
	uint16_t completedGenNum = 0;
	for (auto elem : CPlayerManager::getInstance().m_mpGenlist)
	{
		if (elem.second->IsComplete())
			completedGenNum += 1;
	}
	if (completedGenNum == 10) return true;
	else { completedGenNum = 0; return false; }
}

void CGameFramework::OpenDoor(void)
{
	CGameObject ** ppObjects = m_vtInstancingShader[11]->GetppObjects();
	for (int i = 0; i < m_vtInstancingShader[11]->GetObjectNum(); ++i)
	{
		XMFLOAT3 pos = ppObjects[i]->GetPosition();
		ppObjects[i]->SetRotationYawPitchRoll(0.0f, 0.0f, 0.0f);
		if (i == 0) ppObjects[i]->SetPosition(1167.0f, pos.y, 2482.0f);
		if (i == 1) ppObjects[i]->SetPosition(1280.0f, pos.y, 2482.0f);
		if (i == 2) ppObjects[i]->SetPosition(1277.0f, pos.y, 80.0f);
		if (i == 3) ppObjects[i]->SetPosition(1164.0f, pos.y, 80.0f);
		ppObjects[i]->SetAABB(
			XMFLOAT3(0.0f, 0.0f, 0.0f),
			Vector3::ScalarProduct((m_vtFBXDatas[Escapedoor2])->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
		);
		ppObjects[i]->SetAABBPosition();
	}
}

void CGameFramework::closeDoor(void)
{
	CGameObject ** ppObjects = m_vtInstancingShader[11]->GetppObjects();
	for (int i = 0; i < m_vtInstancingShader[11]->GetObjectNum(); ++i)
	{
		XMFLOAT3 pos = ppObjects[i]->GetPosition();
		ppObjects[i]->SetRotationYawPitchRoll(90.0f, 0.0f, 0.0f);
		if (i == 0) ppObjects[i]->SetPosition(1199.4f, pos.y, 2521.8f);
		if (i == 1) ppObjects[i]->SetPosition(1255.8f, pos.y, 2521.8f);
		if (i == 2) ppObjects[i]->SetPosition(1249.3f, pos.y, 35.5f);
		if (i == 3) ppObjects[i]->SetPosition(1192.9f, pos.y, 35.5f);
		ppObjects[i]->SetAABB(
			XMFLOAT3(0.0f, 0.0f, 0.0f),
			Vector3::ScalarProduct((m_vtFBXDatas[Escapedoor2])->m_pModelMesh->GetBoundingSizes(), 0.5f, false)
		);
		ppObjects[i]->SetAABBPosition();
	}
}


