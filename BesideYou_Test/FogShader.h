#pragma once

//------------------------------------------------------------------
//
// Class : FogShader
//
// Desc : 
//
//------------------------------------------------------------------

class CFogShader : public CShader
{
private:
	struct FogBufferType
	{
		float fogStart;
		float fogEnd;
		float padding1, padding2;
	};

	static ID3D11Buffer *		m_pd3dcbFog;
	ID3D11SamplerState *		m_pd3dSamplerState;
public:
	CFogShader();
	~CFogShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);
	virtual void BuildObjects(ID3D11Device *pd3dDevice);

	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera = NULL);

	static void CreateShaderVariables(ID3D11Device *pd3dDevice);
	static void UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, float fogStart = 0.0f, float fogEnd = 0.0f);
};
