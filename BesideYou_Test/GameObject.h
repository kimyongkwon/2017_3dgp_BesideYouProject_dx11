#pragma once
#include"stdafx.h"
#include"NetworkLib.h"
#include<assert.h>

#define KmForHourToCmForSec(x) x * 1000 / 36
#define KmForHourToPixForSec(x) KmForHourToCmForSec(x) * 0.2f
#define KmForHourToPixForMs(x) KmForHourToPixForSec(x) / 1000.0f

class CGameObject
{
protected:
	wstring _name; // Object name
	D3DXVECTOR3 _pos;
	D3DXVECTOR3 _vUp;
public:
	CGameObject();
	CGameObject(wstring name, D3DXVECTOR3 pos);
	CGameObject(const CGameObject & o);
	virtual ~CGameObject() {}
	const D3DXVECTOR3 & GetPos(); // 현재 좌표 
	BOOL SetPos(D3DXVECTOR3 & v); // 좌표 변경
	virtual DOUBLE getPosx() { 
		double x = _pos.x;
		return x; 
	}
	virtual DOUBLE getPosy() { 
		double z = _pos.z;
		return z; 
	}
	void setname(wstr_t name) { _name.clear(); _name.append(name); }
	const wchar_t * name() { return _name.c_str(); }
};

typedef enum {
	IDLE,										// 보통상태
	CROUCH,										// 숙이는 상태
	CROUCH_WALK,								// 숙이면서 걷기
	WALK,										// 걷기
	RUN,										// 달리기
	INTERACTION_GENERATOR,						// 상호작용
	DIEING,										// 죽은상태
	VICTORY,									// 승리 상태
	DEFEAT,										// 패배 상태
}ANIMATIONSTATE;

typedef enum {
	OWN_PLAYER = 100,
	OTHER_PLAYER,
	DEAD_PLAYER,
	COUNT = 4
}Identification;

typedef enum
{
	IDLE_STATE,				// 보통상태
	WOUNDTRAP_STATE,		// 트랩에 걸린 상태
	EMERGENCY_STATE,		// 체력이 얼마 안 남았을때 상태
	DEAD_STATE,				// 죽은 상태
}STATE_BAR;

/*
	OWN_PLAYER (new)-> [Connect() 완료시], (delete) [(exit(0)) 연결 종료 시]
	OTHER_PLAYER(new) -> [this time EnterRoom Packet recv()], (delete) [game out or game end(victory or defeat)]
*/
class CDisplay;
class CPlayer : public CGameObject
{
public:
	/*static const float SURVIVOR_WALK;
	static const float SURVIVOR_RUN;
	static const float KILLER_RUN;
	static const float KILLER_ATT_DELAY;*/
private:
	UINT64 SCgap;
	bool SCgapSign;

	INT64 _uid;									// User uid
	ROLE _role;									// 역할
	Identification _ident;						// 식별 (OWN_PLAYER =  100, OTHER_PLAYER = 101,DEAD_PLAYER = 102,)
	STATE_BAR _statusbar;
	bool _move;
	std::pair<UINT64, UINT64>	_MoveTime;
	INT64 _roomNumber;


	ANIMATIONSTATE _state;								// 플레이어 상태
	FLOAT _degree;

	D3DXMATRIX _Rotate;							// 각도
	D3DXVECTOR3 _Scale;							// 연산할 속도
	double speed;								// 속도
	BYTE _hp;									// 체력
	INT32 _trapCount;								// 트랩 수


public:
	CPlayer() : _state(IDLE), _role(NONE), _statusbar(IDLE_STATE) , SCgap(0), SCgapSign(false){}
	CPlayer(wstring name, D3DXVECTOR3 pos, Identification ident, ROLE role)
		: CGameObject(name, pos), _ident(ident), _role(role), SCgap(0), SCgapSign(false)
	{
		//mat = new Gdiplus::Matrix();
		_statusbar = IDLE_STATE;
		_roomNumber = 7000;
		_uid = NULL;
		_move = false;
		_state = IDLE;
		this->Initailize();
	}
	CPlayer(wstring name, D3DXVECTOR3 pos, Identification ident,INT64 uid ,ROLE role, INT64 roomnumber)
		: CGameObject(name, pos), _ident(ident), _role(role), _uid(uid), _roomNumber(roomnumber), SCgap(0), SCgapSign(false)
	{
		//mat = new Gdiplus::Matrix();
		_statusbar = IDLE_STATE;
		_move = false;
		_state = IDLE;
		this->Initailize();
	}
	CPlayer(const CPlayer & p) : CGameObject(p), _uid(p._uid), _ident(p._ident), 
		_role(p._role), _state(p._state), _Rotate(p._Rotate), _Scale(p._Scale), speed(p.speed), _hp(p._hp), SCgap(0), SCgapSign(false)
	{
		if (_role == SURVIVOR) {
		}
	}
	virtual ~CPlayer() {

	}
public:
	INT64 uid() { return _uid; }
	void setuid(INT64 uid) { _uid = uid; }
	void Initailize()
	{
		switch (_role)
		{
		case KILLER:
			speed = KmForHourToPixForMs(9.36f);
			_Scale = D3DXVECTOR3(0.0f, 0.0f, speed);
			//D3DXMatrixTranslation(&_Scale, 0.0f, 0.0f, speed);
			//this->_Scale.SetScale(speed, 0.0f);
			_hp = 100;
			break;
		case SURVIVOR:
			speed = KmForHourToPixForMs(7.5f);
			_Scale = D3DXVECTOR3(0.0f, 0.0f, speed);
			_hp = 3;
			break;
		case NONE:
			
			break;
		default:
			assert(0 && "CPlayer's role err!");
			break;
		}
	}
	//void Render(CDisplay & display, Gdiplus::Matrix * mat);
	//Gdiplus::Matrix * Update(CDisplay & display);
	void SetRotate(float degree)
	{
		if (!_ident == OWN_PLAYER)
			return;
		D3DXMATRIX rotate;
		auto radious = DegreeToRadian(degree);
		D3DXMatrixRotationAxis(&rotate, &_vUp, radious);
		_Rotate = rotate;
	}
	void MoveSulCulate();
	void MoveOn();
	void MoveOff();
	void setRole(ROLE role) { _role = role; }
	ROLE & role() { 
		return _role; 
	}
	ANIMATIONSTATE & state() {
		return _state; 
	}
	void setState(ANIMATIONSTATE state) { _state = state; }
	void EnterRoom(INT64 number) { _roomNumber = number; }
	INT64 Roomnumber() { return _roomNumber; }
	void LeaveRoom() { _roomNumber = 7000; }
	INT32 trapCount();
	void userTrap();
	void setTrapCount(INT32 trap);
	void sethp(INT32 hp);
	void hited();
	INT32 hp();
	FLOAT degree();
	void setDegree(FLOAT degree);
	void setstatusbar(STATE_BAR state);
	STATE_BAR statusbar();
	void otherRender(CDisplay & display, double x, double y);
	void setSCgap(UINT64 gap, bool Sign);
	INT64 CSgap();
	bool CSgapsign();				// TRUE : Plus, FALSE : minus
};

// const float CPlayer::SURVIVOR_RUN = 7.5f;

// const float CPlayer::KILLER_RUN = 9.36f;

// const float CPlayer::KILLER_ATT_DELAY = 5.0f;

// TODO : 살인자와 생존자의 걷는 속도 수정 필요

// const float CPlayer::SURVIVOR_WALK = 6.5f;

class ObjectManager : public Singleton<ObjectManager>
{
private:
	CPlayer * _ownPlayer;
	vector<CPlayer *> _playerList;
	vector<CGameObject *> _objectList;
public:
	ObjectManager();
	virtual ~ObjectManager();
	bool addOwnplayer(CPlayer * _own);
	bool addPlayer(CPlayer * player);
	bool removePlayer(DWORD oid);
	size_t Pcount();
	CPlayer * at_own();
	CPlayer * at_Player(DWORD uid);
	CPlayer * at_Object(DWORD oid);
	vector<CPlayer*> * Playerinfo();
public:		// OWN 전용 함수들
	void ownRoomEnter(INT64 roomNumber);
	void ownRoomLeave();
	/*void addObject();
	void removeObject();*/
};