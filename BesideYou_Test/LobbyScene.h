#pragma once

//------------------------------------------------------------------
//
// Class : CLobbyScene
//
// Desc : second scene, Build and Render UI to choose a character
//		    
//------------------------------------------------------------------

class CLobbyScene : public CScene
{
public:
	CLobbyScene();
	~CLobbyScene();

	virtual void Build2dObjects(IDWriteFactory * pdwFactory, IWICImagingFactory * pwicFactory, ID2D1DeviceContext * pd2dDeviceContext, ID2D1Factory1 * pd2dFactory);
	virtual void ReleaseObjects();
	virtual bool OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, CPlayerShader ** playerShader = NULL);
	virtual bool OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, POINT * cursor, ID2D1DeviceContext * pDeviceContext = NULL, CPlayerShader ** playerShader = NULL);
	virtual void Render2D(ID2D1DeviceContext * pd2dDeviceContext, IDWriteFactory * pdwFactory, ID2D1Factory1 * pd2dFactory);
};

