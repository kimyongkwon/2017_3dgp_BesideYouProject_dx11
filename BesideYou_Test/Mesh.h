#pragma once

class AABB
{
public:

	//바운딩 박스의 최소점과 최대점을 나타내는 벡터이다.
	XMFLOAT3							m_xmf3Minimum;
	XMFLOAT3							m_xmf3Maximum;

	AABB() { m_xmf3Minimum = XMFLOAT3(+FLT_MAX, +FLT_MAX, +FLT_MAX); m_xmf3Maximum = XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX); }
	AABB(XMFLOAT3 xmf3Minimum, XMFLOAT3 xmf3Maximum) { m_xmf3Minimum = xmf3Minimum; m_xmf3Maximum = xmf3Maximum; } 

	XMFLOAT3 GetBoundingCubeMin(void) {return m_xmf3Minimum;}
	XMFLOAT3 GetBoundingCubeMax(void) {return m_xmf3Maximum;}
	void SetBoundingCube(XMFLOAT3 min, XMFLOAT3 max) { m_xmf3Minimum = min, m_xmf3Maximum = max;}

	void Merge(XMFLOAT3& xmf3Minimum, XMFLOAT3& xmf3Maximum);
	void Merge(AABB *pAABB);
	//바운딩 박스의 8개의 꼭지점을 행렬로 변환하고 최소점과 최대점을 다시 계산한다.
	void Update(XMFLOAT4X4 *pxmf4x4Transform);
};

//---
class CVertex
{
	XMFLOAT3							m_xmf3Position;
public:
	CVertex() { m_xmf3Position = XMFLOAT3(0, 0, 0); }
	CVertex(XMFLOAT3 xmf3Position) { m_xmf3Position = xmf3Position; }
	~CVertex() { }
};

//---
class CDiffusedVertex
{
	XMFLOAT3							m_xmf3Position;
	XMCOLOR								m_xmcDiffuse;
public:
	//생성자와 소멸자를 선언한다.
	CDiffusedVertex(float x, float y, float z, XMCOLOR xmcDiffuse) { m_xmf3Position = XMFLOAT3(x, y, z); m_xmcDiffuse = xmcDiffuse; }
	CDiffusedVertex(XMFLOAT3 xmf3Position, XMCOLOR xmcDiffuse) { m_xmf3Position = xmf3Position; m_xmcDiffuse = xmcDiffuse; }
	CDiffusedVertex() { m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f); m_xmcDiffuse = XMCOLOR(0.0f, 0.0f, 0.0f, 0.0f); }
	~CDiffusedVertex() { }
};

//---
class CMesh
{	
	int m_nReferences;
public:
	/*각 정점의 위치 벡터를 픽킹을 위하여 저장한다(정점 버퍼를 DYNAMIC으로 생성하고 Map()을 하지 않아도 되도록).*/
	XMFLOAT3 *m_pxmf3Positions;
	/*메쉬의 인덱스를 저장한다(인덱스 버퍼를 DYNAMIC으로 생성하고 Map()을 하지 않아도 되도록).*/
	UINT *m_pnIndices;
	
protected:
	D3D11_PRIMITIVE_TOPOLOGY			m_d3dPrimitiveTopology;

	//정점의 위치 벡터와 색상을 저장하기 위한 버퍼에 대한 인터페이스 포인터이다. 
	ID3D11Buffer					  * m_pd3dPositionBuffer;
	ID3D11Buffer					  * m_pd3dColorBuffer;

	//버퍼들을 입력조립기에 연결하기 위한 시작 슬롯 번호이다. 
	UINT								m_nSlot;
	/*인스턴싱을 위한 정점 버퍼는 메쉬의 정점 데이터와 인스턴싱 데이터(객체의 위치와 방향)를 갖는다. 그러므로 인스턴싱을 위한 정점 버퍼는 하나가 아니라 버퍼들의 배열이다. 정점의 요소들을 나타내는 버퍼들을 입력조립기에 전달하기 위한 버퍼이다.*/
	ID3D11Buffer					**	m_ppd3dVertexBuffers;
	//정점을 조립하기 위해 필요한 버퍼의 개수이다. 
	int									m_nBuffers;

	//정점의 개수이다. 
	int									m_nVertices;
	UINT								m_nStartVertex;
	//정점의 요소들을 나타내는 버퍼들의	원소의 바이트 수를 나타내는 배열이다. 
	UINT							  * m_pnVertexStrides;
	//정점의 요소들을 나타내는 버퍼들의 시작 위치(바이트 수)를 나타내는 배열이다. 
	UINT                              *	m_pnVertexOffsets;

	//인덱스 버퍼(인덱스의 배열)에 대한 인터페이스 포인터이다. 
	ID3D11Buffer                      *	m_pd3dIndexBuffer;
	//인덱스 버퍼가 포함하는 인덱스의 개수이다. 
	UINT								m_nIndices;
	//인덱스 버퍼에서 메쉬를 표현하기 위해 사용되는 시작 인덱스이다. 
	UINT								m_nStartIndex;
	//각 인덱스에 더해질 인덱스이다. 
	int									m_nBaseVertex;
	UINT								m_nIndexOffset;
	//각 인덱스의 형식(DXGI_FORMAT_R32_UINT 또는 DXGI_FORMAT_R16_UINT)이다. 
	DXGI_FORMAT							m_dxgiIndexFormat;

	ID3D11RasterizerState             * m_pd3dRasterizerState;

public:
	CMesh(ID3D11Device *pd3dDevice);
	virtual ~CMesh();

	void AddRef();
	void Release();

	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext);
	virtual void RenderInstanced(ID3D11DeviceContext *pd3dDeviceContext, int nInstances = 0, int nStartInstance = 0);
	virtual void CreateRasterizerState(ID3D11Device *pd3dDevice); 
	void AssembleToVertexBuffer(int nBuffers = 0, ID3D11Buffer **m_pd3dBuffers = NULL, UINT *pnBufferStrides = NULL, UINT *pnBufferOffsets = NULL);
	ID3D11Buffer *CreateBuffer(ID3D11Device *pd3dDevice, UINT nStride, int nElements, void *pBufferData, UINT nBindFlags, D3D11_USAGE d3dUsage, UINT nCPUAccessFlags);

//////////////////////////////////////
//	절두체컬링
protected:
	AABB								m_bcBoundingCube;


//////////////////////////////////////
//	충돌박스에 이용되는 데이터
protected:
	XMFLOAT3							m_xmf3BoundingCenterPos;
	XMFLOAT3							m_xmf3BoundingExtents;
	XMFLOAT3							m_xmf3BoundingSizes;
public:
	XMFLOAT3 GetBoundingCenterPos(void) { return m_xmf3BoundingCenterPos; }
	XMFLOAT3 GetBoundingExtents(void) { return m_xmf3BoundingExtents; }
	XMFLOAT3 GetBoundingSizes(void) { return m_xmf3BoundingSizes; }

	AABB GetBoundingCube() { return(m_bcBoundingCube); }

///////////////////////////////////////
//blend
public:
	ID3D11BlendState * m_pd3dBlendState;
	void CreateBlendState(ID3D11Device * pd3dDevice);

///////////////////////////////////////
//	Animation에 이용되는 데이터
public:
	virtual void UpdateBoneTransform(ID3D11DeviceContext *pd3dDeviceContext, int nAnimationNum, int nNowFrame);
	virtual float GetFBXModelSize() { return 0; }
	virtual float GetFBXAnimationTime() { return 0; }
	virtual int GetFBXAnimationNum() { return 0; }
	virtual int GetFBXNowFrameNum() { return 0; }
	virtual int GetFBXMaxFrameNum() { return 0; }
	virtual bool FBXFrameAdvance(float fTimeElapsed, int32_t inFbx = 0, XMFLOAT3 mypos = XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3 ohterpos = XMFLOAT3(0.0f, 0.0f, 0.0f)) { return 0; }
	virtual void SetAnimation(int nFBXAnimationNum, bool run = false, bool other = false) {}
	virtual int GetAnimation() { return 0; }
};

//---
class CCubeMesh : public CMesh
{
public:
	CCubeMesh(ID3D11Device *pd3dDevice, float fWidth = 2.0f, float fHeight = 2.0f, float fDepth = 2.0f, XMFLOAT4 xmcColor = XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f));
	virtual ~CCubeMesh();
	virtual void CreateRasterizerState(ID3D11Device *pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext);
};

//---
class CMeshIlluminated : public CMesh
{
public:
	CMeshIlluminated(ID3D11Device *pd3dDevice);
	virtual ~CMeshIlluminated();

protected:
	//조명의 영향을 계산하기 위하여 법선벡터가 필요하다.
	ID3D11Buffer						* m_pd3dNormalBuffer;

public:
	//정점이 포함된 삼각형의 법선벡터를 계산하는 함수이다.
	XMFLOAT3 CalculateTriAngleNormal(UINT nIndex0, UINT nIndex1, UINT nIndex2);
	void SetTriAngleListVertexNormal(XMFLOAT3 *pxmf3Normals);
	//정점의 법선벡터의 평균을 계산하는 함수이다.
	void SetAverageVertexNormal(XMFLOAT3 *pxmf3Normals, int nPrimitives, int nOffset, bool bStrip);
	void CalculateVertexNormal(XMFLOAT3 *pxmf3Normals);
};

//---
class CMeshTexturedIlluminated : public CMeshIlluminated
{
public:
	CMeshTexturedIlluminated(ID3D11Device *pd3dDevice);
	virtual ~CMeshTexturedIlluminated();
protected:
	ID3D11Buffer						* m_pd3dTexCoordBuffer;
};

//---
class CMeshDetailTexturedIlluminated : public CMeshIlluminated
{
public:
	CMeshDetailTexturedIlluminated(ID3D11Device *pd3dDevice);
	virtual ~CMeshDetailTexturedIlluminated();
protected:
	ID3D11Buffer						* m_pd3dTexCoordBuffer;
	ID3D11Buffer						* m_pd3dDetailTexCoordBuffer;
};

//---
class CMeshTextured : public CMesh
{
public:
	CMeshTextured(ID3D11Device *pd3dDevice);
	virtual ~CMeshTextured();
protected:
	//텍스쳐 매핑을 하기 위하여 텍스쳐 좌표가 필요하다.
	ID3D11Buffer						* m_pd3dTexCoordBuffer;
};

//---
class CMeshDetailTextured : public CMeshTextured
{
public:
	CMeshDetailTextured(ID3D11Device *pd3dDevice);
	virtual ~CMeshDetailTextured();
protected:
	ID3D11Buffer						* m_pd3dDetailTexCoordBuffer;
};

//---
class CHeightMapGridMesh : public CMeshDetailTexturedIlluminated
{
	//격자의 크기(가로: x방향, 세로: z-방향) 이다.
	int									  m_nWidth;
	int									  m_nLength;
	//격자의 스케일(가로:x-방향, 세로: z-방향, 높이 : y방향) 벡터이다. 
	//실제 격자 메쉬의 각 정점의 x좌표, y좌표, z좌표는
	//스케일 벡터의 x좌표, y표, z좌표로 곱한 값을 갖는다.
	//즉, 실제 격자의 x축 방향의 간격은 1이 아니라 스케일 벡터의 x좌표가 된다.
	//이렇게 하면 작은 격자를 사용하더라도 큰 격자를 생성할 수 있다.
	XMFLOAT3							  m_xmf3Scale;

public:
	CHeightMapGridMesh(ID3D11Device *pd3dDevice, int xStart, int zStart, int nWidth, int nLength, XMFLOAT3 xmf3Scale = XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT4 xmcColor = XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f), void *pContext = NULL);
	virtual ~CHeightMapGridMesh();

	virtual void CreateRasterizerState(ID3D11Device *pd3dDevice);

	virtual float OnGetHeight(int x, int z, void *pContext);
	virtual XMCOLOR OnGetColor(int x, int z, void *pContext);
};

class CSphereMesh : public CMesh
{
public:
	CSphereMesh(ID3D11Device *pd3dDevice, float fRadius = 2.0f, int nSlices = 20, int nStacks = 20, XMFLOAT4 xmcColor = XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f));
	virtual ~CSphereMesh();
	virtual void CreateRasterizerState(ID3D11Device *pd3dDevice);
};