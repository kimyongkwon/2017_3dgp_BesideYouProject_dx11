#include "stdafx.h"

CAssetMesh::CAssetMesh(ID3D11Device *pd3dDevice, const std::string& filename, unsigned int type) : CMeshTexturedIlluminated(pd3dDevice)
{
	m_uitype = type;
	std::ifstream in(filename);
	std::string ignore;

	in >> ignore//[FBX_META_DATA]
		>> ignore//MeshCOUNT:
		>> ignore;//1
	in >> ignore;//MESH_DATA
	in >> ignore >> m_nVertices;//VertexCount	
	in >> ignore >> m_nIndices;//IndexCount;
	in >> ignore >> ignore;//BoneCount;
	in >> ignore >> ignore;//AnimationClips;

	m_pxmf3Positions = new XMFLOAT3[m_nVertices];
	XMFLOAT2 *pxmf2TexCoords = new XMFLOAT2[m_nVertices];
	XMFLOAT3 *pxmf3Normals = new XMFLOAT3[m_nVertices];
	m_pnIndices = new UINT[m_nIndices];
	//생성
	in >> ignore; //[VERTEX_DATA]

	XMFLOAT3 xmf3MinTmp = XMFLOAT3(FLT_MAX, FLT_MAX, FLT_MAX);
	XMFLOAT3 xmf3MaxTmp = XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	for (int i = 0; i < m_nVertices; ++i)
	{
		in >> ignore >> m_pxmf3Positions[i].x >> m_pxmf3Positions[i].y >> m_pxmf3Positions[i].z;
		in >> ignore >> pxmf3Normals[i].x >> pxmf3Normals[i].y >> pxmf3Normals[i].z;
		in >> ignore >> pxmf2TexCoords[i].x >> pxmf2TexCoords[i].y;

		//게임에 적절한 모델 사이즈는 여기서 조정해준다.
		//모델 방향은 Shader.cpp와 FBXInstancingShader에서 해준다.
		if (type == Car1) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 0.3f, false);
		if (type == Tree1) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 1.5f, false);
		if (type == Fence) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 0.3f, false);
		if (type == Drum) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 2.0f, false);
		if (type == Bus) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 0.2f, false);
		if (type == Truck) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 0.2f, false);
		if (type == Generator) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 10.0f, false);
		if (type == Bark) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 0.08f, false);
		if (type == Escapedoor1) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 0.05f, false);
		if (type == Escapedoor2) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 0.3f, false);
		if (type == Stone) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 0.01f, false);
		if (type == StreetLight) m_pxmf3Positions[i] = Vector3::ScalarProduct(m_pxmf3Positions[i], 12.0f, false);

		//모델의 최소점, 최대점 찾기
		if (xmf3MinTmp.x > m_pxmf3Positions[i].x) xmf3MinTmp.x = m_pxmf3Positions[i].x;
		if (xmf3MinTmp.y > m_pxmf3Positions[i].y) xmf3MinTmp.y = m_pxmf3Positions[i].y;
		if (xmf3MinTmp.z > m_pxmf3Positions[i].z) xmf3MinTmp.z = m_pxmf3Positions[i].z;
		if (xmf3MaxTmp.x < m_pxmf3Positions[i].x) xmf3MaxTmp.x = m_pxmf3Positions[i].x;
		if (xmf3MaxTmp.y < m_pxmf3Positions[i].y) xmf3MaxTmp.y = m_pxmf3Positions[i].y;
		if (xmf3MaxTmp.z < m_pxmf3Positions[i].z) xmf3MaxTmp.z = m_pxmf3Positions[i].z;
	}
	in >> ignore;

	//최소점과 최대점을 저장한다
	m_bcBoundingCube.SetBoundingCube(XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX), XMFLOAT3(+FLT_MAX, +FLT_MAX, +FLT_MAX));

	//최소점과 최대점을 이용해서 충돌박스에 중심점, 크기, 중심과 꼭지점까지의 거리를 구한다.
	if (type == Tree1) //나무에 바운딩 박스는 조금 줄인다.
		m_xmf3BoundingSizes = XMFLOAT3(Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).x - 15.0f, Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).y - 15.0f, Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).z + 10.0f);
	else if (type == Bark)
		m_xmf3BoundingSizes = XMFLOAT3(Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).x - 60.0f, Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).y, Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).z - 55.0f);
	else m_xmf3BoundingSizes = XMFLOAT3(Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).x, Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).y, Vector3::Subtract(xmf3MaxTmp, xmf3MinTmp).z);
	//박스의 사이즈를 반으로 나누면 물체의 중심점을 알 수 있다.
	m_xmf3BoundingCenterPos = XMFLOAT3(m_xmf3BoundingSizes.x / 2.0f, m_xmf3BoundingSizes.y / 2.0f, m_xmf3BoundingSizes.z / 2.0f);

	//이게 헷깔린다
	m_xmf3BoundingExtents = XMFLOAT3(m_xmf3BoundingSizes.x / 2.0f, m_xmf3BoundingSizes.y / 2.0f, m_xmf3BoundingSizes.z / 2.0f);

	for (unsigned int i = 0; i < m_nIndices; i++)
	{
		in >> m_pnIndices[i];
	}
	in.close();//file close

	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	//버텍스 버퍼 초기화
	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;
	d3dBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA d3dBufferData;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pxmf3Positions;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dPositionBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT3)* m_nVertices;//Buffer ByteWidth
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = pxmf3Normals;//Sysmem pointer.
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dNormalBuffer);

	d3dBufferDesc.ByteWidth = sizeof(XMFLOAT2)* m_nVertices;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = pxmf2TexCoords;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dTexCoordBuffer);

	ID3D11Buffer *pd3dBuffers[3] = { m_pd3dPositionBuffer, m_pd3dNormalBuffer, m_pd3dTexCoordBuffer };
	UINT pnBufferStrides[3] = { sizeof(XMFLOAT3), sizeof(XMFLOAT3), sizeof(XMFLOAT2) };
	UINT pnBufferOffsets[3] = { 0, 0, 0 };
	AssembleToVertexBuffer(3, pd3dBuffers, pnBufferStrides, pnBufferOffsets);

	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));
	d3dBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dBufferDesc.ByteWidth = sizeof(UINT)* m_nIndices;
	d3dBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	d3dBufferDesc.CPUAccessFlags = 0;
	ZeroMemory(&d3dBufferData, sizeof(D3D11_SUBRESOURCE_DATA));
	d3dBufferData.pSysMem = m_pnIndices;
	pd3dDevice->CreateBuffer(&d3dBufferDesc, &d3dBufferData, &m_pd3dIndexBuffer);

	//blend
	CreateBlendState(pd3dDevice);

	CreateRasterizerState(pd3dDevice);

	delete[] pxmf3Normals;
	delete[] pxmf2TexCoords;
}

CAssetMesh::~CAssetMesh()
{
}
