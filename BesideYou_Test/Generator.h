#pragma once

typedef enum
{
	GENERATIOR_STOP, // 발전기 정지
	GENERATIOR_START, // 발전기 가동
	GENERATIOR_SUSPEND, // 발전기 일시 정지
	GENERATIOR_END, // 발전기 가동 완료
}GEN_STATE;

class CGenerator
{
private:
	XMFLOAT3		Position_;
	float			LocalPersent_;
	INT32			generatorNumber_;
	INT8			ServerPersent_;
	GEN_STATE		genState_;	
	CScene*			m_pInGameScene;
	bool			Sound;
	bool			StartSound;

public:
	CGenerator() {}
	CGenerator(int index, XMFLOAT3 Position, CScene * IngameScene);
	~CGenerator() {};

	bool IsComplete() const;
	INT32 GetNumber() const			 { return generatorNumber_; }
	void SetNumber(int num)			 { generatorNumber_ = num; }
	void SetStartSound(bool OnOff)	 { StartSound = OnOff; };
	bool GetStartSound(void) const	 { return StartSound; }
	void SetSound(bool OnOff)		 { Sound = OnOff; };
	bool GetSound(void) const		 { return Sound; };
	XMFLOAT3 GetPosition(void) const { return Position_; }

	void clear();
	bool compare(XMFLOAT3 Position) const;
	void tick();
	void Read(INT8 ServerPersent);

	void CGenerator::OnLights();
	void CGenerator::OffLights();
};