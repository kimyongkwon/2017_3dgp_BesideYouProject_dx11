#pragma once

class CScene;
class CPlayer;
class CCamera;
class CPlayerShader;
class CPlayerAABBShader;
class CShader;
class CTrapShader;
class CGenerator;

//------------------------------------------------------------------
//
// Class : CGameFramework
//
// Desc : DX11 Device Binding, D2D Device Binding, GameScene Manage, ModelLoad, Player Manage
//
//------------------------------------------------------------------

class CGameFramework : public Singleton<CGameFramework>
{

	//-------------------------------------
	//Game Data
	//-------------------------------------
private:
	vector<ModelContainer*>		m_vtFBXDatas;
	deque <CCSVData*>			m_deqCSVDatas;

	vector<CScene*>				m_vtScenes;
	vector<CPlayer*>			m_vtPlayers;

	int							m_nCameras;
	CCamera**					m_ppCamera;
	vector<CShader*>			m_vtInstancingShader;

	//-------------------------------------
	//DX11, D2D, Windows Data
	//-------------------------------------
private:
	CGameTimer			m_GameTimer;
	_TCHAR						m_pszBuffer[100];
	HINSTANCE					m_hInstance;
	HWND						m_hWnd;

	//주로 리소스를 생성하기 위하여 필요하다.
	ID3D11Device*				m_pd3dDevice{ nullptr };
	//주로 디스플레이를 제어하기 위하여 필요하다.
	IDXGISwapChain*				m_pDXGISwapChain{ nullptr };
	ID3D11RenderTargetView*		m_pd3dRenderTargetView{ nullptr };
	//디바이스 컨텍스트에 대한 포인터이다. 주로 파이프라인 설정을 하기 위하여 필요하다.
	ID3D11DeviceContext*		m_pd3dDeviceContext{ nullptr };
	ID3D11Texture2D*			m_pd3dDepthStencilBuffer{ nullptr };
	ID3D11DepthStencilView*		m_pd3dDepthStencilView{ nullptr };
	ID3D11Buffer*				m_pd3dcbColor{ nullptr };

	//d2d
	ID2D1Factory1*				m_pd2dFactory{ nullptr };
	ID2D1Device*				m_pd2dDevice{ nullptr };
	ID2D1DeviceContext*			m_pd2dDeviceContext{ nullptr };
	IDWriteFactory*				m_pdwFactory{ nullptr };
	IWICImagingFactory*			m_pwicFactory{ nullptr };
	ID2D1Bitmap1*				m_pd2dBitmapBackBuffer{ nullptr };

	//2dObject
	IDWriteTextFormat*			m_dwExplainFormat{ nullptr };
	IDWriteTextFormat*			m_dwMyChattingFormat{ nullptr };
	ID2D1RectangleGeometry*		m_pd2drcBox{ nullptr };
	ID2D1SolidColorBrush*		m_pd2dsbrBeige{ nullptr };
	ID2D1SolidColorBrush*		m_pd2dsbrGreenColor{ nullptr };
	ID2D1SolidColorBrush*		m_pd2dsbrRedColor{ nullptr };

	UINT						m_n4xMSAAQualities;

	int							m_nWndClientWidth;
	int							m_nWndClientHeight;
public:
	CGameFramework();
	~CGameFramework();

	bool OnCreate(HINSTANCE hInstance, HWND hMainWnd);
	void OnDestroy();

	bool CreateRenderTargetDepthStencilView();
	bool CreateDirect3DDisplay();
	void OnResizeBackBuffer(void);

	void CreateAllShaderVariable(void);

	//render for mesh, instance 
	void BuildObjects();
	void ReleaseObjects();

	//프레임워크의 핵심(사용자 입력, 애니메이션, 렌더링)을 구성하는 함수이다. 
	void ProcessInput();
	void AnimateObjects();
	void FrameAdvance();

	//윈도우의 메시지(키보드, 마우스 입력)를 처리하는 함수이다. 
	void OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	void OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	LRESULT CALLBACK OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);

	//마지막으로 마우스 버튼을 클릭할 때의 마우스 커서의 위치이다.
	POINT							m_ptInGameSceneOldCursorPos;
	POINT							m_ptMoveCursorPos;

	int								m_nPlayerShaders;
	CPlayerShader**					m_ppPlayerShaders;

	int								m_nPlayerBoxShaders;
	CPlayerAABBShader**				m_ppPlayerBoxShaders;

	void FBXModelDataLoad(void);
	void CSVDataLoad(void);

#if TCODE
	void SubBuildObject(int who, ROLE role);
#endif

	void OnOffLights(void) const;
	bool IsOpenDoor(void) const;
	void OpenDoor(void);
	void closeDoor(void);
	bool OpenDoorlock;
};

//------------------------------------------------------------------
//
// Class : TrapJob
//
// Desc : 
//
//------------------------------------------------------------------
class TrapJob
{
public:
	TrapJob() {}
	~TrapJob() {}
	void inData(int32_t inJobNumber, uint64_t inTrapid, uint64_t inCatchid, XMFLOAT3 inPosition = XMFLOAT3(0, 0, 0));
	int32_t  mJobNumber;
	XMFLOAT3 minPosition;
	uint64_t mTrapid;
	uint64_t mCatchid;
};

//------------------------------------------------------------------
//
// Class : CPlayerManager
//
// Desc : 
//
//------------------------------------------------------------------
class CPlayerManager : public Singleton<CPlayerManager>
{
	list<CPlayer*>					 m_vtPlayers;
public:
	list<CPlayer*> * players()		{ return &m_vtPlayers; }
	std::map<int, CGenerator*>		 m_mpGenlist;
	std::vector<CGenerator*>		 tempGenList;
	list <CTrapShader*>				 m_ListTrapShader;
	map <int, CTrapShader*>			 mInstalledTrap;
	queue<TrapJob> mJob;
};

