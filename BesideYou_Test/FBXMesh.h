#pragma once

typedef enum
{
	FBX_RUN = 0,
	FBX_TRAP = 1 << 0,
	FBX_DIE = 1 << 1,
	FBX_KILLER = 1 << 2,
	FBX_SURVIVOR = 1 << 3,
};
// 상수 버퍼를 위한 구조체
struct VS_CB_SKINNED
{
	XMFLOAT4X4 m_xmf4x4Bone[96];
};

//---
struct BoneAnimationData
{
	int m_nFrameCount;//필요한가? 필요하다
	float	*m_pfAniTime;//불필요하다. 허나 아직 모름
	XMFLOAT3						* m_pxmf3Scale;
	XMFLOAT3						* m_pxmf3Translate;
	XMFLOAT4						* m_pxmf4Quaternion;
};

//---
class CFBXMesh : public CMeshTexturedIlluminated
{
private:
	XMFLOAT3						* m_pxmf3Positions;
	XMFLOAT3						* m_pxmf3Normals;
	XMFLOAT2						* m_pxmf2TexCoords;
	XMFLOAT4						* m_pxmf4BoneWeights;
	XMFLOAT4						* m_pxmf4BoneIndices;

	BoneAnimationData			   ** m_ppBoneAnimationData;
	XMFLOAT4X4						* m_pxmf4x4SQTTransform;
	XMFLOAT4X4						* m_pxmf4x4FinalBone;

	int m_nBoneCount;
	int m_nAnimationClip;

	// i번 뼈대의 부모 색인(parentIndex)를 담는다.
	// i번 뼈대는 애니메이션 클립의 i번째 BoneAnimation 인스턴스에 대응.
	UINT							* m_pBoneHierarchy;
	XMFLOAT4X4						* m_pxmf4x4BoneOffsets;

	ID3D11Buffer					* m_pd3dWeightBuffer;
	ID3D11Buffer					* m_pd3dBoneIndiceBuffer;
	// 뼈대 상수버퍼
	ID3D11Buffer					* m_pd3dcbBones;

	float							  m_fFBXModelSize;		// 모델의 사이즈 (보통 Animate에서 조절해주기 위함)
	float							  m_fFBXAnimationTime;	// 모델의 AnimationTime (공통적으로 0.0333333f 씩 증가)
	int								  m_nFBXAnimationNum;		// 모델이 실행할 애니메이션의 값을 관리한다.
	int								  m_nFBXMaxFrameNum;		// 모델이 실행할 애니메이션의 최대 프레임 수.
	int								  m_nFBXNowFrameNum;		// 모델이 진행중인 애니메이션의 현재 프레임 값.
	bool							  m_RunAnimation;
	bool							  m_isAnimationOther;
	
	//trap
	int								  m_nFBXFrameState;
public:
	CFBXMesh(ID3D11Device *pd3dDevice, char *pszFileName, float fSize);
	virtual ~CFBXMesh();

	// 해당 프레임의 SR(Q)T 회전이 반영된 행렬을 반환
	void MakeBoneMatrix(int nNowframe, int nAnimationNum, int nBoneNum, XMFLOAT4X4& BoneMatrix);
	// 상수 버퍼로 전달할 최종 본 행렬을 구한다.
	void UpdateBoneTransform(ID3D11DeviceContext *pd3dDeviceContext, int nAnimationNum, int nNowFrame);
	// 뼈대 상수 버퍼 설정
	void CreateConstantBuffer(ID3D11Device *pd3dDevice);

	float GetFBXModelSize() { return m_fFBXModelSize; }
	float GetFBXAnimationTime() { return m_fFBXAnimationTime; }
	int GetFBXAnimationNum() { return m_nFBXAnimationNum; }
	int GetFBXNowFrameNum() { return m_nFBXNowFrameNum; }
	int GetFBXMaxFrameNum() { return m_nFBXMaxFrameNum; }
	bool FBXFrameAdvance(float fTimeElapsed, int32_t inFbx = 0, XMFLOAT3 mypos = XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3 ohterpos = XMFLOAT3(0.0f, 0.0f, 0.0f));
	void SetAnimation(int nFBXAnimationNum, bool run = false, bool other = false);
	virtual int GetAnimation() { return m_nFBXAnimationNum; }
};


