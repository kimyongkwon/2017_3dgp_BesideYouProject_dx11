#pragma once
#include"GameFramework.h"
#include"Shader.h"
#include"Scene.h"

#define EPSILON 0.01      // 정확도.

typedef enum
{
	IDLE_STATE,            // 보통상태
	WOUNDTRAP_STATE,      // 트랩에 걸린 상태
	EMERGENCY_STATE,      // 체력이 얼마 안 남았을때 상태
	DEAD_STATE,            // 죽은 상태
	NOT_USEED,
}STATUS_UI;
typedef enum
{
	EVENT_NONE,
	EVENT_MOVESTART,
	EVENT_MOVESTOP,
	EVENT_DEADRECKNING,
	EVENT_INTERPOLATION,
	EVENT_ATT,
	EVENT_OPERATORON,
	EVENT_TRAPON,
}EVENT_TYPE;
typedef enum
{
	TYPE_IDEL = 1 << 0,
	TYPE_FORWARDRUN = 1 << 1,
	TYPE_BACKRUN = 1 << 2,
	TYPE_LEFTRUN = 1 << 3,
	TYPE_RIGHTRUN = 1 << 4,
	TYPE_INJUREDIDLE = 1 << 5,
	TYPE_INJUREDFORWORDWALK = 1 << 6,
	TYPE_INJUREDBACKWALK = 1 << 7,
	TYPE_INJUREDLEFTWALK = 1 << 8,
	TYPE_INJUREDRIGHTWALK = 1 << 9,
	TYPE_INTERACTION = 1 << 10,
	TYPE_ATTACK = 1 << 11,
	TYPE_HITTED = 1 << 12,
	TYPE_TRAPINSTALL = 1 << 13,
	TYPE_STEPONTRAP = 1 << 14,
	TYPE_CROUSHON = 1 << 15,
	TYPE_CROUSHOFF = 1 << 16,
	TYPE_CROUSHIDLE = 1 << 17,
	TYPE_CROUSHFORWORDWALK = 1 << 18,
	TYPE_CROUSHBACKWALK = 1 << 19,
	TYPE_CROUSHLEFTWALK = 1 << 20,
	TYPE_CROUSHRIGHTWALK = 1 << 21,
	TYPE_FORWORDWALK = 1 << 22,
	TYPE_BACKWALK = 1 << 23,
	TYPE_LEFTWALK = 1 << 24,
	TYPE_RIGHTWALK = 1 << 25,
	TYPE_DIE = 1 << 26,
	TYPE_SPRINT = 1 << 27,
	TYPE_CROUSHALL = TYPE_CROUSHON | TYPE_CROUSHOFF | TYPE_CROUSHIDLE | TYPE_CROUSHFORWORDWALK | TYPE_CROUSHBACKWALK | TYPE_CROUSHLEFTWALK | TYPE_CROUSHRIGHTWALK,
	TYPE_CROUSHNOTOFF = TYPE_CROUSHIDLE | TYPE_CROUSHFORWORDWALK | TYPE_CROUSHBACKWALK | TYPE_CROUSHLEFTWALK | TYPE_CROUSHRIGHTWALK,
	TYPE_WALKALL = TYPE_BACKWALK | TYPE_LEFTWALK | TYPE_FORWORDWALK | TYPE_RIGHTWALK,
}ANIMATION_TYPE;

//------------------------------------------------------------------
//
// Class : CPlayerInfo
//
// Desc : Class that represents information about users playing the game
//
//------------------------------------------------------------------
class CPlayerInfo
{
	// m_ix3ShaderIndex 추가 
	int						m_ix3ShaderIndex;
	INT64					m_ix64UserID;
	INT64					m_ix64RoomNum;
	wstr_t					m_wsUserName;
	XMFLOAT3				m_xmf3PrePosition;
	XMFLOAT3				m_xmf3NextPosition;
	XMFLOAT3				m_xmf3FixPosition;
	UINT64					m_uix64PreTime;
	UINT64					m_uix64CurrentTime;
	UINT64					m_uix64NextTime;
	bool					m_bOwn;
	UINT64					m_uix64TimeGap;
	bool					m_bGapSign;
	BYTE					m_byteHP;
	INT32					m_ix32TrapCount;
	STATUS_UI				m_PlayerStatus;
	ROLE					m_PlayerRole;
	EVENT_TYPE				m_EventType;

	ANIMATION_TYPE			m_AnimationType;

	bool					m_bReplayFlag;
	UINT64					m_uix64LastPacketTime;
	XMFLOAT3				m_xmf3PacketPos;
public:
	CPlayerInfo();
	CPlayerInfo(int shaderIndex);
	~CPlayerInfo();

	void Read(PK_S_NTY_PLAYERPOS * inPacket, uint8_t inPlayerUpdateData);

	INT64 GetUserID(void)										{ return m_ix64UserID; };
	void SetUserID(const INT64& UserID)							{ m_ix64UserID = UserID; };
	INT64 GetRoomNUM(void)										{ return m_ix64RoomNum; };
	void SetRoomNUM(const INT64& RoomNum)						{ m_ix64RoomNum = RoomNum; };
	wstr_t GetUserNAME(void)									{ return m_wsUserName; }
	void SetUserNAME(const wstr_t& UserName)					{ m_wsUserName = UserName; };
	XMFLOAT3 GetPrePosition(void)								{ return m_xmf3PrePosition; };
	void SetPrePosition(const XMFLOAT3& PrePosition)			{ m_xmf3PrePosition = PrePosition; };
	XMFLOAT3 GetNextPosition(void)								{ return m_xmf3NextPosition; };
	void SetNextPosition(const XMFLOAT3& NextPosition)			{ m_xmf3NextPosition = NextPosition; };
	XMFLOAT3 GetFixPosition(void)								{ return m_xmf3FixPosition; };
	void SetFixPosition(const XMFLOAT3& FixPosition)			{ m_xmf3FixPosition = FixPosition; };
	UINT64 GetPreTime(void)										{ return m_uix64PreTime; };
	void SetPreTIme(const UINT64& PreTime)						{ m_uix64PreTime = PreTime; };
	UINT64 GetCurrentTIME(void)									{ return m_uix64CurrentTime; };
	void SetCurrentTIME(const UINT64& CurrentTime)				{ m_uix64CurrentTime = CurrentTime; };
	UINT64 GetNextTIME(void)									{ return m_uix64NextTime;};

	INT32 GetShaderIndex()										{ return m_ix3ShaderIndex; }
	void SetShaderIndex(INT32 index)							{ m_ix3ShaderIndex = index; }
	void SetNextTIME(const UINT64& NextTime)					{ m_uix64NextTime = NextTime; };
	bool GetOwn(void)											{ return m_bOwn; };
	void SetOwn(const bool& Own)								{ m_bOwn = Own; };
	UINT64 GetTimeGap(void)										{ return m_uix64TimeGap; };
	void SetTimeGap(const UINT64& TimeGap)						{ m_uix64TimeGap = TimeGap; };
	bool GetGapSign(void)										{ return m_bGapSign; };
	void SetGapSign(const bool& GapSign)						{ m_bGapSign = GapSign; };
	BYTE GetHP(void)											{ return m_byteHP; };
	void SetHP(const BYTE& hp)									{ m_byteHP = hp; };
	INT32 GetTrap(void)											{ 
		return m_ix32TrapCount; 
	};
	void SetTrap(const INT32& TrapCount)						{ m_ix32TrapCount = TrapCount; };
	STATUS_UI GetStatusUI(void)									{ return m_PlayerStatus; };
	void SetStatusUI(STATUS_UI status)							{ m_PlayerStatus = status; };
	ROLE GetROLE(void)											{ return m_PlayerRole; }
	void SetROLE(ROLE role)										{ m_PlayerRole = role; };
	EVENT_TYPE GetEventType(void)								{ return m_EventType; }
	void SetEventType(EVENT_TYPE type)							{ m_EventType = type; };

	void setSCgap(UINT64 gap, bool Sign)						{ m_uix64TimeGap = gap; m_bGapSign = Sign; }
	void SetReplayflag(bool flag)								{ m_bReplayFlag = flag; }
	bool IsReplayflag()											{ return m_bReplayFlag; }
	UINT64 & LastPacketTime()									{ return m_uix64LastPacketTime; }
	XMFLOAT3 & PacketPos()										{ return m_xmf3PacketPos; }
	int32_t AnimationType()										{ return m_AnimationType;}
	void SetAnimationType(ANIMATION_TYPE any)					{ 
		m_AnimationType = any; 
	}

	void SetReplayflag(bool flag, UINT64 lastpacketTime, XMFLOAT3 pos);
	bool IsLastPacketTimeCheck(UINT64 tick);
	void SetAnimationType(ANIMATION_TYPE any, bool no); 
};



//------------------------------------------------------------------
//
// Class : CPlayer
//
// Desc : 
//
//------------------------------------------------------------------
class CShader;
class CPlayer : public CGameObject
{

	//-------------------------------------
	//AABB Collision
	//-------------------------------------
private:
	BoundingBox							m_motionAABB;
	bool								m_bMotionCheck;

public:
	void SetMotionAABB(void);
	BoundingBox& GetMotionAABB(void)	{ return m_motionAABB; }
	void SetMotioning(bool bMotion)		{ m_bMotionCheck = bMotion; }
	bool GetMotioning(void)				{ return m_bMotionCheck; }


	//-------------------------------------
	//Player Information
	//-------------------------------------
protected:
	bool					m_keyflag;

	XMFLOAT3				m_xmf3Position;
	XMFLOAT3				m_xmf3Right;
	XMFLOAT3				m_xmf3Up;
	XMFLOAT3				m_xmf3Look;

	float					m_fPitch;
	float					m_fYaw;
	float					m_fRoll;

	XMFLOAT3				m_xmf3Velocity;
	XMFLOAT3				m_xmf3Gravity;
	float					m_fMaxVelocityXZ;
	float					m_fMaxVelocityY;
	float					m_fFriction;

	//플레이어의 위치가 바뀔 때마다 호출되는 OnPlayerUpdated() 함수에서 사용하는 데이터이다.
	LPVOID								m_pPlayerUpdatedContext;
	//카메라의 위치가 바뀔 때마다 호출되는 OnCameraUpdated() 함수에서 사용하는 데이터이다.
	LPVOID								m_pCameraUpdatedContext;

	//플레이어에 현재 설정된 카메라이다.
	CCamera	*				m_pCamera;

public:
	CPlayer() {}
	CPlayer(int nMeshes = 1);
	CPlayer(int nMeshes, int nPlayerShaderIndex);
	virtual ~CPlayer();

	CPlayerInfo	*			m_pPlayerInfo;

	void SetCamera(CCamera *pCamera) { m_pCamera = pCamera; }
	CCamera *GetCamera() { return(m_pCamera); }

	//플레이어의 상수 버퍼를 생성하고 갱신하는 멤버 함수를 선언한다.
	void CreateShaderVariables(ID3D11Device *pd3dDevice);
	void UpdateShaderVariables(ID3D11DeviceContext *pd3dDeviceContext);

	XMFLOAT3 GetPosition()										{ return(m_xmf3Position); }
	XMFLOAT3 GetLookVector()									{ return(m_xmf3Look); }
	XMFLOAT3 GetUpVector()										{ return(m_xmf3Up); }
	XMFLOAT3 GetRightVector()									{ return(m_xmf3Right); }

	void SetLookVector(const XMFLOAT3& xmf3Look)				{ m_xmf3Look = xmf3Look; }
	void SetRightVector(const XMFLOAT3& xmf3Right)				{ m_xmf3Right = xmf3Right; }

	void SetFriction(float fFriction)							{ m_fFriction = fFriction; }
	void SetGravity(const XMFLOAT3& xmf3Gravity)				{ m_xmf3Gravity = xmf3Gravity; }
	void SetMaxVelocityXZ(float fMaxVelocity)					{ m_fMaxVelocityXZ = fMaxVelocity; }
	void SetMaxVelocityY(float fMaxVelocity)					{ m_fMaxVelocityY = fMaxVelocity; }
	void SetVelocity(const XMFLOAT3& xmf3Velocity)				{ m_xmf3Velocity = xmf3Velocity; }

	/*플레이어의 위치를 d3dxvPosition 위치로 설정한다. d3dxvPosition 벡터에서 현재 플레이어의 위치 벡터를 빼면 현재 플레이어의 위치에서 d3dxvPosition 방향으로의 방향 벡터가 된다.
	현재 플레이어의 위치에서 이 방향 벡터 만큼 이동한다.*/
	void SetPosition(const XMFLOAT3& xmf3Position)				{ Move(Vector3::Subtract(xmf3Position, m_xmf3Position), false); }

	virtual void SetObjectScale(int Modelidx);

	XMFLOAT3& GetVelocity()										{ return(m_xmf3Velocity); }
	float GetYaw() const										{ return(m_fYaw); }
	float GetPitch() const										{ return(m_fPitch); }
	float GetRoll() const										{ return(m_fRoll); }

	XMFLOAT4X4 Getxmf4x4Rotate(void)							{ return m_xmf4x4Rotate; }

	bool CollisionCheck(BoundingBox& boundingbox, vector<CShader*>& vtInstancingShader);

	void Move(ULONG nDirection, float fDistance, bool bVelocity = false);
	void Move(XMFLOAT3& xmf3Shift, bool bVelocity);
	void Move(float fxOffset = 0.0f, float fyOffset = 0.0f, float fzOffset = 0.0f);
	bool Move(DWORD dwDirection, float fDistance, bool bUpdateVelocity, vector<CShader*>& vtInstancingShader);
	void Rotate(float x, float y, float z);
	
	//플레이어의 위치와 회전 정보를 경과 시간에 따라 갱신하는 함수이다.
	void Update(float fTimeElapsed, int nPlayer, vector<CShader*> vtInstancingShader);

	virtual void SetPlayerRotationPYR(float Pitch, float Yaw, float Roll);

	//플레이어의 위치가 바뀔 때마다 호출되는 함수와 그 함수에서 사용하는 정보를 설정하는 함수이다.
	virtual void OnPlayerUpdated(float fTimeElapsed, float fRegulateHeight = 15.0f);
	void SetPlayerUpdatedContext(LPVOID pContext) { m_pPlayerUpdatedContext = pContext; }
	//카메라의 위치가 바뀔 때마다 호출되는 함수와 그 함수에서 사용하는 정보를 설정하는 함수이다.
	virtual void OnCameraUpdated(float fTimeElapsed);
	void SetCameraUpdatedContext(LPVOID pContext) { m_pCameraUpdatedContext = pContext; }

	CCamera *OnChangeCamera(ID3D11Device *pd3dDevice, DWORD nNewCameraMode, DWORD nCurrentCameraMode);
	virtual void ChangeCamera(ID3D11Device *pd3dDevice, DWORD nNewCameraMode, float fTimeElapsed);
	virtual void OnPrepareRender();
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);

public:
	void SetKeyFlag(bool flag) { m_keyflag = flag; }
	void PlayerPYRUpdate(INT32 keydirection, ANIMATION_TYPE any);
	void PlayerObjectReplay(void);
	bool float_compare(float a, float b) { return fabs(a - b) < EPSILON; }
	void OthersReplay(UINT64 & latency, XMFLOAT3 & pos, XMFLOAT3 & inputKey);

};

//------------------------------------------------------------------
//
// Class : CTerrainPlayer
//
// Desc : A class that represents players affected by terrain
//
//------------------------------------------------------------------
class CTerrainPlayer : public CPlayer
{
public:
	CTerrainPlayer(int nMeshes = 1);
	CTerrainPlayer(int nMeshes, int shaderIndex);
	virtual ~CTerrainPlayer() {};

	virtual void ChangeCamera(ID3D11Device *pd3dDevice, DWORD nNewCameraMode, float fTimeElapsed);

	virtual void OnPlayerUpdated( float fTimeElapsed, float fRegulateHeight = 15.0f);
	virtual void OnCameraUpdated(float fTimeElapsed);

	virtual void OnPrepareRender();
};