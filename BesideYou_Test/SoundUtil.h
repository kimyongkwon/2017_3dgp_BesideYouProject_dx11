#pragma once

#include"inc\/fmod_errors.h"
#include <unordered_map>

#define MAX_SOUND_COUNT 20

class SoundUtil
{
public:
	SoundUtil();
	~SoundUtil();
	void Initalize();
	void Loading(const char* inFileName, FMOD_MODE inMode, int32_t inSoundType);
	void LoopLoading(const char* inFileName, FMOD_MODE inMode, int inSoundType);

	void Play(int32_t inSoundIndex, int32_t inSoundChannel);
	void Stop(int32_t inChannel);
	void Paused(int32_t inChannel);
	void ErrorCheck(FMOD_RESULT inResult);
	//4
	bool isPlaying(int32_t inChannel);
private:
	System* mFmodSystem;
	Channel* mFmodChannel[MAX_SOUND_COUNT];
	Sound* mSound[MAX_SOUND_COUNT];
	FMOD_RESULT mResult;
	float mVolume;
};


typedef enum SOUNDKEY
{
	MAIN_SOUND = 'MAI',
	INGAME_SOUND = 'ING',
	ATT = 'Att',
	HITTED = 'HIT',
	STARTGENON = 'GE1',
	GENERATING = 'GE2',
	GENERATOR = 'GEN',
	RUN = 'RUN',
	INSTALLTRAP = 'INS',
	STEPONTRAP = 'TRP',
	AFTERGENON = 'AGO',

	TRAPED = 'TRA',
	OPENDOOR = 'DOR',
	WIN = 'WIN',
	DEF = 'DEF',
	EMERGENCY = 'EME',
	NEARONE = 'NO',
	NEARTWO = 'NT',
	NEARTHR = 'NR',
	SMAIN = 0,
	SINGAME,
	SGENERATOR,
	SHITTED,
	STRAPED,
	SOPENDOOR,
	SWIN,
	SDEF,
	SEMERGENCY,

};
class SoundEngine : public Singleton<SoundEngine>
{
	typedef void(*RPCFunc)(bool, bool);
public:
	SoundEngine();
	virtual ~SoundEngine() {}
	void initalize();
	void execute(int32_t inSoundKey, bool isOn, bool isLoop = false);

	static SoundUtil mSoundUtil;
private:
	unordered_map<int32_t, RPCFunc> mSoundFunc;

	static void mainOn(bool isloop, bool ison);
	static void inGameOn(bool isloop, bool ison);
	static void Generating(bool isloop, bool ison);
	static void HITTEDOn(bool isloop, bool ison);
	static void ATTOn(bool isloop, bool ison);
	static void Run(bool isloop, bool ison);
	static void StepOnTrap(bool isloop, bool ison);
	static void InstallTrap(bool isloop, bool ison);
	static void startGeneratorOn(bool isloop, bool ison);
	static void AfterGeneratorOn(bool isloop, bool ison);

	static void WINOn(bool isloop, bool ison);
	static void DEFOn(bool isloop, bool ison);
	static void OPENDOOROn(bool isloop, bool ison);
	static void EMERGENCYOn(bool isloop, bool ison);
	static void TRAPEDOn(bool isloop, bool ison);
	static void FNEARONEOn(bool isloop, bool ison);
	static void FNEARTWOOn(bool isloop, bool ison);
	static void FNEARTHREEOn(bool isloop, bool ison);

};


