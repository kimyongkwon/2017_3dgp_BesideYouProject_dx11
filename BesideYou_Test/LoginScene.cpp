#include "stdafx.h"

static WCHAR ID[100] = { NULL };
static size_t ID_len = 0;

static WCHAR PASSWORD[100] = { NULL };
static size_t PASSWORD_len = 0;

bool bID = TRUE;
bool bLoginOK = FALSE;

CLoginScene::CLoginScene() : CScene()
{
	//m_nBitmaps = 0;
}

CLoginScene::~CLoginScene()
{
}

bool CLoginScene::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, POINT * cursorPos, ID2D1DeviceContext * pDeviceContext, CPlayerShader ** playerShader)
{

	float width = pDeviceContext->GetSize().width;
	float height = pDeviceContext->GetSize().height;

	switch (nMessageID)
	{
		//마우스 커서가 로그인이미지에 들어가면 활성화시킨다.
	case WM_MOUSEMOVE:
		if (cursorPos->x > NormalCoordX(0.509, width) && cursorPos->x < NormalCoordX(0.668, width) && cursorPos->y > NormalCoordY(0.833, height) && cursorPos->y < NormalCoordY(0.885, height)) bLoginOK = TRUE;
		else bLoginOK = false;
		break;
	case WM_LBUTTONDOWN:
		// 로그인이미지가 활성화된 상태에서 버튼을 클릭하면 다음씬으로 이동한다.
		if (bLoginOK) {
#if NETWORK
			if (GameFlowManager::getInstance().type() == FLOW_ROBBY) return true;
			ClientNetworkInterface::getInstance().ClientLogin(ID, PASSWORD);
#else
			return true;
#endif
		}
		break;
	}
	return(false);
}

bool CLoginScene::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, CPlayerShader ** playerShader)
{
	switch (nMessageID)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case VK_TAB:
			bID ? (bID = FALSE) : (bID = TRUE);			//TAB을 눌러서 아이디창이나 비밀번호창으로 변경한다.
			break;
		case VK_BACK:										//지울때
			if (bID) {
				if (ID_len == 0)
					break;
				ID[ID_len--] = L' ';
				ID[ID_len] = L'\0';
			}
			else {
				if (PASSWORD_len == 0)
					break;
				PASSWORD[PASSWORD_len--] = L' ';
				PASSWORD[PASSWORD_len] = L'\0';
			}
			break;
		default:
			if (bID) {
				ID[ID_len++] = (TCHAR)wParam;
				ID[ID_len] = L'\0';
				break;
			}
			else {
				PASSWORD[PASSWORD_len++] = (TCHAR)wParam;
				PASSWORD[PASSWORD_len] = L'\0';
				break;
			}
			break;
		}
		break;
	default:
		break;
	}

	return(false);
}

void CLoginScene::Build2dObjects(IDWriteFactory * pdwFactory, IWICImagingFactory * pwicFactory, ID2D1DeviceContext * pd2dDeviceContext, ID2D1Factory1 * pd2dFactory)
{
	HRESULT result;

	//m_nBitmaps = 3;
	//m_vtd2dBitmap = new ID2D1Bitmap1*[m_nBitmaps];
	m_vtd2dBitmap.resize(3, nullptr);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/LoginImage.png", &m_vtd2dBitmap[0], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/Login.png", &m_vtd2dBitmap[1], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/LoginOK.png", &m_vtd2dBitmap[2], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
}


void CLoginScene::Render2D(ID2D1DeviceContext * pd2dDeviceContext, IDWriteFactory * pdwFactory, ID2D1Factory1 * pd2dFactory)
{
	float width = pd2dDeviceContext->GetSize().width;
	float height = pd2dDeviceContext->GetSize().height;

	pd2dDeviceContext->BeginDraw();

	D2D1::Matrix3x2F mtx = D2D1::Matrix3x2F::Identity();
	pd2dDeviceContext->SetTransform(mtx);

	D2D1::Matrix3x2F rot = D2D1::Matrix3x2F::Identity();

	ID2D1RectangleGeometry * pd2drcBox;
	ID2D1SolidColorBrush * pd2dsbrWhiteColor;
	ID2D1SolidColorBrush * pd2dsbrBlackColor;
	IDWriteTextFormat * dwMyChattingFormat;

	//색 설정
	pd2dDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White, 1.0f), &pd2dsbrWhiteColor);
	pd2dDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black, 1.0f), &pd2dsbrBlackColor);

	//글씨체 설정
	pdwFactory->CreateTextFormat(L"맑은고딕", nullptr, DWRITE_FONT_WEIGHT_EXTRA_BLACK, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 20.0f, L"ko-ko", &dwMyChattingFormat);

	//2d이미지 띄우기
	pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[0], D2D1::RectF(0.f, 0.f, width, height), 0.9f);
	pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[1], D2D1::RectF(NormalCoordX(0.322, width), NormalCoordY(0.54, height), NormalCoordX(0.71, width), NormalCoordY(0.91, height)), 1.0f);
	if (bLoginOK)
		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[2], D2D1::RectF(NormalCoordX(0.509, width), NormalCoordY(0.833, height), NormalCoordX(0.668, width), NormalCoordY(0.88, height)), 1.0f);

	//ID입력
	wchar_t IDstr[100];

	wmemset(IDstr, 0, 100);
	wsprintf(IDstr, ID);
	pd2dDeviceContext->DrawTextW(IDstr, 100, dwMyChattingFormat, D2D1::RectF(NormalCoordX(0.361, width), NormalCoordY(0.625, height), NormalCoordX(0.634, width), NormalCoordY(0.651, height)), pd2dsbrWhiteColor);

	wchar_t PWstr[100];
	wmemset(PWstr, 0, 100);
	wsprintf(PWstr, PASSWORD);
	pd2dDeviceContext->DrawTextW(PWstr, 100, dwMyChattingFormat, D2D1::RectF(NormalCoordX(0.361, width), NormalCoordY(0.755, height), NormalCoordX(0.634, width), NormalCoordY(0.781, height)), pd2dsbrWhiteColor);

	pd2dDeviceContext->EndDraw();
}

void CLoginScene::ReleaseObjects()
{
	if (!m_vtd2dBitmap.empty()) {
		for (auto i = 0; i < m_vtd2dBitmap.size(); ++i) {
			m_vtd2dBitmap[i]->Release();
			delete m_vtd2dBitmap[i];
		}
	}
	//delete[]m_vtd2dBitmap;

	CScene::ReleaseObjects();
}