#pragma once

//------------------------------------------------------------------
//
// Class : LIGHT
//
// Desc : Ambient Diffuse, Specular Light
//
//------------------------------------------------------------------

struct LIGHT
{
	XMFLOAT4		m_xmcAmbient;
	XMFLOAT4		m_xmcDiffuse;
	XMFLOAT4		m_xmcSpecular;
	XMFLOAT3		m_xmf3Position;
	float			m_fRange;
	XMFLOAT3		m_xmf3Direction;
	float			m_nType;
	XMFLOAT3		m_xmf3Attenuation;
	float			m_fFalloff;
	float			m_fTheta;	//cos(m_fTheta)
	float			m_fPhi;		//cos(m_fPhi)
	float			m_bEnable;
	float			padding;
};

//상수 버퍼는 크기가 반드시 16 바이트의 배수가 되어야 한다. 
struct LIGHTS
{
	LIGHT			m_pLights[MAX_LIGHTS];
	XMFLOAT4		m_xmcGlobalAmbient;
	XMFLOAT4		m_xmf4CameraPosition;
	//float padding[3];
};