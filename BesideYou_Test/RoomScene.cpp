#include "stdafx.h"
#include "RoomScene.h"


bool bReadyOK = false;
bool bMyID = false;

static WCHAR ID[100] = { NULL };
static size_t ID_len = 0;

CRoomScene::CRoomScene()
{
}

CRoomScene::~CRoomScene()
{
}

bool CRoomScene::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, POINT * cursorPos, ID2D1DeviceContext * pDeviceContext, CPlayerShader ** playerShader)
{
	float width = pDeviceContext->GetSize().width;
	float height = pDeviceContext->GetSize().height;

	switch (nMessageID)
	{
	case WM_MOUSEMOVE:
		{
			if (cursorPos->x > NormalCoordX(0.351, width) && cursorPos->x < NormalCoordX(0.654, width) && cursorPos->y > NormalCoordY(0.82, height) && cursorPos->y < NormalCoordY(0.937, height)){
				bReadyOK = true;
				return true;
			}
			else
				bReadyOK = false;

		break;
		}
	case WM_LBUTTONDOWN:
		{
#if NETWORK
			if (GameFlowManager::getInstance().type() == FLOW_INGAME) return true;
			if (bReadyOK) {
				ClientNetworkInterface::getInstance().ClientRoomReady();
				break;
			}
#endif
		}
		break;
	}
	return(false);
}

bool CRoomScene::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam, CPlayerShader ** playerShader)
{
	return (false);
}

void CRoomScene::Build2dObjects(IDWriteFactory *pdwFactory, IWICImagingFactory * pwicFactory, ID2D1DeviceContext * pd2dDeviceContext, ID2D1Factory1 * pd2dFactory)
{
	HRESULT result;

	
	m_vtd2dBitmap.resize(8, nullptr);
	//m_vtd2dBitmap = new ID2D1Bitmap1*[m_nBitmaps];
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/LobbyImage.png", &m_vtd2dBitmap[0], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/RoomBase.png", &m_vtd2dBitmap[1], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/RoomReady.png", &m_vtd2dBitmap[2], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/RoomReadyOK.png", &m_vtd2dBitmap[3], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/Murderer.png", &m_vtd2dBitmap[4], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/Survivor.png", &m_vtd2dBitmap[5], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/Check.png", &m_vtd2dBitmap[6], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);
	result = LoadImageFromFile(L"../Data/BesideYouData/UI/UnCheck.png", &m_vtd2dBitmap[7], nullptr, 0, 0, WICBitmapTransformRotate0, pwicFactory, pd2dDeviceContext);

}

void CRoomScene::Render2D(ID2D1DeviceContext * pd2dDeviceContext, IDWriteFactory * pdwFactory, ID2D1Factory1 * pd2dFactory)
{
	pd2dDeviceContext->BeginDraw();

	float width = pd2dDeviceContext->GetSize().width;
	float height = pd2dDeviceContext->GetSize().height;

	ID2D1SolidColorBrush * pd2dsbrWhiteColor;
	IDWriteTextFormat * dwMyChattingFormat;

	pd2dDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White, 1.0f), &pd2dsbrWhiteColor);

	pdwFactory->CreateTextFormat(L"��������", nullptr, DWRITE_FONT_WEIGHT_EXTRA_BLACK, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 30.0f, L"ko-ko", &dwMyChattingFormat);

	pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[0], D2D1::RectF(0.f, 0.f, width, height), 0.9f);
	pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[1], D2D1::RectF(NormalCoordX(0.078, width), NormalCoordY(0.13, height), NormalCoordX(0.937, width), NormalCoordY(0.781, height)), 0.9f);
	pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[2], D2D1::RectF(NormalCoordX(0.351, width), NormalCoordY(0.82, height), NormalCoordX(0.654, width), NormalCoordY(0.937, height)), 0.9f);

	if (bReadyOK) pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[3], D2D1::RectF(NormalCoordX(0.351, width), NormalCoordY(0.82, height), NormalCoordX(0.654, width), NormalCoordY(0.937, height)), 0.9f);

	pd2dDeviceContext->DrawTextW(RBPlayerManager::getInstance().at_own()->name(), RBPlayerManager::getInstance().at_own()->sizeOfName(), dwMyChattingFormat, D2D1::RectF(NormalCoordX(0.244, width), NormalCoordY(0.273, height), NormalCoordX(0.488, width), NormalCoordY(0.39, height)), pd2dsbrWhiteColor);

	if (RBPlayerManager::getInstance().at_own()->role() == KILLER)
		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[4], D2D1::RectF(NormalCoordX(0.566, width), NormalCoordY(0.273, height), NormalCoordX(0.693, width), NormalCoordY(0.347, height)), 0.9f);
	else if (RBPlayerManager::getInstance().at_own()->role() == SURVIVOR)
		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[5], D2D1::RectF(NormalCoordX(0.566, width), NormalCoordY(0.273, height), NormalCoordX(0.693, width), NormalCoordY(0.347, height)), 0.9f);
	if (RBPlayerManager::getInstance().at_own()->isReady())
		pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[6], D2D1::RectF(NormalCoordX(0.751, width), NormalCoordY(0.273, height), NormalCoordX(0.878, width), NormalCoordY(0.347, height)), 0.9f);
	else pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[7], D2D1::RectF(NormalCoordX(0.751, width), NormalCoordY(0.273, height), NormalCoordX(0.878, width), NormalCoordY(0.347, height)), 0.9f);
	
	auto players = RBPlayerManager::getInstance().Playerinfo();
	wstr_t _userName[3] = { };
	ROLE _role[3] = { NONE ,NONE ,NONE };
	bool _ready[3];
	ZeroMemory(_role, sizeof(ROLE) * 3);
	ZeroMemory(_ready, sizeof(bool) * 3);
	size_t maxindex = RBPlayerManager::getInstance().Pcount();
	auto pVect = RBPlayerManager::getInstance().Playerinfo();
	int checkcnt = 0;
	for (auto index = 0; index < maxindex; ++index) {
		if ((*pVect)[index]->uid() == RBPlayerManager::getInstance().at_own()->uid())continue;
		_ready[checkcnt] = (*pVect)[index]->isReady();
		_role[checkcnt] = (*pVect)[index]->role();
		_userName[checkcnt] = (*pVect)[index]->name();
		checkcnt++;
	}

	if (_role[0] != NONE) {
		pd2dDeviceContext->DrawTextW(_userName[0].c_str(), _userName[0].size(), dwMyChattingFormat, D2D1::RectF(NormalCoordX(0.244, width), NormalCoordY(0.377, height), NormalCoordX(0.488, width), NormalCoordY(0.493, height)), pd2dsbrWhiteColor);
		if (_role[0] == KILLER)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[4], D2D1::RectF(NormalCoordX(0.566, width), NormalCoordY(0.377, height), NormalCoordX(0.693, width), NormalCoordY(0.451, height)), 0.9f);
		else if (_role[0] == SURVIVOR)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[5], D2D1::RectF(NormalCoordX(0.566, width), NormalCoordY(0.377, height), NormalCoordX(0.693, width), NormalCoordY(0.451, height)), 0.9f);
		if (_ready[0])
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[6], D2D1::RectF(NormalCoordX(0.751, width), NormalCoordY(0.377, height), NormalCoordX(0.878, width), NormalCoordY(0.451, height)), 0.9f);
		else pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[7], D2D1::RectF(NormalCoordX(0.751, width), NormalCoordY(0.377, height), NormalCoordX(0.878, width), NormalCoordY(0.451, height)), 0.9f);
	}

	if (_role[1] != NONE) {
		pd2dDeviceContext->DrawTextW(_userName[1].c_str(), _userName[1].size(), dwMyChattingFormat, D2D1::RectF(NormalCoordX(0.244, width), NormalCoordY(0.481, height), NormalCoordX(0.488, width), NormalCoordY(0.598, height)), pd2dsbrWhiteColor);
		if (_role[1] == KILLER)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[4], D2D1::RectF(NormalCoordX(0.566, width), NormalCoordY(0.481, height), NormalCoordX(0.693, width), NormalCoordY(0.555, height)), 0.9f);
		else if (_role[1] == SURVIVOR)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[5], D2D1::RectF(NormalCoordX(0.566, width), NormalCoordY(0.481, height), NormalCoordX(0.693, width), NormalCoordY(0.555, height)), 0.9f);
		if (_ready[1])
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[6], D2D1::RectF(NormalCoordX(0.751, width), NormalCoordY(0.481, height), NormalCoordX(0.878, width), NormalCoordY(0.555, height)), 0.9f);
		else pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[7], D2D1::RectF(NormalCoordX(0.751, width), NormalCoordY(0.481, height), NormalCoordX(0.878, width), NormalCoordY(0.555, height)), 0.9f);
	}

	if (_role[2] != NONE) {
		pd2dDeviceContext->DrawTextW(_userName[2].c_str(), _userName[2].size(), dwMyChattingFormat, D2D1::RectF(NormalCoordX(0.244, width), NormalCoordY(0.585, height), NormalCoordX(0.488, width), NormalCoordY(0.703, height)), pd2dsbrWhiteColor);
		if (_role[2] == KILLER)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[4], D2D1::RectF(NormalCoordX(0.566, width), NormalCoordY(0.585, height), NormalCoordX(0.693, width), NormalCoordY(0.66, height)), 0.9f);
		else if (_role[2] == SURVIVOR)
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[5], D2D1::RectF(NormalCoordX(0.566, width), NormalCoordY(0.585, height), NormalCoordX(0.693, width), NormalCoordY(0.66, height)), 0.9f);
		if (_ready[2])
			pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[6], D2D1::RectF(NormalCoordX(0.751, width), NormalCoordY(0.585, height), NormalCoordX(0.878, width), NormalCoordY(0.66, height)), 0.9f);
		else pd2dDeviceContext->DrawBitmap(m_vtd2dBitmap[7], D2D1::RectF(NormalCoordX(0.751, width), NormalCoordY(0.585, height), NormalCoordX(0.878, width), NormalCoordY(0.66, height)), 0.9f);
	}

	pd2dDeviceContext->EndDraw();
}

void CRoomScene::ReleaseObjects()
{
	if (!m_vtd2dBitmap.empty()) {
		for (auto i = 0; i < m_vtd2dBitmap.size(); ++i) {
			m_vtd2dBitmap[i]->Release();
			delete m_vtd2dBitmap[i];
		}
	}

	CScene::ReleaseObjects();
}