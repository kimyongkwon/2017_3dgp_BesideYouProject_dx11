#pragma once

struct VS_CB_WORLD_MATRIX
{
	XMFLOAT4X4 m_xmf4x4World;
};

//------------------------------------------------------------------
//
// Class : CShader
//
// Desc : 
//
//------------------------------------------------------------------

class CShader
{
	CHeightMapTerrain					* m_pHeightMapTerrain;
public:
	CShader() {};
	CShader(CHeightMapTerrain *pHeightMapTerrain, vector<ModelContainer*> pvtFBXDatas, CCSVData * pCSVData);
	virtual ~CShader();
	
	//-------------------------------------
	//Model-related data
	//-------------------------------------
private:
	CCSVData							* m_pCSVData;
	vector<ModelContainer*>			      m_pvtFBXDatas;


	//-------------------------------------
	//Shader-related data
	//-------------------------------------
protected:
	ID3D11VertexShader					* m_pd3dVertexShader;
	ID3D11InputLayout					* m_pd3dVertexLayout;
	ID3D11PixelShader					* m_pd3dPixelShader;
	//월드 변환 행렬을 위한 상수 버퍼는 하나만 있어도 되므로 정적 멤버로 선언한다.
	static ID3D11Buffer					* m_pd3dcbWorldMatrix;
public:
	void CreateVertexShaderFromFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderModel, ID3D11VertexShader **ppd3dVertexShader, D3D11_INPUT_ELEMENT_DESC *pd3dInputLayout, UINT nElements, ID3D11InputLayout **ppd3dVertexLayout);
	void CreatePixelShaderFromFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderModel, ID3D11PixelShader **ppd3dPixelShader);

	//for dx11 compiler
	void CreateVertexShaderFromCompiledFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName, D3D11_INPUT_ELEMENT_DESC *pd3dInputLayout, UINT nElements);
	void CreatePixelShaderFromCompiledFile(ID3D11Device *pd3dDevice, WCHAR *pszFileName);

	virtual void CreateShader(ID3D11Device *pd3dDevice);

	static void CreateShaderVariables(ID3D11Device *pd3dDevice);
	static void ReleaseShaderVariables();
	static void UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, XMFLOAT4X4 *pxmf4x4World);


	//-------------------------------------
	//Game Object-related data
	//-------------------------------------
protected:
	//쉐이더 객체가 게임 객체들의 리스트를 가진다.
	CGameObject						   ** m_ppObjects;
	int									  m_nObjects;
public:
	int GetObjectNum(void) { return m_nObjects; };
	CGameObject ** GetppObjects(void) { return m_ppObjects; }

	//게임 객체들을 생성하고 애니메이션 처리를 하고 렌더링하기 위한 함수이다.
	virtual void BuildObjects(ID3D11Device *pd3dDevice);
	virtual void ReleaseObjects();
	virtual void AnimateObjects(float fTimeElapsed);
	virtual void OnPrepareRender(ID3D11DeviceContext *pd3dDeviceContext);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera = NULL);
	virtual void BuildInstanceObjects(ID3D11Device *pd3dDevice, ModelContainer * pFBXDatas, CCSVData * pCSVData, CHeightMapTerrain * pHeightMapTerrain, CScene * pSCene = NULL) {};
	CGameObject* GetGameObject(int nIndex) { return m_ppObjects[nIndex]; }
};

class CIlluminatedShader : public CShader
{
public:
	CIlluminatedShader();
	virtual ~CIlluminatedShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);

	//재질을 설정하기 위한 상수 버퍼이다.
	static ID3D11Buffer					* m_pd3dcbMaterial;

	static void CreateShaderVariables(ID3D11Device *pd3dDevice);
	static void ReleaseShaderVariables();
	//재질을 쉐이더 변수에 설정(연결)하기 위한 함수이다.
	static void UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, MATERIAL *pMaterial);
};

class CTexturedShader : public CShader
{
public:
	CTexturedShader();
	virtual ~CTexturedShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);
};

class CDetailTexturedShader : public CTexturedShader
{
public:
	CDetailTexturedShader();
	virtual ~CDetailTexturedShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);
};

class CTexturedIlluminatedShader : public CIlluminatedShader
{
public:
	CTexturedIlluminatedShader();
	virtual ~CTexturedIlluminatedShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);
};

class CDetailTexturedIlluminatedShader : public CTexturedIlluminatedShader
{
public:
	CDetailTexturedIlluminatedShader();
	virtual ~CDetailTexturedIlluminatedShader();

	virtual void CreateShader(ID3D11Device *pd3dDevice);
};

class CTerrainShader : public CDetailTexturedIlluminatedShader
{
public:
	CTerrainShader();
	virtual ~CTerrainShader();

	virtual void BuildObjects(ID3D11Device * pd3dDevice);

	CHeightMapTerrain * GetTerrain() const;
};

class CPlayerShader : public CTexturedIlluminatedShader
{
private:
	CMaterial *m_pMaterial;
	CTexture *m_pTexture;

public:
	CPlayerShader();
	virtual ~CPlayerShader();
	virtual void CreateShader(ID3D11Device *pd3dDevice);
	void BuildObjects(ID3D11Device *pd3dDevice, ROLE nPlayer, int playerShaderindex);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera = NULL);

	CPlayer *GetPlayer(int nIndex = 0) { return((CPlayer *)m_ppObjects[nIndex]); }
};

class CPlayerAABBShader : public CShader
{
public:
	CPlayerAABBShader() {};
	~CPlayerAABBShader() {};

	virtual void BuildObjects(ID3D11Device *pd3dDevice, vector<CPlayer*>& vtPlayers, int who) {};
	
	void Render(ID3D11DeviceContext *pd3dDeviceContext, CPlayer ** pPlayer);
};

class CTrapShader : public CTexturedIlluminatedShader
{
	bool			m_bTrapState;
	uint64_t		mTrapNumber;
	uint64_t		mCatchPlayerId;
	uint8_t			mUpdate;
	
public:
	CTrapShader() { m_bTrapState = false; };
	virtual ~CTrapShader() {};

	virtual void CreateShader(ID3D11Device *pd3dDevice);

	virtual void BuildObjects(ID3D11Device *pd3dDevice);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera = NULL);

	virtual void SetTrapPosition(CPlayer& player, CHeightMapTerrain& terrain);
	virtual void SetTrapPosition(XMFLOAT3& inPosition, CHeightMapTerrain& terrain);
	bool CheckTrapOn(BoundingBox & playerAABB);

public:
	virtual void SetTrapState(bool state) {
		m_bTrapState = state;
	};
	virtual bool GetTrapState(void) {
		return m_bTrapState;
	};
	
	void SetTrapNumber(uint64_t inNumber) { mTrapNumber = inNumber; }
	uint64_t GetTrapNumber() { return mTrapNumber; }
	void SetCatchPlayer(uint64_t inPlayerId) { mCatchPlayerId = inPlayerId; }
	uint64_t GetCatchPlayerId() { return mCatchPlayerId; }
	void SetUpdate(int8_t inUpdate) { mUpdate = inUpdate; }
	uint64_t GetUpdate() { return mUpdate; }
};

