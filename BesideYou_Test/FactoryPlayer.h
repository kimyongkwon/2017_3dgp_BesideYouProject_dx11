#pragma once
#include"stdafx.h"

class FactoryPlayer : public Singleton<FactoryPlayer>
{
public:
	void BuildObject(ROLE role, int who = 0)
	{
		static int PosNumbers = 1;
		if (PosNumbers > 1)PosNumbers = 1;
		if (who == 0)
		{
			switch (role)
			{
			case KILLER:
				{
				CGameFramework::getInstance().SubBuildObject(0, KILLER);
					return;
				}
			case SURVIVOR:
				{
				CGameFramework::getInstance().SubBuildObject(0, SURVIVOR);
					return;
				}
			}
		}
		else
		{
			switch (role)
			{
			case KILLER:
				{
				CGameFramework::getInstance().SubBuildObject(PosNumbers, KILLER);
				PosNumbers++;
					return;
				}
			case SURVIVOR:
				{
				CGameFramework::getInstance().SubBuildObject(PosNumbers, SURVIVOR);
				PosNumbers++;
					return;
				}
			}

		}
	}

};