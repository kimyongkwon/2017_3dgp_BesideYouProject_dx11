#include "stdafx.h"

CCSVData::CCSVData(const std::string& filename, int DataNum)
{
	m_nDataNum = DataNum;

	m_ipDataType = new int[DataNum];
	m_ipDataNum = new int[DataNum];
	m_ipDataPositionX = new int[DataNum];
	m_ipDataPositionY = new int[DataNum];
	m_ipDataPositionZ = new int[DataNum];
	m_fpDataRotationX = new float[DataNum];
	m_fpDataRotationY = new float[DataNum];
	m_fpDataRotationZ = new float[DataNum];
	m_fpDataScaleX = new float[DataNum];
	m_fpDataScaleY = new float[DataNum];
	m_fpDataScaleZ = new float[DataNum];

	ifstream fin;
	char* ch = new char[100];
	char* sToken = new char[200];
	char* temp;
	fin.open(filename);

	fin.getline(ch, 200);
	for (int i = 0; i < m_nDataNum; i++)
	{
		fin.getline(ch, 100);
		sToken = strtok_s(ch, " ", &temp);
		m_ipDataType[i] = stof(sToken);
		sToken = strtok_s(NULL, " ", &temp);
		m_ipDataNum[i] = stof(sToken);
		sToken = strtok_s(NULL, " ", &temp);
		m_ipDataPositionX[i] = stof(sToken);
		sToken = strtok_s(NULL, " ", &temp);
		m_ipDataPositionY[i] = stof(sToken);
		sToken = strtok_s(NULL, " ", &temp);
		m_ipDataPositionZ[i] = stof(sToken);
		sToken = strtok_s(NULL, " ", &temp);
		m_fpDataRotationX[i] = stof(sToken);
		sToken = strtok_s(NULL, " ", &temp);
		m_fpDataRotationY[i] = stof(sToken);
		sToken = strtok_s(NULL, " ", &temp);
		m_fpDataRotationZ[i] = stof(sToken);
		sToken = strtok_s(NULL, " ", &temp);
		m_fpDataScaleX[i] = stof(sToken);
		sToken = strtok_s(NULL, " ", &temp);
		m_fpDataScaleY[i] = stof(sToken);
		sToken = strtok_s(NULL, " ", &temp);
		m_fpDataScaleZ[i] = stof(sToken);
	}

	fin.close();
}

CCSVData::~CCSVData()
{
	if (m_ipDataType) delete[] m_ipDataType;
	if (m_ipDataNum) delete[] m_ipDataNum;
	if (m_ipDataPositionX) delete[] m_ipDataPositionX;
	if (m_ipDataPositionY) delete[] m_ipDataPositionY;
	if (m_ipDataPositionZ) delete[] m_ipDataPositionZ;
	if (m_fpDataRotationX) delete[] m_fpDataRotationX;
	if (m_fpDataRotationY) delete[] m_fpDataRotationY;
	if (m_fpDataRotationZ) delete[] m_fpDataRotationZ;
	if (m_fpDataScaleX) delete[] m_fpDataScaleX;
	if (m_fpDataScaleY) delete[] m_fpDataScaleY;
	if (m_fpDataScaleZ) delete[] m_fpDataScaleZ;
}