#pragma once
#include"stdafx.h"
#include"NetworkLib.h"
typedef enum {
	NONE,										// NOT ROLE
	KILLER,										// 킬러
	SURVIVOR,									// 생존자
}ROLE;

typedef enum
{
	NONEPLAYER,
	OWNPLAYER = 100,							// 컨트롤 유저
	OTHERPLAYER,								// 그 외의 유저
}IDENTI;


class InputState
{
#if 0
public:
	float rightMount;
	float leftMount;
	float forwardMount;
	float backMount;
	bool IsAttacking;
	XMFLOAT3 _direction;

	XMFLOAT3 _lookvector;
	XMFLOAT3 _rightvector;
	//XMFLOAT4X4 _directionM;
	XMFLOAT3 & Lookvector() { return _lookvector; }
	XMFLOAT3 & Rightvector() { return _rightvector; }
	XMFLOAT3 & direction() { return _direction; }
	InputState() : rightMount(0), leftMount(0), forwardMount(0), backMount(0), IsAttacking(false), _direction(0, 0, 0){}
	float getHorizontalDelta() const
	{
		return (rightMount - leftMount);
	}
	float getVerticalDelta() const
	{
		return (forwardMount - backMount);
	}
	void GetKeyResult(XMFLOAT3 lookvector, XMFLOAT3 rightvector,DWORD dwDirection,DWORD LastDirection_) {
		_lookvector = lookvector;
		_rightvector = rightvector;
		if (DIR_FORWARD == (dwDirection & DIR_FORWARD))
		{
			forwardMount = 1.f;
		}
		if (DIR_BACKWARD == (dwDirection & DIR_BACKWARD))
		{
			backMount = 1.f;
		}
		if (DIR_LEFT == (dwDirection & DIR_LEFT))
		{
			leftMount = 1.f;
		}
		if (DIR_RIGHT == (dwDirection & DIR_RIGHT))
		{
			rightMount = 1.f;
		}

		_direction = { getHorizontalDelta(), 0, getVerticalDelta() };
		return ;
	}
	void clear()
	{
		IsAttacking = false;
		rightMount = 0.f;
		leftMount = 0.f;
		forwardMount = 0.f;
		backMount = 0.f;
	}
#endif
public:
	int dwDirection;
	bool IsAttacking;
	XMFLOAT3 _direction;

	XMFLOAT3 _lookvector;
	XMFLOAT3 _rightvector;
	//XMFLOAT4X4 _directionM;
	XMFLOAT3 & Lookvector() { return _lookvector; }
	XMFLOAT3 & Rightvector() { return _rightvector; }
	XMFLOAT3 & direction() { return _direction; }
	InputState() : dwDirection(0), IsAttacking(false), _direction(0, 0, 0) {}

	void clear()
	{
		IsAttacking = false;

	}
	friend class inputManager;
};

class InputManager : public Singleton<InputManager>
{
public:
	InputState input_;
	DWORD LastDirection_;
	InputManager() : input_() , LastDirection_(0){}
	~InputManager() {}
	InputState & GetInputData() { 
		return input_; }
	void input(DWORD dwDirection, XMFLOAT3 lookvector, XMFLOAT3 rightvector)
	{
		/*input_.clear();
		input_.GetKeyResult(lookvector, rightvector, dwDirection, LastDirection_);*/
		LastDirection_ = dwDirection;
	}
};
class Move
{
private:
	XMFLOAT3 LookVector_;
	XMFLOAT3 RightVector_;

	INT32 nowDirection_;

	UINT64 MoveTimeTampt_;
	INT32 GeneratorNumber_;
	float DeltaTime_;

	INT32 lastDirection_;
	double mVelocity;
public:
	Move() {}
	Move(const Move & instance) 
	{
		LookVector_ = instance.LookVector_;
		RightVector_ = instance.RightVector_;
		nowDirection_ = instance.nowDirection_;
		MoveTimeTampt_ = instance.MoveTimeTampt_;
		DeltaTime_ = instance.DeltaTime_;
		lastDirection_ = instance.lastDirection_;
		mVelocity = instance.mVelocity;
	}
	Move(INT32 NowDirection,INT32 LastDirection_, XMFLOAT3 LookVector, XMFLOAT3 RightVector, UINT64 TimeStampt, float deltaTIme, double inVelocity,INT32 genNumber = -1)
		: nowDirection_(NowDirection), lastDirection_(LastDirection_), 
		LookVector_(LookVector), RightVector_(RightVector),
		MoveTimeTampt_(TimeStampt), DeltaTime_(deltaTIme), GeneratorNumber_(genNumber), mVelocity(inVelocity)
	{}
	~Move() {}
	XMFLOAT3 & LookVector() { return LookVector_; }
	XMFLOAT3 & RightVector() { return RightVector_; }
	INT32 & nowDirection() { return nowDirection_; }
	INT32 & lastDirection() { return lastDirection_; }
	UINT64 & MoveTimeTampt() { return MoveTimeTampt_; }
	float & DetaTime() { return DeltaTime_; }
	INT32 & GeneratorNumber() { return GeneratorNumber_;}
	double & Velocity() { return mVelocity; }
};

class MoveList
{
private:
	// 무브 리스트
	std::deque<Move> moveList_;
	// 마지막 스템프 시간
	INT64 LastStempTime_;
	int cnt;
	INT32 LastDirection_;
public:
	MoveList() : LastStempTime_(0), cnt(0) {}
	~MoveList() { this->clear(); }
	void insertTimeTempt(INT32 dwDirection, XMFLOAT3 LookVector, XMFLOAT3 RightVector, UINT64 TimeStampt, float deltaTIme, double inVelocity, INT32 genNumber = -1)
	{
		//auto deltaTIme = LastStempTime_ == 0 ? 0 : TimeStampt - LastStempTime_;
		moveList_.emplace_back(dwDirection, LastDirection_, LookVector, RightVector, TimeStampt, deltaTIme, inVelocity, genNumber);
		// LastStamptTime and LastDriection save!!!
		LastStempTime_ = TimeStampt;
		LastDirection_ = dwDirection;
		if (++cnt == 5) {
			SendinputPacket();
			cnt = 0;
		}
	}

	void SendinputPacket();
	void removeLastTimeTempt(UINT64 LastTimeTampt)
	{
		while (!moveList_.empty() && moveList_.front().MoveTimeTampt() <= LastTimeTampt)
		{
			moveList_.pop_front();
		}
	}
	void clear() { moveList_.clear(); cnt = 0; }
	bool HasMoves() { return moveList_.size() != 0; }
	size_t size() { return moveList_.size(); }
	deque<Move>::iterator & begin() { return moveList_.begin(); }
	deque<Move>::iterator & end() { return moveList_.end(); }
	std::deque<Move> & movelist() { return moveList_; }
};

class RBPlayerObject
{
	//TODO : Lobby + Room 에서 사용할 유저의 오브젝트 정리
private:
	wstring _name;
	INT64 _uid;			// common Uid;
	//UINT64 _oid;		// DB oid;
	ROLE _role;			// 역할 (0 : 미 선택, 1 : 살인자, 2 : 생존자)
	IDENTI _ident;		// 식별 (100 : 컨트롤 유저, 101 : 타 유저)
	INT64 _roomNumber;	// 방 번호
	bool _roomready;
public:
	RBPlayerObject() : _name(L""), _uid(-1), /*_oid(9000)*/ _role(NONE), _ident(NONEPLAYER), _roomNumber(10000) , _roomready(false){}
	~RBPlayerObject() {}
	const wchar_t * name();
	size_t sizeOfName()
	{
		return _name.size();
	}
	void setname(wstring name);
	INT64 & uid();
	void setuid(INT64 uid);
	/*UINT64 & oid();
	void setoid(UINT64 oid);*/
	ROLE & role();
	void setrole(ROLE role);
	IDENTI & identi();
	void setidenti(IDENTI iden);
	INT64 roomnumber();
	void setroomnumber(INT64 roomnumber);
	void setPlayer(wstring name, INT64 uid, ROLE role, IDENTI iden);
	void ready();
	void unready();
	bool isReady();
};

class RBPlayerManager : public Singleton<RBPlayerManager>
{
	RBPlayerObject * _ownPlayer;
	std::vector<RBPlayerObject *> _playerVector;
public:
	RBPlayerManager();
	virtual ~RBPlayerManager();
	bool addPlayer(RBPlayerObject * player);
	bool removePlayer(DWORD uid);
	size_t Pcount();
	RBPlayerObject * at_own();
	RBPlayerObject * at_Player(DWORD uid);
	vector<RBPlayerObject*> * Playerinfo();
	void ownRoomEnter(INT64 roomNumber);
	void ownRoomLeave();
};
class ClientNetworkInterface : public Singleton<ClientNetworkInterface>
{
private:
	inline bool JoinNameCheak(wchar_t * name, string * rename)
	{
		char ch[20] = { 0, };
		strcpy_s(ch, rename->c_str());
		if (lstrlenW(name) == 0) {
			MessageBoxW(NULL, L"아이디를 입력해 주세요.", L"ERROR", NULL);
			return false;
		}
		if (lstrlenW(name) > 20) {
			MessageBoxW(NULL, L"20글자 이하로 입력해 주세요.", L"ERROR", NULL);
			return false;
		}
		for (auto i = 0; i < strlen(ch); ++i) {
			//44032~55199
			if ((ch[i] & 0x80) == 0x80)
			{
				MessageBoxW(NULL, L"아이디에 한글이 들어가 있습니다.", L"ERROR", NULL);
				return false;
			}
		}
		for (auto i = rename->begin(); i != rename->begin() + strlen(rename->data()); i++) {
			if (isalnum(*i) == 0) {
				MessageBoxW(NULL, L"특수문자,공백이 존재합니다.", L"ERROR", NULL);
				return false;
			}
		}
		return true;
	}
	inline bool JoinPasswordCheak(wchar_t * _pwinput, wchar_t * _pwconfirminput)
	{
		if (lstrcmpW(_pwinput, L"") == 0 || lstrcmpW(_pwconfirminput, L"") == 0) {
			MessageBoxW(NULL, L"비밀번호가 비어져 있습니다. 다시 작성해 주세요.", L"ERROR", NULL);
			return false;
		}
		if (lstrcmpW(_pwinput, _pwconfirminput))
		{
			MessageBoxW(NULL, L"비밀번호가 동일하지 않습니다. 다시 작성해 주세요.", L"ERROR", NULL);
			return false;
		}
		return true;
	}
	inline bool OthersCheak(wchar_t * name, wchar_t * email)
	{
		char ch[50] = { 0, };
		if (lstrlenW(name) == 0) {
			MessageBoxW(NULL, L"이름 칸이 비어 있습니다.", L"ERROR", NULL);
			return false;
		}
		StrConvW2A(name, ch, 50);
		for (auto i = 0; i < strlen(ch); ++i) {
			if ((ch[i] & 0x80) != 0x80)
			{
				MessageBoxW(NULL, L"이름에 한글이 아닌 다른 글자가 입력되었습니다", L"ERROR", NULL);
				return false;
			}
		}
		if (wcslen(email) == 0) {
			MessageBoxW(NULL, L"이메일 칸이 비어 있습니다.", L"EROOR", NULL);					// 이메일 미 작성
			return false;
		}
		if (wcsrchr(email, '@') == NULL) {
			MessageBoxW(NULL, L"이메일 형식이 아닙니다.", L"EROOR", NULL);					//이메일 형식이 아님
			return false;
		}
		return true;
	}
	inline wchar_t * choiceRole(ROLE role) {
		switch (role) {
		case KILLER:
			return L"KILLER";
		case SURVIVOR:
			return L"SURVIVOR";
		}
	}
	inline wchar_t * StrReady(bool ready) {
		if (ready) return L"Ready";
		else return L"unReady";
	}
	MoveList m_MoveList;
	UINT64 LatencyTime_;

public:

	MoveList & moveList() {
		return m_MoveList;
	}
	void MoveListremove(UINT64 & lastTime) {
		m_MoveList.removeLastTimeTempt(lastTime);
	}
	void ClientLogin(wstring _idInput, wstring _pwinput);		// 로그인
	void ClientJOIN(wstring id, wstring pw, wstring email, wstring name);		// 회원가입
	void ClientSelectROLE(ROLE role);	// 역할 변경
	void ClinetReqRoomEnter(); // 방 입장 요청
	void ClientRoomReady();	// 준비, 준비 해제
	void ClientPos(float posx, float posy, float posz, float degree, INT32 roomnumber, INT64 uid);
	void ClientMoveStart();	// 이동 시작
	void ClientMoveEnd();		// 이동 해제
	void ClientAttentionAround();	// 바라보는 방향 변경
	void ClientTrapinstall();		
	void SetLatencyTime(UINT64 tick) {
		LatencyTime_ = tick;
	}
	UINT64 & Latency() { return LatencyTime_; }
	void ClientLogout();

	void ClientChitt();

};