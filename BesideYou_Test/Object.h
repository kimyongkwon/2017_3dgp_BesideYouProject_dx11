#pragma once

//재질의 색상을 나타내는 구조체이다.
struct MATERIAL
{
	XMFLOAT4			m_xmcAmbient;
	XMFLOAT4			m_xmcDiffuse;
	XMFLOAT4			m_xmcSpecular; //(r,g,b,a=power)
	XMFLOAT4			m_xmcEmissive;
};

class CMaterial
{
private:
	int					m_nReferences;
public:
	CMaterial();
	virtual ~CMaterial();
	void AddRef() { m_nReferences++; }
	void Release() { if (--m_nReferences <= 0) delete this; }

	MATERIAL			m_Material;
};

//게임 객체는 하나 이상의 텍스쳐를 가질 수 있다. CTexture는 텍스쳐를 관리하기 위한 클래스이다.
class CTexture
{
private:
	int							m_nReferences;
	int							m_nTextures;
	ID3D11ShaderResourceView**	m_ppd3dsrvTextures;
	int							m_nTextureStartSlot;
	int							m_nSamplers;
	ID3D11SamplerState**		m_ppd3dSamplerStates;
	int							m_nSamplerStartSlot;

public:
	CTexture(int nTextures = 1, int nSamplers = 1, int nTextureStartSlot = 0, int nSamplerStartSlot = 0);
	virtual ~CTexture();

	void AddRef() { m_nReferences++; }
	void Release() { if (--m_nReferences <= 0) delete this; }
	void SetTexture(int nIndex, ID3D11ShaderResourceView *pd3dsrvTexture);
	void SetSampler(int nIndex, ID3D11SamplerState *pd3dSamplerState);
	//텍스쳐 리소스와 샘플러 상태 객체에 대한 쉐이더 변수를 변경한다.
	void UpdateShaderVariable(ID3D11DeviceContext *pd3dDeviceContext);
	//텍스쳐 리소스에 대한 쉐이더 변수를 변경한다.
	void UpdateTextureShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, int nIndex = 0, int nSlot = 0);
	//샘플러 상태 객체에 대한 쉐이더 변수를 변경한다.
	void UpdateSamplerShaderVariable(ID3D11DeviceContext *pd3dDeviceContext, int nIndex = 0, int nSlot = 0);
};

class CGameObject
{
private:
	int					m_nReferences;
protected:
	XMFLOAT4X4			m_xmf4x4World;
	AABB				m_bcMeshBoundingCube;
	BoundingBox			m_xmAABB;
public:
	CMesh**				m_ppMeshes;
	int					m_nMeshes;

	CGameObject(int nMeshes = 0);
	virtual ~CGameObject();

	void AddRef();
	void Release();

	void SetAABB(XMFLOAT3& xmCenter, XMFLOAT3& xmExtents){m_xmAABB = BoundingBox(xmCenter, xmExtents);}
	BoundingBox GetAABB(void) { return m_xmAABB; }

	XMFLOAT4X4			m_xmf4x4Rotate;
	XMFLOAT4X4			m_xmf4x4Rotate2;
	XMFLOAT4X4			m_xmf4x4Scale;
	XMFLOAT4X4			m_xmf4x4Translate;

	virtual void SetMesh(CMesh *pMesh, int nindex = 0);
	virtual void Animate(float fTimeElapsed);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);

	virtual void SetPosition(float x, float y, float z);
	virtual void SetPosition(XMFLOAT3 d3dxvPosition);

	virtual void SetRotationYawPitchRoll(float Pitch, float Yaw, float Roll);
	virtual void SetScale(float x, float y, float z);

	CMesh *GetMesh(int nIndex = 0) { return(m_ppMeshes[nIndex]); }

	XMFLOAT4X4 GetmtxWorld(void) {return m_xmf4x4World;};
	void SetAABBPosition();
	XMFLOAT3 GetPosition(void){return(XMFLOAT3(m_xmf4x4World._41, m_xmf4x4World._42, m_xmf4x4World._43));}

	//로컬 x-축, y-축, z-축 방향으로 이동한다.
	void MoveStrafe(float fDistance = 1.0f);
	void MoveUp(float fDistance = 1.0f);
	void MoveForward(float fDistance = 1.0f);

	//로컬 x-축, y-축, z-축 방향으로 회전한다.
	void Rotate(float fPitch = 10.0f, float fYaw = 10.0f, float fRoll = 10.0f);
	void Rotate(XMFLOAT3 *pd3dxvAxis, float fAngle);

	//객체의 위치, 로컬 x-축, y-축, z-축 방향 벡터를 반환한다.
	XMFLOAT3 GetLookAt();
	XMFLOAT3 GetUp();
	XMFLOAT3 GetRight();

	//객체를 렌더링하기 전에 호출되는 함수이다.
	virtual void OnPrepareRender() {
		m_xmf4x4World = Matrix4x4::Multiply(Matrix4x4::Multiply(Matrix4x4::Multiply(m_xmf4x4Scale, m_xmf4x4Rotate), m_xmf4x4Rotate2), m_xmf4x4Translate);
	}

	//trap
	virtual void SetObjectScale(int Modelidx);
	void SetTrapAABBPosition();
 
public:
	bool IsVisible(CCamera *pCamera = NULL);
private:
	bool m_bActive;

public:
	CMaterial *m_pMaterial;
	void SetMaterial(CMaterial *pMaterial);

	CTexture *m_pTexture;
	void SetTexture(CTexture *pTexture);
};

class CRotatingObject : public CGameObject
{
protected:
	float m_fRotationSpeed;
	XMFLOAT3 m_xmf3RotationAxis;
public:
	CRotatingObject(int nMeshes = 1);
	virtual ~CRotatingObject();

	virtual void Animate(float fTimeElapsed);
	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);

	void SetRotationSpeed(float fRotationSpeed) { m_fRotationSpeed = fRotationSpeed; }
	void SetRotationAxis(XMFLOAT3 d3dxvRotationAxis) { m_xmf3RotationAxis = d3dxvRotationAxis; }
};

class CHeightMap
{
	BYTE * m_pHeightMapImage;

	int m_nWidth;
	int m_nLength;

	XMFLOAT3 m_xmf3Scale;
public:
	CHeightMap(LPCTSTR pFileName, int nWidth, int nLength, XMFLOAT3 d3dxvScale);
	~CHeightMap(void);

	float GetHeight(float x, float z, bool bReverseQuad = false);
	XMFLOAT3 GetHeightMapNormal(int x, int z);
	XMFLOAT3 GetScale() { return(m_xmf3Scale); }
	BYTE *GetHeightMapImage() { return(m_pHeightMapImage); }
	int GetHeightMapWidth() { return(m_nWidth); }
	int GetHeightMapLength() { return(m_nLength); }
};

class CHeightMapTerrain : public CGameObject
{
private:
	//지형의 높이 맵으로 사용할 이미지이다.
	CHeightMap * m_pHeightMap;

	int m_nWidth;
	int m_nLength;

	XMFLOAT3 m_xmf3Scale;

public:
	CHeightMapTerrain(ID3D11Device *pd3dDevice = NULL, int nWidth = 0, int nLength = 0,
		int nBlockWidth = 0, int nBlockLength = 0, LPCTSTR pFileName = NULL, XMFLOAT3 xmf3Scale = XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT4 xmcColor = XMFLOAT4(0.5f, 0.5f, 0.5f, 0.5f));
	virtual ~CHeightMapTerrain();

	//지형의 실제 높이를 반환한다. 높이 맵의 높이에 스케일을 곱한 값이다.
	float GetHeight(float x, float z, bool bReverseQuad = false) { return(m_pHeightMap->GetHeight(x, z, bReverseQuad) * m_xmf3Scale.y); }

	XMFLOAT3 GetNormal(float x, float z) { return(m_pHeightMap->GetHeightMapNormal(int(x / m_xmf3Scale.x), int(z / m_xmf3Scale.z))); }

	XMFLOAT3 GetScale() { return(m_xmf3Scale); };

	float GetWidth() { return(m_nWidth * m_xmf3Scale.x);}
	float GetLength() { return(m_nLength * m_xmf3Scale.z);}
	float GetPeakHeight() {	return(m_bcMeshBoundingCube.GetBoundingCubeMax().y);}

	virtual void Animate(float fTimeElapsed);
};

class CSkyBox : public CGameObject
{
public:
	CSkyBox(ID3D11Device *pd3dDevice);
	virtual ~CSkyBox();

	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
};

class CCSVData;

struct ModelContainer
{
	string					m_sModelName;
	CMesh*					m_pModelMesh;
	CTexture*				m_pModelTexture;

	void AddRef()
	{
		if (m_pModelMesh)
			m_pModelMesh->AddRef();
		if (m_pModelTexture)
			m_pModelTexture->AddRef();
	}
	void Release()
	{
		m_pModelMesh->Release();
		m_pModelTexture->Release();
	}
};

class CTrap : public CGameObject
{
public:
	CTrap();
	virtual ~CTrap() {};

};

class CFog : public CGameObject
{
public:
	CFog();
	virtual ~CFog() {};

	virtual void Render(ID3D11DeviceContext *pd3dDeviceContext, CCamera *pCamera);
};