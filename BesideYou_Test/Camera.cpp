#include "stdafx.h"

CCamera::CCamera(CCamera *pCamera)
{
	if (pCamera)
	{
		//카메라가 이미 있으면 기존 카메라의 정보를 새로운 카메라에 복사한다. 
		m_xmf3Position = pCamera->GetPosition();
		m_xmf3Right = pCamera->GetRightVector();
		m_xmf3Look = pCamera->GetLookVector();
		m_xmf3Up = pCamera->GetUpVector();
		m_xmf3Offset = pCamera->GetOffset();
		m_xmf4x4View = pCamera->GetViewMatrix();
		m_xmf4x4Projection = pCamera->GetProjectionMatrix();
		m_fPitch = pCamera->GetPitch();
		m_fRoll = pCamera->GetRoll();
		m_fYaw = pCamera->GetYaw();
		m_d3dViewport = pCamera->GetViewport();
		m_fTimeLag = pCamera->GetTimeLag();
		m_pPlayer = pCamera->GetPlayer();
		m_pd3dcbCamera = pCamera->GetCameraConstantBuffer();
		if (m_pd3dcbCamera) m_pd3dcbCamera->AddRef();
	}
	else
	{
		//카메라가 없으면 기본 정보를 설정한다. 
		m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f);
		m_xmf3Right = XMFLOAT3(1.0f, 0.0f, 0.0f);
		m_xmf3Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
		m_xmf3Look = XMFLOAT3(0.0f, 0.0f, 1.0f);
		m_xmf3Offset = XMFLOAT3(0.0f, 0.0f, 0.0f);
		m_xmf4x4View = Matrix4x4::Identity();
		m_xmf4x4Projection = Matrix4x4::Identity();
		m_fPitch = 0.0f;
		m_fRoll = 0.0f;
		m_fYaw = 0.0f;
		m_fTimeLag = 0.0f;
		m_nMode = 0x00;
		m_pPlayer = NULL;
		m_pd3dcbCamera = NULL;
	}
}

CCamera::~CCamera()
{
	if (m_pd3dcbCamera) m_pd3dcbCamera->Release();
}

//Direct3D 디바이스의 뷰-포트를 설정하기 위한 함수이다.
void CCamera::SetViewport(ID3D11DeviceContext *pd3dDeviceContext, DWORD xTopLeft, DWORD yTopLeft, DWORD nWidth, DWORD nHeight, float fMinZ, float fMaxZ)
{
	m_d3dViewport.TopLeftX = float(xTopLeft);
	m_d3dViewport.TopLeftY = float(yTopLeft);
	m_d3dViewport.Width = float(nWidth);
	m_d3dViewport.Height = float(nHeight);
	m_d3dViewport.MinDepth = fMinZ;
	m_d3dViewport.MaxDepth = fMaxZ;
	pd3dDeviceContext->RSSetViewports(1, &m_d3dViewport);
}

/*카메라 변환 행렬을 생성하는 함수이다. 카메라의 위치 벡터, 카메라가 바라보는 지점, 카메라의 Up 벡터(로컬 y-축 벡터)를 파라메터로 사용하는 D3DXMatrixLookAtLH() 함수를 사용한다.*/
void CCamera::GenerateViewMatrix()
{
	m_xmf4x4View = Matrix4x4::LookAtLH(m_xmf3Position, m_pPlayer->GetPosition(), m_xmf3Up);
}

void CCamera::RegenerateViewMatrix()
{
	//카메라의 z-축 벡터를 정규화한다.
	m_xmf3Look = Vector3::Normalize(m_xmf3Look);
	//카메라의 z-축과 y-축에 수직인 벡터를 x-축으로 설정한다. 정규화도 해준다.
	m_xmf3Right = Vector3::CrossProduct(m_xmf3Up, m_xmf3Look, true);
	//카메라의 z-축과 x-축에 수직인 벡터를 y-축으로 설정한다. 정규화도 해준다.
	m_xmf3Up = Vector3::CrossProduct(m_xmf3Look, m_xmf3Right, true);

	m_xmf4x4View._11 = m_xmf3Right.x;
	m_xmf4x4View._12 = m_xmf3Up.x;
	m_xmf4x4View._13 = m_xmf3Look.x;
	m_xmf4x4View._21 = m_xmf3Right.y;
	m_xmf4x4View._22 = m_xmf3Up.y;
	m_xmf4x4View._23 = m_xmf3Look.y;
	m_xmf4x4View._31 = m_xmf3Right.z;
	m_xmf4x4View._32 = m_xmf3Up.z;
	m_xmf4x4View._33 = m_xmf3Look.z;
	m_xmf4x4View._41 = -Vector3::DotProduct(m_xmf3Position, m_xmf3Right); 
	m_xmf4x4View._42 = -Vector3::DotProduct(m_xmf3Position, m_xmf3Up);
	m_xmf4x4View._43 = -Vector3::DotProduct(m_xmf3Position, m_xmf3Look);

	//카메라의 위치와 방향이 바뀌면(카메라 변환 행렬이 바뀌면) 절두체 평면을 다시 계산한다.
	CalculateFrustumPlanes();
}

void CCamera::GenerateProjectionMatrix(float fNearPlaneDistance, float fFarPlaneDistance, float fAspectRatio, float fFOVAngle)
{
	m_xmf4x4Projection = Matrix4x4::PerspectiveFovLH(fFOVAngle, fAspectRatio, fNearPlaneDistance, fFarPlaneDistance);
}

void CCamera::CreateShaderVariables(ID3D11Device *pd3dDevice)
{
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(VS_CB_CAMERA);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	pd3dDevice->CreateBuffer(&bd, NULL, &m_pd3dcbCamera);
}

void CCamera::UpdateShaderVariables(ID3D11DeviceContext *pd3dDeviceContext)
{
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	/*상수 버퍼의 메모리 주소를 가져와서 카메라 변환 행렬과 투영 변환 행렬을 복사한다. 쉐이더에서 행렬의 행과 열이 바뀌는 것에 주의하라.*/
	pd3dDeviceContext->Map(m_pd3dcbCamera, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource);
	VS_CB_CAMERA *pcbViewProjection = (VS_CB_CAMERA *)d3dMappedResource.pData;
	pcbViewProjection->m_xmf4x4View = Matrix4x4::Transpose(m_xmf4x4View);
	pcbViewProjection->m_xmf4x4Projection = Matrix4x4::Transpose(m_xmf4x4Projection);
	pd3dDeviceContext->Unmap(m_pd3dcbCamera, 0);

	//상수 버퍼를 슬롯(VS_SLOT_CAMERA)에 설정한다.
	pd3dDeviceContext->VSSetConstantBuffers(VS_SLOT_CAMERA, 1, &m_pd3dcbCamera);
}

void CCamera::CalculateFrustumPlanes()
{
	/*카메라 변환 행렬과 원근 투영 변환 행렬을 곱한 행렬을 사용하여 절두체 평면들을 구한다. 즉 월드 좌표계에서 절두체 컬링을 한다.*/
	XMFLOAT4X4 mxf4x4ViewProject = Matrix4x4::Multiply(m_xmf4x4View, m_xmf4x4Projection);

	//절두체의 왼쪽 평면
	m_pxmfFrustumPlanes[0].x = -(mxf4x4ViewProject._14 + mxf4x4ViewProject._11);
	m_pxmfFrustumPlanes[0].y = -(mxf4x4ViewProject._24 + mxf4x4ViewProject._21);
	m_pxmfFrustumPlanes[0].z = -(mxf4x4ViewProject._34 + mxf4x4ViewProject._31);
	m_pxmfFrustumPlanes[0].w = -(mxf4x4ViewProject._44 + mxf4x4ViewProject._41);

	//절두체의 오른쪽 평면
	m_pxmfFrustumPlanes[1].x = -(mxf4x4ViewProject._14 - mxf4x4ViewProject._11);
	m_pxmfFrustumPlanes[1].y = -(mxf4x4ViewProject._24 - mxf4x4ViewProject._21);
	m_pxmfFrustumPlanes[1].z = -(mxf4x4ViewProject._34 - mxf4x4ViewProject._31);
	m_pxmfFrustumPlanes[1].w = -(mxf4x4ViewProject._44 - mxf4x4ViewProject._41);

	//절두체의 위쪽 평면
	m_pxmfFrustumPlanes[2].x = -(mxf4x4ViewProject._14 - mxf4x4ViewProject._12);
	m_pxmfFrustumPlanes[2].y = -(mxf4x4ViewProject._24 - mxf4x4ViewProject._22);
	m_pxmfFrustumPlanes[2].z = -(mxf4x4ViewProject._34 - mxf4x4ViewProject._32);
	m_pxmfFrustumPlanes[2].w = -(mxf4x4ViewProject._44 - mxf4x4ViewProject._42);

	//절두체의 아래쪽 평면
	m_pxmfFrustumPlanes[3].x = -(mxf4x4ViewProject._14 + mxf4x4ViewProject._12);
	m_pxmfFrustumPlanes[3].y = -(mxf4x4ViewProject._24 + mxf4x4ViewProject._22);
	m_pxmfFrustumPlanes[3].z = -(mxf4x4ViewProject._34 + mxf4x4ViewProject._32);
	m_pxmfFrustumPlanes[3].w = -(mxf4x4ViewProject._44 + mxf4x4ViewProject._42);

	//절두체의 근평면
	m_pxmfFrustumPlanes[4].x = -(mxf4x4ViewProject._13);
	m_pxmfFrustumPlanes[4].y = -(mxf4x4ViewProject._23);
	m_pxmfFrustumPlanes[4].z = -(mxf4x4ViewProject._33);
	m_pxmfFrustumPlanes[4].w = -(mxf4x4ViewProject._43);

	//절두체의 원평면
	m_pxmfFrustumPlanes[5].x = -(mxf4x4ViewProject._14 - mxf4x4ViewProject._13);
	m_pxmfFrustumPlanes[5].y = -(mxf4x4ViewProject._24 - mxf4x4ViewProject._23);
	m_pxmfFrustumPlanes[5].z = -(mxf4x4ViewProject._34 - mxf4x4ViewProject._33);
	m_pxmfFrustumPlanes[5].w = -(mxf4x4ViewProject._44 - mxf4x4ViewProject._43);

	/*절두체의 각 평면의 법선 벡터 (a, b. c)의 크기로 a, b, c, d를 나눈다. 즉, 법선 벡터를 정규화하고 원점에서 평면까지의 거리를 계산한다.*/
	for (int i = 0; i < 6; i++) m_pxmfFrustumPlanes[i] = Plane::Normalize(m_pxmfFrustumPlanes[i]);
}

bool CCamera::IsInFrustum(XMFLOAT3& xmf3Minimum, XMFLOAT3& xmf3Maximum)
{
	XMFLOAT3 xmf3NearPoint, xmf3FarPoint, xmf3Normal;
	for (int i = 0; i < 6; i++)
	{
		/*절두체의 각 평면에 대하여 바운딩 박스의 근접점을 계산한다. 근접점의 x, y, z 좌표는 법선 벡터의 각 요소가 음수이면 바운딩 박스의 최대점의 좌표가 되고 그렇지 않으면 바운딩 박스의 최소점의 좌표가 된다.*/
		xmf3Normal = XMFLOAT3(m_pxmfFrustumPlanes[i].x, m_pxmfFrustumPlanes[i].y, m_pxmfFrustumPlanes[i].z);
		if (xmf3Normal.x >= 0.0f)
		{
			if (xmf3Normal.y >= 0.0f)
			{
				if (xmf3Normal.z >= 0.0f)
				{
					//법선 벡터의 x, y, z 좌표의 부호가 모두 양수이므로 근접점은 바운딩 박스의 최소점이다.
					xmf3NearPoint.x = xmf3Minimum.x; xmf3NearPoint.y = xmf3Minimum.y; xmf3NearPoint.z = xmf3Minimum.z;
				}
				else
				{
					/*법선 벡터의 x, y 좌표의 부호가 모두 양수이므로 근접점의 x, y 좌표는 바운딩 박스의 최소점의 x, y 좌표이고 법선 벡터의 z 좌표가 움수이므로 근접점의 z 좌표는 바운딩 박스의 최대점의 z 좌표이다.*/
					xmf3NearPoint.x = xmf3Minimum.x; xmf3NearPoint.y = xmf3Minimum.y; xmf3NearPoint.z = xmf3Maximum.z;
				}
			}
			else
			{
				if (xmf3Normal.z >= 0.0f)
				{
					/*법선 벡터의 x, z 좌표의 부호가 모두 양수이므로 근접점의 x, z 좌표는 바운딩 박스의 최소점의 x, z 좌표이고 법선 벡터의 y 좌표가 움수이므로 근접점의 y 좌표는 바운딩 박스의 최대점의 y 좌표이다.*/
					xmf3NearPoint.x = xmf3Minimum.x; xmf3NearPoint.y = xmf3Maximum.y; xmf3NearPoint.z = xmf3Minimum.z;
				}
				else
				{
					/*법선 벡터의 y, z 좌표의 부호가 모두 음수이므로 근접점의 y, z 좌표는 바운딩 박스의 최대점의 y, z 좌표이고 법선 벡터의 x 좌표가 양수이므로 근접점의 x 좌표는 바운딩 박스의 최소점의 x 좌표이다.*/
					xmf3NearPoint.x = xmf3Minimum.x; xmf3NearPoint.y = xmf3Maximum.y; xmf3NearPoint.z = xmf3Maximum.z;
				}
			}
		}
		else
		{
			if (xmf3Normal.y >= 0.0f)
			{
				if (xmf3Normal.z >= 0.0f)
				{
					/*법선 벡터의 y, z 좌표의 부호가 모두 양수이므로 근접점의 y, z 좌표는 바운딩 박스의 최소점의 y, z 좌표이고 법선 벡터의 x 좌표가 음수이므로 근접점의 x 좌표는 바운딩 박스의 최대점의 x 좌표이다.*/
					xmf3NearPoint.x = xmf3Maximum.x; xmf3NearPoint.y = xmf3Minimum.y; xmf3NearPoint.z = xmf3Minimum.z;
				}
				else
				{
					/*법선 벡터의 x, z 좌표의 부호가 모두 음수이므로 근접점의 x, z 좌표는 바운딩 박스의 최대점의 x, z 좌표이고 법선 벡터의 y 좌표가 양수이므로 근접점의 y 좌표는 바운딩 박스의 최소점의 y 좌표이다.*/
					xmf3NearPoint.x = xmf3Maximum.x; xmf3NearPoint.y = xmf3Minimum.y; xmf3NearPoint.z = xmf3Maximum.z;
				}
			}
			else
			{
				if (xmf3Normal.z >= 0.0f)
				{
					/*법선 벡터의 x, y 좌표의 부호가 모두 음수이므로 근접점의 x, y 좌표는 바운딩 박스의 최대점의 x, y 좌표이고 법선 벡터의 z 좌표가 양수이므로 근접점의 z 좌표는 바운딩 박스의 최소점의 z 좌표이다.*/
					xmf3NearPoint.x = xmf3Maximum.x; xmf3NearPoint.y = xmf3Maximum.y; xmf3NearPoint.z = xmf3Minimum.z;
				}
				else
				{
					//법선 벡터의 x, y, z 좌표의 부호가 모두 음수이므로 근접점은 바운딩 박스의 최대점이다.
					xmf3NearPoint.x = xmf3Maximum.x; xmf3NearPoint.y = xmf3Maximum.y; xmf3NearPoint.z = xmf3Maximum.z;
				}
			}
		}
		/*근접점이 절두체 평면 중 하나의 평면의 바깥(앞)쪽에 있으면 근접점은 절두체에 포함되지 않는다. 근접점이 어떤 평면의 바깥(앞)쪽에 있으면 근접점을 평면의 방정식에 대입하면 부호가 양수가 된다. 각 평면의 방정식에 근접점을 대입하는 것은 근접점 (x, y, z)과 평면의 법선벡터 (a, b, c)의 내적에 원점에서 평면까지의 거리를 더한 것과 같다.*/

		if ((Vector3::DotProduct(xmf3Normal,xmf3NearPoint) + m_pxmfFrustumPlanes[i].w) > 0.0f) 
			return(false);
		//if ((D3DXVec3Dot(&xmf3Normal, &xmf3NearPoint) + m_pxmfFrustumPlanes[i].w) > 0.0f) return(false);
	}
	return(true);
}

bool CCamera::IsInFrustum(AABB *pAABB)
{
	return(IsInFrustum(pAABB->GetBoundingCubeMin(), pAABB->GetBoundingCubeMax()));
}

CSpaceShipCamera::CSpaceShipCamera(CCamera *pCamera) : CCamera(pCamera)
{
	m_nMode = SPACESHIP_CAMERA;
}

void CSpaceShipCamera::Rotate(float x, float y, float z)
{
	XMFLOAT4X4 xmf4x4Rotate;
	if (m_pPlayer && (x != 0.0f))
	{
		//플레이어의 로컬 x-축에 대한 x 각도의 회전 행렬을 계산한다.
		xmf4x4Rotate = Matrix4x4::RotationAxis(m_pPlayer->GetRightVector(), x);
		//카메라의 로컬 x-축, y-축, z-축을 회전한다.
		m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4Rotate);
		m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4Rotate);
		m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4Rotate);
		/*카메라의 위치 벡터에서 플레이어의 위치 벡터를 뺀다. 결과는 플레이어 위치를 기준으로 한 카메라의 위치 벡터이다.*/
		m_xmf3Position = Vector3::Subtract(m_xmf3Position, m_pPlayer->GetPosition());
		//플레이어의 위치를 중심으로 카메라의 위치 벡터(플레이어를 기준으로 한)를 회전한다.
		m_xmf3Position = Vector3::TransformCoord(m_xmf3Position, xmf4x4Rotate);
		//회전시킨 카메라의 위치 벡터에 플레이어의 위치를 더한다.
		m_xmf3Position = Vector3::Add(m_xmf3Position, m_pPlayer->GetPosition());
	}
	if (m_pPlayer && (y != 0.0f))
	{
		xmf4x4Rotate = Matrix4x4::RotationAxis(m_pPlayer->GetUpVector(), y);
		m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4Rotate);
		m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4Rotate);
		m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4Rotate);
		m_xmf3Position = Vector3::Subtract(m_xmf3Position, m_pPlayer->GetPosition());
		m_xmf3Position = Vector3::TransformCoord(m_xmf3Position, xmf4x4Rotate);
		m_xmf3Position = Vector3::Add(m_xmf3Position, m_pPlayer->GetPosition());
	}
	if (m_pPlayer && (z != 0.0f))
	{
		xmf4x4Rotate = Matrix4x4::RotationAxis(m_pPlayer->GetUpVector(), z);
		m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4Rotate);
		m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4Rotate);
		m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4Rotate);
		m_xmf3Position = Vector3::Subtract(m_xmf3Position, m_pPlayer->GetPosition());
		m_xmf3Position = Vector3::TransformCoord(m_xmf3Position, xmf4x4Rotate);
		m_xmf3Position = Vector3::Add(m_xmf3Position, m_pPlayer->GetPosition());
	}
}

CFirstPersonCamera::CFirstPersonCamera(CCamera *pCamera) : CCamera(pCamera)
{
	m_nMode = FIRST_PERSON_CAMERA;
	if (pCamera)
	{
		/*1인칭 카메라로 변경하기 이전의 카메라가 스페이스-쉽 카메라이면 카메라의 Up 벡터를 월드좌표의 y-축이 되도록 한다. 이것은 스페이스-쉽 카메라의 로컬 y-축 벡터가 어떤 방향이든지 1인칭 카메라(대부분 사람인 경우)의 로컬 y-축 벡터가 월드좌표의 y-축이 되도록 즉, 똑바로 서있는 형태로 설정한다는 의미이다. 그리고 로컬 x-축 벡터와 로컬 z-축 벡터의 y-좌표가 0.0f가 되도록 한다. 이것은 <그림 8>과 같이 로컬 x-축 벡터와 로컬 z-축 벡터를 xz-평면(지면)으로 투영하는 것을 의미한다. 즉, 1인칭 카메라의 로컬 x-축 벡터와 로컬 z-축 벡터는 xz-평면에 평행하다.*/
		if (pCamera->GetMode() == SPACESHIP_CAMERA)
		{
			m_xmf3Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
			m_xmf3Right.y = 0.0f;
			m_xmf3Look.y = 0.0f;
			m_xmf3Right = Vector3::Normalize(m_xmf3Right);
			m_xmf3Look = Vector3::Normalize(m_xmf3Look);
		}
	}
}

void CFirstPersonCamera::Rotate(float x, float y, float z)
{
	XMFLOAT4X4 xmf4x4Rotate;
	if (x != 0.0f)
	{
		//카메라의 로컬 x-축을 기준으로 회전하는 행렬을 생성한다. 고개를 끄떡이는 동작이다.
		xmf4x4Rotate = Matrix4x4::RotationAxis(m_xmf3Right, x);
		//카메라의 로컬 x-축, y-축, z-축을 회전한다.
		m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4Rotate);
		m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4Rotate);
		m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4Rotate);
	}
	if (m_pPlayer && (y != 0.0f))
	{
		//플레이어의 로컬 y-축을 기준으로 회전하는 행렬을 생성한다.
		xmf4x4Rotate = Matrix4x4::RotationAxis(m_xmf3Up, y);
		//카메라의 로컬 x-축, y-축, z-축을 회전한다.
		m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4Rotate);
		m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4Rotate);
		m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4Rotate);
	}
	if (m_pPlayer && (z != 0.0f))
	{
		//플레이어의 로컬 z-축을 기준으로 회전하는 행렬을 생성한다.
		xmf4x4Rotate = Matrix4x4::RotationAxis(m_xmf3Look, z);
		//카메라의 위치 벡터를 플레이어 좌표계로 표현한다(오프셋 벡터).
		m_xmf3Position = Vector3::Subtract(m_xmf3Position, m_pPlayer->GetPosition());
		//오프셋 벡터 벡터를 회전한다.
		m_xmf3Position = Vector3::TransformCoord(m_xmf3Position, xmf4x4Rotate);
		//회전한 카메라의 위치를 월드 좌표계로 표현한다.
		m_xmf3Position = Vector3::Add(m_xmf3Position, m_pPlayer->GetPosition());
		//카메라의 로컬 x-축, y-축, z-축을 회전한다.
		m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4Rotate);
		m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4Rotate);
		m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4Rotate);
	}
}

CThirdPersonCamera::CThirdPersonCamera(CCamera *pCamera) : CCamera(pCamera)
{
	m_nMode = THIRD_PERSON_CAMERA;
	if (pCamera)
	{
		/*3인칭 카메라로 변경하기 이전의 카메라가 스페이스-쉽 카메라이면 카메라의 Up 벡터를 월드좌표의 y-축이 되도록 한다. 이것은 스페이스-쉽 카메라의 로컬 y-축 벡터가 어떤 방향이든지 3인칭 카메라(대부분 사람인 경우)의 로컬 y-축 벡터가 월드좌표의 y-축이 되도록 즉, 똑바로 서있는 형태로 설정한다는 의미이다. 그리고 로컬 x-축 벡터와 로컬 z-축 벡터의 y-좌표가 0.0f가 되도록 한다. 이것은 로컬 x-축 벡터와 로컬 z-축 벡터를 xz-평면(지면)으로 투영하는 것을 의미한다. 즉, 3인칭 카메라의 로컬 x-축 벡터와 로컬 z-축 벡터는 xz-평면에 평행하다.*/
		if (pCamera->GetMode() == SPACESHIP_CAMERA)
		{
			m_xmf3Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
			m_xmf3Right.y = 0.0f;
			m_xmf3Look.y = 0.0f;
			m_xmf3Right = Vector3::Normalize(m_xmf3Right);
			m_xmf3Look = Vector3::Normalize(m_xmf3Look);
		}
	}
}

void CThirdPersonCamera::Update(XMFLOAT3& xmf3LookAt, float fTimeElapsed)
{
	//플레이어의 회전에 따라 3인칭 카메라도 회전해야 한다.
	if (m_pPlayer)
	{
	XMFLOAT4X4 xmf4x4Rotate;
	xmf4x4Rotate = Matrix4x4::Identity();
	XMFLOAT3 xmf3Right = m_pPlayer->GetRightVector();
	XMFLOAT3 xmf3Up = m_pPlayer->GetUpVector();
	XMFLOAT3 xmf3Look = m_pPlayer->GetLookVector();
	//플레이어의 로컬 x-축, y-축, z-축 벡터로부터 회전 행렬을 생성한다.
	xmf4x4Rotate._11 = xmf3Right.x; xmf4x4Rotate._21 = xmf3Up.x; xmf4x4Rotate._31 = xmf3Look.x;
	xmf4x4Rotate._12 = xmf3Right.y; xmf4x4Rotate._22 = xmf3Up.y; xmf4x4Rotate._32 = xmf3Look.y;
	xmf4x4Rotate._13 = xmf3Right.z; xmf4x4Rotate._23 = xmf3Up.z; xmf4x4Rotate._33 = xmf3Look.z;

	XMFLOAT3 xmf3Offset;
	xmf3Offset = Vector3::TransformCoord(m_xmf3Offset, xmf4x4Rotate);
	//회전한 카메라의 위치는 플레이어의 위치에 회전한 카메라 오프셋 벡터를 더한 것이다.
	XMFLOAT3 xmf3Position = Vector3::Add(m_pPlayer->GetPosition(), xmf3Offset);
	//현재의 카메라의 위치에서 회전한 카메라의 위치까지의 벡터이다.
	XMFLOAT3 xmf3Direction = Vector3::Subtract(xmf3Position, m_xmf3Position);
	float fLength = Vector3::Length(xmf3Direction);
	xmf3Direction = Vector3::Normalize(xmf3Direction);
	//3인칭 카메라의 래그(Lag)는 플레이어가 회전하더라도 카메라가 동시에 따라서 회전하지 않고 약간의 시차를 두고 회전하는 효과를 구현하기 위한 것이다. m_fTimeLag가 1보다 크면 fTimeLagScale이 작아지고 실제 회전이 적게 일어날 것이다.
	float fTimeLagScale = (m_fTimeLag) ? fTimeElapsed * (1.0f / m_fTimeLag) : 1.0f;
	float fDistance = fLength * fTimeLagScale;
	if (fDistance > fLength) fDistance = fLength;
	if (fLength < 0.01f) fDistance = fLength;
	if (fDistance > 0)
	{
		m_xmf3Position = Vector3::Add( m_xmf3Position, Vector3::ScalarProduct(xmf3Direction, fDistance, true) );
		SetLookAt(xmf3LookAt);
	}
}
}

void CThirdPersonCamera::SetLookAt(XMFLOAT3& xmf3LookAt)
{
	XMFLOAT4X4 xmf4x4LookAt;
	xmf4x4LookAt = Matrix4x4::LookAtLH(m_xmf3Position, xmf3LookAt, m_pPlayer->GetUpVector());
	m_xmf3Right = XMFLOAT3(xmf4x4LookAt._11, xmf4x4LookAt._21, xmf4x4LookAt._31);
	m_xmf3Up = XMFLOAT3(xmf4x4LookAt._12, xmf4x4LookAt._22, xmf4x4LookAt._32);
	m_xmf3Look = XMFLOAT3(xmf4x4LookAt._13, xmf4x4LookAt._23, xmf4x4LookAt._33);

}